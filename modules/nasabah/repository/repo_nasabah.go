package repository

import (
	"context"
	"errors"
	"fmt"
	"log"
	"ms-bribrain-link5/helper/logger"
	models2 "ms-bribrain-link5/helper/models"
	"ms-bribrain-link5/models"
	"ms-bribrain-link5/modules/nasabah"
	"strings"
	"time"

	"gorm.io/gorm"
)

type NasabahRepository struct {
	Conn  *gorm.DB
	log   logger.Logger
	table *models.NasabahTable
}

func NewNasabahRepository(Conn *gorm.DB, log logger.Logger) nasabah.Repository {
	table := &models.NasabahTable{}
	table = table.MappingTable()
	return &NasabahRepository{Conn, log, table}
}

func (n NasabahRepository) queryGetDetail(param *models.RequestDetailNasabahEncrypted) (res *models.GetDetailQueryStruct, err error) {
	var hasil models.GetDetailQueryStruct
	// query := n.Conn.Table(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table).
	// 	Select(n.table.SelectAllColumnBribrainLink5DashboardDetailInformasiNasabahBRITable() + `,` + n.table.SelectAllColumnBribrainLink5StatusTindaklanjutTable())
	// query = query.Joins(`LEFT JOIN ` + n.table.BribrainLink5StatusTindaklanjut.Table + ` ON ` + n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.CifNasabahRekan + ` = ` + n.table.BribrainLink5StatusTindaklanjut.Cifno)
	// query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.CifNasabahRekan+`= ?`, param.Cifno)

	// query := n.Conn.Table(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table).
	// 	Select(n.table.SelectAllColumnBribrainLink5DashboardDetailInformasiNasabahBRITable())
	// query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.CifNasabahRekan+`= ?`, param.Cifno)

	// code tabel baru
	query := n.Conn.Table(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table).
		Select(n.table.SelectAllColumnBribrainLink5DashboardDetailInformasiNasabahBRITable() + `,` +
			` case when ` + n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.TopupSimpanan + `=0 then "Belum"` +
			` when ` + n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.TopupSimpanan + `=1 then "Sudah"` +
			` END AS STATUS_AKUISISI_SIMPANAN,` +
			` case when ` + n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.AkuisisiPinjaman + `=0 AND ` + n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.SuplesiPinjaman + `=0 then "Belum"` +
			` when ` + n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.AkuisisiPinjaman + `=1 OR ` + n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.SuplesiPinjaman + `=1 then "Sudah"` +
			` END AS STATUS_AKUISISI_PINJAMAN`)
	query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.CifNasabahRekan+`= ?`, param.Cifno)

	result := query.Limit(1).Scan(&hasil)
	// fmt.Println("query result ->", &hasil)

	rnf := result.RowsAffected
	fmt.Println("not found status -> ", rnf)

	if rnf == 0 {
		err := errors.New("record not found")
		return res, err
	}

	err = result.Error
	res = &hasil
	return res, err
}

func (n NasabahRepository) queryGetDetailDitindaklanjuti(param *models.RequestDetailNasabahEncrypted) (res *models.GetDetailQueryStruct, err error) {
	var hasil models.GetDetailQueryStruct
	// query := n.Conn.Table(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table).
	// 	Select(n.table.SelectAllColumnBribrainLink5DashboardDetailInformasiNasabahBRITable() + `,` + n.table.SelectAllColumnBribrainLink5StatusTindaklanjutTable())
	// query = query.Joins(`LEFT JOIN ` + n.table.BribrainLink5StatusTindaklanjut.Table + ` ON ` + n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.IdStatus + ` = ` + n.table.BribrainLink5StatusTindaklanjut.Id)
	// query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.CifNasabahRekan+`= ?`, param.Cifno)

	//code tabel baru
	query := n.Conn.Table(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table).
		Select(n.table.SelectAllColumnBribrainLink5DashboardDetailInformasiNasabahBRITable() + `,` + n.table.SelectAllColumnBribrainLink5StatusTindaklanjutTable() + `,` +
			` case when ` + n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.TopupSimpanan + `=0 then "Belum"` +
			` when ` + n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.TopupSimpanan + `=1 then "Sudah"` +
			` END AS STATUS_AKUISISI_SIMPANAN,` +
			` case when ` + n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.AkuisisiPinjaman + `=0 AND ` + n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.SuplesiPinjaman + `=0 then "Belum"` +
			` when ` + n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.AkuisisiPinjaman + `=1 OR ` + n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.SuplesiPinjaman + `=1 then "Sudah"` +
			` END AS STATUS_AKUISISI_PINJAMAN`)
	query = query.Joins(`LEFT JOIN ` + n.table.BribrainLink5StatusTindaklanjut.Table + ` ON ` + n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.CifNasabahRekan + ` = ` + n.table.BribrainLink5StatusTindaklanjut.Cifno)
	query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.CifNasabahRekan+`= ?`, param.Cifno)

	result := query.Limit(1).Scan(&hasil)
	// fmt.Println("query result ->", &hasil)

	rnf := result.RowsAffected
	fmt.Println("not found status -> ", rnf)

	if rnf == 0 {
		err := errors.New("record not found")
		return res, err
	}

	err = result.Error
	res = &hasil
	return res, err
}

func (n NasabahRepository) GetDetail(ctx context.Context, param *models.RequestDetailNasabahEncrypted) (res *models.GetDetailQueryStruct, err error) {
	res, err = n.queryGetDetail(param)

	if err != nil {
		n.log.Error("nasabah.repository.NasabahRepository.GetDetail: %s", err.Error())
		if err.Error() != gorm.ErrRecordNotFound.Error() {
			return nil, err
		}
		return nil, models2.ErrNotFound
	}

	return res, nil
}

func (n NasabahRepository) GetDetailDitindaklanjuti(ctx context.Context, param *models.RequestDetailNasabahEncrypted) (res *models.GetDetailQueryStruct, err error) {
	res, err = n.queryGetDetailDitindaklanjuti(param)

	if err != nil {
		n.log.Error("nasabah.repository.NasabahRepository.GetDetailDitindaklanjuti: %s", err.Error())
		if err.Error() != gorm.ErrRecordNotFound.Error() {
			return nil, err
		}
		return nil, models2.ErrNotFound
	}

	return res, nil
}

func (n NasabahRepository) GetListRekomendasi(ctx context.Context, param *models.ParamRequestListNasabah) (res []models.GetListQueryStruct, err error) {
	// query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table).
	// 	Select(n.table.SelectAllColumnBribrainLink5DashboardDetailInformasiNasabahBRITable() + `,` + n.table.SelectAllColumnBribrainLink5StatusTindaklanjutTable())
	// query = query.Joins(`LEFT JOIN ` + n.table.BribrainLink5StatusTindaklanjut.Table + ` ON ` + n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.CifNasabahRekan + ` = ` + n.table.BribrainLink5StatusTindaklanjut.Cifno)
	// query = n.addRoleBasedClause(query, param.PnPengelola, param.Role)
	// // query = g.dynamicWhereClause(query, param)
	// query = query.Where(n.table.BribrainLink5StatusTindaklanjut.KodeHubungi+` = ? AND `+n.table.BribrainLink5StatusTindaklanjut.KodeKunjungan+` = ?`, 0, 0)
	// query = n.querySearch(query, param)

	// query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table).
	// 	Select(n.table.SelectAllColumnBribrainLink5DashboardDetailInformasiNasabahBRITable())
	// query = n.addRoleBasedClause(query, param.PnPengelola, param.Role)
	// // query = g.dynamicWhereClause(query, param)
	// query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.IdStatus + ` is null`)
	// query = n.querySearch(query, param)

	//code tabel baru
	query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table).
		Select(n.table.SelectAllColumnBribrainLink5DashboardDetailInformasiNasabahBRITable())
	query = n.addRoleBasedClause(query, param.PnPengelola, param.Role)
	// query = g.dynamicWhereClause(query, param)
	query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.StatusTindaklanjut + ` = 0`)
	// query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.TopupSimpanan + ` != 0 OR ` + n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.SuplesiPinjaman + ` != 0 OR ` + n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.AkuisisiPinjaman + ` != 0 `)
	query = n.querySearch(query, param)

	err = query.
		Offset(param.Offset).Limit(param.Limit).
		// Order(`case when ` + n.table.BribrainLink5StatusTindaklanjut.KodeKunjungan + ` = 3 then 1 when (` + n.table.BribrainLink5StatusTindaklanjut.KodeHubungi + ` = 1 AND ` + n.table.BribrainLink5StatusTindaklanjut.KodeKunjungan + ` != 1) then 2 when (` + n.table.BribrainLink5StatusTindaklanjut.KodeHubungi + ` = 1 AND ` + n.table.BribrainLink5StatusTindaklanjut.KodeKunjungan + ` = 1) then 3 when (` + n.table.BribrainLink5StatusTindaklanjut.KodeHubungi + ` != 1 AND ` + n.table.BribrainLink5StatusTindaklanjut.KodeKunjungan + ` = 1) then 4 when ` + n.table.BribrainLink5StatusTindaklanjut.KodeHubungi + ` = 2 then 5 when ` + n.table.BribrainLink5StatusTindaklanjut.KodeHubungi + `= 3 then 6 ELSE 7 end`).
		Order(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.Peringkat + ` ASC`).
		Order(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.NamaNasabahRekan + ` ASC`).
		Scan(&res).Error

	if err != nil {
		n.log.Error("nasabah.repository.NasabahRepository.GetListRekomendasi: %s", err.Error())
		if err.Error() != gorm.ErrRecordNotFound.Error() {
			return nil, err
		}
	}

	return res, nil
}

func (n NasabahRepository) GetListRekomendasiNonBRI(ctx context.Context, param *models.ParamRequestListNasabah) (res []models.GetListQueryStruct, err error) {
	// query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.Table).
	// 	Select(n.table.SelectAllColumnBribrainLink5DashboardDetailInformasiNasabahNonBRITable() + `,` + n.table.SelectAllColumnBribrainLink5StatusTindaklanjutTable())
	// query = query.Joins(`LEFT JOIN ` + n.table.BribrainLink5StatusTindaklanjut.Table + ` ON ` + n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.KodeNonBri + ` = ` + n.table.BribrainLink5StatusTindaklanjut.KodeNonBri)
	// query = n.addRoleBasedClauseNonBRI(query, param.PnPengelola, param.Role)
	// // query = g.dynamicWhereClause(query, param)
	// query = query.Where(n.table.BribrainLink5StatusTindaklanjut.KodeHubungi+` = ? AND `+n.table.BribrainLink5StatusTindaklanjut.KodeKunjungan+` = ?`, 0, 0)
	// query = n.querySearchNonBRI(query, param)

	// query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.Table).
	// 	Select(n.table.SelectAllColumnBribrainLink5DashboardDetailInformasiNasabahNonBRITable())
	// query = n.addRoleBasedClauseNonBRI(query, param.PnPengelola, param.Role)
	// // query = g.dynamicWhereClause(query, param)
	// query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.IdStatus + ` is null`)
	// query = n.querySearchNonBRI(query, param)

	//code tabel baru
	query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.Table).
		Select(n.table.SelectAllColumnBribrainLink5DashboardDetailInformasiNasabahNonBRITable())
	query = n.addRoleBasedClauseNonBRI(query, param.PnPengelola, param.Role)
	// query = g.dynamicWhereClause(query, param)
	query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.StatusTindaklanjut + ` = 0`)
	// query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.AkuisisiSimpanan + ` = 1`)
	query = n.querySearchNonBRI(query, param)

	err = query.
		Offset(param.Offset).Limit(param.Limit).
		// Order(`case when ` + n.table.BribrainLink5StatusTindaklanjut.KodeKunjungan + ` = 3 then 1 when (` + n.table.BribrainLink5StatusTindaklanjut.KodeHubungi + ` = 1 AND ` + n.table.BribrainLink5StatusTindaklanjut.KodeKunjungan + ` != 1) then 2 when (` + n.table.BribrainLink5StatusTindaklanjut.KodeHubungi + ` = 1 AND ` + n.table.BribrainLink5StatusTindaklanjut.KodeKunjungan + ` = 1) then 3 when (` + n.table.BribrainLink5StatusTindaklanjut.KodeHubungi + ` != 1 AND ` + n.table.BribrainLink5StatusTindaklanjut.KodeKunjungan + ` = 1) then 4 when ` + n.table.BribrainLink5StatusTindaklanjut.KodeHubungi + ` = 2 then 5 when ` + n.table.BribrainLink5StatusTindaklanjut.KodeHubungi + `= 3 then 6 ELSE 7 end`).
		Order(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.Peringkat + ` ASC`).
		Order(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.NamaNasabahNonBri + ` ASC`).
		Scan(&res).Error

	if err != nil {
		n.log.Error("nasabah.repository.NasabahRepository.GetListRekomendasiNonBRI: %s", err.Error())
		if err.Error() != gorm.ErrRecordNotFound.Error() {
			return nil, err
		}
	}

	return res, nil
}

func (n NasabahRepository) dynamicWhereClause(query *gorm.DB, param *models.RequestListNasabah) *gorm.DB {

	return query
}

func (n NasabahRepository) addRoleBasedClause(query *gorm.DB, pn string, role string) *gorm.DB {
	// res = clause
	// switch userlogin.Data.Role {
	// case "MANTRI":
	// 	query = query.Where(n.table.BribrainBridayaFactResult.PnAo+` = ?`, userlogin.Data.Pernr)
	// case "UNIT":
	// 	query = query.Where(n.table.BribrainBridayaFactResult.AoBranchCode+` = ?`, userlogin.Data.Branch)
	// case "KC":
	// 	query = query.Where(n.table.BribrainBridayaFactResult.AoMainbrCode+` = ?`, userlogin.Data.Mainbranch)
	// case "KW":
	// 	query = query.Where(n.table.BribrainBridayaFactResult.AoRegionCode+` = ?`, userlogin.Data.Region)

	// 	// case "KP":
	// }
	switch role {
	case "RM":
		query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.PnRm+` = ?`, pn)
	}

	return query
}

func (n NasabahRepository) addRoleBasedClauseNonBRI(query *gorm.DB, pn string, role string) *gorm.DB {
	// res = clause
	// switch userlogin.Data.Role {
	// case "MANTRI":
	// 	query = query.Where(n.table.BribrainBridayaFactResult.PnAo+` = ?`, userlogin.Data.Pernr)
	// case "UNIT":
	// 	query = query.Where(n.table.BribrainBridayaFactResult.AoBranchCode+` = ?`, userlogin.Data.Branch)
	// case "KC":
	// 	query = query.Where(n.table.BribrainBridayaFactResult.AoMainbrCode+` = ?`, userlogin.Data.Mainbranch)
	// case "KW":
	// 	query = query.Where(n.table.BribrainBridayaFactResult.AoRegionCode+` = ?`, userlogin.Data.Region)

	// 	// case "KP":
	// }
	switch role {
	case "RM":
		query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.PnRm+` = ?`, pn)
	}

	return query
}

func (n NasabahRepository) addRoleBasedClauseDitindaklanjuti(query *gorm.DB, pn string, role string) *gorm.DB {
	// res = clause
	// switch userlogin.Data.Role {
	// case "MANTRI":
	// 	query = query.Where(n.table.BribrainBridayaFactResult.PnAo+` = ?`, userlogin.Data.Pernr)
	// case "UNIT":
	// 	query = query.Where(n.table.BribrainBridayaFactResult.AoBranchCode+` = ?`, userlogin.Data.Branch)
	// case "KC":
	// 	query = query.Where(n.table.BribrainBridayaFactResult.AoMainbrCode+` = ?`, userlogin.Data.Mainbranch)
	// case "KW":
	// 	query = query.Where(n.table.BribrainBridayaFactResult.AoRegionCode+` = ?`, userlogin.Data.Region)

	// 	// case "KP":
	// }
	// switch role {
	// case "RM":
	// 	query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.PnRm+` = ? OR `+n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.PnRm+` = ?`, pn, pn)
	// }

	//code tabel baru
	switch role {
	case "RM":
		query = query.Where(n.table.BribrainLink5StatusTindaklanjut.PnRm+` = ?`, pn)
	}

	return query
}

func (n NasabahRepository) querySearch(query *gorm.DB, param *models.ParamRequestListNasabah) *gorm.DB {

	if param.Search != "" {
		query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.NamaNasabahRekan+` like ?`, "%"+strings.ToUpper(param.Search)+"%")
		// _, err := strconv.Atoi(param.Search)
		// if err == nil {
		// 	query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.PhoneNumber+` like ?`, "%"+param.Search+"%")
		// } else {
		// 	query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.NamaDebitur+` like ?`, "%"+strings.ToUpper(param.Search)+"%")
		// }
	}

	return query
}

func (n NasabahRepository) querySearchNonBRI(query *gorm.DB, param *models.ParamRequestListNasabah) *gorm.DB {

	if param.Search != "" {
		query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.NamaNasabahNonBri+` like ?`, "%"+strings.ToUpper(param.Search)+"%")
		// _, err := strconv.Atoi(param.Search)
		// if err == nil {
		// 	query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.PhoneNumber+` like ?`, "%"+param.Search+"%")
		// } else {
		// 	query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.NamaDebitur+` like ?`, "%"+strings.ToUpper(param.Search)+"%")
		// }
	}

	return query
}

func (n NasabahRepository) querySearchDitindaklanjuti(query *gorm.DB, param *models.ParamRequestListNasabah) *gorm.DB {

	if param.Search != "" {
		query = query.Where(
			`(`+
				n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.NamaNasabahNonBri+` like ? AND `+n.table.BribrainLink5StatusTindaklanjut.TipeCustomer+`="NONBRI")`+` OR (`+
				n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.NamaNasabahRekan+` like ? AND `+n.table.BribrainLink5StatusTindaklanjut.TipeCustomer+`="BRI")`, "%"+
				strings.ToUpper(param.Search)+"%", "%"+strings.ToUpper(param.Search)+"%")
		// _, err := strconv.Atoi(param.Search)
		// if err == nil {
		// 	query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.PhoneNumber+` like ?`, "%"+param.Search+"%")
		// } else {
		// 	query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.NamaDebitur+` like ?`, "%"+strings.ToUpper(param.Search)+"%")
		// }
	}

	//code tabel baru
	// if param.Search != "" {
	// 	query = query.Where(n.table.BribrainLink5StatusTindaklanjut.NamaNasabah+` like ?`, "%"+strings.ToUpper(param.Search)+"%")
	// }
	return query
}

//CountRekomendasiBRI ...
func (n NasabahRepository) CountRekomendasiBRI(ctx context.Context, param *models.ParamRequestListNasabah) (res int, err error) {
	// var countRekomendasi int64
	var list []models.GetListQueryStruct

	// query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table)
	// query = query.Joins(`LEFT JOIN ` + n.table.BribrainLink5StatusTindaklanjut.Table + ` ON ` + n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.CifNasabahRekan + ` = ` + n.table.BribrainLink5StatusTindaklanjut.Cifno)
	// query = n.addRoleBasedClause(query, param.PnPengelola, param.Role)
	// // query = n.dynamicWhereClause(query, param)
	// query = query.Where(n.table.BribrainLink5StatusTindaklanjut.KodeHubungi+` = ? AND `+n.table.BribrainLink5StatusTindaklanjut.KodeKunjungan+` = ?`, 0, 0)
	// query = n.querySearch(query, param)
	// err = query.Count(&countRekomendasi).Error

	// query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table)
	// query = n.addRoleBasedClause(query, param.PnPengelola, param.Role)
	// // query = n.dynamicWhereClause(query, param)
	// query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.IdStatus + ` is null`)
	// query = n.querySearch(query, param)
	// err = query.Count(&countRekomendasi).Error

	//code tabel baru
	// query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table)
	// query = n.addRoleBasedClause(query, param.PnPengelola, param.Role)
	// // query = n.dynamicWhereClause(query, param)
	// query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.StatusTindaklanjut + `= 0`)
	// query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.TopupSimpanan + ` != 0 OR ` + n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.SuplesiPinjaman + ` != 0 OR ` + n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.AkuisisiPinjaman + ` != 0 `)
	// query = n.querySearch(query, param)
	// err = query.Count(&countRekomendasi).Error

	//code top 20
	query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table).
		Select(n.table.SelectAllColumnBribrainLink5DashboardDetailInformasiNasabahBRITable())
	query = n.addRoleBasedClause(query, param.PnPengelola, param.Role)
	// query = g.dynamicWhereClause(query, param)
	query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.StatusTindaklanjut + ` = 0`)
	// query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.TopupSimpanan + ` != 0 OR ` + n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.SuplesiPinjaman + ` != 0 OR ` + n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.AkuisisiPinjaman + ` != 0 `)
	query = n.querySearch(query, param)

	err = query.
		Offset(param.Offset).Limit(param.Limit).
		Scan(&list).Error

	if err != nil {
		n.log.Error("nasabah.repository.NasabahRepository.CountRekomendasiBRI: %s", err.Error())
		if err.Error() != gorm.ErrRecordNotFound.Error() {
			return 0, err
		}
	}

	// res = int(countRekomendasi)
	res = len(list)
	return res, nil
}

//CountRekomendasiNonBRI ...
func (n NasabahRepository) CountRekomendasiNonBRI(ctx context.Context, param *models.ParamRequestListNasabah) (res int, err error) {
	// var countRekomendasiNonBRI int64
	var list []models.GetListQueryStruct

	// query skema join
	// query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.Table)
	// query = query.Joins(`LEFT JOIN ` + n.table.BribrainLink5StatusTindaklanjut.Table + ` ON ` + n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.KodeNonBri + ` = ` + n.table.BribrainLink5StatusTindaklanjut.KodeNonBri)
	// query = n.addRoleBasedClauseNonBRI(query, param.PnPengelola, param.Role)
	// // query = g.dynamicWhereClause(query, param)
	// query = query.Where(n.table.BribrainLink5StatusTindaklanjut.KodeHubungi + ` = 0 AND ` + n.table.BribrainLink5StatusTindaklanjut.KodeKunjungan + ` = 0`)
	// query = n.querySearchNonBRI(query, param)
	// err = query.Count(&countRekomendasiNonBRI).Error

	//query skema 1 tabel
	// query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.Table)
	// query = n.addRoleBasedClauseNonBRI(query, param.PnPengelola, param.Role)
	// // query = g.dynamicWhereClause(query, param)
	// query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.IdStatus + ` is null`)
	// query = n.querySearchNonBRI(query, param)
	// err = query.Count(&countRekomendasiNonBRI).Error

	//code tabel baru
	// query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.Table)
	// query = n.addRoleBasedClauseNonBRI(query, param.PnPengelola, param.Role)
	// // query = g.dynamicWhereClause(query, param)
	// query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.StatusTindaklanjut + `=0`)
	// query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.AkuisisiSimpanan + `=1`)
	// query = n.querySearchNonBRI(query, param)
	// err = query.Count(&countRekomendasiNonBRI).Error

	//code top 20
	query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.Table).
		Select(n.table.SelectAllColumnBribrainLink5DashboardDetailInformasiNasabahNonBRITable())
	query = n.addRoleBasedClauseNonBRI(query, param.PnPengelola, param.Role)
	// query = g.dynamicWhereClause(query, param)
	query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.StatusTindaklanjut + ` = 0`)
	// query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.AkuisisiSimpanan + ` = 1`)
	query = n.querySearchNonBRI(query, param)

	err = query.
		Offset(param.Offset).Limit(param.Limit).
		Scan(&list).Error

	if err != nil {
		n.log.Error("nasabah.repository.NasabahRepository.CountRekomendasiNonBRI: %s", err.Error())
		if err.Error() != gorm.ErrRecordNotFound.Error() {
			return 0, err
		}
	}

	// res = int(countRekomendasiNonBRI)
	res = len(list)
	return res, nil
}

//CountDitindaklanjutiBRI ...
func (n NasabahRepository) CountDitindaklanjutiBRI(ctx context.Context, param *models.ParamRequestListNasabah) (res int, err error) {
	var countDitindaklanjutiBRI int64
	// query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table)
	// query = query.Joins(`LEFT JOIN ` + n.table.BribrainLink5StatusTindaklanjut.Table + ` ON ` + n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.CifNasabahRekan + ` = ` + n.table.BribrainLink5StatusTindaklanjut.Cifno)
	// query = n.addRoleBasedClause(query, param.PnPengelola, param.Role)
	// // query = g.dynamicWhereClause(query, param)
	// query = query.Where(n.table.BribrainLink5StatusTindaklanjut.KodeHubungi + ` != 0 OR ` + n.table.BribrainLink5StatusTindaklanjut.KodeKunjungan + ` != 0`)
	// query = n.querySearch(query, param)
	// err = query.Count(&countDitindaklanjutiBRI).Error

	// query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table)
	// query = n.addRoleBasedClause(query, param.PnPengelola, param.Role)
	// // query = g.dynamicWhereClause(query, param)
	// query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.IdStatus + ` is not null`)
	// query = n.querySearch(query, param)
	// err = query.Count(&countDitindaklanjutiBRI).Error

	//code tabel baru
	query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table)
	query = n.addRoleBasedClause(query, param.PnPengelola, param.Role)
	// query = g.dynamicWhereClause(query, param)
	query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.StatusTindaklanjut + ` != 0`)
	query = n.querySearch(query, param)
	err = query.Count(&countDitindaklanjutiBRI).Error

	if err != nil {
		n.log.Error("nasabah.repository.NasabahRepository.CountDitindaklanjutiBRI: %s", err.Error())
		if err.Error() != gorm.ErrRecordNotFound.Error() {
			return 0, err
		}
	}

	res = int(countDitindaklanjutiBRI)
	return res, nil
}

//CountDitindaklanjutiNonBRI ...
func (n NasabahRepository) CountDitindaklanjutiNonBRI(ctx context.Context, param *models.ParamRequestListNasabah) (res int, err error) {
	var countDitindaklanjutiNonBRI int64
	// query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.Table)
	// query = query.Joins(`LEFT JOIN ` + n.table.BribrainLink5StatusTindaklanjut.Table + ` ON ` + n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.KodeNonBri + ` = ` + n.table.BribrainLink5StatusTindaklanjut.KodeNonBri)
	// query = n.addRoleBasedClauseNonBRI(query, param.PnPengelola, param.Role)
	// // query = g.dynamicWhereClause(query, param)
	// query = query.Where(n.table.BribrainLink5StatusTindaklanjut.KodeHubungi + ` != 0 OR ` + n.table.BribrainLink5StatusTindaklanjut.KodeKunjungan + ` != 0`)
	// query = n.querySearchNonBRI(query, param)
	// err = query.Count(&countDitindaklanjutiNonBRI).Error

	// query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.Table)
	// query = n.addRoleBasedClauseNonBRI(query, param.PnPengelola, param.Role)
	// // query = g.dynamicWhereClause(query, param)
	// query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.IdStatus + ` is not null`)
	// query = n.querySearchNonBRI(query, param)
	// err = query.Count(&countDitindaklanjutiNonBRI).Error

	//code tabel baru
	query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.Table)
	query = n.addRoleBasedClauseNonBRI(query, param.PnPengelola, param.Role)
	// query = g.dynamicWhereClause(query, param)
	query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.StatusTindaklanjut + ` != 0`)
	query = n.querySearchNonBRI(query, param)
	err = query.Count(&countDitindaklanjutiNonBRI).Error

	if err != nil {
		n.log.Error("nasabah.repository.NasabahRepository.CountDitindaklanjutiNonBRI: %s", err.Error())
		if err.Error() != gorm.ErrRecordNotFound.Error() {
			return 0, err
		}
	}

	res = int(countDitindaklanjutiNonBRI)
	return res, nil
}

//TotalRekomendasiBRI ...
func (n NasabahRepository) TotalRekomendasiBRI(ctx context.Context, param *models.ParamRequestListNasabah) (res int, err error) {
	// var totalRekomendasi int64
	var list []models.GetListQueryStruct

	// query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table)
	// query = query.Joins(`LEFT JOIN ` + n.table.BribrainLink5StatusTindaklanjut.Table + ` ON ` + n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.CifNasabahRekan + ` = ` + n.table.BribrainLink5StatusTindaklanjut.Cifno)
	// query = n.addRoleBasedClause(query, param.PnPengelola, param.Role)
	// // query = n.dynamicWhereClause(query, param)
	// query = query.Where(n.table.BribrainLink5StatusTindaklanjut.KodeHubungi+` = ? AND `+n.table.BribrainLink5StatusTindaklanjut.KodeKunjungan+` = ?`, 0, 0)
	// err = query.Count(&totalRekomendasi).Error

	// query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table)
	// query = n.addRoleBasedClause(query, param.PnPengelola, param.Role)
	// // query = n.dynamicWhereClause(query, param)
	// query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.IdStatus + ` is null `)
	// err = query.Count(&totalRekomendasi).Error

	//code tabel baru
	// query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table)
	// query = n.addRoleBasedClause(query, param.PnPengelola, param.Role)
	// // query = n.dynamicWhereClause(query, param)
	// query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.StatusTindaklanjut + ` = 0 `)
	// query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.TopupSimpanan + ` != 0 OR ` + n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.SuplesiPinjaman + ` != 0 OR ` + n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.AkuisisiPinjaman + ` != 0 `)
	// err = query.Count(&totalRekomendasi).Error

	query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table).
		Select(n.table.SelectAllColumnBribrainLink5DashboardDetailInformasiNasabahBRITable())
	query = n.addRoleBasedClause(query, param.PnPengelola, param.Role)
	// query = g.dynamicWhereClause(query, param)
	query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.StatusTindaklanjut + ` = 0`)
	// query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.TopupSimpanan + ` != 0 OR ` + n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.SuplesiPinjaman + ` != 0 OR ` + n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.AkuisisiPinjaman + ` != 0 `)

	err = query.
		Offset(param.Offset).Limit(param.Limit).
		Scan(&list).Error

	if err != nil {
		n.log.Error("nasabah.repository.NasabahRepository.TotalRekomendasiBRI: %s", err.Error())
		if err.Error() != gorm.ErrRecordNotFound.Error() {
			return 0, err
		}
	}

	// res = int(totalRekomendasi)
	res = len(list)
	return res, nil
}

//TotalRekomendasiNonBRI ...
func (n NasabahRepository) TotalRekomendasiNonBRI(ctx context.Context, param *models.ParamRequestListNasabah) (res int, err error) {
	// var totalRekomendasiNonBRI int64
	var list []models.GetListQueryStruct

	// query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.Table)
	// query = query.Joins(`LEFT JOIN ` + n.table.BribrainLink5StatusTindaklanjut.Table + ` ON ` + n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.KodeNonBri + ` = ` + n.table.BribrainLink5StatusTindaklanjut.KodeNonBri)
	// query = n.addRoleBasedClauseNonBRI(query, param.PnPengelola, param.Role)
	// // query = g.dynamicWhereClause(query, param)
	// query = query.Where(n.table.BribrainLink5StatusTindaklanjut.KodeHubungi + ` = 0 AND ` + n.table.BribrainLink5StatusTindaklanjut.KodeKunjungan + ` = 0`)
	// err = query.Count(&totalRekomendasiNonBRI).Error

	// query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.Table)
	// query = n.addRoleBasedClauseNonBRI(query, param.PnPengelola, param.Role)
	// // query = g.dynamicWhereClause(query, param)
	// query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.IdStatus + ` is null`)
	// err = query.Count(&totalRekomendasiNonBRI).Error

	//code tabel baru
	// query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.Table)
	// query = n.addRoleBasedClauseNonBRI(query, param.PnPengelola, param.Role)
	// // query = g.dynamicWhereClause(query, param)
	// query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.StatusTindaklanjut + `=0`)
	// query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.AkuisisiSimpanan + `=1`)
	// err = query.Count(&totalRekomendasiNonBRI).Error

	//code top 20
	query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.Table).
		Select(n.table.SelectAllColumnBribrainLink5DashboardDetailInformasiNasabahNonBRITable())
	query = n.addRoleBasedClauseNonBRI(query, param.PnPengelola, param.Role)
	// query = g.dynamicWhereClause(query, param)
	query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.StatusTindaklanjut + ` = 0`)
	// query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.AkuisisiSimpanan + ` = 1`)

	err = query.
		Offset(param.Offset).Limit(param.Limit).
		Scan(&list).Error

	if err != nil {
		n.log.Error("nasabah.repository.NasabahRepository.TotalRekomendasiNonBRI: %s", err.Error())
		if err.Error() != gorm.ErrRecordNotFound.Error() {
			return 0, err
		}
	}

	// res = int(totalRekomendasiNonBRI)
	res = len(list)
	return res, nil
}

//TotalDitindaklanjutiBRI ...
func (n NasabahRepository) TotalDitindaklanjutiBRI(ctx context.Context, param *models.ParamRequestListNasabah) (res int, err error) {
	var totalDitindaklanjutiBRI int64
	// query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table)
	// query = query.Joins(`LEFT JOIN ` + n.table.BribrainLink5StatusTindaklanjut.Table + ` ON ` + n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.CifNasabahRekan + ` = ` + n.table.BribrainLink5StatusTindaklanjut.Cifno)
	// query = n.addRoleBasedClause(query, param.PnPengelola, param.Role)
	// // query = g.dynamicWhereClause(query, param)
	// query = query.Where(n.table.BribrainLink5StatusTindaklanjut.KodeHubungi + ` != 0 OR ` + n.table.BribrainLink5StatusTindaklanjut.KodeKunjungan + ` != 0`)
	// err = query.Count(&totalDitindaklanjutiBRI).Error

	// query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table)
	// query = n.addRoleBasedClause(query, param.PnPengelola, param.Role)
	// // query = g.dynamicWhereClause(query, param)
	// query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.IdStatus + ` is not null`)
	// err = query.Count(&totalDitindaklanjutiBRI).Error

	//code tabel baru
	query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table)
	query = n.addRoleBasedClause(query, param.PnPengelola, param.Role)
	// query = g.dynamicWhereClause(query, param)
	query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.StatusTindaklanjut + ` != 0`)
	err = query.Count(&totalDitindaklanjutiBRI).Error

	if err != nil {
		n.log.Error("nasabah.repository.NasabahRepository.TotalDitindaklanjutiBRI: %s", err.Error())
		if err.Error() != gorm.ErrRecordNotFound.Error() {
			return 0, err
		}
	}

	res = int(totalDitindaklanjutiBRI)
	return res, nil
}

//TotalDitindaklanjutiNonBRI ...
func (n NasabahRepository) TotalDitindaklanjutiNonBRI(ctx context.Context, param *models.ParamRequestListNasabah) (res int, err error) {
	var totalDitindaklanjutiNonBRI int64
	// query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.Table)
	// query = query.Joins(`LEFT JOIN ` + n.table.BribrainLink5StatusTindaklanjut.Table + ` ON ` + n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.KodeNonBri + ` = ` + n.table.BribrainLink5StatusTindaklanjut.KodeNonBri)
	// query = n.addRoleBasedClauseNonBRI(query, param.PnPengelola, param.Role)
	// // query = g.dynamicWhereClause(query, param)
	// query = query.Where(n.table.BribrainLink5StatusTindaklanjut.KodeHubungi + ` != 0 OR ` + n.table.BribrainLink5StatusTindaklanjut.KodeKunjungan + ` != 0`)
	// err = query.Count(&totalDitindaklanjutiNonBRI).Error

	// query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.Table)
	// query = n.addRoleBasedClauseNonBRI(query, param.PnPengelola, param.Role)
	// // query = g.dynamicWhereClause(query, param)
	// query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.IdStatus + ` is not null`)
	// err = query.Count(&totalDitindaklanjutiNonBRI).Error

	//code tabel baru
	query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.Table)
	query = n.addRoleBasedClauseNonBRI(query, param.PnPengelola, param.Role)
	// query = g.dynamicWhereClause(query, param)
	query = query.Where(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.StatusTindaklanjut + ` != 0`)
	err = query.Count(&totalDitindaklanjutiNonBRI).Error

	if err != nil {
		n.log.Error("nasabah.repository.NasabahRepository.TotalDitindaklanjutiNonBRI: %s", err.Error())
		if err.Error() != gorm.ErrRecordNotFound.Error() {
			return 0, err
		}
	}

	res = int(totalDitindaklanjutiNonBRI)
	return res, nil
}

func (n NasabahRepository) GetListDitindaklanjuti(ctx context.Context, param *models.ParamRequestListNasabah) (res []models.GetListQueryStruct, err error) {
	query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5StatusTindaklanjut.Table).
		Select(n.table.SelectAllColumnBribrainLink5DashboardDetailInformasiNasabahBRITable() + `,` + n.table.SelectAllColumnBribrainLink5StatusTindaklanjutTable() + `,` + n.table.SelectAllColumnBribrainLink5DashboardDetailInformasiNasabahNonBRITable())
	query = query.Joins(`LEFT JOIN ` + n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table + ` ON ` + n.table.BribrainLink5StatusTindaklanjut.Cifno + ` = ` + n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.CifNasabahRekan)
	query = query.Joins(`LEFT JOIN ` + n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.Table + ` ON ` + n.table.BribrainLink5StatusTindaklanjut.KodeNonBri + ` = ` + n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.KodeNonBri)
	query = n.addRoleBasedClauseDitindaklanjuti(query, param.PnPengelola, param.Role)
	// query = g.dynamicWhereClause(query, param)
	query = query.Where(n.table.BribrainLink5StatusTindaklanjut.KodeHubungi+` != ? OR `+n.table.BribrainLink5StatusTindaklanjut.KodeKunjungan+` != ?`, 0, 0)
	query = n.querySearchDitindaklanjuti(query, param)

	// query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5StatusTindaklanjut.Table).
	// 	Select(n.table.SelectAllColumnBribrainLink5DashboardDetailInformasiNasabahBRITable() + `,` + n.table.SelectAllColumnBribrainLink5StatusTindaklanjutTable() + `,` + n.table.SelectAllColumnBribrainLink5DashboardDetailInformasiNasabahNonBRITable())
	// query = query.Joins(`LEFT JOIN ` + n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table + ` ON ` + n.table.BribrainLink5StatusTindaklanjut.Id + ` = ` + n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.IdStatus)
	// query = query.Joins(`LEFT JOIN ` + n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.Table + ` ON ` + n.table.BribrainLink5StatusTindaklanjut.Id + ` = ` + n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.IdStatus)
	// query = n.addRoleBasedClauseDitindaklanjuti(query, param.PnPengelola, param.Role)
	// // query = g.dynamicWhereClause(query, param)
	// // query = query.Where(n.table.BribrainLink5StatusTindaklanjut.KodeHubungi+` != ? OR `+n.table.BribrainLink5StatusTindaklanjut.KodeKunjungan+` != ?`, 0, 0)
	// query = n.querySearchDitindaklanjuti(query, param)

	//code tabel baru
	// query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5StatusTindaklanjut.Table).
	// 	Select(n.table.SelectAllColumnBribrainLink5StatusTindaklanjutTable())
	// query = n.addRoleBasedClauseDitindaklanjuti(query, param.PnPengelola, param.Role)
	// // query = g.dynamicWhereClause(query, param)
	// query = query.Where(n.table.BribrainLink5StatusTindaklanjut.KodeHubungi+` != ? OR `+n.table.BribrainLink5StatusTindaklanjut.KodeKunjungan+` != ?`, 0, 0)
	// query = n.querySearchDitindaklanjuti(query, param)

	err = query.
		Offset(param.Offset).Limit(param.Limit).
		// Order(`case when ` + n.table.BribrainLink5StatusTindaklanjut.KodeKunjungan + ` = 3 then 1 when (` + n.table.BribrainLink5StatusTindaklanjut.KodeHubungi + ` = 1 AND ` + n.table.BribrainLink5StatusTindaklanjut.KodeKunjungan + ` != 1) then 2 when (` + n.table.BribrainLink5StatusTindaklanjut.KodeHubungi + ` = 1 AND ` + n.table.BribrainLink5StatusTindaklanjut.KodeKunjungan + ` = 1) then 3 when (` + n.table.BribrainLink5StatusTindaklanjut.KodeHubungi + ` != 1 AND ` + n.table.BribrainLink5StatusTindaklanjut.KodeKunjungan + ` = 1) then 4 when ` + n.table.BribrainLink5StatusTindaklanjut.KodeHubungi + ` = 2 then 5 when ` + n.table.BribrainLink5StatusTindaklanjut.KodeHubungi + `= 3 then 6 ELSE 7 end`).
		// Order(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.NamaNasabahRekan + ` ASC`).
		// Order(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.NamaNasabahNonBri + ` ASC`).
		Order(n.table.BribrainLink5StatusTindaklanjut.UpdatedDate + ` DESC`).
		Scan(&res).Error

	if err != nil {
		n.log.Error("nasabah.repository.NasabahRepository.GetListDitindaklanjuti: %s", err.Error())
		if err.Error() != gorm.ErrRecordNotFound.Error() {
			return nil, err
		}
	}

	return res, nil
}

func (n NasabahRepository) GetLastUpdateTabelBRI(ctx context.Context, pn string, role string) (res *time.Time, err error) {
	// var listRekomendasi []models.ResponseListDebitur
	// var lastupdatestring time.Time
	var lastupdate models.TanggalUpdateStruct
	query := n.Conn.Table(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table).Select(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.UpdatedDate + ` AS UPDATED_DATE`)
	// query = query.Joins(`LEFT JOIN ` + n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table + ` ON ` + n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.NoRekening + ` = ` + n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.NoRekening)
	query = n.addRoleBasedClause(query, pn, role)
	// query = n.dynamicWhereClause(query, param)
	query = query.Order(n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.UpdatedDate + ` desc`)
	err = query.Scan(&lastupdate).Limit(1).Error
	if err != nil {
		errlog, _ := fmt.Printf("repo.getLastUpdate: %s ", err.Error())
		log.Print(errlog)
		return res, err
	}
	// lastupdatestring, _ = time.Parse("2 January 2006", lastupdate)
	// lastupdate = time.Now()
	log.Print(lastupdate)
	// log.Print(lastupdatestring)
	// res = lastupdate.TanggalUpdate.Format("2 January 2006")
	res = lastupdate.TanggalUpdate

	// res = listRekomendasi
	return res, nil
}

func (n NasabahRepository) GetLastUpdateTabelNonBRI(ctx context.Context, pn string, role string) (res *time.Time, err error) {
	// var listRekomendasi []models.ResponseListDebitur
	// var lastupdatestring time.Time
	var lastupdate models.TanggalUpdateStruct
	query := n.Conn.Table(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.Table).Select(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.UpdatedDate + ` AS UPDATED_DATE`)
	// query = query.Joins(`LEFT JOIN ` + n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.Table + ` ON ` + n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.NoRekening + ` = ` + n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.NoRekening)
	query = n.addRoleBasedClauseNonBRI(query, pn, role)
	// query = n.dynamicWhereClause(query, param)
	query = query.Order(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.UpdatedDate + ` desc`)
	err = query.Scan(&lastupdate).Limit(1).Error
	if err != nil {
		errlog, _ := fmt.Printf("repo.GetLastUpdateTabelNonBRI: %s ", err.Error())
		log.Print(errlog)
		return res, err
	}
	// lastupdatestring, _ = time.Parse("2 January 2006", lastupdate)
	// lastupdate = time.Now()
	log.Print(lastupdate)
	// log.Print(lastupdatestring)
	// res = lastupdate.TanggalUpdate.Format("2 January 2006")
	res = lastupdate.TanggalUpdate

	// res = listRekomendasi
	return res, nil
}

func (n NasabahRepository) GetLastUpdateDitindaklanjuti(ctx context.Context, pn string, role string) (res *time.Time, err error) {
	// var listRekomendasi []models.ResponseListDebitur
	// var lastupdatestring time.Time
	var lastupdate models.TanggalUpdateStruct
	// query := n.Conn.Table(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.Table).Select(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.UpdatedDate + ` AS UPDATED_DATE`)
	// // query = query.Joins(`LEFT JOIN ` + n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.Table + ` ON ` + n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.NoRekening + ` = ` + n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.NoRekening)
	// query = n.addRoleBasedClause(query, pn, role)
	// // query = n.dynamicWhereClause(query, param)
	// query = query.Order(n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.UpdatedDate + ` desc`)
	// err = query.Scan(&lastupdate).Limit(1).Error

	// query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5StatusTindaklanjut.Table).
	query := n.Conn.Table(n.table.BribrainLink5StatusTindaklanjut.Table).Select(n.table.BribrainLink5StatusTindaklanjut.UpdatedDate + ` AS UPDATED_DATE`)
	// Select(n.table.SelectAllColumnBribrainLink5DashboardDetailInformasiNasabahBRITable() + `,` + n.table.SelectAllColumnBribrainLink5StatusTindaklanjutTable() + `,` + n.table.SelectAllColumnBribrainLink5DashboardDetailInformasiNasabahNonBRITable())
	query = query.Joins(`LEFT JOIN ` + n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table + ` ON ` + n.table.BribrainLink5StatusTindaklanjut.Cifno + ` = ` + n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.CifNasabahRekan)
	query = query.Joins(`LEFT JOIN ` + n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.Table + ` ON ` + n.table.BribrainLink5StatusTindaklanjut.KodeNonBri + ` = ` + n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.KodeNonBri)
	query = n.addRoleBasedClauseDitindaklanjuti(query, pn, role)
	// query = g.dynamicWhereClause(query, param)
	query = query.Where(n.table.BribrainLink5StatusTindaklanjut.KodeHubungi+` != ? OR `+n.table.BribrainLink5StatusTindaklanjut.KodeKunjungan+` != ?`, 0, 0)
	query = query.Order(n.table.BribrainLink5StatusTindaklanjut.UpdatedDate + ` desc`)
	// query = query.Order(`UPDATED_DATE desc`)
	err = query.Scan(&lastupdate).Limit(1).Error

	if err != nil {
		errlog, _ := fmt.Printf("repo.GetLastUpdateDitindaklanjuti: %s ", err.Error())
		log.Print(errlog)
		return res, err
	}
	// lastupdatestring, _ = time.Parse("2 January 2006", lastupdate)
	// lastupdate = time.Now()
	log.Print(lastupdate)
	// log.Print(lastupdatestring)
	// res = lastupdate.TanggalUpdate.Format("2 January 2006")
	res = lastupdate.TanggalUpdate

	// res = listRekomendasi
	return res, nil
}
