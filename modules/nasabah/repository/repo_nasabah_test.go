package repository_test

import (
	"context"
	"ms-bribrain-link5/helper"
	"ms-bribrain-link5/helper/logger"
	"ms-bribrain-link5/models"
	nasabahRepo "ms-bribrain-link5/modules/nasabah/repository"
	"regexp"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
)

func TestRekomendasiRepository_GetLastUpdateTabelBRI(t *testing.T) {
	err := helper.NewMockEnv()
	db, mock, err := helper.NewMockDB()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	//resultMock
	mockResponseTanggalUpdate := &models.TanggalUpdateStruct{}
	mockResponseTanggalUpdate = mockResponseTanggalUpdate.MappingExampleData()
	pnPengelola := "00999950"
	role := "RM"

	//rowAndColumnMock
	table := &models.NasabahTable{}
	table = table.MappingTable()

	// querySelect := table.BribrainLink5DashboardDetailInformasiNasabahBRI.UpdatedDate + ` AS UPDATED_DATE`

	// queryJoin := `INNER join ` + table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table + ` on ` + table.BribrainLink5DashboardDetailInformasiNasabahBRI.IdRekomendasi + ` = ` + table.BribrainLink5DashboardDetailInformasiNasabahBRI.Id

	// queryWhere := table.BribrainLink5DashboardDetailInformasiNasabahBRI.PnRm + " =?"

	// queryOrder := table.BribrainLink5DashboardDetailInformasiNasabahBRI.UpdatedDate + " desc"
	t.Run("success", func(t *testing.T) {
		rows := sqlmock.NewRows([]string{"UPDATED_DATE"}).
			AddRow(mockResponseTanggalUpdate.TanggalUpdate)
		// query := "SELECT " + querySelect + " FROM `" + table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table + "` " + " WHERE " + queryWhere + " ORDER BY " + queryOrder
		query := "SELECT . AS UPDATED_DATE FROM ` WHERE . = ? ORDER BY . desc"
		queryRegex := regexp.QuoteMeta(query)
		mock.ExpectQuery(queryRegex).
			WithArgs(pnPengelola).
			WillReturnRows(rows)
		a := nasabahRepo.NewNasabahRepository(db, logger.L)

		getLastUpdate, err := a.GetLastUpdateTabelBRI(context.TODO(), pnPengelola, role)
		assert.NoError(t, err)
		assert.Equal(t, mockResponseTanggalUpdate.TanggalUpdate, getLastUpdate)
	})

	t.Run("error-regex-query", func(t *testing.T) {
		rows := sqlmock.NewRows([]string{"UPDATED_DATE"}).
			AddRow(mockResponseTanggalUpdate.TanggalUpdate)
		query := "SELECT `UPDATED_DATE` FROM `bribrain_link5_detail_informasi_nasabah_bri` WHERE pn_rm = ? ORDER BY UPDATED_DATE desc"
		queryRegex := regexp.QuoteMeta(query)
		mock.ExpectQuery(queryRegex).
			WithArgs(pnPengelola).
			WillReturnRows(rows)
		a := nasabahRepo.NewNasabahRepository(db, logger.L)

		getLastUpdate, err := a.GetLastUpdateTabelBRI(context.TODO(), pnPengelola, role)
		assert.Error(t, err)
		assert.Nil(t, getLastUpdate)
	})
}

func TestRekomendasiRepository_GetLastUpdateTabelNonBRI(t *testing.T) {
	err := helper.NewMockEnv()
	db, mock, err := helper.NewMockDB()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	//resultMock
	mockResponseTanggalUpdate := &models.TanggalUpdateStruct{}
	mockResponseTanggalUpdate = mockResponseTanggalUpdate.MappingExampleData()
	pnPengelola := "00999950"
	role := "RM"

	//rowAndColumnMock
	table := &models.NasabahTable{}
	table = table.MappingTable()

	t.Run("success", func(t *testing.T) {
		rows := sqlmock.NewRows([]string{"UPDATED_DATE"}).
			AddRow(mockResponseTanggalUpdate.TanggalUpdate)
		query := "SELECT . AS UPDATED_DATE FROM ` WHERE . = ? ORDER BY . desc"
		queryRegex := regexp.QuoteMeta(query)
		mock.ExpectQuery(queryRegex).
			WithArgs(pnPengelola).
			WillReturnRows(rows)
		a := nasabahRepo.NewNasabahRepository(db, logger.L)

		getLastUpdate, err := a.GetLastUpdateTabelNonBRI(context.TODO(), pnPengelola, role)
		assert.NoError(t, err)
		assert.Equal(t, mockResponseTanggalUpdate.TanggalUpdate, getLastUpdate)
	})

	t.Run("error-regex-query", func(t *testing.T) {
		rows := sqlmock.NewRows([]string{"UPDATED_DATE"}).
			AddRow(mockResponseTanggalUpdate.TanggalUpdate)
		query := "SELECT `UPDATED_DATE` FROM `bribrain_link5_detail_informasi_nasabah__non_bri` WHERE pn_rm = ? ORDER BY UPDATED_DATE desc"
		queryRegex := regexp.QuoteMeta(query)
		mock.ExpectQuery(queryRegex).
			WithArgs(pnPengelola).
			WillReturnRows(rows)
		a := nasabahRepo.NewNasabahRepository(db, logger.L)

		getLastUpdate, err := a.GetLastUpdateTabelNonBRI(context.TODO(), pnPengelola, role)
		assert.Error(t, err)
		assert.Nil(t, getLastUpdate)
	})
}

func TestRekomendasiRepository_GetLastUpdateDitindaklanjuti(t *testing.T) {
	err := helper.NewMockEnv()
	db, mock, err := helper.NewMockDB()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	//resultMock
	mockResponseTanggalUpdate := &models.TanggalUpdateStruct{}
	mockResponseTanggalUpdate = mockResponseTanggalUpdate.MappingExampleData()
	pnPengelola := "00999950"
	role := "RM"

	//rowAndColumnMock
	table := &models.NasabahTable{}
	table = table.MappingTable()

	// querySelect := table.BribrainLink5DashboardDetailInformasiNasabahBRI.UpdatedDate + ` AS UPDATED_DATE`

	// queryJoin := `INNER join ` + table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table + ` on ` + table.BribrainLink5DashboardDetailInformasiNasabahBRI.IdRekomendasi + ` = ` + table.BribrainLink5DashboardDetailInformasiNasabahBRI.Id

	// queryWhere := table.BribrainLink5DashboardDetailInformasiNasabahBRI.PnRm + " =?"

	// queryOrder := table.BribrainLink5DashboardDetailInformasiNasabahBRI.UpdatedDate + " desc"
	t.Run("success", func(t *testing.T) {
		rows := sqlmock.NewRows([]string{"UPDATED_DATE"}).
			AddRow(mockResponseTanggalUpdate.TanggalUpdate)
		// query := "SELECT " + querySelect + " FROM `" + table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table + "` " + " WHERE " + queryWhere + " ORDER BY " + queryOrder
		// query := "SELECT . AS UPDATED_DATE FROM ` LEFT JOIN ON . = . LEFT JOIN ON . = . WHERE (. = ? OR . = ?) AND (. != ? OR . != ?) ORDER BY . desc"
		query := "SELECT . AS UPDATED_DATE FROM ` LEFT JOIN ON . = . LEFT JOIN ON . = . WHERE . = ? AND (. != ? OR . != ?) ORDER BY . desc"
		queryRegex := regexp.QuoteMeta(query)
		mock.ExpectQuery(queryRegex).
			WithArgs(pnPengelola, 0, 0).
			WillReturnRows(rows)
		a := nasabahRepo.NewNasabahRepository(db, logger.L)

		getLastUpdate, err := a.GetLastUpdateDitindaklanjuti(context.TODO(), pnPengelola, role)
		assert.NoError(t, err)
		assert.Equal(t, mockResponseTanggalUpdate.TanggalUpdate, getLastUpdate)
	})

	t.Run("error-regex-query", func(t *testing.T) {
		rows := sqlmock.NewRows([]string{"UPDATED_DATE"}).
			AddRow(mockResponseTanggalUpdate.TanggalUpdate)
		query := "SELECT `UPDATED_DATE` FROM `bribrain_link5_status_tindaklanjut` WHERE pn_rm = ? ORDER BY UPDATED_DATE desc"
		queryRegex := regexp.QuoteMeta(query)
		mock.ExpectQuery(queryRegex).
			WithArgs(pnPengelola).
			WillReturnRows(rows)
		a := nasabahRepo.NewNasabahRepository(db, logger.L)

		getLastUpdate, err := a.GetLastUpdateDitindaklanjuti(context.TODO(), pnPengelola, role)
		assert.Error(t, err)
		assert.Nil(t, getLastUpdate)
	})
}

func TestRekomendasiRepository_CountRekomendasiBRI(t *testing.T) {
	err := helper.NewMockEnv()
	db, mock, err := helper.NewMockDB()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}

	mockParamRequestListNasabah := &models.ParamRequestListNasabah{}
	mockParamRequestListNasabah = mockParamRequestListNasabah.MappingExampleData()
	pnPengelola := "00999950"
	// role := "RM"

	//resultMock
	mockCountRekomendasiBRI := 1

	//rowAndColumnMock
	table := &models.NasabahTable{}
	table = table.MappingTable()

	// querySelect := table.BribrainLink5DashboardDetailInformasiNasabahBRI.UpdatedDate + ` AS UPDATED_DATE`

	// queryJoin := `INNER join ` + table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table + ` on ` + table.BribrainLink5DashboardDetailInformasiNasabahBRI.IdRekomendasi + ` = ` + table.BribrainLink5DashboardDetailInformasiNasabahBRI.Id

	// queryWhere := table.BribrainLink5DashboardDetailInformasiNasabahBRI.PnRm + " =?"

	// queryOrder := table.BribrainLink5DashboardDetailInformasiNasabahBRI.UpdatedDate + " desc"
	// t.Run("success", func(t *testing.T) {
	// 	rows := sqlmock.NewRows([]string{"COUNT"}).
	// 		AddRow(mockCountRekomendasiBRI)
	// 	query := "SELECT " + querySelect + " FROM `" + table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table + "` " + " WHERE " + queryWhere + " ORDER BY " + queryOrder
	// 	// query := "SELECT . AS UPDATED_DATE FROM ` LEFT JOIN ON . = . LEFT JOIN ON . = . WHERE (. = ? OR . = ?) AND (. != ? OR . != ?) ORDER BY . desc"
	// 	queryRegex := regexp.QuoteMeta(query)
	// 	mock.ExpectQuery(queryRegex).
	// 		WithArgs(pnPengelola, pnPengelola, 0, 0).
	// 		WillReturnRows(rows)
	// 	a := nasabahRepo.NewNasabahRepository(db, logger.L)

	// 	count, err := a.CountRekomendasiBRI(context.TODO(), mockParamRequestListNasabah)
	// 	assert.NoError(t, err)
	// 	assert.Equal(t, 1, count)
	// })

	t.Run("error-regex-query", func(t *testing.T) {
		rows := sqlmock.NewRows([]string{"COUNT"}).
			AddRow(mockCountRekomendasiBRI)
		query := "SELECT `UPDATED_DATE` FROM `bribrain_link5_status_tindaklanjut` WHERE pn_rm = ? ORDER BY UPDATED_DATE desc"
		queryRegex := regexp.QuoteMeta(query)
		mock.ExpectQuery(queryRegex).
			WithArgs(pnPengelola).
			WillReturnRows(rows)
		a := nasabahRepo.NewNasabahRepository(db, logger.L)

		count, err := a.CountRekomendasiBRI(context.TODO(), mockParamRequestListNasabah)
		assert.Error(t, err)
		assert.Equal(t, 0, count)
	})
}

func TestRekomendasiRepository_CountRekomendasiNonBRI(t *testing.T) {
	err := helper.NewMockEnv()
	db, mock, err := helper.NewMockDB()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}

	mockParamRequestListNasabah := &models.ParamRequestListNasabah{}
	mockParamRequestListNasabah = mockParamRequestListNasabah.MappingExampleData()
	pnPengelola := "00999950"
	// role := "RM"

	//resultMock
	mockCountRekomendasiNonBRI := 1

	//rowAndColumnMock
	table := &models.NasabahTable{}
	table = table.MappingTable()

	t.Run("error-regex-query", func(t *testing.T) {
		rows := sqlmock.NewRows([]string{"COUNT"}).
			AddRow(mockCountRekomendasiNonBRI)
		query := "SELECT `UPDATED_DATE` FROM `bribrain_link5_status_tindaklanjut` WHERE pn_rm = ? ORDER BY UPDATED_DATE desc"
		queryRegex := regexp.QuoteMeta(query)
		mock.ExpectQuery(queryRegex).
			WithArgs(pnPengelola).
			WillReturnRows(rows)
		a := nasabahRepo.NewNasabahRepository(db, logger.L)

		count, err := a.CountRekomendasiNonBRI(context.TODO(), mockParamRequestListNasabah)
		assert.Error(t, err)
		assert.Equal(t, 0, count)
	})
}

func TestRekomendasiRepository_CountDitindaklanjutiBRI(t *testing.T) {
	err := helper.NewMockEnv()
	db, mock, err := helper.NewMockDB()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}

	mockParamRequestListNasabah := &models.ParamRequestListNasabah{}
	mockParamRequestListNasabah = mockParamRequestListNasabah.MappingExampleData()
	pnPengelola := "00999950"
	// role := "RM"

	//resultMock
	mockCountDitindaklanjutiBRI := 1

	//rowAndColumnMock
	table := &models.NasabahTable{}
	table = table.MappingTable()

	t.Run("error-regex-query", func(t *testing.T) {
		rows := sqlmock.NewRows([]string{"COUNT"}).
			AddRow(mockCountDitindaklanjutiBRI)
		query := "SELECT `UPDATED_DATE` FROM `bribrain_link5_status_tindaklanjut` WHERE pn_rm = ? ORDER BY UPDATED_DATE desc"
		queryRegex := regexp.QuoteMeta(query)
		mock.ExpectQuery(queryRegex).
			WithArgs(pnPengelola).
			WillReturnRows(rows)
		a := nasabahRepo.NewNasabahRepository(db, logger.L)

		count, err := a.CountDitindaklanjutiBRI(context.TODO(), mockParamRequestListNasabah)
		assert.Error(t, err)
		assert.Equal(t, 0, count)
	})
}

func TestRekomendasiRepository_CountDitindaklanjutiNonBRI(t *testing.T) {
	err := helper.NewMockEnv()
	db, mock, err := helper.NewMockDB()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}

	mockParamRequestListNasabah := &models.ParamRequestListNasabah{}
	mockParamRequestListNasabah = mockParamRequestListNasabah.MappingExampleData()
	pnPengelola := "00999950"
	// role := "RM"

	//resultMock
	mockCountDitindaklanjutiNonBRI := 1

	//rowAndColumnMock
	table := &models.NasabahTable{}
	table = table.MappingTable()

	t.Run("error-regex-query", func(t *testing.T) {
		rows := sqlmock.NewRows([]string{"COUNT"}).
			AddRow(mockCountDitindaklanjutiNonBRI)
		query := "SELECT `UPDATED_DATE` FROM `bribrain_link5_status_tindaklanjut` WHERE pn_rm = ? ORDER BY UPDATED_DATE desc"
		queryRegex := regexp.QuoteMeta(query)
		mock.ExpectQuery(queryRegex).
			WithArgs(pnPengelola).
			WillReturnRows(rows)
		a := nasabahRepo.NewNasabahRepository(db, logger.L)

		count, err := a.CountDitindaklanjutiNonBRI(context.TODO(), mockParamRequestListNasabah)
		assert.Error(t, err)
		assert.Equal(t, 0, count)
	})
}

func TestRekomendasiRepository_TotalRekomendasiBRI(t *testing.T) {
	err := helper.NewMockEnv()
	db, mock, err := helper.NewMockDB()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}

	mockParamRequestListNasabah := &models.ParamRequestListNasabah{}
	mockParamRequestListNasabah = mockParamRequestListNasabah.MappingExampleData()
	pnPengelola := "00999950"
	// role := "RM"

	//resultMock
	mockTotalRekomendasiBRI := 1

	//rowAndColumnMock
	table := &models.NasabahTable{}
	table = table.MappingTable()

	t.Run("error-regex-query", func(t *testing.T) {
		rows := sqlmock.NewRows([]string{"COUNT"}).
			AddRow(mockTotalRekomendasiBRI)
		query := "SELECT `UPDATED_DATE` FROM `bribrain_link5_status_tindaklanjut` WHERE pn_rm = ? ORDER BY UPDATED_DATE desc"
		queryRegex := regexp.QuoteMeta(query)
		mock.ExpectQuery(queryRegex).
			WithArgs(pnPengelola).
			WillReturnRows(rows)
		a := nasabahRepo.NewNasabahRepository(db, logger.L)

		count, err := a.TotalRekomendasiBRI(context.TODO(), mockParamRequestListNasabah)
		assert.Error(t, err)
		assert.Equal(t, 0, count)
	})
}

func TestRekomendasiRepository_TotalRekomendasiNonBRI(t *testing.T) {
	err := helper.NewMockEnv()
	db, mock, err := helper.NewMockDB()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}

	mockParamRequestListNasabah := &models.ParamRequestListNasabah{}
	mockParamRequestListNasabah = mockParamRequestListNasabah.MappingExampleData()
	pnPengelola := "00999950"
	// role := "RM"

	//resultMock
	mockTotalRekomendasiNonBRI := 1

	//rowAndColumnMock
	table := &models.NasabahTable{}
	table = table.MappingTable()

	t.Run("error-regex-query", func(t *testing.T) {
		rows := sqlmock.NewRows([]string{"COUNT"}).
			AddRow(mockTotalRekomendasiNonBRI)
		query := "SELECT `UPDATED_DATE` FROM `bribrain_link5_status_tindaklanjut` WHERE pn_rm = ? ORDER BY UPDATED_DATE desc"
		queryRegex := regexp.QuoteMeta(query)
		mock.ExpectQuery(queryRegex).
			WithArgs(pnPengelola).
			WillReturnRows(rows)
		a := nasabahRepo.NewNasabahRepository(db, logger.L)

		count, err := a.TotalRekomendasiNonBRI(context.TODO(), mockParamRequestListNasabah)
		assert.Error(t, err)
		assert.Equal(t, 0, count)
	})
}

func TestRekomendasiRepository_TotalDitindaklanjutiBRI(t *testing.T) {
	err := helper.NewMockEnv()
	db, mock, err := helper.NewMockDB()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}

	mockParamRequestListNasabah := &models.ParamRequestListNasabah{}
	mockParamRequestListNasabah = mockParamRequestListNasabah.MappingExampleData()
	pnPengelola := "00999950"
	// role := "RM"

	//resultMock
	mockTotalDitindaklanjutiBRI := 1

	//rowAndColumnMock
	table := &models.NasabahTable{}
	table = table.MappingTable()

	t.Run("error-regex-query", func(t *testing.T) {
		rows := sqlmock.NewRows([]string{"COUNT"}).
			AddRow(mockTotalDitindaklanjutiBRI)
		query := "SELECT `UPDATED_DATE` FROM `bribrain_link5_status_tindaklanjut` WHERE pn_rm = ? ORDER BY UPDATED_DATE desc"
		queryRegex := regexp.QuoteMeta(query)
		mock.ExpectQuery(queryRegex).
			WithArgs(pnPengelola).
			WillReturnRows(rows)
		a := nasabahRepo.NewNasabahRepository(db, logger.L)

		count, err := a.TotalDitindaklanjutiBRI(context.TODO(), mockParamRequestListNasabah)
		assert.Error(t, err)
		assert.Equal(t, 0, count)
	})
}

func TestRekomendasiRepository_TotalDitindaklanjutiNonBRI(t *testing.T) {
	err := helper.NewMockEnv()
	db, mock, err := helper.NewMockDB()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}

	mockParamRequestListNasabah := &models.ParamRequestListNasabah{}
	mockParamRequestListNasabah = mockParamRequestListNasabah.MappingExampleData()
	pnPengelola := "00999950"
	// role := "RM"

	//resultMock
	mockTotalDitindaklanjutiNonBRI := 1

	//rowAndColumnMock
	table := &models.NasabahTable{}
	table = table.MappingTable()

	t.Run("error-regex-query", func(t *testing.T) {
		rows := sqlmock.NewRows([]string{"COUNT"}).
			AddRow(mockTotalDitindaklanjutiNonBRI)
		query := "SELECT `UPDATED_DATE` FROM `bribrain_link5_status_tindaklanjut` WHERE pn_rm = ? ORDER BY UPDATED_DATE desc"
		queryRegex := regexp.QuoteMeta(query)
		mock.ExpectQuery(queryRegex).
			WithArgs(pnPengelola).
			WillReturnRows(rows)
		a := nasabahRepo.NewNasabahRepository(db, logger.L)

		count, err := a.TotalDitindaklanjutiNonBRI(context.TODO(), mockParamRequestListNasabah)
		assert.Error(t, err)
		assert.Equal(t, 0, count)
	})
}

func TestRekomendasiRepository_GetDetail(t *testing.T) {
	err := helper.NewMockEnv()
	db, mock, err := helper.NewMockDB()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	//requestMock
	request := &models.RequestDetailNasabahEncrypted{}
	request = request.MappingExampleData()

	//resultMock
	mockDetailQueryStruct := &models.GetDetailQueryStruct{}
	mockDetailQueryStruct = mockDetailQueryStruct.MappingExampleData()
	pnPengelola := "00999950"
	// role := "RM"

	//rowAndColumnMock
	table := &models.NasabahTable{}
	table = table.MappingTable()
	//rowAndColumnMock
	// row, fields := helper.GetValueAndColumnStructToDriverValue(mockDetailQueryStruct)

	// querySelect := table.BribrainLink5DashboardDetailInformasiNasabahBRI.UpdatedDate + ` AS UPDATED_DATE`

	// queryJoin := `INNER join ` + table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table + ` on ` + table.BribrainLink5DashboardDetailInformasiNasabahBRI.IdRekomendasi + ` = ` + table.BribrainLink5DashboardDetailInformasiNasabahBRI.Id

	// queryWhere := table.BribrainLink5DashboardDetailInformasiNasabahBRI.PnRm + " =?"

	// queryOrder := table.BribrainLink5DashboardDetailInformasiNasabahBRI.UpdatedDate + " desc"
	// t.Run("success", func(t *testing.T) {
	// 	rows := sqlmock.NewRows(fields).
	// 		AddRow(row...)
	// 	// query := "SELECT " + querySelect + " FROM `" + table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table + "` " + " WHERE " + queryWhere + " ORDER BY " + queryOrder
	// 	query := `SELECT . AS CIF_NASABAH_REKAN,. AS NAMA_NASABAH_REKAN,. AS NAMA_NASABAH_UTAMA,. AS POSISI_TIER,. AS USIA,. AS NOMOR_KTP,. AS PN_RM,. AS JENIS_KELAMIN,. AS SEKTOR_EKONOMI,. AS TIER_DIATASNYA,. AS RM_DIATASNYA,. AS BRANCH,. AS RGDESC,. AS MBDESC,. AS SBDESC,. AS BRDESC,. AS PHONENUMBER,. AS ALAMAT_ID,. AS ALAMAT_DOMISILI,. AS ALAMAT_PINJAMAN,. AS USIA_REKENING_BULAN,. AS PRODUK_SIMPANAN,. AS SNAME_RM_SIMPANAN,. AS PRODUK_PINJAMAN,. AS SNAME_RM_PINJAMAN,. AS SEGMEN,. AS PHONENUMBER_RM_PINJAMAN,. AS JENIS_PEKERJAAN,. AS BIDANG_PEKERJAAN,. AS KOLEKTABILITAS,. AS TOTAL_PLAFON,. AS TOTAL_OUTSTANDING,. AS INET_BANKING,. AS SMS_BANKING,. AS TOTAL_CREDIT_BRI,. AS TOTAL_DEBIT_BRI,. AS TOTAL_KREDIT_NON_BRI,. AS TOTAL_DEBIT_NON_BRI,. AS TOTAL_KREDIT_DEBIT_NON_BRI,. AS NAMA_NASABAH_NON_BRI,. AS NAMA_BANK_LAWAN,. AS RATAS_SALDO_SIMPANAN_BRI,. AS TOTAL_CREDIT_KESELURUHAN,. AS TOTAL_DEBIT_KESELURUHAN,. AS CIF_NASABAH_TIER_1,. AS NAMA_NASABAH_TIER_1,. AS SNAME_RM_PINJAMAN_TIER_1,. AS UNIT_RM_TIER_1,. AS PHONENUMBER_RM_TIER_1,. AS CIF_NASABAH_TIER_2,. AS NAMA_NASABAH_TIER_2,. AS SNAME_RM_PINJAMAN_TIER_2,. AS UNIT_RM_TIER_2,. AS PHONENUMBER_TIER_2,. AS CIF_NASABAH_TIER_3,. AS NAMA_NASABAH_TIER_3,. AS SNAME_RM_PINJAMAN_TIER_3,. AS UNIT_RM_TIER_3,. AS PHONENUMBER_TIER_3,. AS CIF_NASABAH_TIER_4,. AS NAMA_NASABAH_TIER_4,. AS SNAME_RM_PINJAMAN_TIER_4,. AS UNIT_RM_TIER_4,. AS PHONENUMBER_TIER_4,. AS STATUS_REKENING,. AS AKUISISI_PINJAMAN,. AS LAMA_NASABAH_BULAN,. AS TANGGAL_LAHIR,. AS DS,. AS CREATED_DATE,. AS STATUS_TINDAKLANJUT,. AS TOPUP_SIMPANAN,. AS SUPLESI_PINJAMAN,. AS VOLUME_POTENSI_PINJAMAN,. AS VOLUME_POTENSI_SIMPANAN, case when .=0 then` + "Belum" + ` when .=1 then ` + "Sudah" + ` END AS STATUS_AKUISISI_SIMPANAN, case when .=0 AND .=0 then ` + "Belum" + ` when .=1 OR .=1 then ` + "Sudah" + ` END AS STATUS_AKUISISI_PINJAMAN FROM ` + ` WHERE .= ? LIMIT 1`
	// 	queryRegex := regexp.QuoteMeta(query)
	// 	mock.ExpectQuery(queryRegex).
	// 		WithArgs(pnPengelola).
	// 		WillReturnRows(rows)
	// 	a := nasabahRepo.NewNasabahRepository(db, logger.L)

	// 	detail, err := a.GetDetail(context.TODO(), request)
	// 	assert.NoError(t, err)
	// 	assert.NotNil(t, detail)
	// })

	t.Run("error-regex-query", func(t *testing.T) {
		// rows := sqlmock.NewRows(fields).
		// 	AddRow(row...)
		// query := "SELECT " + querySelect + " FROM `" + table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table + "` " + " WHERE " + queryWhere + " ORDER BY " + queryOrder
		query := `SELECT . AS CIF_NASABAH_REKAN,. AS NAMA_NASABAH_REKAN,. AS NAMA_NASABAH_UTAMA,. AS POSISI_TIER,. AS USIA,. AS NOMOR_KTP,. AS PN_RM,. AS JENIS_KELAMIN,. AS SEKTOR_EKONOMI,. AS TIER_DIATASNYA,. AS RM_DIATASNYA,. AS BRANCH,. AS RGDESC,. AS MBDESC,. AS SBDESC,. AS BRDESC,. AS PHONENUMBER,. AS ALAMAT_ID,. AS ALAMAT_DOMISILI,. AS ALAMAT_PINJAMAN,. AS USIA_REKENING_BULAN,. AS PRODUK_SIMPANAN,. AS SNAME_RM_SIMPANAN,. AS PRODUK_PINJAMAN,. AS SNAME_RM_PINJAMAN,. AS SEGMEN,. AS PHONENUMBER_RM_PINJAMAN,. AS JENIS_PEKERJAAN,. AS BIDANG_PEKERJAAN,. AS KOLEKTABILITAS,. AS TOTAL_PLAFON,. AS TOTAL_OUTSTANDING,. AS INET_BANKING,. AS SMS_BANKING,. AS TOTAL_CREDIT_BRI,. AS TOTAL_DEBIT_BRI,. AS TOTAL_KREDIT_NON_BRI,. AS TOTAL_DEBIT_NON_BRI,. AS TOTAL_KREDIT_DEBIT_NON_BRI,. AS NAMA_NASABAH_NON_BRI,. AS NAMA_BANK_LAWAN,. AS RATAS_SALDO_SIMPANAN_BRI,. AS TOTAL_CREDIT_KESELURUHAN,. AS TOTAL_DEBIT_KESELURUHAN,. AS CIF_NASABAH_TIER_1,. AS NAMA_NASABAH_TIER_1,. AS SNAME_RM_PINJAMAN_TIER_1,. AS UNIT_RM_TIER_1,. AS PHONENUMBER_RM_TIER_1,. AS CIF_NASABAH_TIER_2,. AS NAMA_NASABAH_TIER_2,. AS SNAME_RM_PINJAMAN_TIER_2,. AS UNIT_RM_TIER_2,. AS PHONENUMBER_TIER_2,. AS CIF_NASABAH_TIER_3,. AS NAMA_NASABAH_TIER_3,. AS SNAME_RM_PINJAMAN_TIER_3,. AS UNIT_RM_TIER_3,. AS PHONENUMBER_TIER_3,. AS CIF_NASABAH_TIER_4,. AS NAMA_NASABAH_TIER_4,. AS SNAME_RM_PINJAMAN_TIER_4,. AS UNIT_RM_TIER_4,. AS PHONENUMBER_TIER_4,. AS STATUS_REKENING,. AS AKUISISI_PINJAMAN,. AS LAMA_NASABAH_BULAN,. AS TANGGAL_LAHIR,. AS DS,. AS CREATED_DATE,. AS STATUS_TINDAKLANJUT,. AS TOPUP_SIMPANAN,. AS SUPLESI_PINJAMAN,. AS VOLUME_POTENSI_PINJAMAN,. AS VOLUME_POTENSI_SIMPANAN, case when .=0 then` + "Belum" + ` when .=1 then ` + "Sudah" + ` END AS STATUS_AKUISISI_SIMPANAN, case when .=0 AND .=0 then ` + "Belum" + ` when .=1 OR .=1 then ` + "Sudah" + ` END AS STATUS_AKUISISI_PINJAMAN FROM ` + ` WHERE .= ? LIMIT 1`
		queryRegex := regexp.QuoteMeta(query)
		mock.ExpectQuery(queryRegex).
			WithArgs(pnPengelola)
			// WillReturnRows(rows)
		a := nasabahRepo.NewNasabahRepository(db, logger.L)

		detail, err := a.GetDetail(context.TODO(), request)
		assert.Error(t, err)
		assert.Nil(t, detail)
	})
}

func TestRekomendasiRepository_GetDetailDitindaklanjuti(t *testing.T) {
	err := helper.NewMockEnv()
	db, mock, err := helper.NewMockDB()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	//requestMock
	request := &models.RequestDetailNasabahEncrypted{}
	request = request.MappingExampleData()

	//resultMock
	mockDetailQueryStruct := &models.GetDetailQueryStruct{}
	mockDetailQueryStruct = mockDetailQueryStruct.MappingExampleData()
	pnPengelola := "00999950"
	// role := "RM"

	//rowAndColumnMock
	table := &models.NasabahTable{}
	table = table.MappingTable()
	//rowAndColumnMock
	// row, fields := helper.GetValueAndColumnStructToDriverValue(mockDetailQueryStruct)

	// querySelect := table.BribrainLink5DashboardDetailInformasiNasabahBRI.UpdatedDate + ` AS UPDATED_DATE`

	// queryJoin := `INNER join ` + table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table + ` on ` + table.BribrainLink5DashboardDetailInformasiNasabahBRI.IdRekomendasi + ` = ` + table.BribrainLink5DashboardDetailInformasiNasabahBRI.Id

	// queryWhere := table.BribrainLink5DashboardDetailInformasiNasabahBRI.PnRm + " =?"

	// queryOrder := table.BribrainLink5DashboardDetailInformasiNasabahBRI.UpdatedDate + " desc"

	t.Run("error-regex-query", func(t *testing.T) {
		// rows := sqlmock.NewRows(fields).
		// 	AddRow(row...)
		// query := "SELECT " + querySelect + " FROM `" + table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table + "` " + " WHERE " + queryWhere + " ORDER BY " + queryOrder
		query := `SELECT . AS CIF_NASABAH_REKAN,. AS NAMA_NASABAH_REKAN,. AS NAMA_NASABAH_UTAMA,. AS POSISI_TIER,. AS USIA,. AS NOMOR_KTP,. AS PN_RM,. AS JENIS_KELAMIN,. AS SEKTOR_EKONOMI,. AS TIER_DIATASNYA,. AS RM_DIATASNYA,. AS BRANCH,. AS RGDESC,. AS MBDESC,. AS SBDESC,. AS BRDESC,. AS PHONENUMBER,. AS ALAMAT_ID,. AS ALAMAT_DOMISILI,. AS ALAMAT_PINJAMAN,. AS USIA_REKENING_BULAN,. AS PRODUK_SIMPANAN,. AS SNAME_RM_SIMPANAN,. AS PRODUK_PINJAMAN,. AS SNAME_RM_PINJAMAN,. AS SEGMEN,. AS PHONENUMBER_RM_PINJAMAN,. AS JENIS_PEKERJAAN,. AS BIDANG_PEKERJAAN,. AS KOLEKTABILITAS,. AS TOTAL_PLAFON,. AS TOTAL_OUTSTANDING,. AS INET_BANKING,. AS SMS_BANKING,. AS TOTAL_CREDIT_BRI,. AS TOTAL_DEBIT_BRI,. AS TOTAL_KREDIT_NON_BRI,. AS TOTAL_DEBIT_NON_BRI,. AS TOTAL_KREDIT_DEBIT_NON_BRI,. AS NAMA_NASABAH_NON_BRI,. AS NAMA_BANK_LAWAN,. AS RATAS_SALDO_SIMPANAN_BRI,. AS TOTAL_CREDIT_KESELURUHAN,. AS TOTAL_DEBIT_KESELURUHAN,. AS CIF_NASABAH_TIER_1,. AS NAMA_NASABAH_TIER_1,. AS SNAME_RM_PINJAMAN_TIER_1,. AS UNIT_RM_TIER_1,. AS PHONENUMBER_RM_TIER_1,. AS CIF_NASABAH_TIER_2,. AS NAMA_NASABAH_TIER_2,. AS SNAME_RM_PINJAMAN_TIER_2,. AS UNIT_RM_TIER_2,. AS PHONENUMBER_TIER_2,. AS CIF_NASABAH_TIER_3,. AS NAMA_NASABAH_TIER_3,. AS SNAME_RM_PINJAMAN_TIER_3,. AS UNIT_RM_TIER_3,. AS PHONENUMBER_TIER_3,. AS CIF_NASABAH_TIER_4,. AS NAMA_NASABAH_TIER_4,. AS SNAME_RM_PINJAMAN_TIER_4,. AS UNIT_RM_TIER_4,. AS PHONENUMBER_TIER_4,. AS STATUS_REKENING,. AS AKUISISI_PINJAMAN,. AS LAMA_NASABAH_BULAN,. AS TANGGAL_LAHIR,. AS DS,. AS CREATED_DATE,. AS STATUS_TINDAKLANJUT,. AS TOPUP_SIMPANAN,. AS SUPLESI_PINJAMAN,. AS VOLUME_POTENSI_PINJAMAN,. AS VOLUME_POTENSI_SIMPANAN, case when .=0 then` + "Belum" + ` when .=1 then ` + "Sudah" + ` END AS STATUS_AKUISISI_SIMPANAN, case when .=0 AND .=0 then ` + "Belum" + ` when .=1 OR .=1 then ` + "Sudah" + ` END AS STATUS_AKUISISI_PINJAMAN FROM ` + ` WHERE .= ? LIMIT 1`
		queryRegex := regexp.QuoteMeta(query)
		mock.ExpectQuery(queryRegex).
			WithArgs(pnPengelola)
			// WillReturnRows(rows)
		a := nasabahRepo.NewNasabahRepository(db, logger.L)

		detail, err := a.GetDetailDitindaklanjuti(context.TODO(), request)
		assert.Error(t, err)
		assert.Nil(t, detail)
	})
}

func TestRekomendasiRepository_GetListRekomendasi(t *testing.T) {
	err := helper.NewMockEnv()
	db, mock, err := helper.NewMockDB()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	//requestMock
	request := &models.ParamRequestListNasabah{}
	request = request.MappingExampleData()

	//resultMock
	mockResponseListRekomendasi := models.GetListQueryStruct{}
	mockResponseListRekomendasi = mockResponseListRekomendasi.MappingExampleData()
	mockResponseListRekomendasiArray := []models.GetListQueryStruct{}
	mockResponseListRekomendasiArray = append(mockResponseListRekomendasiArray, mockResponseListRekomendasi)
	pnPengelola := "00999950"
	// role := "RM"

	//rowAndColumnMock
	table := &models.NasabahTable{}
	table = table.MappingTable()
	//rowAndColumnMock
	// row, fields := helper.GetValueAndColumnStructToDriverValue(mockResponseListRekomendasiArray)

	// querySelect := table.BribrainLink5DashboardDetailInformasiNasabahBRI.UpdatedDate + ` AS UPDATED_DATE`

	// queryJoin := `INNER join ` + table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table + ` on ` + table.BribrainLink5DashboardDetailInformasiNasabahBRI.IdRekomendasi + ` = ` + table.BribrainLink5DashboardDetailInformasiNasabahBRI.Id

	// queryWhere := table.BribrainLink5DashboardDetailInformasiNasabahBRI.PnRm + " =?"

	// queryOrder := table.BribrainLink5DashboardDetailInformasiNasabahBRI.UpdatedDate + " desc"

	t.Run("error-regex-query", func(t *testing.T) {
		// rows := sqlmock.NewRows(fields).
		// 	AddRow(row...)
		// query := "SELECT " + querySelect + " FROM `" + table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table + "` " + " WHERE " + queryWhere + " ORDER BY " + queryOrder
		query := `SELECT . AS CIF_NASABAH_REKAN,. AS NAMA_NASABAH_REKAN,. AS NAMA_NASABAH_UTAMA,. AS POSISI_TIER,. AS USIA,. AS NOMOR_KTP,. AS PN_RM,. AS JENIS_KELAMIN,. AS SEKTOR_EKONOMI,. AS TIER_DIATASNYA,. AS RM_DIATASNYA,. AS BRANCH,. AS RGDESC,. AS MBDESC,. AS SBDESC,. AS BRDESC,. AS PHONENUMBER,. AS ALAMAT_ID,. AS ALAMAT_DOMISILI,. AS ALAMAT_PINJAMAN,. AS USIA_REKENING_BULAN,. AS PRODUK_SIMPANAN,. AS SNAME_RM_SIMPANAN,. AS PRODUK_PINJAMAN,. AS SNAME_RM_PINJAMAN,. AS SEGMEN,. AS PHONENUMBER_RM_PINJAMAN,. AS JENIS_PEKERJAAN,. AS BIDANG_PEKERJAAN,. AS KOLEKTABILITAS,. AS TOTAL_PLAFON,. AS TOTAL_OUTSTANDING,. AS INET_BANKING,. AS SMS_BANKING,. AS TOTAL_CREDIT_BRI,. AS TOTAL_DEBIT_BRI,. AS TOTAL_KREDIT_NON_BRI,. AS TOTAL_DEBIT_NON_BRI,. AS TOTAL_KREDIT_DEBIT_NON_BRI,. AS NAMA_NASABAH_NON_BRI,. AS NAMA_BANK_LAWAN,. AS RATAS_SALDO_SIMPANAN_BRI,. AS TOTAL_CREDIT_KESELURUHAN,. AS TOTAL_DEBIT_KESELURUHAN,. AS CIF_NASABAH_TIER_1,. AS NAMA_NASABAH_TIER_1,. AS SNAME_RM_PINJAMAN_TIER_1,. AS UNIT_RM_TIER_1,. AS PHONENUMBER_RM_TIER_1,. AS CIF_NASABAH_TIER_2,. AS NAMA_NASABAH_TIER_2,. AS SNAME_RM_PINJAMAN_TIER_2,. AS UNIT_RM_TIER_2,. AS PHONENUMBER_TIER_2,. AS CIF_NASABAH_TIER_3,. AS NAMA_NASABAH_TIER_3,. AS SNAME_RM_PINJAMAN_TIER_3,. AS UNIT_RM_TIER_3,. AS PHONENUMBER_TIER_3,. AS CIF_NASABAH_TIER_4,. AS NAMA_NASABAH_TIER_4,. AS SNAME_RM_PINJAMAN_TIER_4,. AS UNIT_RM_TIER_4,. AS PHONENUMBER_TIER_4,. AS STATUS_REKENING,. AS AKUISISI_PINJAMAN,. AS LAMA_NASABAH_BULAN,. AS TANGGAL_LAHIR,. AS DS,. AS CREATED_DATE,. AS STATUS_TINDAKLANJUT,. AS TOPUP_SIMPANAN,. AS SUPLESI_PINJAMAN,. AS VOLUME_POTENSI_PINJAMAN,. AS VOLUME_POTENSI_SIMPANAN, case when .=0 then` + "Belum" + ` when .=1 then ` + "Sudah" + ` END AS STATUS_AKUISISI_SIMPANAN, case when .=0 AND .=0 then ` + "Belum" + ` when .=1 OR .=1 then ` + "Sudah" + ` END AS STATUS_AKUISISI_PINJAMAN FROM ` + ` WHERE .= ? LIMIT 1`
		queryRegex := regexp.QuoteMeta(query)
		mock.ExpectQuery(queryRegex).
			WithArgs(pnPengelola)
			// WillReturnRows(rows)
		a := nasabahRepo.NewNasabahRepository(db, logger.L)

		detail, err := a.GetListRekomendasi(context.TODO(), request)
		assert.Error(t, err)
		assert.Nil(t, detail)
	})
}

func TestRekomendasiRepository_GetListRekomendasiNonBRI(t *testing.T) {
	err := helper.NewMockEnv()
	db, mock, err := helper.NewMockDB()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	//requestMock
	request := &models.ParamRequestListNasabah{}
	request = request.MappingExampleData()

	//resultMock
	mockResponseListRekomendasi := models.GetListQueryStruct{}
	mockResponseListRekomendasi = mockResponseListRekomendasi.MappingExampleData()
	mockResponseListRekomendasiArray := []models.GetListQueryStruct{}
	mockResponseListRekomendasiArray = append(mockResponseListRekomendasiArray, mockResponseListRekomendasi)
	pnPengelola := "00999950"
	// role := "RM"

	//rowAndColumnMock
	table := &models.NasabahTable{}
	table = table.MappingTable()
	//rowAndColumnMock
	// row, fields := helper.GetValueAndColumnStructToDriverValue(mockResponseListRekomendasiArray)

	// querySelect := table.BribrainLink5DashboardDetailInformasiNasabahBRI.UpdatedDate + ` AS UPDATED_DATE`

	// queryJoin := `INNER join ` + table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table + ` on ` + table.BribrainLink5DashboardDetailInformasiNasabahBRI.IdRekomendasi + ` = ` + table.BribrainLink5DashboardDetailInformasiNasabahBRI.Id

	// queryWhere := table.BribrainLink5DashboardDetailInformasiNasabahBRI.PnRm + " =?"

	// queryOrder := table.BribrainLink5DashboardDetailInformasiNasabahBRI.UpdatedDate + " desc"

	t.Run("error-regex-query", func(t *testing.T) {
		// rows := sqlmock.NewRows(fields).
		// 	AddRow(row...)
		// query := "SELECT " + querySelect + " FROM `" + table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table + "` " + " WHERE " + queryWhere + " ORDER BY " + queryOrder
		query := `SELECT . AS CIF_NASABAH_REKAN,. AS NAMA_NASABAH_REKAN,. AS NAMA_NASABAH_UTAMA,. AS POSISI_TIER,. AS USIA,. AS NOMOR_KTP,. AS PN_RM,. AS JENIS_KELAMIN,. AS SEKTOR_EKONOMI,. AS TIER_DIATASNYA,. AS RM_DIATASNYA,. AS BRANCH,. AS RGDESC,. AS MBDESC,. AS SBDESC,. AS BRDESC,. AS PHONENUMBER,. AS ALAMAT_ID,. AS ALAMAT_DOMISILI,. AS ALAMAT_PINJAMAN,. AS USIA_REKENING_BULAN,. AS PRODUK_SIMPANAN,. AS SNAME_RM_SIMPANAN,. AS PRODUK_PINJAMAN,. AS SNAME_RM_PINJAMAN,. AS SEGMEN,. AS PHONENUMBER_RM_PINJAMAN,. AS JENIS_PEKERJAAN,. AS BIDANG_PEKERJAAN,. AS KOLEKTABILITAS,. AS TOTAL_PLAFON,. AS TOTAL_OUTSTANDING,. AS INET_BANKING,. AS SMS_BANKING,. AS TOTAL_CREDIT_BRI,. AS TOTAL_DEBIT_BRI,. AS TOTAL_KREDIT_NON_BRI,. AS TOTAL_DEBIT_NON_BRI,. AS TOTAL_KREDIT_DEBIT_NON_BRI,. AS NAMA_NASABAH_NON_BRI,. AS NAMA_BANK_LAWAN,. AS RATAS_SALDO_SIMPANAN_BRI,. AS TOTAL_CREDIT_KESELURUHAN,. AS TOTAL_DEBIT_KESELURUHAN,. AS CIF_NASABAH_TIER_1,. AS NAMA_NASABAH_TIER_1,. AS SNAME_RM_PINJAMAN_TIER_1,. AS UNIT_RM_TIER_1,. AS PHONENUMBER_RM_TIER_1,. AS CIF_NASABAH_TIER_2,. AS NAMA_NASABAH_TIER_2,. AS SNAME_RM_PINJAMAN_TIER_2,. AS UNIT_RM_TIER_2,. AS PHONENUMBER_TIER_2,. AS CIF_NASABAH_TIER_3,. AS NAMA_NASABAH_TIER_3,. AS SNAME_RM_PINJAMAN_TIER_3,. AS UNIT_RM_TIER_3,. AS PHONENUMBER_TIER_3,. AS CIF_NASABAH_TIER_4,. AS NAMA_NASABAH_TIER_4,. AS SNAME_RM_PINJAMAN_TIER_4,. AS UNIT_RM_TIER_4,. AS PHONENUMBER_TIER_4,. AS STATUS_REKENING,. AS AKUISISI_PINJAMAN,. AS LAMA_NASABAH_BULAN,. AS TANGGAL_LAHIR,. AS DS,. AS CREATED_DATE,. AS STATUS_TINDAKLANJUT,. AS TOPUP_SIMPANAN,. AS SUPLESI_PINJAMAN,. AS VOLUME_POTENSI_PINJAMAN,. AS VOLUME_POTENSI_SIMPANAN, case when .=0 then` + "Belum" + ` when .=1 then ` + "Sudah" + ` END AS STATUS_AKUISISI_SIMPANAN, case when .=0 AND .=0 then ` + "Belum" + ` when .=1 OR .=1 then ` + "Sudah" + ` END AS STATUS_AKUISISI_PINJAMAN FROM ` + ` WHERE .= ? LIMIT 1`
		queryRegex := regexp.QuoteMeta(query)
		mock.ExpectQuery(queryRegex).
			WithArgs(pnPengelola)
			// WillReturnRows(rows)
		a := nasabahRepo.NewNasabahRepository(db, logger.L)

		detail, err := a.GetListRekomendasiNonBRI(context.TODO(), request)
		assert.Error(t, err)
		assert.Nil(t, detail)
	})
}

func TestRekomendasiRepository_GetListDitindaklanjuti(t *testing.T) {
	err := helper.NewMockEnv()
	db, mock, err := helper.NewMockDB()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	//requestMock
	request := &models.ParamRequestListNasabah{}
	request = request.MappingExampleData()

	//resultMock
	mockResponseListRekomendasi := models.GetListQueryStruct{}
	mockResponseListRekomendasi = mockResponseListRekomendasi.MappingExampleData()
	mockResponseListRekomendasiArray := []models.GetListQueryStruct{}
	mockResponseListRekomendasiArray = append(mockResponseListRekomendasiArray, mockResponseListRekomendasi)
	pnPengelola := "00999950"
	// role := "RM"

	//rowAndColumnMock
	table := &models.NasabahTable{}
	table = table.MappingTable()
	//rowAndColumnMock
	// row, fields := helper.GetValueAndColumnStructToDriverValue(mockResponseListRekomendasiArray)

	// querySelect := table.BribrainLink5DashboardDetailInformasiNasabahBRI.UpdatedDate + ` AS UPDATED_DATE`

	// queryJoin := `INNER join ` + table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table + ` on ` + table.BribrainLink5DashboardDetailInformasiNasabahBRI.IdRekomendasi + ` = ` + table.BribrainLink5DashboardDetailInformasiNasabahBRI.Id

	// queryWhere := table.BribrainLink5DashboardDetailInformasiNasabahBRI.PnRm + " =?"

	// queryOrder := table.BribrainLink5DashboardDetailInformasiNasabahBRI.UpdatedDate + " desc"

	t.Run("error-regex-query", func(t *testing.T) {
		// rows := sqlmock.NewRows(fields).
		// 	AddRow(row...)
		// query := "SELECT " + querySelect + " FROM `" + table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table + "` " + " WHERE " + queryWhere + " ORDER BY " + queryOrder
		query := `SELECT . AS CIF_NASABAH_REKAN,. AS NAMA_NASABAH_REKAN,. AS NAMA_NASABAH_UTAMA,. AS POSISI_TIER,. AS USIA,. AS NOMOR_KTP,. AS PN_RM,. AS JENIS_KELAMIN,. AS SEKTOR_EKONOMI,. AS TIER_DIATASNYA,. AS RM_DIATASNYA,. AS BRANCH,. AS RGDESC,. AS MBDESC,. AS SBDESC,. AS BRDESC,. AS PHONENUMBER,. AS ALAMAT_ID,. AS ALAMAT_DOMISILI,. AS ALAMAT_PINJAMAN,. AS USIA_REKENING_BULAN,. AS PRODUK_SIMPANAN,. AS SNAME_RM_SIMPANAN,. AS PRODUK_PINJAMAN,. AS SNAME_RM_PINJAMAN,. AS SEGMEN,. AS PHONENUMBER_RM_PINJAMAN,. AS JENIS_PEKERJAAN,. AS BIDANG_PEKERJAAN,. AS KOLEKTABILITAS,. AS TOTAL_PLAFON,. AS TOTAL_OUTSTANDING,. AS INET_BANKING,. AS SMS_BANKING,. AS TOTAL_CREDIT_BRI,. AS TOTAL_DEBIT_BRI,. AS TOTAL_KREDIT_NON_BRI,. AS TOTAL_DEBIT_NON_BRI,. AS TOTAL_KREDIT_DEBIT_NON_BRI,. AS NAMA_NASABAH_NON_BRI,. AS NAMA_BANK_LAWAN,. AS RATAS_SALDO_SIMPANAN_BRI,. AS TOTAL_CREDIT_KESELURUHAN,. AS TOTAL_DEBIT_KESELURUHAN,. AS CIF_NASABAH_TIER_1,. AS NAMA_NASABAH_TIER_1,. AS SNAME_RM_PINJAMAN_TIER_1,. AS UNIT_RM_TIER_1,. AS PHONENUMBER_RM_TIER_1,. AS CIF_NASABAH_TIER_2,. AS NAMA_NASABAH_TIER_2,. AS SNAME_RM_PINJAMAN_TIER_2,. AS UNIT_RM_TIER_2,. AS PHONENUMBER_TIER_2,. AS CIF_NASABAH_TIER_3,. AS NAMA_NASABAH_TIER_3,. AS SNAME_RM_PINJAMAN_TIER_3,. AS UNIT_RM_TIER_3,. AS PHONENUMBER_TIER_3,. AS CIF_NASABAH_TIER_4,. AS NAMA_NASABAH_TIER_4,. AS SNAME_RM_PINJAMAN_TIER_4,. AS UNIT_RM_TIER_4,. AS PHONENUMBER_TIER_4,. AS STATUS_REKENING,. AS AKUISISI_PINJAMAN,. AS LAMA_NASABAH_BULAN,. AS TANGGAL_LAHIR,. AS DS,. AS CREATED_DATE,. AS STATUS_TINDAKLANJUT,. AS TOPUP_SIMPANAN,. AS SUPLESI_PINJAMAN,. AS VOLUME_POTENSI_PINJAMAN,. AS VOLUME_POTENSI_SIMPANAN, case when .=0 then` + "Belum" + ` when .=1 then ` + "Sudah" + ` END AS STATUS_AKUISISI_SIMPANAN, case when .=0 AND .=0 then ` + "Belum" + ` when .=1 OR .=1 then ` + "Sudah" + ` END AS STATUS_AKUISISI_PINJAMAN FROM ` + ` WHERE .= ? LIMIT 1`
		queryRegex := regexp.QuoteMeta(query)
		mock.ExpectQuery(queryRegex).
			WithArgs(pnPengelola)
			// WillReturnRows(rows)
		a := nasabahRepo.NewNasabahRepository(db, logger.L)

		detail, err := a.GetListDitindaklanjuti(context.TODO(), request)
		assert.Error(t, err)
		assert.Nil(t, detail)
	})
}
