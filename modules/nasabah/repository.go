package nasabah

import (
	"context"
	"ms-bribrain-link5/models"
	"time"
)

type Repository interface {
	GetDetail(ctx context.Context, param *models.RequestDetailNasabahEncrypted) (res *models.GetDetailQueryStruct, err error)
	GetDetailDitindaklanjuti(ctx context.Context, param *models.RequestDetailNasabahEncrypted) (res *models.GetDetailQueryStruct, err error)
	GetListRekomendasi(ctx context.Context, param *models.ParamRequestListNasabah) (res []models.GetListQueryStruct, err error)
	GetListRekomendasiNonBRI(ctx context.Context, param *models.ParamRequestListNasabah) (res []models.GetListQueryStruct, err error)
	CountRekomendasiBRI(ctx context.Context, param *models.ParamRequestListNasabah) (res int, err error)
	CountRekomendasiNonBRI(ctx context.Context, param *models.ParamRequestListNasabah) (res int, err error)
	CountDitindaklanjutiBRI(ctx context.Context, param *models.ParamRequestListNasabah) (res int, err error)
	CountDitindaklanjutiNonBRI(ctx context.Context, param *models.ParamRequestListNasabah) (res int, err error)
	TotalRekomendasiBRI(ctx context.Context, param *models.ParamRequestListNasabah) (res int, err error)
	TotalRekomendasiNonBRI(ctx context.Context, param *models.ParamRequestListNasabah) (res int, err error)
	TotalDitindaklanjutiBRI(ctx context.Context, param *models.ParamRequestListNasabah) (res int, err error)
	TotalDitindaklanjutiNonBRI(ctx context.Context, param *models.ParamRequestListNasabah) (res int, err error)
	GetListDitindaklanjuti(ctx context.Context, param *models.ParamRequestListNasabah) (res []models.GetListQueryStruct, err error)
	GetLastUpdateTabelBRI(ctx context.Context, pn string, role string) (res *time.Time, err error)
	GetLastUpdateTabelNonBRI(ctx context.Context, pn string, role string) (res *time.Time, err error)
	GetLastUpdateDitindaklanjuti(ctx context.Context, pn string, role string) (res *time.Time, err error)
}
