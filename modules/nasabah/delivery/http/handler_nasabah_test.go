package http_test

import (
	"bytes"
	"encoding/json"
	"ms-bribrain-link5/helper/logger"
	models2 "ms-bribrain-link5/helper/models"
	"ms-bribrain-link5/middleware"
	"ms-bribrain-link5/models"
	nasabahHttp "ms-bribrain-link5/modules/nasabah/delivery/http"
	nasabahHttpHandler "ms-bribrain-link5/modules/nasabah/delivery/http"
	_nasabahUsecaseMock "ms-bribrain-link5/modules/nasabah/mocks"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

var (
	l                            = logger.L
	DetailNasabah                = "/detail_nasabah"
	DetailNasabahDitindaklanjuti = "/detail_nasabah_ditindaklanjuti"
	ListRekomendasiBri           = "/rekomendasi_bri"
	ListRekomendasiNonBri        = "/ditindaklanjuti"
	ListDitindaklanjuti          = "/rekomendasi_non_bri"
	InfoTier                     = "/info_tier"
	jwtClaimsstring              = `{"access_level":"","branch":806,"brdesc":"UNIT GUNUNG PUTRI CIBUBUR","description_1":"Jakarta","description_2":"2","exp":1630401199,"expired_token":1630318399,"iat":1630314799,"iss":"BRIBRAIN","jenis_kelamin":"P","jti":"234f3706-7df8-4c18-88c1-7a80a90d23b1","mainbr":384,"mbdesc":"Kantor Cabang Cibubur","orgeh_tx":"BRI UNIT GUNUNG PUTRI","pernr":"00999950","region":"I","rgdesc":"Kantor Wilayah Jakarta 2","role":"RM","sname":"User RM BRIBrain","stell_tx":"RM"}`
)

func TestNasabahHandler_LoadHandler(t *testing.T) {
	mockNasabahUsecase := new(_nasabahUsecaseMock.Usecase)
	e := echo.New()
	middL := middleware.InitMiddleware()
	nasabahHttpHandler.NewNasabahHandler(e, middL, mockNasabahUsecase, l)

	mockNasabahUsecase.AssertExpectations(t)
}

func TestNasabahHandler_GetDetailNasabah(t *testing.T) {
	//requestMock
	request := &models.RequestDetailNasabahEncrypted{}
	request = request.MappingExampleData()
	requestBad := request.MappingExampleDataBadRequest()

	//resultMock
	mockResponseDetailNasabah := &models.ResponseDetailNasabah{}
	mockResponseDetailNasabah = mockResponseDetailNasabah.MappingExampleDataNil()

	t.Run("success", func(t *testing.T) {
		//usecaseMock
		mockNasabahUsecase := new(_nasabahUsecaseMock.Usecase)

		mockNasabahUsecase.On("GetDetail", mock.Anything, mock.AnythingOfType("*models.RequestDetailNasabahEncrypted")).
			Return(mockResponseDetailNasabah, models2.GeneralSuccess, nil)

		e := echo.New()

		j, err := json.Marshal(request)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, DetailNasabah, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(DetailNasabah)
		c.Request().ParseForm()
		handler := nasabahHttp.NasabahHandler{
			NasabahUsecase: mockNasabahUsecase,
			Log:            l,
		}
		err = handler.GetDetailNasabah(c)
		require.NoError(t, err)

		assert.Equal(t, http.StatusOK, rec.Code)
		mockNasabahUsecase.AssertExpectations(t)
	})

	t.Run("error-500", func(t *testing.T) {
		//usecaseMock
		mockNasabahUsecase := new(_nasabahUsecaseMock.Usecase)

		mockNasabahUsecase.On("GetDetail", mock.Anything, mock.AnythingOfType("*models.RequestDetailNasabahEncrypted")).
			Return(nil, models2.ErrInternalServerError.Error(), models2.ErrInternalServerError)

		e := echo.New()

		j, err := json.Marshal(request)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, DetailNasabah, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(DetailNasabah)
		c.Request().ParseForm()
		handler := nasabahHttp.NasabahHandler{
			NasabahUsecase: mockNasabahUsecase,
			Log:            l,
		}
		err = handler.GetDetailNasabah(c)

		assert.Equal(t, http.StatusInternalServerError, rec.Code)
		mockNasabahUsecase.AssertExpectations(t)
	})

	t.Run("error-400", func(t *testing.T) {
		//usecaseMock
		mockNasabahUsecase := new(_nasabahUsecaseMock.Usecase)

		// mockNasabahUsecase.On("GetDetail", mock.Anything, mock.AnythingOfType("*models.RequestDetailNasabahEncrypted")).
		// 	Return(nil, models2.ErrBadParamInput.Error(), models2.ErrBadParamInput)

		e := echo.New()

		j, err := json.Marshal(requestBad)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, DetailNasabah, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(DetailNasabah)
		c.Request().ParseForm()
		handler := nasabahHttp.NasabahHandler{
			NasabahUsecase: mockNasabahUsecase,
			Log:            l,
		}
		err = handler.GetDetailNasabah(c)

		assert.Equal(t, http.StatusBadRequest, rec.Code)
		mockNasabahUsecase.AssertExpectations(t)
	})

	t.Run("error-json-decode", func(t *testing.T) {
		//usecaseMock
		mockNasabahUsecase := new(_nasabahUsecaseMock.Usecase)

		// mockNasabahUsecase.On("GetDetail", mock.Anything, mock.AnythingOfType("*models.RequestDetailNasabahEncrypted")).
		// 	Return(nil, models2.ErrBadParamInput.Error(), models2.ErrBadParamInput)

		e := echo.New()

		// j, err := json.Marshal(request)
		// assert.NoError(t, err)
		var data []byte
		req, err := http.NewRequest(echo.POST, DetailNasabah, bytes.NewReader(data))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(DetailNasabah)
		c.Request().ParseForm()
		handler := nasabahHttp.NasabahHandler{
			NasabahUsecase: mockNasabahUsecase,
			Log:            l,
		}
		err = handler.GetDetailNasabah(c)

		assert.Equal(t, http.StatusBadRequest, rec.Code)
		mockNasabahUsecase.AssertExpectations(t)
	})
}

func TestNasabahHandler_GetDetailNasabahDitindaklanjuti(t *testing.T) {
	//requestMock
	request := &models.RequestDetailNasabahEncrypted{}
	request = request.MappingExampleData()
	requestBad := request.MappingExampleDataBadRequest()

	//resultMock
	mockResponseDetailNasabahDitindaklanjuti := &models.ResponseDetailNasabahDitindaklanjuti{}
	mockResponseDetailNasabahDitindaklanjuti = mockResponseDetailNasabahDitindaklanjuti.MappingExampleDataNil()

	t.Run("success", func(t *testing.T) {
		//usecaseMock
		mockNasabahUsecase := new(_nasabahUsecaseMock.Usecase)

		mockNasabahUsecase.On("GetDetailDitindaklanjuti", mock.Anything, mock.AnythingOfType("*models.RequestDetailNasabahEncrypted")).
			Return(mockResponseDetailNasabahDitindaklanjuti, models2.GeneralSuccess, nil)

		e := echo.New()

		j, err := json.Marshal(request)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, DetailNasabahDitindaklanjuti, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(DetailNasabahDitindaklanjuti)
		c.Request().ParseForm()
		handler := nasabahHttp.NasabahHandler{
			NasabahUsecase: mockNasabahUsecase,
			Log:            l,
		}
		err = handler.GetDetailNasabahDitindaklanjuti(c)
		require.NoError(t, err)

		assert.Equal(t, http.StatusOK, rec.Code)
		mockNasabahUsecase.AssertExpectations(t)
	})

	t.Run("error-500", func(t *testing.T) {
		//usecaseMock
		mockNasabahUsecase := new(_nasabahUsecaseMock.Usecase)

		mockNasabahUsecase.On("GetDetailDitindaklanjuti", mock.Anything, mock.AnythingOfType("*models.RequestDetailNasabahEncrypted")).
			Return(nil, models2.ErrInternalServerError.Error(), models2.ErrInternalServerError)

		e := echo.New()

		j, err := json.Marshal(request)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, DetailNasabahDitindaklanjuti, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(DetailNasabahDitindaklanjuti)
		c.Request().ParseForm()
		handler := nasabahHttp.NasabahHandler{
			NasabahUsecase: mockNasabahUsecase,
			Log:            l,
		}
		err = handler.GetDetailNasabahDitindaklanjuti(c)

		assert.Equal(t, http.StatusInternalServerError, rec.Code)
		mockNasabahUsecase.AssertExpectations(t)
	})

	t.Run("error-400", func(t *testing.T) {
		//usecaseMock
		mockNasabahUsecase := new(_nasabahUsecaseMock.Usecase)

		// mockNasabahUsecase.On("GetDetailDitindaklanjuti", mock.Anything, mock.AnythingOfType("*models.RequestDetailNasabahEncrypted")).
		// 	Return(nil, models2.ErrBadParamInput.Error(), models2.ErrBadParamInput)

		e := echo.New()

		j, err := json.Marshal(requestBad)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, DetailNasabahDitindaklanjuti, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(DetailNasabahDitindaklanjuti)
		c.Request().ParseForm()
		handler := nasabahHttp.NasabahHandler{
			NasabahUsecase: mockNasabahUsecase,
			Log:            l,
		}
		err = handler.GetDetailNasabahDitindaklanjuti(c)

		assert.Equal(t, http.StatusBadRequest, rec.Code)
		mockNasabahUsecase.AssertExpectations(t)
	})

	t.Run("error-json-decode", func(t *testing.T) {
		//usecaseMock
		mockNasabahUsecase := new(_nasabahUsecaseMock.Usecase)

		// mockNasabahUsecase.On("GetDetailDitindaklanjuti", mock.Anything, mock.AnythingOfType("*models.RequestDetailNasabahEncrypted")).
		// 	Return(nil, models2.ErrBadParamInput.Error(), models2.ErrBadParamInput)

		e := echo.New()

		// j, err := json.Marshal(request)
		// assert.NoError(t, err)
		var data []byte
		req, err := http.NewRequest(echo.POST, DetailNasabahDitindaklanjuti, bytes.NewReader(data))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(DetailNasabahDitindaklanjuti)
		c.Request().ParseForm()
		handler := nasabahHttp.NasabahHandler{
			NasabahUsecase: mockNasabahUsecase,
			Log:            l,
		}
		err = handler.GetDetailNasabahDitindaklanjuti(c)

		assert.Equal(t, http.StatusBadRequest, rec.Code)
		mockNasabahUsecase.AssertExpectations(t)
	})
}

func TestNasabahHandler_GetListRekomendasi(t *testing.T) {
	//requestMock
	request := &models.RequestListNasabahTop20{}
	request = request.MappingExampleData()
	requestBad := request.MappingExampleDataBadRequest()

	//resultMock
	mockResponseListRekomendasiNasabahPaginationDto := &models.ResponseListRekomendasiNasabahPaginationDto{}
	mockResponseListRekomendasiNasabahPaginationDto = mockResponseListRekomendasiNasabahPaginationDto.MappingExampleDataNil()

	t.Run("success", func(t *testing.T) {
		//usecaseMock
		mockNasabahUsecase := new(_nasabahUsecaseMock.Usecase)

		mockNasabahUsecase.On("GetListRekomendasi", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).
			Return(mockResponseListRekomendasiNasabahPaginationDto, models2.GeneralSuccess, nil)

		e := echo.New()

		j, err := json.Marshal(request)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, ListRekomendasiBri, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(ListRekomendasiBri)
		c.Request().ParseForm()
		handler := nasabahHttp.NasabahHandler{
			NasabahUsecase: mockNasabahUsecase,
			Log:            l,
		}
		err = handler.GetListRekomendasi(c)
		require.NoError(t, err)

		assert.Equal(t, http.StatusOK, rec.Code)
		mockNasabahUsecase.AssertExpectations(t)
	})

	t.Run("error-500", func(t *testing.T) {
		//usecaseMock
		mockNasabahUsecase := new(_nasabahUsecaseMock.Usecase)

		mockNasabahUsecase.On("GetListRekomendasi", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).
			Return(nil, models2.ErrInternalServerError.Error(), models2.ErrInternalServerError)

		e := echo.New()

		j, err := json.Marshal(request)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, ListRekomendasiBri, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(ListRekomendasiBri)
		c.Request().ParseForm()
		handler := nasabahHttp.NasabahHandler{
			NasabahUsecase: mockNasabahUsecase,
			Log:            l,
		}
		err = handler.GetListRekomendasi(c)

		assert.Equal(t, http.StatusInternalServerError, rec.Code)
		mockNasabahUsecase.AssertExpectations(t)
	})

	t.Run("error-400", func(t *testing.T) {
		//usecaseMock
		mockNasabahUsecase := new(_nasabahUsecaseMock.Usecase)

		// mockNasabahUsecase.On("GetListRekomendasi", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).
		// 	Return(nil, models2.ErrBadParamInput.Error(), models2.ErrBadParamInput)

		e := echo.New()

		j, err := json.Marshal(requestBad)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, ListRekomendasiBri, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(ListRekomendasiBri)
		c.Request().ParseForm()
		handler := nasabahHttp.NasabahHandler{
			NasabahUsecase: mockNasabahUsecase,
			Log:            l,
		}
		err = handler.GetListRekomendasi(c)

		assert.Equal(t, http.StatusBadRequest, rec.Code)
		mockNasabahUsecase.AssertExpectations(t)
	})

	t.Run("error-json-decode", func(t *testing.T) {
		//usecaseMock
		mockNasabahUsecase := new(_nasabahUsecaseMock.Usecase)

		// mockNasabahUsecase.On("GetListRekomendasi", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).
		// 	Return(nil, models2.ErrBadParamInput.Error(), models2.ErrBadParamInput)

		e := echo.New()

		// j, err := json.Marshal(request)
		// assert.NoError(t, err)
		var data []byte
		req, err := http.NewRequest(echo.POST, ListRekomendasiBri, bytes.NewReader(data))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(ListRekomendasiBri)
		c.Request().ParseForm()
		handler := nasabahHttp.NasabahHandler{
			NasabahUsecase: mockNasabahUsecase,
			Log:            l,
		}
		err = handler.GetListRekomendasi(c)

		assert.Equal(t, http.StatusBadRequest, rec.Code)
		mockNasabahUsecase.AssertExpectations(t)
	})
}

func TestNasabahHandler_GetListRekomendasiNonBRI(t *testing.T) {
	//requestMock
	request := &models.RequestListNasabahTop20{}
	request = request.MappingExampleData()
	requestBad := request.MappingExampleDataBadRequest()

	//resultMock
	mockResponseListRekomendasiNasabahNonBRIPaginationDto := &models.ResponseListRekomendasiNasabahNonBRIPaginationDto{}
	mockResponseListRekomendasiNasabahNonBRIPaginationDto = mockResponseListRekomendasiNasabahNonBRIPaginationDto.MappingExampleDataNil()

	t.Run("success", func(t *testing.T) {
		//usecaseMock
		mockNasabahUsecase := new(_nasabahUsecaseMock.Usecase)

		mockNasabahUsecase.On("GetListRekomendasiNonBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).
			Return(mockResponseListRekomendasiNasabahNonBRIPaginationDto, models2.GeneralSuccess, nil)

		e := echo.New()

		j, err := json.Marshal(request)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, ListRekomendasiNonBri, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.Request().Header.Set("Content-Type", "UNIT-TEST")
		c.Request().Header.Set("Authorization", "UNIT_TEST")
		c.SetPath(ListRekomendasiNonBri)
		os.Setenv("AUDIT_TRAIL_URL", "https://api-dev.bribrain.com")
		c.Request().ParseForm()
		handler := nasabahHttp.NasabahHandler{
			NasabahUsecase: mockNasabahUsecase,
			Log:            l,
		}
		err = handler.GetListRekomendasiNonBRI(c)
		require.NoError(t, err)

		assert.Equal(t, http.StatusOK, rec.Code)
		mockNasabahUsecase.AssertExpectations(t)
	})

	t.Run("error-500", func(t *testing.T) {
		//usecaseMock
		mockNasabahUsecase := new(_nasabahUsecaseMock.Usecase)

		mockNasabahUsecase.On("GetListRekomendasiNonBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).
			Return(nil, models2.ErrInternalServerError.Error(), models2.ErrInternalServerError)

		e := echo.New()

		j, err := json.Marshal(request)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, ListRekomendasiNonBri, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.Request().Header.Set("Content-Type", "UNIT-TEST")
		c.Request().Header.Set("Authorization", "UNIT_TEST")
		os.Setenv("AUDIT_TRAIL_URL", "https://api-dev.bribrain.com")
		c.SetPath(ListRekomendasiNonBri)
		c.Request().ParseForm()
		handler := nasabahHttp.NasabahHandler{
			NasabahUsecase: mockNasabahUsecase,
			Log:            l,
		}
		err = handler.GetListRekomendasiNonBRI(c)

		assert.Equal(t, http.StatusInternalServerError, rec.Code)
		mockNasabahUsecase.AssertExpectations(t)
	})

	t.Run("error-400", func(t *testing.T) {
		//usecaseMock
		mockNasabahUsecase := new(_nasabahUsecaseMock.Usecase)

		// mockNasabahUsecase.On("GetListRekomendasiNonBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).
		// 	Return(nil, models2.ErrBadParamInput.Error(), models2.ErrBadParamInput)

		e := echo.New()

		j, err := json.Marshal(requestBad)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, ListRekomendasiNonBri, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.Request().Header.Set("Content-Type", "UNIT-TEST")
		c.Request().Header.Set("Authorization", "UNIT_TEST")
		os.Setenv("AUDIT_TRAIL_URL", "https://api-dev.bribrain.com")
		c.SetPath(ListRekomendasiNonBri)
		c.Request().ParseForm()
		handler := nasabahHttp.NasabahHandler{
			NasabahUsecase: mockNasabahUsecase,
			Log:            l,
		}
		err = handler.GetListRekomendasiNonBRI(c)

		assert.Equal(t, http.StatusBadRequest, rec.Code)
		mockNasabahUsecase.AssertExpectations(t)
	})

	t.Run("error-json-decode", func(t *testing.T) {
		//usecaseMock
		mockNasabahUsecase := new(_nasabahUsecaseMock.Usecase)

		// mockNasabahUsecase.On("GetListRekomendasiNonBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).
		// 	Return(nil, models2.ErrBadParamInput.Error(), models2.ErrBadParamInput)

		e := echo.New()

		// j, err := json.Marshal(request)
		// assert.NoError(t, err)
		var data []byte
		req, err := http.NewRequest(echo.POST, ListRekomendasiNonBri, bytes.NewReader(data))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.Request().Header.Set("Content-Type", "UNIT-TEST")
		c.Request().Header.Set("Authorization", "UNIT_TEST")
		os.Setenv("AUDIT_TRAIL_URL", "https://api-dev.bribrain.com")
		c.SetPath(ListRekomendasiNonBri)
		c.Request().ParseForm()
		handler := nasabahHttp.NasabahHandler{
			NasabahUsecase: mockNasabahUsecase,
			Log:            l,
		}
		err = handler.GetListRekomendasiNonBRI(c)

		assert.Equal(t, http.StatusBadRequest, rec.Code)
		mockNasabahUsecase.AssertExpectations(t)
	})
}

func TestNasabahHandler_GetListDitindaklanjuti(t *testing.T) {
	//requestMock
	request := &models.RequestListNasabah{}
	request = request.MappingExampleData()
	requestBad := request.MappingExampleDataBadRequest()

	//resultMock
	mockResponseListDitindaklanjutiPaginationDto := &models.ResponseListDitindaklanjutiPaginationDto{}
	mockResponseListDitindaklanjutiPaginationDto = mockResponseListDitindaklanjutiPaginationDto.MappingExampleDataNil()

	t.Run("success", func(t *testing.T) {
		//usecaseMock
		mockNasabahUsecase := new(_nasabahUsecaseMock.Usecase)

		mockNasabahUsecase.On("GetListDitindaklanjuti", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).
			Return(mockResponseListDitindaklanjutiPaginationDto, models2.GeneralSuccess, nil)

		e := echo.New()

		j, err := json.Marshal(request)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, ListDitindaklanjuti, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(ListDitindaklanjuti)
		c.Request().ParseForm()
		handler := nasabahHttp.NasabahHandler{
			NasabahUsecase: mockNasabahUsecase,
			Log:            l,
		}
		err = handler.GetListDitindaklanjuti(c)
		require.NoError(t, err)

		assert.Equal(t, http.StatusOK, rec.Code)
		mockNasabahUsecase.AssertExpectations(t)
	})

	t.Run("error-500", func(t *testing.T) {
		//usecaseMock
		mockNasabahUsecase := new(_nasabahUsecaseMock.Usecase)

		mockNasabahUsecase.On("GetListDitindaklanjuti", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).
			Return(nil, models2.ErrInternalServerError.Error(), models2.ErrInternalServerError)

		e := echo.New()

		j, err := json.Marshal(request)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, ListDitindaklanjuti, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(ListDitindaklanjuti)
		c.Request().ParseForm()
		handler := nasabahHttp.NasabahHandler{
			NasabahUsecase: mockNasabahUsecase,
			Log:            l,
		}
		err = handler.GetListDitindaklanjuti(c)

		assert.Equal(t, http.StatusInternalServerError, rec.Code)
		mockNasabahUsecase.AssertExpectations(t)
	})

	t.Run("error-400", func(t *testing.T) {
		//usecaseMock
		mockNasabahUsecase := new(_nasabahUsecaseMock.Usecase)

		// mockNasabahUsecase.On("GetListDitindaklanjuti", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).
		// 	Return(nil, models2.ErrBadParamInput.Error(), models2.ErrBadParamInput)

		e := echo.New()

		j, err := json.Marshal(requestBad)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, ListDitindaklanjuti, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(ListDitindaklanjuti)
		c.Request().ParseForm()
		handler := nasabahHttp.NasabahHandler{
			NasabahUsecase: mockNasabahUsecase,
			Log:            l,
		}
		err = handler.GetListDitindaklanjuti(c)

		assert.Equal(t, http.StatusBadRequest, rec.Code)
		mockNasabahUsecase.AssertExpectations(t)
	})

	t.Run("error-json-decode", func(t *testing.T) {
		//usecaseMock
		mockNasabahUsecase := new(_nasabahUsecaseMock.Usecase)

		// mockNasabahUsecase.On("GetListDitindaklanjuti", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).
		// 	Return(nil, models2.ErrBadParamInput.Error(), models2.ErrBadParamInput)

		e := echo.New()

		// j, err := json.Marshal(request)
		// assert.NoError(t, err)
		var data []byte
		req, err := http.NewRequest(echo.POST, ListDitindaklanjuti, bytes.NewReader(data))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(ListDitindaklanjuti)
		c.Request().ParseForm()
		handler := nasabahHttp.NasabahHandler{
			NasabahUsecase: mockNasabahUsecase,
			Log:            l,
		}
		err = handler.GetListDitindaklanjuti(c)

		assert.Equal(t, http.StatusBadRequest, rec.Code)
		mockNasabahUsecase.AssertExpectations(t)
	})
}

func TestNasabahHandler_GetInfoTierNasabah(t *testing.T) {
	//requestMock
	request := &models.RequestDetailNasabahEncrypted{}
	request = request.MappingExampleData()
	requestBad := request.MappingExampleDataBadRequest()

	//resultMock
	mockResponseInfoTierNasabahV110Dto := &models.ResponseInfoTierNasabahV110Dto{}
	mockResponseInfoTierNasabahV110Dto = mockResponseInfoTierNasabahV110Dto.MappingExampleDataNil()

	t.Run("success", func(t *testing.T) {
		//usecaseMock
		mockNasabahUsecase := new(_nasabahUsecaseMock.Usecase)

		mockNasabahUsecase.On("GetInfoTierNasabahInRow", mock.Anything, mock.AnythingOfType("*models.RequestDetailNasabahEncrypted")).
			Return(mockResponseInfoTierNasabahV110Dto, models2.GeneralSuccess, nil)

		e := echo.New()

		j, err := json.Marshal(request)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, InfoTier, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(InfoTier)
		c.Request().ParseForm()
		handler := nasabahHttp.NasabahHandler{
			NasabahUsecase: mockNasabahUsecase,
			Log:            l,
		}
		err = handler.GetInfoTierNasabah(c)
		require.NoError(t, err)

		assert.Equal(t, http.StatusOK, rec.Code)
		mockNasabahUsecase.AssertExpectations(t)
	})

	t.Run("error-500", func(t *testing.T) {
		//usecaseMock
		mockNasabahUsecase := new(_nasabahUsecaseMock.Usecase)

		mockNasabahUsecase.On("GetInfoTierNasabahInRow", mock.Anything, mock.AnythingOfType("*models.RequestDetailNasabahEncrypted")).
			Return(nil, models2.ErrInternalServerError.Error(), models2.ErrInternalServerError)

		e := echo.New()

		j, err := json.Marshal(request)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, InfoTier, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(InfoTier)
		c.Request().ParseForm()
		handler := nasabahHttp.NasabahHandler{
			NasabahUsecase: mockNasabahUsecase,
			Log:            l,
		}
		err = handler.GetInfoTierNasabah(c)

		assert.Equal(t, http.StatusInternalServerError, rec.Code)
		mockNasabahUsecase.AssertExpectations(t)
	})

	t.Run("error-400", func(t *testing.T) {
		//usecaseMock
		mockNasabahUsecase := new(_nasabahUsecaseMock.Usecase)

		// mockNasabahUsecase.On("GetInfoTierNasabahInRow", mock.Anything, mock.AnythingOfType("*models.RequestDetailNasabahEncrypted")).
		// 	Return(nil, models2.ErrBadParamInput.Error(), models2.ErrBadParamInput)

		e := echo.New()

		j, err := json.Marshal(requestBad)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, InfoTier, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(InfoTier)
		c.Request().ParseForm()
		handler := nasabahHttp.NasabahHandler{
			NasabahUsecase: mockNasabahUsecase,
			Log:            l,
		}
		err = handler.GetInfoTierNasabah(c)

		assert.Equal(t, http.StatusBadRequest, rec.Code)
		mockNasabahUsecase.AssertExpectations(t)
	})

	t.Run("error-json-decode", func(t *testing.T) {
		//usecaseMock
		mockNasabahUsecase := new(_nasabahUsecaseMock.Usecase)

		// mockNasabahUsecase.On("GetInfoTierNasabahInRow", mock.Anything, mock.AnythingOfType("*models.RequestDetailNasabahEncrypted")).
		// 	Return(nil, models2.ErrBadParamInput.Error(), models2.ErrBadParamInput)

		e := echo.New()

		// j, err := json.Marshal(request)
		// assert.NoError(t, err)
		var data []byte
		req, err := http.NewRequest(echo.POST, InfoTier, bytes.NewReader(data))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(InfoTier)
		c.Request().ParseForm()
		handler := nasabahHttp.NasabahHandler{
			NasabahUsecase: mockNasabahUsecase,
			Log:            l,
		}
		err = handler.GetInfoTierNasabah(c)

		assert.Equal(t, http.StatusBadRequest, rec.Code)
		mockNasabahUsecase.AssertExpectations(t)
	})
}
