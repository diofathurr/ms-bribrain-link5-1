package http

import (
	"context"
	"encoding/json"
	"ms-bribrain-link5/helper"
	"ms-bribrain-link5/helper/logger"
	models2 "ms-bribrain-link5/helper/models"
	"ms-bribrain-link5/middleware"
	"ms-bribrain-link5/models"
	"ms-bribrain-link5/modules/nasabah"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
)

// ResponseError represent the response error struct
type ResponseError struct {
	Message string `json:"message"`
}

type NasabahHandler struct {
	NasabahUsecase nasabah.Usecase
	Log            logger.Logger
}

func NewNasabahHandler(e *echo.Echo, middL *middleware.GoMiddleware, us nasabah.Usecase, log logger.Logger) {
	handler := &NasabahHandler{
		Log:            log,
		NasabahUsecase: us,
	}

	e.POST("/detail_nasabah", handler.GetDetailNasabah, middL.Authentication)
	e.POST("/detail_nasabah_ditindaklanjuti", handler.GetDetailNasabahDitindaklanjuti, middL.Authentication)
	e.POST("/rekomendasi_bri", handler.GetListRekomendasi, middL.Authentication)
	e.POST("/rekomendasi_non_bri", handler.GetListRekomendasiNonBRI, middL.Authentication)
	e.POST("/ditindaklanjuti", handler.GetListDitindaklanjuti, middL.Authentication)
	e.POST("/info_tier", handler.GetInfoTierNasabah, middL.Authentication)

}

// func (a *NasabahHandler) handlerValidation(c echo.Context, log string, response *models2.Response, request interface{}) error {
// 	dec, err := helper.JsonDecode(c, request)
// 	if err != nil {
// 		response.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), err.Error(), err,c)
// 		return c.JSON(response.StatusCode, response)
// 	}
// 	request = dec.(*models.RequestDetailNasabahEncrypted)
// 	validate := validator.New()
// 	errvalidate := validate.Struct(request)
// 	if errvalidate != nil {
// 		errID := time.Now().Format(helper.DateTimeFormatDefault) + " " + helper.RandomString(20)
// 		a.Log.Error("["+errID+"]  "+log, errvalidate.Error())
// 		response.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), models2.ErrInvalidValue.Error(), models2.ErrInvalidValue.Error(),c)
// 		return c.JSON(response.StatusCode, response)
// 	}
// 	return c.JSON(response.StatusCode, response)
// }

func (a *NasabahHandler) jsonValidation(c echo.Context, response *models2.Response, err error) error {
	if err != nil {
		response.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), err.Error(), err, c)
		return c.JSON(response.StatusCode, response)

	}
	return nil
}

func (a *NasabahHandler) requestValidation(c echo.Context, log string, response *models2.Response, errvalidate error) error {
	if errvalidate != nil {
		errID := time.Now().Format(helper.DateTimeFormatDefault) + " " + helper.RandomString(20)
		a.Log.Error("["+errID+"]  "+log, errvalidate.Error())
		response.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), models2.ErrInvalidValue.Error(), models2.ErrInvalidValue.Error(), c)
		return c.JSON(response.StatusCode, response)
	}

	return c.JSON(response.StatusCode, response)
}

func (a *NasabahHandler) errorJsonValidation(c echo.Context, response *models2.Response, err error) {
	response.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), err.Error(), err, c)
	// return c.JSON(response.StatusCode, response)

}

func (a *NasabahHandler) errorRequestValidation(c echo.Context, log string, response *models2.Response, errvalidate error) {
	errID := time.Now().Format(helper.DateTimeFormatDefault) + " " + helper.RandomString(20)
	a.Log.Error("["+errID+"]  "+log, errvalidate.Error())
	response.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), models2.ErrInvalidValue.Error(), models2.ErrInvalidValue.Error(), c)
}

func (a *NasabahHandler) errorUseCaseFunction(c echo.Context, log string, message string, response *models2.Response, err error) {
	errID := time.Now().Format(helper.DateTimeFormatDefault) + " " + helper.RandomString(20)
	a.Log.Error("["+errID+"]  "+log, err.Error())
	response.MappingResponseError(helper.GetStatusCode(err), message, errID, c)
}

// func (a *NasabahHandler) resultResponse(c echo.Context, log string, response *models2.Response, result interface{}, message string, err error) error {
// 	if err != nil {
// 		errID := time.Now().Format(helper.DateTimeFormatDefault) + " " + helper.RandomString(20)
// 		a.Log.Error("["+errID+"]  "+log, err.Error())
// 		response.MappingResponseError(helper.GetStatusCode(err), message, errID)
// 		return c.JSON(response.StatusCode, response)
// 	}

// 	// a.Log.Info(log, helper.JsonString(result))

// 	// response.MappingResponseSuccess(message, result,c)
// 	// return c.JSON(response.StatusCode, response)
// 	return c.JSON(response.StatusCode, response)
// }

// GetDetailNasabah godoc
// @Summary Get Detail Nasabah
// @Description get detail nasabah by cifno
// @Tags DetailNasabah
// @ID get-detail-nasabah
// @Accept  application-bribrain/json
// @Produce  json
// @Param Content-Type header string true "Content-Type" default(application-bribrain/json)
// @Param Authorization header string true "token" default(QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUp3WlhKdWNpSTZJakF3T1RrNU9UazVJaXdpYzI1aGJXVWlPaUpWYzJWeUlFMWhiblJ5YVNCQ1VrbENjbUZwYmlJc0luSnZiR1VpT2lKTlFVNVVVa2tpTENKa1pYTmpjbWx3ZEdsdmJsOHhJam9pU21GcllYSjBZU0lzSW1SbGMyTnlhWEIwYVc5dVh6SWlPaUl5SWl3aWIzSm5aV2hmZEhnaU9pSlZUa2xVSUUxQlRWQkJUa2NnVUZKQlVFRlVRVTRpTENKemRHVnNiRjkwZUNJNklrcFZUa2xQVWlCQlUxTlBRMGxCVkVVZ1RVRk9WRkpKSWl3aWNtZGtaWE5qSWpvaVMyRnVkRzl5SUZkcGJHRjVZV2dnUzJGdWRHOXlJRmRwYkdGNVlXZ2dTbUZyWVhKMFlTQXlJaXdpYldKa1pYTmpJam9pUzJGdWRHOXlJRU5oWW1GdVp5QkxZVzUwYjNJZ1EyRmlZVzVuSUVwaGEyRnlkR0VnVUdGellYSWdUV2x1WjJkMUlpd2lZbkprWlhOaklqb2lWVTVKVkNCQ1JVNUVRU0JTUVZsQklGQkJVMEZTSUUxSlRrZEhWU0lzSW5KbFoybHZiaUk2SWtraUxDSnRZV2x1WW5JaU9qTXpPU3dpWW5KaGJtTm9Jam80TXpRc0ltcGxibWx6WDJ0bGJHRnRhVzRpT2lKUUlpd2lZV05qWlhOelgyeGxkbVZzSWpvaVRsVk1UQ0lzSW1WNGNHbHlaV1JmZEc5clpXNGlPakUyTWprek9ERTBNellzSW1WNGNDSTZNVFl5T1RRMk5ESXpOaXdpYW5ScElqb2lOakF6TVRrME1HSXRNelF3WmkwMFl6QTFMV0UyTm1JdE9EQXdaVEZpWkRCbE56WmpJaXdpYVdGMElqb3hOakk1TXpjM09ETTJMQ0pwYzNNaU9pSkNVa2xDVWtGSlRpSjkuam5BQjUyZFNzTk9jRC1HLWpYeDdKZFhtZHpLeFhTVm9EVk5Kbmh4aGtTZw==)
// @Param request body models.SwaggerGetDetailRequest true "Cif" default(ABC1264)
// @Success 200 {object} models.SwaggerGetDetailRekomendasiSuccess
// @Failure 401 {object} models.SwaggerErrorExpiredTokenUnAuthorizeAndTokenUnAuthorize
// @Failure 403 {object} models.SwaggerErrorTokenForbiddenPermission
// @Failure 405 {object} models.SwaggerErrorMethodNotAllowed
// @Failure 500 {object} models.SwaggerErrorInternalServerError
// @Failure 400 {object} models.SwaggerErrorBadParamInput
// @Router /detail_nasabah [post]
func (a *NasabahHandler) GetDetailNasabah(c echo.Context) error {
	log := "nasabah.delivery.http.NasabahHandler.GetDetailNasabah: %s"
	response := new(models2.Response)
	request := new(models.RequestDetailNasabahEncrypted)

	userLogin := c.Get("JWT_CLAIMS").(*models.BRIBrainClaims)
	userAuditTrailDetailBri := models.BribrainAuditTrail{}
	payloadAuditTrailDetailBri := models.PayloadBribrainAuditTrail{}
	headersAuditTrailDetailBri := models.HeadersPayloadBribrainAuditTrail{}

	contentTypeHeaderDetailBri := c.Request().Header.Get(echo.HeaderContentType)
	authorizationHeaderDetailBri := c.Request().Header.Get(echo.HeaderAuthorization)
	urlpayloadDetailBri := c.Request().Host + c.Request().URL.Path
	headersAuditTrailDetailBri = headersAuditTrailDetailBri.MappingHeadersPayloadBribrainAuditTrail(contentTypeHeaderDetailBri, authorizationHeaderDetailBri)

	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	dec, err := helper.JsonDecode(c, request)
	if err != nil {
		a.errorJsonValidation(c, response, err)
		return c.JSON(response.StatusCode, response)
	}

	request = dec.(*models.RequestDetailNasabahEncrypted)
	validate := validator.New()

	reqbodyDetailBri, err := json.Marshal(request)
	if err != nil {
		a.Log.Error("nasabah.delivery.http.NasabahHandler.GetDetailNasabah.userAuditTrailDetailBri: %s", err.Error())
		// return nil, nil, err
	}

	errvalidate := validate.Struct(request)
	if errvalidate != nil {
		a.errorRequestValidation(c, log, response, errvalidate)

		remarkAuditTrailDetailBri := helper.IntToString(response.StatusCode) + " " + errvalidate.Error()
		payloadAuditTrailDetailBri = payloadAuditTrailDetailBri.MappingPayloadBribrainAuditTrail(urlpayloadDetailBri, headersAuditTrailDetailBri, string(reqbodyDetailBri), helper.JsonString(response), response.StatusCode)
		userAuditTrailDetailBri = userAuditTrailDetailBri.MappingBribrainAuditTrail("Get Detail Nasabah BRI", remarkAuditTrailDetailBri, userLogin.Pernr, userLogin.Role, helper.JsonString(payloadAuditTrailDetailBri))
		_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailDetailBri)
		if errAuditTrail != nil {
			a.Log.Error("nasabah.delivery.http.NasabahHandler.GetDetailNasabah.userAuditTrailDetailBri: %s", err.Error())

		}

		return c.JSON(response.StatusCode, response)
	}

	result, message, err := a.NasabahUsecase.GetDetail(ctx, request)
	if err != nil {
		a.errorUseCaseFunction(c, log, message, response, err)

		remarkAuditTrailDetailBri := helper.IntToString(response.StatusCode) + " " + errvalidate.Error()
		payloadAuditTrailDetailBri = payloadAuditTrailDetailBri.MappingPayloadBribrainAuditTrail(urlpayloadDetailBri, headersAuditTrailDetailBri, string(reqbodyDetailBri), helper.JsonString(response), response.StatusCode)
		userAuditTrailDetailBri = userAuditTrailDetailBri.MappingBribrainAuditTrail("Get Detail Nasabah BRI", remarkAuditTrailDetailBri, userLogin.Pernr, userLogin.Role, helper.JsonString(payloadAuditTrailDetailBri))
		_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailDetailBri)
		if errAuditTrail != nil {
			a.Log.Error("nasabah.delivery.http.NasabahHandler.GetDetailNasabah.userAuditTrailDetailBri: %s", err.Error())

		}

		return c.JSON(response.StatusCode, response)
	}

	a.Log.Info(log, helper.JsonString(result))

	response.MappingResponseSuccess(message, result, c)

	payloadAuditTrailDetailBri = payloadAuditTrailDetailBri.MappingPayloadBribrainAuditTrail(urlpayloadDetailBri, headersAuditTrailDetailBri, string(reqbodyDetailBri), helper.JsonString(response), response.StatusCode)
	userAuditTrailDetailBri = userAuditTrailDetailBri.MappingBribrainAuditTrail("Get Detail Nasabah BRI", "200 Success", userLogin.Pernr, userLogin.Role, helper.JsonString(payloadAuditTrailDetailBri))
	_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailDetailBri)
	if errAuditTrail != nil {
		a.Log.Error("nasabah.delivery.http.NasabahHandler.GetDetailNasabah.userAuditTrailDetailBri: %s", err.Error())

	}

	return c.JSON(response.StatusCode, response)
}

// GetDetailNasabahDitindaklanjuti godoc
// @Summary Get Detail Nasabah Ditindaklanjuti
// @Description get detail nasabah ditindaklanjuti by cifno
// @Tags DetailNasabah
// @ID get-detail-nasabah-ditindaklanjuti
// @Accept  application-bribrain/json
// @Produce  json
// @Param Content-Type header string true "Content-Type" default(application-bribrain/json)
// @Param Authorization header string true "token" default(QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUp3WlhKdWNpSTZJakF3T1RrNU9UazVJaXdpYzI1aGJXVWlPaUpWYzJWeUlFMWhiblJ5YVNCQ1VrbENjbUZwYmlJc0luSnZiR1VpT2lKTlFVNVVVa2tpTENKa1pYTmpjbWx3ZEdsdmJsOHhJam9pU21GcllYSjBZU0lzSW1SbGMyTnlhWEIwYVc5dVh6SWlPaUl5SWl3aWIzSm5aV2hmZEhnaU9pSlZUa2xVSUUxQlRWQkJUa2NnVUZKQlVFRlVRVTRpTENKemRHVnNiRjkwZUNJNklrcFZUa2xQVWlCQlUxTlBRMGxCVkVVZ1RVRk9WRkpKSWl3aWNtZGtaWE5qSWpvaVMyRnVkRzl5SUZkcGJHRjVZV2dnUzJGdWRHOXlJRmRwYkdGNVlXZ2dTbUZyWVhKMFlTQXlJaXdpYldKa1pYTmpJam9pUzJGdWRHOXlJRU5oWW1GdVp5QkxZVzUwYjNJZ1EyRmlZVzVuSUVwaGEyRnlkR0VnVUdGellYSWdUV2x1WjJkMUlpd2lZbkprWlhOaklqb2lWVTVKVkNCQ1JVNUVRU0JTUVZsQklGQkJVMEZTSUUxSlRrZEhWU0lzSW5KbFoybHZiaUk2SWtraUxDSnRZV2x1WW5JaU9qTXpPU3dpWW5KaGJtTm9Jam80TXpRc0ltcGxibWx6WDJ0bGJHRnRhVzRpT2lKUUlpd2lZV05qWlhOelgyeGxkbVZzSWpvaVRsVk1UQ0lzSW1WNGNHbHlaV1JmZEc5clpXNGlPakUyTWprek9ERTBNellzSW1WNGNDSTZNVFl5T1RRMk5ESXpOaXdpYW5ScElqb2lOakF6TVRrME1HSXRNelF3WmkwMFl6QTFMV0UyTm1JdE9EQXdaVEZpWkRCbE56WmpJaXdpYVdGMElqb3hOakk1TXpjM09ETTJMQ0pwYzNNaU9pSkNVa2xDVWtGSlRpSjkuam5BQjUyZFNzTk9jRC1HLWpYeDdKZFhtZHpLeFhTVm9EVk5Kbmh4aGtTZw==)
// @Param request body models.SwaggerGetDetailRequest true "Cif" default(ABC1264)
// @Failure 401 {object} models.SwaggerErrorExpiredTokenUnAuthorizeAndTokenUnAuthorize
// @Failure 403 {object} models.SwaggerErrorTokenForbiddenPermission
// @Failure 405 {object} models.SwaggerErrorMethodNotAllowed
// @Failure 500 {object} models.SwaggerErrorInternalServerError
// @Failure 400 {object} models.SwaggerErrorBadParamInput
// @Router /detail_nasabah_ditindaklanjuti [post]
func (a *NasabahHandler) GetDetailNasabahDitindaklanjuti(c echo.Context) error {
	log := "nasabah.delivery.http.NasabahHandler.GetDetailNasabahDitindaklanjuti: %s"
	response := new(models2.Response)
	request := new(models.RequestDetailNasabahEncrypted)

	userLogin := c.Get("JWT_CLAIMS").(*models.BRIBrainClaims)
	userAuditTrailDetailBriDitindaklanjuti := models.BribrainAuditTrail{}
	payloadAuditTrailDetailBriDitindaklanjuti := models.PayloadBribrainAuditTrail{}
	headersAuditTrailDetailBriDitindaklanjuti := models.HeadersPayloadBribrainAuditTrail{}

	contentTypeHeaderDetailBriDitindaklanjuti := c.Request().Header.Get(echo.HeaderContentType)
	authorizationHeaderDetailBriDitindaklanjuti := c.Request().Header.Get(echo.HeaderAuthorization)
	urlpayloadDetailBriDitindaklanjuti := c.Request().Host + c.Request().URL.Path
	headersAuditTrailDetailBriDitindaklanjuti = headersAuditTrailDetailBriDitindaklanjuti.MappingHeadersPayloadBribrainAuditTrail(contentTypeHeaderDetailBriDitindaklanjuti, authorizationHeaderDetailBriDitindaklanjuti)

	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	dec, err := helper.JsonDecode(c, request)

	if err != nil {
		a.errorJsonValidation(c, response, err)
	} else {
		request = dec.(*models.RequestDetailNasabahEncrypted)
		validate := validator.New()

		reqbodyDetailBriDitindaklanjuti, err := json.Marshal(request)
		if err != nil {
			a.Log.Error("nasabah.delivery.http.NasabahHandler.GetDetailNasabahDitindaklanjuti.userAuditTrailDetailNasabahDitindaklanjuti: %s", err.Error())
			// return nil, nil, err
		}

		errvalidate := validate.Struct(request)
		if errvalidate != nil {
			a.errorRequestValidation(c, log, response, errvalidate)

			remarkAuditTrailDetailBriDitindaklanjuti := helper.IntToString(response.StatusCode) + " " + errvalidate.Error()
			payloadAuditTrailDetailBriDitindaklanjuti = payloadAuditTrailDetailBriDitindaklanjuti.MappingPayloadBribrainAuditTrail(urlpayloadDetailBriDitindaklanjuti, headersAuditTrailDetailBriDitindaklanjuti, string(reqbodyDetailBriDitindaklanjuti), helper.JsonString(response), response.StatusCode)
			userAuditTrailDetailBriDitindaklanjuti = userAuditTrailDetailBriDitindaklanjuti.MappingBribrainAuditTrail("Get Detail Nasabah BRI Ditindaklanjuti", remarkAuditTrailDetailBriDitindaklanjuti, userLogin.Pernr, userLogin.Role, helper.JsonString(payloadAuditTrailDetailBriDitindaklanjuti))
			_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailDetailBriDitindaklanjuti)
			if errAuditTrail != nil {
				a.Log.Error("nasabah.delivery.http.NasabahHandler.GetDetailNasabah.userAuditTrailDetailBriDitindaklanjuti: %s", err.Error())

			}
		} else {
			result, message, err := a.NasabahUsecase.GetDetailDitindaklanjuti(ctx, request)
			if err != nil {
				a.errorUseCaseFunction(c, log, message, response, err)

				remarkAuditTrailDetailBriDitindaklanjuti := helper.IntToString(response.StatusCode) + " " + err.Error()
				payloadAuditTrailDetailBriDitindaklanjuti = payloadAuditTrailDetailBriDitindaklanjuti.MappingPayloadBribrainAuditTrail(urlpayloadDetailBriDitindaklanjuti, headersAuditTrailDetailBriDitindaklanjuti, string(reqbodyDetailBriDitindaklanjuti), helper.JsonString(response), response.StatusCode)
				userAuditTrailDetailBriDitindaklanjuti = userAuditTrailDetailBriDitindaklanjuti.MappingBribrainAuditTrail("Get Detail Nasabah BRI Ditindaklanjuti", remarkAuditTrailDetailBriDitindaklanjuti, userLogin.Pernr, userLogin.Role, helper.JsonString(payloadAuditTrailDetailBriDitindaklanjuti))
				_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailDetailBriDitindaklanjuti)
				if errAuditTrail != nil {
					a.Log.Error("nasabah.delivery.http.NasabahHandler.GetDetailNasabah.userAuditTrailDetailBriDitindaklanjuti: %s", err.Error())

				}
			} else {
				a.Log.Info(log, helper.JsonString(result))

				response.MappingResponseSuccess(message, result, c)

				payloadAuditTrailDetailBriDitindaklanjuti = payloadAuditTrailDetailBriDitindaklanjuti.MappingPayloadBribrainAuditTrail(urlpayloadDetailBriDitindaklanjuti, headersAuditTrailDetailBriDitindaklanjuti, string(reqbodyDetailBriDitindaklanjuti), helper.JsonString(response), response.StatusCode)
				userAuditTrailDetailBriDitindaklanjuti = userAuditTrailDetailBriDitindaklanjuti.MappingBribrainAuditTrail("Get Detail Nasabah BRI Ditindaklanjuti", "200 Success", userLogin.Pernr, userLogin.Role, helper.JsonString(payloadAuditTrailDetailBriDitindaklanjuti))
				_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailDetailBriDitindaklanjuti)
				if errAuditTrail != nil {
					a.Log.Error("nasabah.delivery.http.NasabahHandler.GetDetailNasabah.userAuditTrailDetailBriDitindaklanjuti: %s", err.Error())

				}
			}

		}
	}
	return c.JSON(response.StatusCode, response)
}

// GetListRekomendasi godoc
// @Summary Get List Rekomendasi
// @Description get list rekomendasi nasabah bri
// @Tags ListNasabah
// @ID get-list-rekomendasi-bri
// @Accept  application-bribrain/json
// @Produce  json
// @Param Content-Type header string true "Content-Type" default(application-bribrain/json)
// @Param Authorization header string true "token" default(QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUp3WlhKdWNpSTZJakF3T1RrNU9UazVJaXdpYzI1aGJXVWlPaUpWYzJWeUlFMWhiblJ5YVNCQ1VrbENjbUZwYmlJc0luSnZiR1VpT2lKTlFVNVVVa2tpTENKa1pYTmpjbWx3ZEdsdmJsOHhJam9pU21GcllYSjBZU0lzSW1SbGMyTnlhWEIwYVc5dVh6SWlPaUl5SWl3aWIzSm5aV2hmZEhnaU9pSlZUa2xVSUUxQlRWQkJUa2NnVUZKQlVFRlVRVTRpTENKemRHVnNiRjkwZUNJNklrcFZUa2xQVWlCQlUxTlBRMGxCVkVVZ1RVRk9WRkpKSWl3aWNtZGtaWE5qSWpvaVMyRnVkRzl5SUZkcGJHRjVZV2dnUzJGdWRHOXlJRmRwYkdGNVlXZ2dTbUZyWVhKMFlTQXlJaXdpYldKa1pYTmpJam9pUzJGdWRHOXlJRU5oWW1GdVp5QkxZVzUwYjNJZ1EyRmlZVzVuSUVwaGEyRnlkR0VnVUdGellYSWdUV2x1WjJkMUlpd2lZbkprWlhOaklqb2lWVTVKVkNCQ1JVNUVRU0JTUVZsQklGQkJVMEZTSUUxSlRrZEhWU0lzSW5KbFoybHZiaUk2SWtraUxDSnRZV2x1WW5JaU9qTXpPU3dpWW5KaGJtTm9Jam80TXpRc0ltcGxibWx6WDJ0bGJHRnRhVzRpT2lKUUlpd2lZV05qWlhOelgyeGxkbVZzSWpvaVRsVk1UQ0lzSW1WNGNHbHlaV1JmZEc5clpXNGlPakUyTWprek9ERTBNellzSW1WNGNDSTZNVFl5T1RRMk5ESXpOaXdpYW5ScElqb2lOakF6TVRrME1HSXRNelF3WmkwMFl6QTFMV0UyTm1JdE9EQXdaVEZpWkRCbE56WmpJaXdpYVdGMElqb3hOakk1TXpjM09ETTJMQ0pwYzNNaU9pSkNVa2xDVWtGSlRpSjkuam5BQjUyZFNzTk9jRC1HLWpYeDdKZFhtZHpLeFhTVm9EVk5Kbmh4aGtTZw==)
// @Param request body models.SwaggerGetListRequest true "body"
// @Failure 401 {object} models.SwaggerErrorExpiredTokenUnAuthorizeAndTokenUnAuthorize
// @Failure 403 {object} models.SwaggerErrorTokenForbiddenPermission
// @Failure 405 {object} models.SwaggerErrorMethodNotAllowed
// @Failure 500 {object} models.SwaggerErrorInternalServerError
// @Failure 400 {object} models.SwaggerErrorBadParamInput
// @Router /rekomendasi_bri [post]
func (a *NasabahHandler) GetListRekomendasi(c echo.Context) error {
	log := "nasabah.delivery.http.NasabahHandler.GetListRekomendasi: %s"
	response := new(models2.Response)
	request := new(models.RequestListNasabahTop20)
	userLogin := c.Get("JWT_CLAIMS").(*models.BRIBrainClaims)

	userAuditTrailListRekomendasiBri := models.BribrainAuditTrail{}
	payloadAuditTrailListRekomendasiBri := models.PayloadBribrainAuditTrail{}
	headersAuditTrailListRekomendasiBri := models.HeadersPayloadBribrainAuditTrail{}

	contentTypeHeaderListRekomendasiBri := c.Request().Header.Get(echo.HeaderContentType)
	authorizationHeaderListRekomendasiBri := c.Request().Header.Get(echo.HeaderAuthorization)
	urlpayloadListRekomendasiBri := c.Request().Host + c.Request().URL.Path
	headersAuditTrailListRekomendasiBri = headersAuditTrailListRekomendasiBri.MappingHeadersPayloadBribrainAuditTrail(contentTypeHeaderListRekomendasiBri, authorizationHeaderListRekomendasiBri)

	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	dec, err := helper.JsonDecode(c, request)
	if err != nil {
		a.errorJsonValidation(c, response, err)
	} else {
		request = dec.(*models.RequestListNasabahTop20)
		validate := validator.New()

		reqbodyListRekomendasiBri, err := json.Marshal(request)
		if err != nil {
			a.Log.Error("nasabah.delivery.http.NasabahHandler.GetListRekomendasi.userAuditTrail: %s", err.Error())
			// return nil, nil, err
		}

		errvalidate := validate.Struct(request)
		if errvalidate != nil {
			a.errorRequestValidation(c, log, response, errvalidate)

			remarkAuditTrailListRekomendasiBri := helper.IntToString(response.StatusCode) + " " + errvalidate.Error()
			payloadAuditTrailListRekomendasiBri = payloadAuditTrailListRekomendasiBri.MappingPayloadBribrainAuditTrail(urlpayloadListRekomendasiBri, headersAuditTrailListRekomendasiBri, string(reqbodyListRekomendasiBri), helper.JsonString(response), response.StatusCode)
			userAuditTrailListRekomendasiBri = userAuditTrailListRekomendasiBri.MappingBribrainAuditTrail("Get List Rekomendasi Nasabah BRI", remarkAuditTrailListRekomendasiBri, userLogin.Pernr, userLogin.Role, helper.JsonString(payloadAuditTrailListRekomendasiBri))
			_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailListRekomendasiBri)
			if errAuditTrail != nil {
				a.Log.Error("nasabah.delivery.http.NasabahHandler.GetListRekomendasi.userAuditTrail: %s", err.Error())

			}

		} else {
			limit, page, offset := helper.Pagination(request.Page, request.Limit)
			param := request.MappingToParamListRekomendasiTop20(userLogin.Pernr, userLogin.Role, limit, page, offset)
			result, message, err := a.NasabahUsecase.GetListRekomendasi(ctx, param)
			if err != nil {
				a.errorUseCaseFunction(c, log, message, response, err)

				remarkAuditTrailListRekomendasiBri := helper.IntToString(response.StatusCode) + " " + err.Error()
				payloadAuditTrailListRekomendasiBri = payloadAuditTrailListRekomendasiBri.MappingPayloadBribrainAuditTrail(urlpayloadListRekomendasiBri, headersAuditTrailListRekomendasiBri, string(reqbodyListRekomendasiBri), helper.JsonString(response), response.StatusCode)
				userAuditTrailListRekomendasiBri = userAuditTrailListRekomendasiBri.MappingBribrainAuditTrail("Get List Rekomendasi Nasabah BRI", remarkAuditTrailListRekomendasiBri, userLogin.Pernr, userLogin.Role, helper.JsonString(payloadAuditTrailListRekomendasiBri))
				_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailListRekomendasiBri)
				if errAuditTrail != nil {
					a.Log.Error("nasabah.delivery.http.NasabahHandler.GetListRekomendasi.userAuditTrail: %s", err.Error())

				}

			} else {
				a.Log.Info(log, helper.JsonString(result))

				response.MappingResponseSuccess(message, result, c)

				payloadAuditTrailListRekomendasiBri = payloadAuditTrailListRekomendasiBri.MappingPayloadBribrainAuditTrail(urlpayloadListRekomendasiBri, headersAuditTrailListRekomendasiBri, string(reqbodyListRekomendasiBri), helper.JsonString(response), response.StatusCode)
				userAuditTrailListRekomendasiBri = userAuditTrailListRekomendasiBri.MappingBribrainAuditTrail("Get List Rekomendasi Nasabah BRI", "200 Success", userLogin.Pernr, userLogin.Role, helper.JsonString(payloadAuditTrailListRekomendasiBri))
				_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailListRekomendasiBri)
				if errAuditTrail != nil {
					a.Log.Error("nasabah.delivery.http.NasabahHandler.GetListRekomendasi.userAuditTrailListRekomendasiBri: %s", err.Error())

				}

			}

		}
	}
	return c.JSON(response.StatusCode, response)

}

// GetListRekomendasiNonBRI godoc
// @Summary Get List Rekomendasi Non BRI
// @Description get list rekomendasi nasabah non bri
// @Tags ListNasabah
// @ID get-list-rekomendasi-non-bri
// @Accept  application-bribrain/json
// @Produce  json
// @Param Content-Type header string true "Content-Type" default(application-bribrain/json)
// @Param Authorization header string true "token" default(QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUp3WlhKdWNpSTZJakF3T1RrNU9UazVJaXdpYzI1aGJXVWlPaUpWYzJWeUlFMWhiblJ5YVNCQ1VrbENjbUZwYmlJc0luSnZiR1VpT2lKTlFVNVVVa2tpTENKa1pYTmpjbWx3ZEdsdmJsOHhJam9pU21GcllYSjBZU0lzSW1SbGMyTnlhWEIwYVc5dVh6SWlPaUl5SWl3aWIzSm5aV2hmZEhnaU9pSlZUa2xVSUUxQlRWQkJUa2NnVUZKQlVFRlVRVTRpTENKemRHVnNiRjkwZUNJNklrcFZUa2xQVWlCQlUxTlBRMGxCVkVVZ1RVRk9WRkpKSWl3aWNtZGtaWE5qSWpvaVMyRnVkRzl5SUZkcGJHRjVZV2dnUzJGdWRHOXlJRmRwYkdGNVlXZ2dTbUZyWVhKMFlTQXlJaXdpYldKa1pYTmpJam9pUzJGdWRHOXlJRU5oWW1GdVp5QkxZVzUwYjNJZ1EyRmlZVzVuSUVwaGEyRnlkR0VnVUdGellYSWdUV2x1WjJkMUlpd2lZbkprWlhOaklqb2lWVTVKVkNCQ1JVNUVRU0JTUVZsQklGQkJVMEZTSUUxSlRrZEhWU0lzSW5KbFoybHZiaUk2SWtraUxDSnRZV2x1WW5JaU9qTXpPU3dpWW5KaGJtTm9Jam80TXpRc0ltcGxibWx6WDJ0bGJHRnRhVzRpT2lKUUlpd2lZV05qWlhOelgyeGxkbVZzSWpvaVRsVk1UQ0lzSW1WNGNHbHlaV1JmZEc5clpXNGlPakUyTWprek9ERTBNellzSW1WNGNDSTZNVFl5T1RRMk5ESXpOaXdpYW5ScElqb2lOakF6TVRrME1HSXRNelF3WmkwMFl6QTFMV0UyTm1JdE9EQXdaVEZpWkRCbE56WmpJaXdpYVdGMElqb3hOakk1TXpjM09ETTJMQ0pwYzNNaU9pSkNVa2xDVWtGSlRpSjkuam5BQjUyZFNzTk9jRC1HLWpYeDdKZFhtZHpLeFhTVm9EVk5Kbmh4aGtTZw==)
// @Param request body models.SwaggerGetListRequest true "body"
// @Failure 401 {object} models.SwaggerErrorExpiredTokenUnAuthorizeAndTokenUnAuthorize
// @Failure 403 {object} models.SwaggerErrorTokenForbiddenPermission
// @Failure 405 {object} models.SwaggerErrorMethodNotAllowed
// @Failure 500 {object} models.SwaggerErrorInternalServerError
// @Failure 400 {object} models.SwaggerErrorBadParamInput
// @Router /rekomendasi_non_bri [post]
func (a *NasabahHandler) GetListRekomendasiNonBRI(c echo.Context) error {
	log := "nasabah.delivery.http.NasabahHandler.GetListRekomendasiNonBRI: %s"
	response := new(models2.Response)
	request := new(models.RequestListNasabahTop20)
	userLogin := c.Get("JWT_CLAIMS").(*models.BRIBrainClaims)
	userAuditTrailListNonBri := models.BribrainAuditTrail{}
	payloadAuditTrailListNonBri := models.PayloadBribrainAuditTrail{}
	headersAuditTrailListNonBri := models.HeadersPayloadBribrainAuditTrail{}

	contentTypeHeaderListNonBri := c.Request().Header.Get(echo.HeaderContentType)
	authorizationHeaderListNonBri := c.Request().Header.Get(echo.HeaderAuthorization)
	urlpayloadListNonBri := c.Request().Host + c.Request().URL.Path
	headersAuditTrailListNonBri = headersAuditTrailListNonBri.MappingHeadersPayloadBribrainAuditTrail(contentTypeHeaderListNonBri, authorizationHeaderListNonBri)

	// realIP := c.RealIP()
	// loggolang.Print("IP_ADDRESS ->", realIP)
	// a.Log.Info("IP ADDRESS ->", realIP)

	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	dec, err := helper.JsonDecode(c, request)
	if err != nil {
		a.errorJsonValidation(c, response, err)
		return c.JSON(response.StatusCode, response)
	}
	request = dec.(*models.RequestListNasabahTop20)
	validate := validator.New()

	reqbody, err := json.Marshal(request)
	if err != nil {
		a.Log.Error("nasabah.delivery.http.NasabahHandler.GetListRekomendasiNonBRI.userAuditTrailListNonBri: %s", err.Error())
		// return nil, nil, err
	}

	errvalidate := validate.Struct(request)
	if errvalidate != nil {

		a.errorRequestValidation(c, log, response, errvalidate)

		remarkAuditTrail := helper.IntToString(response.StatusCode) + " " + errvalidate.Error()
		payloadAuditTrailListNonBri = payloadAuditTrailListNonBri.MappingPayloadBribrainAuditTrail(urlpayloadListNonBri, headersAuditTrailListNonBri, string(reqbody), helper.JsonString(response), response.StatusCode)
		userAuditTrailListNonBri = userAuditTrailListNonBri.MappingBribrainAuditTrail("Get List Non BRI", remarkAuditTrail, userLogin.Pernr, userLogin.Role, helper.JsonString(payloadAuditTrailListNonBri))
		_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailListNonBri)
		if errAuditTrail != nil {
			a.Log.Error("nasabah.delivery.http.NasabahHandler.GetListRekomendasiNonBRI.userAuditTrailListNonBri: %s", err.Error())

		}

		return c.JSON(response.StatusCode, response)
	}

	limit, page, offset := helper.Pagination(request.Page, request.Limit)
	param := request.MappingToParamListRekomendasiTop20(userLogin.Pernr, userLogin.Role, limit, page, offset)

	result, message, err := a.NasabahUsecase.GetListRekomendasiNonBRI(ctx, param)
	if err != nil {
		a.errorUseCaseFunction(c, log, message, response, err)

		remarkAuditTrail := helper.IntToString(response.StatusCode) + " " + err.Error()
		payloadAuditTrailListNonBri = payloadAuditTrailListNonBri.MappingPayloadBribrainAuditTrail(urlpayloadListNonBri, headersAuditTrailListNonBri, string(reqbody), helper.JsonString(response), response.StatusCode)
		userAuditTrailListNonBri = userAuditTrailListNonBri.MappingBribrainAuditTrail("Get List Non BRI", remarkAuditTrail, userLogin.Pernr, userLogin.Role, helper.JsonString(payloadAuditTrailListNonBri))
		_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailListNonBri)
		if errAuditTrail != nil {
			a.Log.Error("nasabah.delivery.http.NasabahHandler.GetListRekomendasiNonBRI.userAuditTrailListNonBri: %s", err.Error())

		}

		return c.JSON(response.StatusCode, response)
	}

	a.Log.Info(log, helper.JsonString(result))

	response.MappingResponseSuccess(message, result, c)

	payloadAuditTrailListNonBri = payloadAuditTrailListNonBri.MappingPayloadBribrainAuditTrail(urlpayloadListNonBri, headersAuditTrailListNonBri, string(reqbody), helper.JsonString(response), response.StatusCode)
	userAuditTrailListNonBri = userAuditTrailListNonBri.MappingBribrainAuditTrail("Get List Non BRI", "200 Success", userLogin.Pernr, userLogin.Role, helper.JsonString(payloadAuditTrailListNonBri))
	_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailListNonBri)
	if errAuditTrail != nil {
		a.Log.Error("nasabah.delivery.http.NasabahHandler.GetListRekomendasiNonBRI.userAuditTrailListNonBri: %s", err.Error())
		// errId := time.Now().Format(helper.DateTimeFormatDefault) + " " + helper.RandomString(20)
		// a.Log.Error("["+errId+"]  "+log, err.Error())
		// response.MappingResponseError(helper.GetStatusCode(err), message, errId,c)
	}

	// respL, errL := http.Get(`https://ipapi.co/` + "125.161.221.139" + `/json/`)
	// if errL != nil {
	// 	loggolang.Fatal(errL)
	// }

	// defer respL.Body.Close()

	// body, errB := ioutil.ReadAll(respL.Body)
	// if errB != nil {
	// 	loggolang.Fatal(errL)
	// }

	// fmt.Println(string(body))

	return c.JSON(response.StatusCode, response)
}

// GetListDitindaklanjuti godoc
// @Summary Get List Ditindaklanjuti
// @Description get list nasabah ditindaklanjuti
// @Tags ListNasabah
// @ID get-list-nasabah-ditindaklanjuti
// @Accept  application-bribrain/json
// @Produce  json
// @Param Content-Type header string true "Content-Type" default(application-bribrain/json)
// @Param Authorization header string true "token" default(QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUp3WlhKdWNpSTZJakF3T1RrNU9UazVJaXdpYzI1aGJXVWlPaUpWYzJWeUlFMWhiblJ5YVNCQ1VrbENjbUZwYmlJc0luSnZiR1VpT2lKTlFVNVVVa2tpTENKa1pYTmpjbWx3ZEdsdmJsOHhJam9pU21GcllYSjBZU0lzSW1SbGMyTnlhWEIwYVc5dVh6SWlPaUl5SWl3aWIzSm5aV2hmZEhnaU9pSlZUa2xVSUUxQlRWQkJUa2NnVUZKQlVFRlVRVTRpTENKemRHVnNiRjkwZUNJNklrcFZUa2xQVWlCQlUxTlBRMGxCVkVVZ1RVRk9WRkpKSWl3aWNtZGtaWE5qSWpvaVMyRnVkRzl5SUZkcGJHRjVZV2dnUzJGdWRHOXlJRmRwYkdGNVlXZ2dTbUZyWVhKMFlTQXlJaXdpYldKa1pYTmpJam9pUzJGdWRHOXlJRU5oWW1GdVp5QkxZVzUwYjNJZ1EyRmlZVzVuSUVwaGEyRnlkR0VnVUdGellYSWdUV2x1WjJkMUlpd2lZbkprWlhOaklqb2lWVTVKVkNCQ1JVNUVRU0JTUVZsQklGQkJVMEZTSUUxSlRrZEhWU0lzSW5KbFoybHZiaUk2SWtraUxDSnRZV2x1WW5JaU9qTXpPU3dpWW5KaGJtTm9Jam80TXpRc0ltcGxibWx6WDJ0bGJHRnRhVzRpT2lKUUlpd2lZV05qWlhOelgyeGxkbVZzSWpvaVRsVk1UQ0lzSW1WNGNHbHlaV1JmZEc5clpXNGlPakUyTWprek9ERTBNellzSW1WNGNDSTZNVFl5T1RRMk5ESXpOaXdpYW5ScElqb2lOakF6TVRrME1HSXRNelF3WmkwMFl6QTFMV0UyTm1JdE9EQXdaVEZpWkRCbE56WmpJaXdpYVdGMElqb3hOakk1TXpjM09ETTJMQ0pwYzNNaU9pSkNVa2xDVWtGSlRpSjkuam5BQjUyZFNzTk9jRC1HLWpYeDdKZFhtZHpLeFhTVm9EVk5Kbmh4aGtTZw==)
// @Param request body models.SwaggerGetListRequest true "body"
// @Failure 401 {object} models.SwaggerErrorExpiredTokenUnAuthorizeAndTokenUnAuthorize
// @Failure 403 {object} models.SwaggerErrorTokenForbiddenPermission
// @Failure 405 {object} models.SwaggerErrorMethodNotAllowed
// @Failure 500 {object} models.SwaggerErrorInternalServerError
// @Failure 400 {object} models.SwaggerErrorBadParamInput
// @Router /ditindaklanjuti [post]
func (a *NasabahHandler) GetListDitindaklanjuti(c echo.Context) error {
	log := "nasabah.delivery.http.NasabahHandler.GetListDitindaklanjuti: %s"
	response := new(models2.Response)
	request := new(models.RequestListNasabah)
	userLogin := c.Get("JWT_CLAIMS").(*models.BRIBrainClaims)

	userAuditTrailListDitindaklanjuti := models.BribrainAuditTrail{}
	payloadAuditTrailListDitindaklanjuti := models.PayloadBribrainAuditTrail{}
	headersAuditTrailListDitindaklanjuti := models.HeadersPayloadBribrainAuditTrail{}

	contentTypeHeaderListDitindaklanjuti := c.Request().Header.Get(echo.HeaderContentType)
	authorizationHeaderListDitindaklanjuti := c.Request().Header.Get(echo.HeaderAuthorization)
	urlpayloadListDitindaklanjuti := c.Request().Host + c.Request().URL.Path
	headersAuditTrailListDitindaklanjuti = headersAuditTrailListDitindaklanjuti.MappingHeadersPayloadBribrainAuditTrail(contentTypeHeaderListDitindaklanjuti, authorizationHeaderListDitindaklanjuti)

	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	dec, err := helper.JsonDecode(c, request)
	if err != nil {
		response.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), err.Error(), err, c)
		return c.JSON(response.StatusCode, response)
	}
	request = dec.(*models.RequestListNasabah)
	validate := validator.New()

	reqbodyListDitindaklanjuti, err := json.Marshal(request)
	if err != nil {
		a.Log.Error("nasabah.delivery.http.NasabahHandler.GetListDitindaklanjuti.userAuditTrail: %s", err.Error())
		// return nil, nil, err
	}

	errvalidate := validate.Struct(request)
	if errvalidate != nil {
		errID := time.Now().Format(helper.DateTimeFormatDefault) + " " + helper.RandomString(20)
		a.Log.Error("["+errID+"]  "+log, errvalidate.Error())
		response.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), models2.ErrInvalidValue.Error(), models2.ErrInvalidValue.Error(), c)

		remarkAuditTrailListDitindaklanjuti := helper.IntToString(response.StatusCode) + " " + errvalidate.Error()
		payloadAuditTrailListDitindaklanjuti = payloadAuditTrailListDitindaklanjuti.MappingPayloadBribrainAuditTrail(urlpayloadListDitindaklanjuti, headersAuditTrailListDitindaklanjuti, string(reqbodyListDitindaklanjuti), helper.JsonString(response), response.StatusCode)
		userAuditTrailListDitindaklanjuti = userAuditTrailListDitindaklanjuti.MappingBribrainAuditTrail("Get List Ditindaklanjuti", remarkAuditTrailListDitindaklanjuti, userLogin.Pernr, userLogin.Role, helper.JsonString(payloadAuditTrailListDitindaklanjuti))
		_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailListDitindaklanjuti)
		if errAuditTrail != nil {
			a.Log.Error("nasabah.delivery.http.NasabahHandler.GetListDitindaklanjuti.userAuditTrail: %s", err.Error())

		}

		return c.JSON(response.StatusCode, response)
	}

	limit, page, offset := helper.Pagination(request.Page, request.Limit)
	param := request.MappingToParamListRekomendasi(userLogin.Pernr, userLogin.Role, limit, page, offset)

	result, message, err := a.NasabahUsecase.GetListDitindaklanjuti(ctx, param)
	if err != nil {
		errId := time.Now().Format(helper.DateTimeFormatDefault) + " " + helper.RandomString(20)
		a.Log.Error("["+errId+"]  "+log, err.Error())
		response.MappingResponseError(helper.GetStatusCode(err), message, errId, c)

		remarkAuditTrailListDitindaklanjuti := helper.IntToString(response.StatusCode) + " " + err.Error()
		payloadAuditTrailListDitindaklanjuti = payloadAuditTrailListDitindaklanjuti.MappingPayloadBribrainAuditTrail(urlpayloadListDitindaklanjuti, headersAuditTrailListDitindaklanjuti, string(reqbodyListDitindaklanjuti), helper.JsonString(response), response.StatusCode)
		userAuditTrailListDitindaklanjuti = userAuditTrailListDitindaklanjuti.MappingBribrainAuditTrail("Get List Ditindaklanjuti", remarkAuditTrailListDitindaklanjuti, userLogin.Pernr, userLogin.Role, helper.JsonString(payloadAuditTrailListDitindaklanjuti))
		_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailListDitindaklanjuti)
		if errAuditTrail != nil {
			a.Log.Error("nasabah.delivery.http.NasabahHandler.GetListDitindaklanjuti.userAuditTrail: %s", err.Error())

		}

		return c.JSON(response.StatusCode, response)
	}

	a.Log.Info(log, helper.JsonString(result))

	response.MappingResponseSuccess(message, result, c)

	payloadAuditTrailListDitindaklanjuti = payloadAuditTrailListDitindaklanjuti.MappingPayloadBribrainAuditTrail(urlpayloadListDitindaklanjuti, headersAuditTrailListDitindaklanjuti, string(reqbodyListDitindaklanjuti), helper.JsonString(response), response.StatusCode)
	userAuditTrailListDitindaklanjuti = userAuditTrailListDitindaklanjuti.MappingBribrainAuditTrail("Get List Ditindaklanjuti", "200 Success", userLogin.Pernr, userLogin.Role, helper.JsonString(payloadAuditTrailListDitindaklanjuti))
	_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailListDitindaklanjuti)
	if errAuditTrail != nil {
		a.Log.Error("nasabah.delivery.http.NasabahHandler.GetListDitindaklanjuti.userAuditTrail: %s", err.Error())

	}

	return c.JSON(response.StatusCode, response)
}

// GetInfoTierNasabah godoc
// @Summary Get Info Tier Nasabah
// @Description get info tier nasabah
// @Tags DetailNasabah
// @ID get-info-tier-nasabah
// @Accept  application-bribrain/json
// @Produce  json
// @Param Content-Type header string true "Content-Type" default(application-bribrain/json)
// @Param Authorization header string true "token" default(QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUp3WlhKdWNpSTZJakF3T1RrNU9UazVJaXdpYzI1aGJXVWlPaUpWYzJWeUlFMWhiblJ5YVNCQ1VrbENjbUZwYmlJc0luSnZiR1VpT2lKTlFVNVVVa2tpTENKa1pYTmpjbWx3ZEdsdmJsOHhJam9pU21GcllYSjBZU0lzSW1SbGMyTnlhWEIwYVc5dVh6SWlPaUl5SWl3aWIzSm5aV2hmZEhnaU9pSlZUa2xVSUUxQlRWQkJUa2NnVUZKQlVFRlVRVTRpTENKemRHVnNiRjkwZUNJNklrcFZUa2xQVWlCQlUxTlBRMGxCVkVVZ1RVRk9WRkpKSWl3aWNtZGtaWE5qSWpvaVMyRnVkRzl5SUZkcGJHRjVZV2dnUzJGdWRHOXlJRmRwYkdGNVlXZ2dTbUZyWVhKMFlTQXlJaXdpYldKa1pYTmpJam9pUzJGdWRHOXlJRU5oWW1GdVp5QkxZVzUwYjNJZ1EyRmlZVzVuSUVwaGEyRnlkR0VnVUdGellYSWdUV2x1WjJkMUlpd2lZbkprWlhOaklqb2lWVTVKVkNCQ1JVNUVRU0JTUVZsQklGQkJVMEZTSUUxSlRrZEhWU0lzSW5KbFoybHZiaUk2SWtraUxDSnRZV2x1WW5JaU9qTXpPU3dpWW5KaGJtTm9Jam80TXpRc0ltcGxibWx6WDJ0bGJHRnRhVzRpT2lKUUlpd2lZV05qWlhOelgyeGxkbVZzSWpvaVRsVk1UQ0lzSW1WNGNHbHlaV1JmZEc5clpXNGlPakUyTWprek9ERTBNellzSW1WNGNDSTZNVFl5T1RRMk5ESXpOaXdpYW5ScElqb2lOakF6TVRrME1HSXRNelF3WmkwMFl6QTFMV0UyTm1JdE9EQXdaVEZpWkRCbE56WmpJaXdpYVdGMElqb3hOakk1TXpjM09ETTJMQ0pwYzNNaU9pSkNVa2xDVWtGSlRpSjkuam5BQjUyZFNzTk9jRC1HLWpYeDdKZFhtZHpLeFhTVm9EVk5Kbmh4aGtTZw==)
// @Param request body models.SwaggerGetDetailRequest true "Cif" default(ABC1264)
// @Failure 401 {object} models.SwaggerErrorExpiredTokenUnAuthorizeAndTokenUnAuthorize
// @Failure 403 {object} models.SwaggerErrorTokenForbiddenPermission
// @Failure 405 {object} models.SwaggerErrorMethodNotAllowed
// @Failure 500 {object} models.SwaggerErrorInternalServerError
// @Failure 400 {object} models.SwaggerErrorBadParamInput
// @Router /info_tier [post]
func (a *NasabahHandler) GetInfoTierNasabah(c echo.Context) error {
	log := "nasabah.delivery.http.NasabahHandler.GetInfoTierNasabah: %s"
	templateResponse := new(models2.Response)
	reqparam := new(models.RequestDetailNasabahEncrypted)

	userLogin := c.Get("JWT_CLAIMS").(*models.BRIBrainClaims)

	userAuditTrailInfoTier := models.BribrainAuditTrail{}
	payloadAuditTrailInfoTier := models.PayloadBribrainAuditTrail{}
	headersAuditTrailInfoTier := models.HeadersPayloadBribrainAuditTrail{}

	contentTypeHeaderInfoTier := c.Request().Header.Get(echo.HeaderContentType)
	authorizationHeaderInfoTier := c.Request().Header.Get(echo.HeaderAuthorization)
	urlpayloadInfoTier := c.Request().Host + c.Request().URL.Path
	headersAuditTrailInfoTier = headersAuditTrailInfoTier.MappingHeadersPayloadBribrainAuditTrail(contentTypeHeaderInfoTier, authorizationHeaderInfoTier)

	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}
	decode, err := helper.JsonDecode(c, reqparam)
	if err != nil {
		a.errorJsonValidation(c, templateResponse, err)
		return c.JSON(templateResponse.StatusCode, templateResponse)
	}
	reqparam = decode.(*models.RequestDetailNasabahEncrypted)
	validate := validator.New()

	reqbodyInfoTier, err := json.Marshal(reqparam)
	if err != nil {
		a.Log.Error("nasabah.delivery.http.NasabahHandler.GetInfoTierNasabah.userAuditTrail: %s", err.Error())
		// return nil, nil, err
	}

	validationerror := validate.Struct(reqparam)
	if validationerror != nil {
		a.errorRequestValidation(c, log, templateResponse, validationerror)

		remarkAuditTrailInfoTier := helper.IntToString(templateResponse.StatusCode) + " " + validationerror.Error()
		payloadAuditTrailInfoTier = payloadAuditTrailInfoTier.MappingPayloadBribrainAuditTrail(urlpayloadInfoTier, headersAuditTrailInfoTier, string(reqbodyInfoTier), helper.JsonString(templateResponse), templateResponse.StatusCode)
		userAuditTrailInfoTier = userAuditTrailInfoTier.MappingBribrainAuditTrail("Get Info Tier Nasabah", remarkAuditTrailInfoTier, userLogin.Pernr, userLogin.Role, helper.JsonString(payloadAuditTrailInfoTier))
		_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailInfoTier)
		if errAuditTrail != nil {
			a.Log.Error("nasabah.delivery.http.NasabahHandler.GetInfoTierNasabah.userAuditTrail: %s", err.Error())

		}

		return c.JSON(templateResponse.StatusCode, templateResponse)
	}
	usecaseRes, message, err := a.NasabahUsecase.GetInfoTierNasabahInRow(ctx, reqparam)
	if err != nil {
		a.errorUseCaseFunction(c, log, message, templateResponse, err)

		remarkAuditTrailInfoTier := helper.IntToString(templateResponse.StatusCode) + " " + err.Error()
		payloadAuditTrailInfoTier = payloadAuditTrailInfoTier.MappingPayloadBribrainAuditTrail(urlpayloadInfoTier, headersAuditTrailInfoTier, string(reqbodyInfoTier), helper.JsonString(templateResponse), templateResponse.StatusCode)
		userAuditTrailInfoTier = userAuditTrailInfoTier.MappingBribrainAuditTrail("Get Info Tier Nasabah", remarkAuditTrailInfoTier, userLogin.Pernr, userLogin.Role, helper.JsonString(payloadAuditTrailInfoTier))
		_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailInfoTier)
		if errAuditTrail != nil {
			a.Log.Error("nasabah.delivery.http.NasabahHandler.GetInfoTier.userAuditTrail: %s", err.Error())

		}

		return c.JSON(templateResponse.StatusCode, templateResponse)
	}
	a.Log.Info(log, helper.JsonString(usecaseRes))
	templateResponse.MappingResponseSuccess(message, usecaseRes, c)

	payloadAuditTrailInfoTier = payloadAuditTrailInfoTier.MappingPayloadBribrainAuditTrail(urlpayloadInfoTier, headersAuditTrailInfoTier, string(reqbodyInfoTier), helper.JsonString(templateResponse), templateResponse.StatusCode)
	userAuditTrailInfoTier = userAuditTrailInfoTier.MappingBribrainAuditTrail("Get Info Tier Nasabah", "200 Success", userLogin.Pernr, userLogin.Role, helper.JsonString(payloadAuditTrailInfoTier))
	_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailInfoTier)
	if errAuditTrail != nil {
		a.Log.Error("nasabah.delivery.http.NasabahHandler.GetInfoTierNasabah.userAuditTrail: %s", err.Error())

	}

	return c.JSON(templateResponse.StatusCode, templateResponse)
}
