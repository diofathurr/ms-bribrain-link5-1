package nasabah

import (
	"context"
	"ms-bribrain-link5/models"
)

type Usecase interface {
	GetDetail(ctx context.Context, param *models.RequestDetailNasabahEncrypted) (res *models.ResponseDetailNasabah, message string, err error)
	GetDetailDitindaklanjuti(c context.Context, param *models.RequestDetailNasabahEncrypted) (res *models.ResponseDetailNasabahDitindaklanjuti, message string, err error)
	GetListRekomendasi(c context.Context, param *models.ParamRequestListNasabah) (res *models.ResponseListRekomendasiNasabahPaginationDto, message string, err error)
	GetListRekomendasiNonBRI(c context.Context, param *models.ParamRequestListNasabah) (res *models.ResponseListRekomendasiNasabahNonBRIPaginationDto, message string, err error)
	GetListDitindaklanjuti(c context.Context, param *models.ParamRequestListNasabah) (res *models.ResponseListDitindaklanjutiPaginationDto, message string, err error)
	GetInfoTierNasabah(c context.Context, param *models.RequestDetailNasabahEncrypted) (res *models.ResponseInfoTierNasabahDto, message string, err error)
	GetInfoTierNasabahInRow(c context.Context, param *models.RequestDetailNasabahEncrypted) (res *models.ResponseInfoTierNasabahV110Dto, message string, err error)
}
