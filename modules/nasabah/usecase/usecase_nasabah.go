package usecase

import (
	"context"
	"fmt"
	"ms-bribrain-link5/helper"
	"ms-bribrain-link5/helper/logger"
	models2 "ms-bribrain-link5/helper/models"
	"ms-bribrain-link5/models"
	"ms-bribrain-link5/modules/nasabah"
	"time"
)

type NasabahUsecase struct {
	nasabahRepo    nasabah.Repository
	contextTimeout time.Duration
	log            logger.Logger
}

func NewNasabahUsecase(nasabahRepo nasabah.Repository, log logger.Logger, timeout time.Duration) nasabah.Usecase {
	return &NasabahUsecase{
		nasabahRepo:    nasabahRepo,
		contextTimeout: timeout,
		log:            log,
	}
}

func (n NasabahUsecase) GetDetail(c context.Context, param *models.RequestDetailNasabahEncrypted) (res *models.ResponseDetailNasabah, message string, err error) {
	ctx, cancel := context.WithTimeout(c, n.contextTimeout)
	defer cancel()
	log := "nasabah.usecase.NasabahUsecase.GetDetail: %s"

	detail, err := n.nasabahRepo.GetDetail(ctx, param)
	if err != nil {
		n.log.Error(log, err.Error())
		return nil, models2.ErrGeneralMessage.Error(), err
	}
	// fmt.Println(&detail)

	res = models.MappingResponseDetailNasabah(detail)

	n.log.Info(log, helper.JsonString(detail))
	return res, models2.GeneralSuccess, nil
}

func (n NasabahUsecase) GetDetailDitindaklanjuti(c context.Context, param *models.RequestDetailNasabahEncrypted) (res *models.ResponseDetailNasabahDitindaklanjuti, message string, err error) {
	ctx, cancel := context.WithTimeout(c, n.contextTimeout)
	defer cancel()
	log := "nasabah.usecase.NasabahUsecase.GetDetailDitindaklanjuti: %s"

	detail, err := n.nasabahRepo.GetDetailDitindaklanjuti(ctx, param)
	if err != nil {
		n.log.Error(log, err.Error())
		return nil, models2.ErrGeneralMessage.Error(), err
	}
	// fmt.Println(&detail)

	res = models.MappingResponseDetailNasabahDitindaklanjuti(detail)

	n.log.Info(log, helper.JsonString(detail))
	return res, models2.GeneralSuccess, nil
}

func (n NasabahUsecase) GetListRekomendasi(c context.Context, param *models.ParamRequestListNasabah) (res *models.ResponseListRekomendasiNasabahPaginationDto, message string, err error) {
	ctx, cancel := context.WithTimeout(c, n.contextTimeout)
	defer cancel()
	log := "nasabah.usecase.NasabahUsecase.GetListRekomendasi: %s"

	list, err := n.nasabahRepo.GetListRekomendasi(ctx, param)
	if err != nil {
		n.log.Error(log, err.Error())
		return nil, models2.ErrGeneralMessage.Error(), err
	}
	n.log.Info(log, helper.JsonString(list))

	data := make([]*models.ResponseListRekomendasiNasabah, len(list))
	// fmt.Println("len list ->", len(list))
	for i, val := range list {
		data[i] = val.MappingResponseListRekomendasiNasabah()
		// fmt.Println("data -> ", data[i])
	}

	totalRecords, err := n.nasabahRepo.CountRekomendasiBRI(ctx, param)
	if err != nil {
		n.log.Error(log, err.Error())
		return nil, models2.ErrGeneralMessage.Error(), err
	}
	n.log.Info(log, totalRecords)

	totalNonBRI, err := n.nasabahRepo.TotalRekomendasiNonBRI(ctx, param)
	if err != nil {
		n.log.Error(log, err.Error())
		return nil, models2.ErrGeneralMessage.Error(), err
	}
	n.log.Info(log, totalNonBRI)

	totalDitindaklanjutiBRI, err := n.nasabahRepo.TotalDitindaklanjutiBRI(ctx, param)
	if err != nil {
		n.log.Error(log, err.Error())
		return nil, models2.ErrGeneralMessage.Error(), err
	}
	n.log.Info(log, totalDitindaklanjutiBRI)

	totalDitindaklanjutiNonBRI, err := n.nasabahRepo.TotalDitindaklanjutiNonBRI(ctx, param)
	if err != nil {
		n.log.Error(log, err.Error())
		return nil, models2.ErrGeneralMessage.Error(), err
	}
	n.log.Info(log, totalDitindaklanjutiNonBRI)

	totalDitindaklanjuti := totalDitindaklanjutiBRI + totalDitindaklanjutiNonBRI

	// var lastupdate *time.Time
	// lastupdate = helper.DateTimeToDateTimeNUllable(time.Now())
	lastupdate, err := n.nasabahRepo.GetLastUpdateTabelBRI(ctx, param.PnPengelola, param.Role)
	if err != nil {
		n.log.Error(log, err.Error())
		return nil, models2.ErrGeneralMessage.Error(), err
	}
	n.log.Info(log, lastupdate)

	result := models.ResponseListRekomendasiNasabahPaginationDto{}
	result = result.MappingToPaginatorDto(data, lastupdate, param.Page, param.Limit, totalRecords, totalNonBRI, totalDitindaklanjuti)

	return &result, models2.GeneralSuccess, nil
}

func (n NasabahUsecase) GetListRekomendasiNonBRI(c context.Context, param *models.ParamRequestListNasabah) (res *models.ResponseListRekomendasiNasabahNonBRIPaginationDto, message string, err error) {
	ctx, cancel := context.WithTimeout(c, n.contextTimeout)
	defer cancel()
	log := "nasabah.usecase.NasabahUsecase.GetListRekomendasiNonBRI: %s"

	list, err := n.nasabahRepo.GetListRekomendasiNonBRI(ctx, param)
	if err != nil {
		n.log.Error(log, err.Error())
		return nil, models2.ErrGeneralMessage.Error(), err
	}
	n.log.Info(log, helper.JsonString(list))

	data := make([]*models.ResponseListRekomendasiNasabahNonBRI, len(list))
	// fmt.Println("len list ->", len(list))
	for i, val := range list {
		data[i] = val.MappingResponseListRekomendasiNasabahNonBRI()
		// fmt.Println("data -> ", data[i])
	}

	totalRecords, err := n.nasabahRepo.CountRekomendasiNonBRI(ctx, param)
	if err != nil {
		n.log.Error(log, err.Error())
		return nil, models2.ErrGeneralMessage.Error(), err
	}
	n.log.Info(log, totalRecords)

	totalBRI, err := n.nasabahRepo.TotalRekomendasiBRI(ctx, param)
	if err != nil {
		n.log.Error(log, err.Error())
		return nil, models2.ErrGeneralMessage.Error(), err
	}
	n.log.Info(log, totalBRI)

	totalDitindaklanjutiBRI, err := n.nasabahRepo.TotalDitindaklanjutiBRI(ctx, param)
	if err != nil {
		n.log.Error(log, err.Error())
		return nil, models2.ErrGeneralMessage.Error(), err
	}
	n.log.Info(log, totalDitindaklanjutiBRI)

	totalDitindaklanjutiNonBRI, err := n.nasabahRepo.TotalDitindaklanjutiNonBRI(ctx, param)
	if err != nil {
		n.log.Error(log, err.Error())
		return nil, models2.ErrGeneralMessage.Error(), err
	}
	n.log.Info(log, totalDitindaklanjutiNonBRI)

	totalDitindaklanjuti := totalDitindaklanjutiBRI + totalDitindaklanjutiNonBRI

	// var lastupdate *time.Time
	// lastupdate = helper.DateTimeToDateTimeNUllable(time.Now())
	lastupdate, err := n.nasabahRepo.GetLastUpdateTabelNonBRI(ctx, param.PnPengelola, param.Role)
	if err != nil {
		n.log.Error(log, err.Error())
		return nil, models2.ErrGeneralMessage.Error(), err
	}
	n.log.Info(log, lastupdate)

	result := models.ResponseListRekomendasiNasabahNonBRIPaginationDto{}
	result = result.MappingToPaginatorDto(data, lastupdate, param.Page, param.Limit, totalBRI, totalRecords, totalDitindaklanjuti)

	return &result, models2.GeneralSuccess, nil
}

func (n NasabahUsecase) GetListDitindaklanjuti(c context.Context, param *models.ParamRequestListNasabah) (res *models.ResponseListDitindaklanjutiPaginationDto, message string, err error) {
	ctx, cancel := context.WithTimeout(c, n.contextTimeout)
	defer cancel()
	log := "nasabah.usecase.NasabahUsecase.GetListDitindaklanjuti: %s"

	list, err := n.nasabahRepo.GetListDitindaklanjuti(ctx, param)
	if err != nil {
		n.log.Error(log, err.Error())
		return nil, models2.ErrGeneralMessage.Error(), err
	}
	n.log.Info(log, helper.JsonString(list))

	data := make([]*models.ResponseListDitindaklanjuti, len(list))
	// fmt.Println("len list ->", len(list))
	for i, val := range list {
		data[i] = val.MappingResponseListDitindaklanjuti()
		// fmt.Println("data -> ", data[i])
	}

	totalNonBRI, err := n.nasabahRepo.TotalRekomendasiNonBRI(ctx, param)
	if err != nil {
		n.log.Error(log, err.Error())
		return nil, models2.ErrGeneralMessage.Error(), err
	}
	n.log.Info(log, totalNonBRI)

	totalBRI, err := n.nasabahRepo.TotalRekomendasiBRI(ctx, param)
	if err != nil {
		n.log.Error(log, err.Error())
		return nil, models2.ErrGeneralMessage.Error(), err
	}
	n.log.Info(log, totalBRI)

	countDitindaklanjutiBRI, err := n.nasabahRepo.CountDitindaklanjutiBRI(ctx, param)
	if err != nil {
		n.log.Error(log, err.Error())
		return nil, models2.ErrGeneralMessage.Error(), err
	}
	n.log.Info(log, countDitindaklanjutiBRI)

	totalDitindaklanjutiNonBRI, err := n.nasabahRepo.CountDitindaklanjutiNonBRI(ctx, param)
	if err != nil {
		n.log.Error(log, err.Error())
		return nil, models2.ErrGeneralMessage.Error(), err
	}
	n.log.Info(log, totalDitindaklanjutiNonBRI)

	totalRecords := countDitindaklanjutiBRI + totalDitindaklanjutiNonBRI

	// var lastupdate *time.Time
	// lastupdate = helper.DateTimeToDateTimeNUllable(time.Now())

	lastupdate, err := n.nasabahRepo.GetLastUpdateDitindaklanjuti(ctx, param.PnPengelola, param.Role)
	if err != nil {
		n.log.Error(log, err.Error())
		return nil, models2.ErrGeneralMessage.Error(), err
	}
	n.log.Info(log, lastupdate)

	result := models.ResponseListDitindaklanjutiPaginationDto{}
	result = result.MappingToPaginatorDto(data, lastupdate, param.Page, param.Limit, totalBRI, totalNonBRI, totalRecords)

	return &result, models2.GeneralSuccess, nil
}

func (n NasabahUsecase) GetInfoTierNasabah(c context.Context, param *models.RequestDetailNasabahEncrypted) (res *models.ResponseInfoTierNasabahDto, message string, err error) {
	ctx, cancel := context.WithTimeout(c, n.contextTimeout)
	defer cancel()
	log := "nasabah.usecase.NasabahUsecase.GetInfoTierNasabah: %s"

	detail, err := n.nasabahRepo.GetDetail(ctx, param)
	if err != nil {
		n.log.Error(log, err.Error())
		return nil, models2.ErrGeneralMessage.Error(), err
	}
	dataTier := make([]*models.ResponseInfoTierNasabah, 0)
	// for i := 1; i < 2; i++ {
	fmt.Println("posisi tier -> ", int(*detail.PosisiTier))
	tier := 2
	for i := 1; i < int(*detail.PosisiTier); i++ {
		fmt.Println(i)

		switch tier {
		case 2:
			param.Cifno = helper.StringNullableToString(detail.CifNasabahTier1)
		case 3:
			param.Cifno = helper.StringNullableToString(detail.CifNasabahTier2)
		case 4:
			param.Cifno = helper.StringNullableToString(detail.CifNasabahTier3)
		case 5:
			param.Cifno = helper.StringNullableToString(detail.CifNasabahTier4)
		}

		detail2, err := n.nasabahRepo.GetDetail(ctx, param)
		if err != nil {
			n.log.Error(log, err.Error())
			return nil, models2.ErrGeneralMessage.Error(), err
		}

		fmt.Println(helper.StringNullableToString(detail.Cifno))
		fmt.Println(param.Cifno)
		data := models.MappingResponseInfoTierNasabah(detail2)
		dataTier = append(dataTier, data)

		tier++

	}
	var dataNasabah []*models.InfoTierCurrentNasabah
	dataCurrentNasabah := models.MappingInfoTierCurrentNasabah(detail)
	dataNasabah = append(dataNasabah, dataCurrentNasabah)
	result := models.MappingResponseInfoTierNasabahDto(dataTier, dataNasabah)

	// data = models.MappingResponseInfoTierNasabah(detail)
	res = result
	n.log.Info(log, helper.JsonString(detail))
	return res, models2.GeneralSuccess, nil
}

func (n NasabahUsecase) GetInfoTierNasabahInRow(c context.Context, param *models.RequestDetailNasabahEncrypted) (res *models.ResponseInfoTierNasabahV110Dto, message string, err error) {
	ctx, cancel := context.WithTimeout(c, n.contextTimeout)
	defer cancel()
	log := "nasabah.usecase.NasabahUsecase.GetInfoTierNasabah: %s"

	detail, err := n.nasabahRepo.GetDetail(ctx, param)
	if err != nil {
		n.log.Error(log, err.Error())
		return nil, models2.ErrGeneralMessage.Error(), err
	}

	// var dataNasabah []*models.InfoTierCurrentNasabah
	dataCurrentNasabah := models.MappingInfoTierCurrentNasabahV110(detail)
	// dataNasabah = append(dataNasabah, dataCurrentNasabah)
	dataTier := models.MappingResponseInfoTierNasabahInRowV110(detail)
	result := models.MappingResponseInfoTierNasabahV110Dto(dataTier, dataCurrentNasabah)

	// data = models.MappingResponseInfoTierNasabah(detail)
	res = result
	n.log.Info(log, helper.JsonString(detail))
	return res, models2.GeneralSuccess, nil
}
