package usecase_test

import (
	"context"
	"ms-bribrain-link5/helper/logger"
	models2 "ms-bribrain-link5/helper/models"
	"ms-bribrain-link5/models"
	_nasabahRepositoryMock "ms-bribrain-link5/modules/nasabah/mocks"
	"ms-bribrain-link5/modules/nasabah/usecase"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

var (
	l              = logger.L
	timeoutContext = time.Second * 30
)

func TestNasabahUsecase_GetDetail(t *testing.T) {
	//requestMock
	request := &models.RequestDetailNasabahEncrypted{}
	request = request.MappingExampleData()

	//resultMock
	mockResponseRepoGetDetail := &models.GetDetailQueryStruct{}
	mockResponseRepoGetDetail = mockResponseRepoGetDetail.MappingExampleData()

	t.Run("success", func(t *testing.T) {
		//repositoryMock
		mockNasabahRepo := new(_nasabahRepositoryMock.Repository)

		mockNasabahRepo.On("GetDetail", mock.Anything, mock.AnythingOfType("*models.RequestDetailNasabahEncrypted")).Return(mockResponseRepoGetDetail, nil).Once()

		u := usecase.NewNasabahUsecase(mockNasabahRepo, l, timeoutContext)

		a, _, err := u.GetDetail(context.TODO(), request)

		assert.NoError(t, err)
		assert.NotNil(t, a)

		mockNasabahRepo.AssertExpectations(t)
	})

	t.Run("error", func(t *testing.T) {
		//repositoryMock
		mockNasabahRepo := new(_nasabahRepositoryMock.Repository)

		mockNasabahRepo.On("GetDetail", mock.Anything, mock.AnythingOfType("*models.RequestDetailNasabahEncrypted")).Return(nil, models2.ErrInternalServerError).Once()

		u := usecase.NewNasabahUsecase(mockNasabahRepo, l, timeoutContext)

		a, _, err := u.GetDetail(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockNasabahRepo.AssertExpectations(t)
	})
}

func TestNasabahUsecase_GetDetailDitindaklanjuti(t *testing.T) {
	// /requestMock
	request := &models.RequestDetailNasabahEncrypted{}
	request = request.MappingExampleData()

	//resultMock
	mockResponseRepoGetDetail := &models.GetDetailQueryStruct{}
	mockResponseRepoGetDetail = mockResponseRepoGetDetail.MappingExampleData()

	t.Run("success", func(t *testing.T) {
		//repositoryMock
		mockNasabahRepo := new(_nasabahRepositoryMock.Repository)

		mockNasabahRepo.On("GetDetailDitindaklanjuti", mock.Anything, mock.AnythingOfType("*models.RequestDetailNasabahEncrypted")).Return(mockResponseRepoGetDetail, nil).Once()

		u := usecase.NewNasabahUsecase(mockNasabahRepo, l, timeoutContext)

		a, _, err := u.GetDetailDitindaklanjuti(context.TODO(), request)

		assert.NoError(t, err)
		assert.NotNil(t, a)

		mockNasabahRepo.AssertExpectations(t)
	})

	t.Run("error", func(t *testing.T) {
		//repositoryMock
		mockNasabahRepo := new(_nasabahRepositoryMock.Repository)

		mockNasabahRepo.On("GetDetailDitindaklanjuti", mock.Anything, mock.AnythingOfType("*models.RequestDetailNasabahEncrypted")).Return(nil, models2.ErrInternalServerError).Once()

		u := usecase.NewNasabahUsecase(mockNasabahRepo, l, timeoutContext)

		a, _, err := u.GetDetailDitindaklanjuti(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockNasabahRepo.AssertExpectations(t)
	})
}

func TestNasabahUsecase_GetListRekomendasi(t *testing.T) {
	//requestMock
	request := &models.ParamRequestListNasabah{}
	request = request.MappingExampleData()

	//resultMock
	mockResponseListRekomendasi := models.GetListQueryStruct{}
	mockResponseListRekomendasi = mockResponseListRekomendasi.MappingExampleData()
	mockResponseListRekomendasiArray := []models.GetListQueryStruct{}
	mockResponseListRekomendasiArray = append(mockResponseListRekomendasiArray, mockResponseListRekomendasi)
	mockLastUpdate := time.Now()

	t.Run("success", func(t *testing.T) {
		//repositoryMock
		mockNasabahRepo := new(_nasabahRepositoryMock.Repository)

		mockNasabahRepo.On("GetListRekomendasi", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(mockResponseListRekomendasiArray, nil).Once()
		mockNasabahRepo.On("CountRekomendasiBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("TotalRekomendasiNonBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("TotalDitindaklanjutiBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("TotalDitindaklanjutiNonBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("GetLastUpdateTabelBRI", mock.Anything, mock.AnythingOfType("string"), mock.AnythingOfType("string")).Return(&mockLastUpdate, nil).Once()

		u := usecase.NewNasabahUsecase(mockNasabahRepo, l, timeoutContext)

		a, _, err := u.GetListRekomendasi(context.TODO(), request)

		assert.NoError(t, err)
		assert.NotNil(t, a)

		mockNasabahRepo.AssertExpectations(t)
	})

	t.Run("error-getlist", func(t *testing.T) {
		//repositoryMock
		mockNasabahRepo := new(_nasabahRepositoryMock.Repository)

		mockNasabahRepo.On("GetListRekomendasi", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(mockResponseListRekomendasiArray, models2.ErrInternalServerError).Once()

		u := usecase.NewNasabahUsecase(mockNasabahRepo, l, timeoutContext)

		a, _, err := u.GetListRekomendasi(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockNasabahRepo.AssertExpectations(t)
	})

	t.Run("error-countrekomendasibri", func(t *testing.T) {
		//repositoryMock
		mockNasabahRepo := new(_nasabahRepositoryMock.Repository)

		mockNasabahRepo.On("GetListRekomendasi", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(mockResponseListRekomendasiArray, nil).Once()
		mockNasabahRepo.On("CountRekomendasiBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(0, models2.ErrInternalServerError).Once()

		u := usecase.NewNasabahUsecase(mockNasabahRepo, l, timeoutContext)

		a, _, err := u.GetListRekomendasi(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockNasabahRepo.AssertExpectations(t)
	})

	t.Run("error-totalrekomendasinonbri", func(t *testing.T) {
		//repositoryMock
		mockNasabahRepo := new(_nasabahRepositoryMock.Repository)

		mockNasabahRepo.On("GetListRekomendasi", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(mockResponseListRekomendasiArray, nil).Once()
		mockNasabahRepo.On("CountRekomendasiBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("TotalRekomendasiNonBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(0, models2.ErrInternalServerError).Once()

		u := usecase.NewNasabahUsecase(mockNasabahRepo, l, timeoutContext)

		a, _, err := u.GetListRekomendasi(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockNasabahRepo.AssertExpectations(t)
	})

	t.Run("error-totalditindaklanjutibri", func(t *testing.T) {
		//repositoryMock
		mockNasabahRepo := new(_nasabahRepositoryMock.Repository)

		mockNasabahRepo.On("GetListRekomendasi", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(mockResponseListRekomendasiArray, nil).Once()
		mockNasabahRepo.On("CountRekomendasiBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("TotalRekomendasiNonBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("TotalDitindaklanjutiBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(0, models2.ErrInternalServerError).Once()

		u := usecase.NewNasabahUsecase(mockNasabahRepo, l, timeoutContext)

		a, _, err := u.GetListRekomendasi(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockNasabahRepo.AssertExpectations(t)
	})

	t.Run("error-totalditindaklanjutinonbri", func(t *testing.T) {
		//repositoryMock
		mockNasabahRepo := new(_nasabahRepositoryMock.Repository)

		mockNasabahRepo.On("GetListRekomendasi", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(mockResponseListRekomendasiArray, nil).Once()
		mockNasabahRepo.On("CountRekomendasiBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("TotalRekomendasiNonBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("TotalDitindaklanjutiBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("TotalDitindaklanjutiNonBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(0, models2.ErrInternalServerError).Once()

		u := usecase.NewNasabahUsecase(mockNasabahRepo, l, timeoutContext)

		a, _, err := u.GetListRekomendasi(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockNasabahRepo.AssertExpectations(t)
	})

	t.Run("error-getlastupdatetabelbri", func(t *testing.T) {
		//repositoryMock
		mockNasabahRepo := new(_nasabahRepositoryMock.Repository)

		mockNasabahRepo.On("GetListRekomendasi", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(mockResponseListRekomendasiArray, nil).Once()
		mockNasabahRepo.On("CountRekomendasiBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("TotalRekomendasiNonBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("TotalDitindaklanjutiBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("TotalDitindaklanjutiNonBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("GetLastUpdateTabelBRI", mock.Anything, mock.AnythingOfType("string"), mock.AnythingOfType("string")).Return(nil, models2.ErrInternalServerError).Once()

		u := usecase.NewNasabahUsecase(mockNasabahRepo, l, timeoutContext)

		a, _, err := u.GetListRekomendasi(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockNasabahRepo.AssertExpectations(t)
	})
}

func TestNasabahUsecase_GetListRekomendasiNonBRI(t *testing.T) {
	//requestMock
	request := &models.ParamRequestListNasabah{}
	request = request.MappingExampleData()

	//resultMock
	mockResponseListRekomendasi := models.GetListQueryStruct{}
	mockResponseListRekomendasi = mockResponseListRekomendasi.MappingExampleData()
	mockResponseListRekomendasiArray := []models.GetListQueryStruct{}
	mockResponseListRekomendasiArray = append(mockResponseListRekomendasiArray, mockResponseListRekomendasi)
	mockLastUpdate := time.Now()

	t.Run("success", func(t *testing.T) {
		//repositoryMock
		mockNasabahRepo := new(_nasabahRepositoryMock.Repository)

		mockNasabahRepo.On("GetListRekomendasiNonBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(mockResponseListRekomendasiArray, nil).Once()
		mockNasabahRepo.On("CountRekomendasiNonBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("TotalRekomendasiBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("TotalDitindaklanjutiBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("TotalDitindaklanjutiNonBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("GetLastUpdateTabelNonBRI", mock.Anything, mock.AnythingOfType("string"), mock.AnythingOfType("string")).Return(&mockLastUpdate, nil).Once()

		u := usecase.NewNasabahUsecase(mockNasabahRepo, l, timeoutContext)

		a, _, err := u.GetListRekomendasiNonBRI(context.TODO(), request)

		assert.NoError(t, err)
		assert.NotNil(t, a)

		mockNasabahRepo.AssertExpectations(t)
	})

	t.Run("error-getlist", func(t *testing.T) {
		//repositoryMock
		mockNasabahRepo := new(_nasabahRepositoryMock.Repository)

		mockNasabahRepo.On("GetListRekomendasiNonBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(mockResponseListRekomendasiArray, models2.ErrInternalServerError).Once()

		u := usecase.NewNasabahUsecase(mockNasabahRepo, l, timeoutContext)

		a, _, err := u.GetListRekomendasiNonBRI(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockNasabahRepo.AssertExpectations(t)
	})

	t.Run("error-countrekomendasibri", func(t *testing.T) {
		//repositoryMock
		mockNasabahRepo := new(_nasabahRepositoryMock.Repository)

		mockNasabahRepo.On("GetListRekomendasiNonBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(mockResponseListRekomendasiArray, nil).Once()
		mockNasabahRepo.On("CountRekomendasiNonBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(0, models2.ErrInternalServerError).Once()

		u := usecase.NewNasabahUsecase(mockNasabahRepo, l, timeoutContext)

		a, _, err := u.GetListRekomendasiNonBRI(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockNasabahRepo.AssertExpectations(t)
	})

	t.Run("error-totalrekomendasinonbri", func(t *testing.T) {
		//repositoryMock
		mockNasabahRepo := new(_nasabahRepositoryMock.Repository)

		mockNasabahRepo.On("GetListRekomendasiNonBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(mockResponseListRekomendasiArray, nil).Once()
		mockNasabahRepo.On("CountRekomendasiNonBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("TotalRekomendasiBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(0, models2.ErrInternalServerError).Once()

		u := usecase.NewNasabahUsecase(mockNasabahRepo, l, timeoutContext)

		a, _, err := u.GetListRekomendasiNonBRI(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockNasabahRepo.AssertExpectations(t)
	})

	t.Run("error-totalditindaklanjutibri", func(t *testing.T) {
		//repositoryMock
		mockNasabahRepo := new(_nasabahRepositoryMock.Repository)

		mockNasabahRepo.On("GetListRekomendasiNonBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(mockResponseListRekomendasiArray, nil).Once()
		mockNasabahRepo.On("CountRekomendasiNonBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("TotalRekomendasiBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("TotalDitindaklanjutiBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(0, models2.ErrInternalServerError).Once()

		u := usecase.NewNasabahUsecase(mockNasabahRepo, l, timeoutContext)

		a, _, err := u.GetListRekomendasiNonBRI(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockNasabahRepo.AssertExpectations(t)
	})

	t.Run("error-totalditindaklanjutinonbri", func(t *testing.T) {
		//repositoryMock
		mockNasabahRepo := new(_nasabahRepositoryMock.Repository)

		mockNasabahRepo.On("GetListRekomendasiNonBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(mockResponseListRekomendasiArray, nil).Once()
		mockNasabahRepo.On("CountRekomendasiNonBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("TotalRekomendasiBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("TotalDitindaklanjutiBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("TotalDitindaklanjutiNonBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(0, models2.ErrInternalServerError).Once()

		u := usecase.NewNasabahUsecase(mockNasabahRepo, l, timeoutContext)

		a, _, err := u.GetListRekomendasiNonBRI(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockNasabahRepo.AssertExpectations(t)
	})

	t.Run("error-getlastupdatetabelbri", func(t *testing.T) {
		//repositoryMock
		mockNasabahRepo := new(_nasabahRepositoryMock.Repository)

		mockNasabahRepo.On("GetListRekomendasiNonBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(mockResponseListRekomendasiArray, nil).Once()
		mockNasabahRepo.On("CountRekomendasiNonBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("TotalRekomendasiBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("TotalDitindaklanjutiBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("TotalDitindaklanjutiNonBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("GetLastUpdateTabelNonBRI", mock.Anything, mock.AnythingOfType("string"), mock.AnythingOfType("string")).Return(nil, models2.ErrInternalServerError).Once()

		u := usecase.NewNasabahUsecase(mockNasabahRepo, l, timeoutContext)

		a, _, err := u.GetListRekomendasiNonBRI(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockNasabahRepo.AssertExpectations(t)
	})
}

func TestNasabahUsecase_GetListDitindaklanjuti(t *testing.T) {
	//requestMock
	request := &models.ParamRequestListNasabah{}
	request = request.MappingExampleData()

	//resultMock
	mockResponseListRekomendasi := models.GetListQueryStruct{}
	mockResponseListRekomendasi = mockResponseListRekomendasi.MappingExampleData()
	mockResponseListRekomendasiArray := []models.GetListQueryStruct{}
	mockResponseListRekomendasiArray = append(mockResponseListRekomendasiArray, mockResponseListRekomendasi)
	mockLastUpdate := time.Now()

	t.Run("success", func(t *testing.T) {
		//repositoryMock
		mockNasabahRepo := new(_nasabahRepositoryMock.Repository)

		mockNasabahRepo.On("GetListDitindaklanjuti", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(mockResponseListRekomendasiArray, nil).Once()
		mockNasabahRepo.On("TotalRekomendasiNonBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("TotalRekomendasiBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("CountDitindaklanjutiBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("CountDitindaklanjutiNonBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("GetLastUpdateDitindaklanjuti", mock.Anything, mock.AnythingOfType("string"), mock.AnythingOfType("string")).Return(&mockLastUpdate, nil).Once()

		u := usecase.NewNasabahUsecase(mockNasabahRepo, l, timeoutContext)

		a, _, err := u.GetListDitindaklanjuti(context.TODO(), request)

		assert.NoError(t, err)
		assert.NotNil(t, a)

		mockNasabahRepo.AssertExpectations(t)
	})

	t.Run("error-getlist", func(t *testing.T) {
		//repositoryMock
		mockNasabahRepo := new(_nasabahRepositoryMock.Repository)

		mockNasabahRepo.On("GetListDitindaklanjuti", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(mockResponseListRekomendasiArray, models2.ErrInternalServerError).Once()

		u := usecase.NewNasabahUsecase(mockNasabahRepo, l, timeoutContext)

		a, _, err := u.GetListDitindaklanjuti(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockNasabahRepo.AssertExpectations(t)
	})

	t.Run("error-countrekomendasibri", func(t *testing.T) {
		//repositoryMock
		mockNasabahRepo := new(_nasabahRepositoryMock.Repository)

		mockNasabahRepo.On("GetListDitindaklanjuti", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(mockResponseListRekomendasiArray, nil).Once()
		mockNasabahRepo.On("TotalRekomendasiNonBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(0, models2.ErrInternalServerError).Once()

		u := usecase.NewNasabahUsecase(mockNasabahRepo, l, timeoutContext)

		a, _, err := u.GetListDitindaklanjuti(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockNasabahRepo.AssertExpectations(t)
	})

	t.Run("error-totalrekomendasinonbri", func(t *testing.T) {
		//repositoryMock
		mockNasabahRepo := new(_nasabahRepositoryMock.Repository)

		mockNasabahRepo.On("GetListDitindaklanjuti", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(mockResponseListRekomendasiArray, nil).Once()
		mockNasabahRepo.On("TotalRekomendasiNonBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("TotalRekomendasiBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(0, models2.ErrInternalServerError).Once()

		u := usecase.NewNasabahUsecase(mockNasabahRepo, l, timeoutContext)

		a, _, err := u.GetListDitindaklanjuti(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockNasabahRepo.AssertExpectations(t)
	})

	t.Run("error-totalditindaklanjutibri", func(t *testing.T) {
		//repositoryMock
		mockNasabahRepo := new(_nasabahRepositoryMock.Repository)

		mockNasabahRepo.On("GetListDitindaklanjuti", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(mockResponseListRekomendasiArray, nil).Once()
		mockNasabahRepo.On("TotalRekomendasiNonBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("TotalRekomendasiBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("CountDitindaklanjutiBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(0, models2.ErrInternalServerError).Once()

		u := usecase.NewNasabahUsecase(mockNasabahRepo, l, timeoutContext)

		a, _, err := u.GetListDitindaklanjuti(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockNasabahRepo.AssertExpectations(t)
	})

	t.Run("error-totalditindaklanjutinonbri", func(t *testing.T) {
		//repositoryMock
		mockNasabahRepo := new(_nasabahRepositoryMock.Repository)

		mockNasabahRepo.On("GetListDitindaklanjuti", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(mockResponseListRekomendasiArray, nil).Once()
		mockNasabahRepo.On("TotalRekomendasiNonBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("TotalRekomendasiBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("CountDitindaklanjutiBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("CountDitindaklanjutiNonBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(0, models2.ErrInternalServerError).Once()

		u := usecase.NewNasabahUsecase(mockNasabahRepo, l, timeoutContext)

		a, _, err := u.GetListDitindaklanjuti(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockNasabahRepo.AssertExpectations(t)
	})

	t.Run("error-getlastupdatetabelbri", func(t *testing.T) {
		//repositoryMock
		mockNasabahRepo := new(_nasabahRepositoryMock.Repository)

		mockNasabahRepo.On("GetListDitindaklanjuti", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(mockResponseListRekomendasiArray, nil).Once()
		mockNasabahRepo.On("TotalRekomendasiNonBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("TotalRekomendasiBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("CountDitindaklanjutiBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("CountDitindaklanjutiNonBRI", mock.Anything, mock.AnythingOfType("*models.ParamRequestListNasabah")).Return(1, nil).Once()
		mockNasabahRepo.On("GetLastUpdateDitindaklanjuti", mock.Anything, mock.AnythingOfType("string"), mock.AnythingOfType("string")).Return(nil, models2.ErrInternalServerError).Once()

		u := usecase.NewNasabahUsecase(mockNasabahRepo, l, timeoutContext)

		a, _, err := u.GetListDitindaklanjuti(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockNasabahRepo.AssertExpectations(t)
	})
}

func TestNasabahUsecase_GetInfoTierNasabah(t *testing.T) {
	//requestMock
	request := &models.RequestDetailNasabahEncrypted{}
	request = request.MappingExampleData()

	//resultMock
	mockResponseRepoGetDetail := &models.GetDetailQueryStruct{}
	mockResponseRepoGetDetail = mockResponseRepoGetDetail.MappingExampleData()
	mockResponseRepoGetDetailTier2 := mockResponseRepoGetDetail.MappingExampleDataTier2()
	mockResponseRepoGetDetailTier3 := mockResponseRepoGetDetail.MappingExampleDataTier3()
	mockResponseRepoGetDetailTier4 := mockResponseRepoGetDetail.MappingExampleDataTier4()
	mockResponseRepoGetDetailTier5 := mockResponseRepoGetDetail.MappingExampleDataTier5()

	t.Run("success", func(t *testing.T) {
		//repositoryMock
		mockNasabahRepo := new(_nasabahRepositoryMock.Repository)

		mockNasabahRepo.On("GetDetail", mock.Anything, mock.AnythingOfType("*models.RequestDetailNasabahEncrypted")).Return(mockResponseRepoGetDetailTier5, nil).Once()
		mockNasabahRepo.On("GetDetail", mock.Anything, mock.AnythingOfType("*models.RequestDetailNasabahEncrypted")).Return(mockResponseRepoGetDetail, nil).Once()
		mockNasabahRepo.On("GetDetail", mock.Anything, mock.AnythingOfType("*models.RequestDetailNasabahEncrypted")).Return(mockResponseRepoGetDetailTier2, nil).Once()
		mockNasabahRepo.On("GetDetail", mock.Anything, mock.AnythingOfType("*models.RequestDetailNasabahEncrypted")).Return(mockResponseRepoGetDetailTier3, nil).Once()
		mockNasabahRepo.On("GetDetail", mock.Anything, mock.AnythingOfType("*models.RequestDetailNasabahEncrypted")).Return(mockResponseRepoGetDetailTier4, nil).Once()

		u := usecase.NewNasabahUsecase(mockNasabahRepo, l, timeoutContext)

		a, _, err := u.GetInfoTierNasabah(context.TODO(), request)

		assert.NoError(t, err)
		assert.NotNil(t, a)

		mockNasabahRepo.AssertExpectations(t)
	})

	t.Run("error", func(t *testing.T) {
		//repositoryMock
		mockNasabahRepo := new(_nasabahRepositoryMock.Repository)

		mockNasabahRepo.On("GetDetail", mock.Anything, mock.AnythingOfType("*models.RequestDetailNasabahEncrypted")).Return(nil, models2.ErrInternalServerError).Once()

		u := usecase.NewNasabahUsecase(mockNasabahRepo, l, timeoutContext)

		a, _, err := u.GetInfoTierNasabah(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockNasabahRepo.AssertExpectations(t)
	})

	t.Run("error-inloop", func(t *testing.T) {
		//repositoryMock
		mockNasabahRepo := new(_nasabahRepositoryMock.Repository)

		mockNasabahRepo.On("GetDetail", mock.Anything, mock.AnythingOfType("*models.RequestDetailNasabahEncrypted")).Return(mockResponseRepoGetDetailTier5, nil).Once()
		mockNasabahRepo.On("GetDetail", mock.Anything, mock.AnythingOfType("*models.RequestDetailNasabahEncrypted")).Return(nil, models2.ErrInternalServerError).Once()

		u := usecase.NewNasabahUsecase(mockNasabahRepo, l, timeoutContext)

		a, _, err := u.GetInfoTierNasabah(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockNasabahRepo.AssertExpectations(t)
	})
}

func TestNasabahUsecase_GetInfoTierNasabahInRow(t *testing.T) {
	//requestMock
	request := &models.RequestDetailNasabahEncrypted{}
	request = request.MappingExampleData()

	//resultMock
	mockResponseRepoGetDetail := &models.GetDetailQueryStruct{}
	mockResponseRepoGetDetail = mockResponseRepoGetDetail.MappingExampleData()

	t.Run("success", func(t *testing.T) {
		//repositoryMock
		mockNasabahRepo := new(_nasabahRepositoryMock.Repository)

		mockNasabahRepo.On("GetDetail", mock.Anything, mock.AnythingOfType("*models.RequestDetailNasabahEncrypted")).Return(mockResponseRepoGetDetail, nil).Once()

		u := usecase.NewNasabahUsecase(mockNasabahRepo, l, timeoutContext)

		a, _, err := u.GetInfoTierNasabahInRow(context.TODO(), request)

		assert.NoError(t, err)
		assert.NotNil(t, a)

		mockNasabahRepo.AssertExpectations(t)
	})

	t.Run("error", func(t *testing.T) {
		//repositoryMock
		mockNasabahRepo := new(_nasabahRepositoryMock.Repository)

		mockNasabahRepo.On("GetDetail", mock.Anything, mock.AnythingOfType("*models.RequestDetailNasabahEncrypted")).Return(nil, models2.ErrInternalServerError).Once()

		u := usecase.NewNasabahUsecase(mockNasabahRepo, l, timeoutContext)

		a, _, err := u.GetInfoTierNasabahInRow(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockNasabahRepo.AssertExpectations(t)
	})
}
