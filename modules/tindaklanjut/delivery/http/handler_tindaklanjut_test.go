package http_test

import (
	"bytes"
	"encoding/json"
	"ms-bribrain-link5/helper/logger"
	"ms-bribrain-link5/middleware"
	"ms-bribrain-link5/models"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	models2 "ms-bribrain-link5/helper/models"
	tindaklanjutHttp "ms-bribrain-link5/modules/tindaklanjut/delivery/http"
	tindaklanjutHttpHandler "ms-bribrain-link5/modules/tindaklanjut/delivery/http"
	_tindaklanjutUsecaseMock "ms-bribrain-link5/modules/tindaklanjut/mocks"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

var (
	l               = logger.L
	Positive1       = "/submit_tindaklanjut_positive_1"
	Positive2       = "/submit_tindaklanjut_positive_2"
	Negative1       = "/submit_tindaklanjut_negative_1"
	Negative2       = "/submit_tindaklanjut_negative_2"
	Negative3       = "/submit_tindaklanjut_negative_3"
	OtherCase       = "/submit_tindaklanjut_other"
	Positive1Tandai = "/submit_tindaklanjut_positive_1_tandai"
	OtherCaseTandai = "/submit_tindaklanjut_other_tandai"
	CaseNonBri      = "/submit_tindaklanjut_non_bri"
	jwtClaimsstring = `{"access_level":"","branch":806,"brdesc":"UNIT GUNUNG PUTRI CIBUBUR","description_1":"Jakarta","description_2":"2","exp":1630401199,"expired_token":1630318399,"iat":1630314799,"iss":"BRIBRAIN","jenis_kelamin":"P","jti":"234f3706-7df8-4c18-88c1-7a80a90d23b1","mainbr":384,"mbdesc":"Kantor Cabang Cibubur","orgeh_tx":"BRI UNIT GUNUNG PUTRI","pernr":"00999950","region":"I","rgdesc":"Kantor Wilayah Jakarta 2","role":"RM","sname":"User RM BRIBrain","stell_tx":"RM"}`
)

func TestTindaklanjutHandler_LoadHandler(t *testing.T) {
	mockTindaklanjutUsecase := new(_tindaklanjutUsecaseMock.Usecase)
	e := echo.New()
	middL := middleware.InitMiddleware()
	tindaklanjutHttpHandler.NewTindaklanjutHandler(e, middL, mockTindaklanjutUsecase, l)

	mockTindaklanjutUsecase.AssertExpectations(t)
}
func TestTindaklanjutHandler_SubmitTindaklanjutPositiveCase1(t *testing.T) {
	//requestMock
	request := &models.RequestSubmitTindaklanjutPositive1{}
	request = request.MappingExampleData()
	requestBad := request.MappingExampleDataBadRequest()

	//resultMock
	mockResponseDetailNasabah := &models.ResponseDetailNasabah{}
	mockResponseDetailNasabah = mockResponseDetailNasabah.MappingExampleDataNil()

	t.Run("success", func(t *testing.T) {
		//usecaseMock
		mockTindaklanjutUsecase := new(_tindaklanjutUsecaseMock.Usecase)

		mockTindaklanjutUsecase.On("SubmitTindaklanjutPositiveCase1", mock.Anything, mock.AnythingOfType("*models.RequestSubmitTindaklanjutPositive1")).
			Return(mockResponseDetailNasabah, models2.GeneralSuccess, nil)

		e := echo.New()

		j, err := json.Marshal(request)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, Positive1, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(Positive1)
		c.Request().ParseForm()
		handler := tindaklanjutHttp.TindaklanjutHandler{
			TindaklanjutUsecase: mockTindaklanjutUsecase,
			Log:                 l,
		}
		err = handler.SubmitTindaklanjutPositiveCase1(c)
		require.NoError(t, err)

		assert.Equal(t, http.StatusOK, rec.Code)
		mockTindaklanjutUsecase.AssertExpectations(t)
	})

	t.Run("error-500", func(t *testing.T) {
		//usecaseMock
		mockTindaklanjutUsecase := new(_tindaklanjutUsecaseMock.Usecase)

		mockTindaklanjutUsecase.On("SubmitTindaklanjutPositiveCase1", mock.Anything, mock.AnythingOfType("*models.RequestSubmitTindaklanjutPositive1")).
			Return(nil, models2.ErrInternalServerError.Error(), models2.ErrInternalServerError)

		e := echo.New()

		j, err := json.Marshal(request)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, Positive1, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(Positive1)
		c.Request().ParseForm()
		handler := tindaklanjutHttp.TindaklanjutHandler{
			TindaklanjutUsecase: mockTindaklanjutUsecase,
			Log:                 l,
		}
		err = handler.SubmitTindaklanjutPositiveCase1(c)

		assert.Equal(t, http.StatusInternalServerError, rec.Code)
		mockTindaklanjutUsecase.AssertExpectations(t)
	})

	t.Run("error-400", func(t *testing.T) {
		//usecaseMock
		mockTindaklanjutUsecase := new(_tindaklanjutUsecaseMock.Usecase)

		// mockTindaklanjutUsecase.On("SubmitTindaklanjutPositiveCase1", mock.Anything, mock.AnythingOfType("*models.RequestSubmitTindaklanjutPositive1")).
		// 	Return(nil, models2.ErrBadParamInput.Error(), models2.ErrBadParamInput)

		e := echo.New()

		j, err := json.Marshal(requestBad)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, Positive1, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(Positive1)
		c.Request().ParseForm()
		handler := tindaklanjutHttp.TindaklanjutHandler{
			TindaklanjutUsecase: mockTindaklanjutUsecase,
			Log:                 l,
		}
		err = handler.SubmitTindaklanjutPositiveCase1(c)

		assert.Equal(t, http.StatusBadRequest, rec.Code)
		mockTindaklanjutUsecase.AssertExpectations(t)
	})

	t.Run("error-json-decode", func(t *testing.T) {
		//usecaseMock
		mockTindaklanjutUsecase := new(_tindaklanjutUsecaseMock.Usecase)

		// mockTindaklanjutUsecase.On("SubmitTindaklanjutPositiveCase1", mock.Anything, mock.AnythingOfType("*models.RequestSubmitTindaklanjutPositive1")).
		// 	Return(nil, models2.ErrBadParamInput.Error(), models2.ErrBadParamInput)

		e := echo.New()

		// j, err := json.Marshal(request)
		// assert.NoError(t, err)
		var data []byte
		req, err := http.NewRequest(echo.POST, Positive1, bytes.NewReader(data))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(Positive1)
		c.Request().ParseForm()
		handler := tindaklanjutHttp.TindaklanjutHandler{
			TindaklanjutUsecase: mockTindaklanjutUsecase,
			Log:                 l,
		}
		err = handler.SubmitTindaklanjutPositiveCase1(c)

		assert.Equal(t, http.StatusBadRequest, rec.Code)
		mockTindaklanjutUsecase.AssertExpectations(t)
	})
}
func TestTindaklanjutHandler_SubmitTindaklanjutPositiveCase2(t *testing.T) {
	//requestMock
	request := &models.RequestSubmitTindaklanjutPositive2{}
	request = request.MappingExampleData()
	requestBad := request.MappingExampleDataBadRequest()

	//resultMock
	mockResponseDetailNasabah := &models.ResponseDetailNasabah{}
	mockResponseDetailNasabah = mockResponseDetailNasabah.MappingExampleDataNil()

	t.Run("success", func(t *testing.T) {
		//usecaseMock
		mockTindaklanjutUsecase := new(_tindaklanjutUsecaseMock.Usecase)

		mockTindaklanjutUsecase.On("SubmitTindaklanjutPositiveCase2", mock.Anything, mock.AnythingOfType("*models.RequestSubmitTindaklanjutPositive2")).
			Return(mockResponseDetailNasabah, models2.GeneralSuccess, nil)

		e := echo.New()

		j, err := json.Marshal(request)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, Positive2, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(Positive2)
		c.Request().ParseForm()
		handler := tindaklanjutHttp.TindaklanjutHandler{
			TindaklanjutUsecase: mockTindaklanjutUsecase,
			Log:                 l,
		}
		err = handler.SubmitTindaklanjutPositiveCase2(c)
		require.NoError(t, err)

		assert.Equal(t, http.StatusOK, rec.Code)
		mockTindaklanjutUsecase.AssertExpectations(t)
	})

	t.Run("error-500", func(t *testing.T) {
		//usecaseMock
		mockTindaklanjutUsecase := new(_tindaklanjutUsecaseMock.Usecase)

		mockTindaklanjutUsecase.On("SubmitTindaklanjutPositiveCase2", mock.Anything, mock.AnythingOfType("*models.RequestSubmitTindaklanjutPositive2")).
			Return(nil, models2.ErrInternalServerError.Error(), models2.ErrInternalServerError)

		e := echo.New()

		j, err := json.Marshal(request)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, Positive2, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(Positive2)
		c.Request().ParseForm()
		handler := tindaklanjutHttp.TindaklanjutHandler{
			TindaklanjutUsecase: mockTindaklanjutUsecase,
			Log:                 l,
		}
		err = handler.SubmitTindaklanjutPositiveCase2(c)

		assert.Equal(t, http.StatusInternalServerError, rec.Code)
		mockTindaklanjutUsecase.AssertExpectations(t)
	})

	t.Run("error-400", func(t *testing.T) {
		//usecaseMock
		mockTindaklanjutUsecase := new(_tindaklanjutUsecaseMock.Usecase)

		// mockTindaklanjutUsecase.On("SubmitTindaklanjutPositiveCase2", mock.Anything, mock.AnythingOfType("*models.RequestSubmitTindaklanjutPositive2")).
		// 	Return(nil, models2.ErrBadParamInput.Error(), models2.ErrBadParamInput)

		e := echo.New()

		j, err := json.Marshal(requestBad)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, Positive2, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(Positive2)
		c.Request().ParseForm()
		handler := tindaklanjutHttp.TindaklanjutHandler{
			TindaklanjutUsecase: mockTindaklanjutUsecase,
			Log:                 l,
		}
		err = handler.SubmitTindaklanjutPositiveCase2(c)

		assert.Equal(t, http.StatusBadRequest, rec.Code)
		mockTindaklanjutUsecase.AssertExpectations(t)
	})

	t.Run("error-json-decode", func(t *testing.T) {
		//usecaseMock
		mockTindaklanjutUsecase := new(_tindaklanjutUsecaseMock.Usecase)

		// mockTindaklanjutUsecase.On("SubmitTindaklanjutPositiveCase2", mock.Anything, mock.AnythingOfType("*models.RequestSubmitTindaklanjutPositive2")).
		// 	Return(nil, models2.ErrBadParamInput.Error(), models2.ErrBadParamInput)

		e := echo.New()

		// j, err := json.Marshal(request)
		// assert.NoError(t, err)
		var data []byte
		req, err := http.NewRequest(echo.POST, Positive2, bytes.NewReader(data))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(Positive2)
		c.Request().ParseForm()
		handler := tindaklanjutHttp.TindaklanjutHandler{
			TindaklanjutUsecase: mockTindaklanjutUsecase,
			Log:                 l,
		}
		err = handler.SubmitTindaklanjutPositiveCase2(c)

		assert.Equal(t, http.StatusBadRequest, rec.Code)
		mockTindaklanjutUsecase.AssertExpectations(t)
	})
}

func TestTindaklanjutHandler_SubmitTindaklanjutNegativeCase1(t *testing.T) {
	//requestMock
	request := &models.RequestSubmitTindaklanjutNegative1{}
	request = request.MappingExampleData()
	requestBad := request.MappingExampleDataBadRequest()

	//resultMock
	mockResponseDetailNasabah := &models.ResponseDetailNasabah{}
	mockResponseDetailNasabah = mockResponseDetailNasabah.MappingExampleDataNil()

	t.Run("success", func(t *testing.T) {
		//usecaseMock
		mockTindaklanjutUsecase := new(_tindaklanjutUsecaseMock.Usecase)

		mockTindaklanjutUsecase.On("SubmitTindaklanjutNegativeCase1", mock.Anything, mock.AnythingOfType("*models.RequestSubmitTindaklanjutNegative1")).
			Return(mockResponseDetailNasabah, models2.GeneralSuccess, nil)

		e := echo.New()

		j, err := json.Marshal(request)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, Negative1, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(Negative1)
		c.Request().ParseForm()
		handler := tindaklanjutHttp.TindaklanjutHandler{
			TindaklanjutUsecase: mockTindaklanjutUsecase,
			Log:                 l,
		}
		err = handler.SubmitTindaklanjutNegativeCase1(c)
		require.NoError(t, err)

		assert.Equal(t, http.StatusOK, rec.Code)
		mockTindaklanjutUsecase.AssertExpectations(t)
	})

	t.Run("error-500", func(t *testing.T) {
		//usecaseMock
		mockTindaklanjutUsecase := new(_tindaklanjutUsecaseMock.Usecase)

		mockTindaklanjutUsecase.On("SubmitTindaklanjutNegativeCase1", mock.Anything, mock.AnythingOfType("*models.RequestSubmitTindaklanjutNegative1")).
			Return(nil, models2.ErrInternalServerError.Error(), models2.ErrInternalServerError)

		e := echo.New()

		j, err := json.Marshal(request)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, Negative1, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(Negative1)
		c.Request().ParseForm()
		handler := tindaklanjutHttp.TindaklanjutHandler{
			TindaklanjutUsecase: mockTindaklanjutUsecase,
			Log:                 l,
		}
		err = handler.SubmitTindaklanjutNegativeCase1(c)

		assert.Equal(t, http.StatusInternalServerError, rec.Code)
		mockTindaklanjutUsecase.AssertExpectations(t)
	})

	t.Run("error-400", func(t *testing.T) {
		//usecaseMock
		mockTindaklanjutUsecase := new(_tindaklanjutUsecaseMock.Usecase)

		// mockTindaklanjutUsecase.On("SubmitTindaklanjutNegativeCase1", mock.Anything, mock.AnythingOfType("*models.RequestSubmitTindaklanjutNegative1")).
		// 	Return(nil, models2.ErrBadParamInput.Error(), models2.ErrBadParamInput)

		e := echo.New()

		j, err := json.Marshal(requestBad)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, Negative1, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(Negative1)
		c.Request().ParseForm()
		handler := tindaklanjutHttp.TindaklanjutHandler{
			TindaklanjutUsecase: mockTindaklanjutUsecase,
			Log:                 l,
		}
		err = handler.SubmitTindaklanjutNegativeCase1(c)

		assert.Equal(t, http.StatusBadRequest, rec.Code)
		mockTindaklanjutUsecase.AssertExpectations(t)
	})

	t.Run("error-json-decode", func(t *testing.T) {
		//usecaseMock
		mockTindaklanjutUsecase := new(_tindaklanjutUsecaseMock.Usecase)

		// mockTindaklanjutUsecase.On("SubmitTindaklanjutNegativeCase1", mock.Anything, mock.AnythingOfType("*models.RequestSubmitTindaklanjutNegative1")).
		// 	Return(nil, models2.ErrBadParamInput.Error(), models2.ErrBadParamInput)

		e := echo.New()

		// j, err := json.Marshal(request)
		// assert.NoError(t, err)
		var data []byte
		req, err := http.NewRequest(echo.POST, Negative1, bytes.NewReader(data))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(Negative1)
		c.Request().ParseForm()
		handler := tindaklanjutHttp.TindaklanjutHandler{
			TindaklanjutUsecase: mockTindaklanjutUsecase,
			Log:                 l,
		}
		err = handler.SubmitTindaklanjutNegativeCase1(c)

		assert.Equal(t, http.StatusBadRequest, rec.Code)
		mockTindaklanjutUsecase.AssertExpectations(t)
	})
}

func TestTindaklanjutHandler_SubmitTindaklanjutNegativeCase2(t *testing.T) {
	//requestMock
	request := &models.RequestSubmitTindaklanjutNegative2{}
	request = request.MappingExampleData()
	requestBad := request.MappingExampleDataBadRequest()

	//resultMock
	mockResponseDetailNasabah := &models.ResponseDetailNasabah{}
	mockResponseDetailNasabah = mockResponseDetailNasabah.MappingExampleDataNil()

	t.Run("success", func(t *testing.T) {
		//usecaseMock
		mockTindaklanjutUsecase := new(_tindaklanjutUsecaseMock.Usecase)

		mockTindaklanjutUsecase.On("SubmitTindaklanjutNegativeCase2", mock.Anything, mock.AnythingOfType("*models.RequestSubmitTindaklanjutNegative2")).
			Return(mockResponseDetailNasabah, models2.GeneralSuccess, nil)

		e := echo.New()

		j, err := json.Marshal(request)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, Negative2, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(Negative2)
		c.Request().ParseForm()
		handler := tindaklanjutHttp.TindaklanjutHandler{
			TindaklanjutUsecase: mockTindaklanjutUsecase,
			Log:                 l,
		}
		err = handler.SubmitTindaklanjutNegativeCase2(c)
		require.NoError(t, err)

		assert.Equal(t, http.StatusOK, rec.Code)
		mockTindaklanjutUsecase.AssertExpectations(t)
	})

	t.Run("error-500", func(t *testing.T) {
		//usecaseMock
		mockTindaklanjutUsecase := new(_tindaklanjutUsecaseMock.Usecase)

		mockTindaklanjutUsecase.On("SubmitTindaklanjutNegativeCase2", mock.Anything, mock.AnythingOfType("*models.RequestSubmitTindaklanjutNegative2")).
			Return(nil, models2.ErrInternalServerError.Error(), models2.ErrInternalServerError)

		e := echo.New()

		j, err := json.Marshal(request)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, Negative2, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(Negative2)
		c.Request().ParseForm()
		handler := tindaklanjutHttp.TindaklanjutHandler{
			TindaklanjutUsecase: mockTindaklanjutUsecase,
			Log:                 l,
		}
		err = handler.SubmitTindaklanjutNegativeCase2(c)

		assert.Equal(t, http.StatusInternalServerError, rec.Code)
		mockTindaklanjutUsecase.AssertExpectations(t)
	})

	t.Run("error-400", func(t *testing.T) {
		//usecaseMock
		mockTindaklanjutUsecase := new(_tindaklanjutUsecaseMock.Usecase)

		// mockTindaklanjutUsecase.On("SubmitTindaklanjutNegativeCase2", mock.Anything, mock.AnythingOfType("*models.RequestSubmitTindaklanjutNegative2")).
		// 	Return(nil, models2.ErrBadParamInput.Error(), models2.ErrBadParamInput)

		e := echo.New()

		j, err := json.Marshal(requestBad)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, Negative2, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(Negative2)
		c.Request().ParseForm()
		handler := tindaklanjutHttp.TindaklanjutHandler{
			TindaklanjutUsecase: mockTindaklanjutUsecase,
			Log:                 l,
		}
		err = handler.SubmitTindaklanjutNegativeCase2(c)

		assert.Equal(t, http.StatusBadRequest, rec.Code)
		mockTindaklanjutUsecase.AssertExpectations(t)
	})

	t.Run("error-json-decode", func(t *testing.T) {
		//usecaseMock
		mockTindaklanjutUsecase := new(_tindaklanjutUsecaseMock.Usecase)

		// mockTindaklanjutUsecase.On("SubmitTindaklanjutNegativeCase2", mock.Anything, mock.AnythingOfType("*models.RequestSubmitTindaklanjutNegative2")).
		// 	Return(nil, models2.ErrBadParamInput.Error(), models2.ErrBadParamInput)

		e := echo.New()

		// j, err := json.Marshal(request)
		// assert.NoError(t, err)
		var data []byte
		req, err := http.NewRequest(echo.POST, Negative2, bytes.NewReader(data))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(Negative2)
		c.Request().ParseForm()
		handler := tindaklanjutHttp.TindaklanjutHandler{
			TindaklanjutUsecase: mockTindaklanjutUsecase,
			Log:                 l,
		}
		err = handler.SubmitTindaklanjutNegativeCase2(c)

		assert.Equal(t, http.StatusBadRequest, rec.Code)
		mockTindaklanjutUsecase.AssertExpectations(t)
	})
}

func TestTindaklanjutHandler_SubmitTindaklanjutNegativeCase3(t *testing.T) {
	//requestMock
	request := &models.RequestSubmitTindaklanjutNegative3{}
	request = request.MappingExampleData()
	requestBad := request.MappingExampleDataBadRequest()

	//resultMock
	mockResponseDetailNasabah := &models.ResponseDetailNasabah{}
	mockResponseDetailNasabah = mockResponseDetailNasabah.MappingExampleDataNil()

	t.Run("success", func(t *testing.T) {
		//usecaseMock
		mockTindaklanjutUsecase := new(_tindaklanjutUsecaseMock.Usecase)

		mockTindaklanjutUsecase.On("SubmitTindaklanjutNegativeCase3", mock.Anything, mock.AnythingOfType("*models.RequestSubmitTindaklanjutNegative3")).
			Return(mockResponseDetailNasabah, models2.GeneralSuccess, nil)

		e := echo.New()

		j, err := json.Marshal(request)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, Negative3, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(Negative3)
		c.Request().ParseForm()
		handler := tindaklanjutHttp.TindaklanjutHandler{
			TindaklanjutUsecase: mockTindaklanjutUsecase,
			Log:                 l,
		}
		err = handler.SubmitTindaklanjutNegativeCase3(c)
		require.NoError(t, err)

		assert.Equal(t, http.StatusOK, rec.Code)
		mockTindaklanjutUsecase.AssertExpectations(t)
	})

	t.Run("error-500", func(t *testing.T) {
		//usecaseMock
		mockTindaklanjutUsecase := new(_tindaklanjutUsecaseMock.Usecase)

		mockTindaklanjutUsecase.On("SubmitTindaklanjutNegativeCase3", mock.Anything, mock.AnythingOfType("*models.RequestSubmitTindaklanjutNegative3")).
			Return(nil, models2.ErrInternalServerError.Error(), models2.ErrInternalServerError)

		e := echo.New()

		j, err := json.Marshal(request)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, Negative3, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(Negative3)
		c.Request().ParseForm()
		handler := tindaklanjutHttp.TindaklanjutHandler{
			TindaklanjutUsecase: mockTindaklanjutUsecase,
			Log:                 l,
		}
		err = handler.SubmitTindaklanjutNegativeCase3(c)

		assert.Equal(t, http.StatusInternalServerError, rec.Code)
		mockTindaklanjutUsecase.AssertExpectations(t)
	})

	t.Run("error-400", func(t *testing.T) {
		//usecaseMock
		mockTindaklanjutUsecase := new(_tindaklanjutUsecaseMock.Usecase)

		// mockTindaklanjutUsecase.On("SubmitTindaklanjutNegativeCase3", mock.Anything, mock.AnythingOfType("*models.RequestSubmitTindaklanjutNegative3")).
		// 	Return(nil, models2.ErrBadParamInput.Error(), models2.ErrBadParamInput)

		e := echo.New()

		j, err := json.Marshal(requestBad)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, Negative3, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(Negative3)
		c.Request().ParseForm()
		handler := tindaklanjutHttp.TindaklanjutHandler{
			TindaklanjutUsecase: mockTindaklanjutUsecase,
			Log:                 l,
		}
		err = handler.SubmitTindaklanjutNegativeCase3(c)

		assert.Equal(t, http.StatusBadRequest, rec.Code)
		mockTindaklanjutUsecase.AssertExpectations(t)
	})

	t.Run("error-json-decode", func(t *testing.T) {
		//usecaseMock
		mockTindaklanjutUsecase := new(_tindaklanjutUsecaseMock.Usecase)

		// mockTindaklanjutUsecase.On("SubmitTindaklanjutNegativeCase3", mock.Anything, mock.AnythingOfType("*models.RequestSubmitTindaklanjutNegative3")).
		// 	Return(nil, models2.ErrBadParamInput.Error(), models2.ErrBadParamInput)

		e := echo.New()

		// j, err := json.Marshal(request)
		// assert.NoError(t, err)
		var data []byte
		req, err := http.NewRequest(echo.POST, Negative3, bytes.NewReader(data))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(Negative3)
		c.Request().ParseForm()
		handler := tindaklanjutHttp.TindaklanjutHandler{
			TindaklanjutUsecase: mockTindaklanjutUsecase,
			Log:                 l,
		}
		err = handler.SubmitTindaklanjutNegativeCase3(c)

		assert.Equal(t, http.StatusBadRequest, rec.Code)
		mockTindaklanjutUsecase.AssertExpectations(t)
	})
}

func TestTindaklanjutHandler_SubmitTindaklanjutOtherCase(t *testing.T) {
	//requestMock
	request := &models.RequestSubmitTindaklanjutOther{}
	request = request.MappingExampleData()
	requestBad := request.MappingExampleDataBadRequest()

	//resultMock
	mockResponseDetailNasabah := &models.ResponseDetailNasabah{}
	mockResponseDetailNasabah = mockResponseDetailNasabah.MappingExampleDataNil()

	t.Run("success", func(t *testing.T) {
		//usecaseMock
		mockTindaklanjutUsecase := new(_tindaklanjutUsecaseMock.Usecase)

		mockTindaklanjutUsecase.On("SubmitTindaklanjutOtherCase", mock.Anything, mock.AnythingOfType("*models.RequestSubmitTindaklanjutOther")).
			Return(mockResponseDetailNasabah, models2.GeneralSuccess, nil)

		e := echo.New()

		j, err := json.Marshal(request)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, OtherCase, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(OtherCase)
		c.Request().ParseForm()
		handler := tindaklanjutHttp.TindaklanjutHandler{
			TindaklanjutUsecase: mockTindaklanjutUsecase,
			Log:                 l,
		}
		err = handler.SubmitTindaklanjutOtherCase(c)
		require.NoError(t, err)

		assert.Equal(t, http.StatusOK, rec.Code)
		mockTindaklanjutUsecase.AssertExpectations(t)
	})

	t.Run("error-500", func(t *testing.T) {
		//usecaseMock
		mockTindaklanjutUsecase := new(_tindaklanjutUsecaseMock.Usecase)

		mockTindaklanjutUsecase.On("SubmitTindaklanjutOtherCase", mock.Anything, mock.AnythingOfType("*models.RequestSubmitTindaklanjutOther")).
			Return(nil, models2.ErrInternalServerError.Error(), models2.ErrInternalServerError)

		e := echo.New()

		j, err := json.Marshal(request)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, OtherCase, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(OtherCase)
		c.Request().ParseForm()
		handler := tindaklanjutHttp.TindaklanjutHandler{
			TindaklanjutUsecase: mockTindaklanjutUsecase,
			Log:                 l,
		}
		err = handler.SubmitTindaklanjutOtherCase(c)

		assert.Equal(t, http.StatusInternalServerError, rec.Code)
		mockTindaklanjutUsecase.AssertExpectations(t)
	})

	t.Run("error-400", func(t *testing.T) {
		//usecaseMock
		mockTindaklanjutUsecase := new(_tindaklanjutUsecaseMock.Usecase)

		// mockTindaklanjutUsecase.On("SubmitTindaklanjutOtherCase", mock.Anything, mock.AnythingOfType("*models.RequestSubmitTindaklanjutOther")).
		// 	Return(nil, models2.ErrBadParamInput.Error(), models2.ErrBadParamInput)

		e := echo.New()

		j, err := json.Marshal(requestBad)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, OtherCase, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(OtherCase)
		c.Request().ParseForm()
		handler := tindaklanjutHttp.TindaklanjutHandler{
			TindaklanjutUsecase: mockTindaklanjutUsecase,
			Log:                 l,
		}
		err = handler.SubmitTindaklanjutOtherCase(c)

		assert.Equal(t, http.StatusBadRequest, rec.Code)
		mockTindaklanjutUsecase.AssertExpectations(t)
	})

	t.Run("error-json-decode", func(t *testing.T) {
		//usecaseMock
		mockTindaklanjutUsecase := new(_tindaklanjutUsecaseMock.Usecase)

		// mockTindaklanjutUsecase.On("SubmitTindaklanjutOtherCase", mock.Anything, mock.AnythingOfType("*models.RequestSubmitTindaklanjutOther")).
		// 	Return(nil, models2.ErrBadParamInput.Error(), models2.ErrBadParamInput)

		e := echo.New()

		// j, err := json.Marshal(request)
		// assert.NoError(t, err)
		var data []byte
		req, err := http.NewRequest(echo.POST, OtherCase, bytes.NewReader(data))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(OtherCase)
		c.Request().ParseForm()
		handler := tindaklanjutHttp.TindaklanjutHandler{
			TindaklanjutUsecase: mockTindaklanjutUsecase,
			Log:                 l,
		}
		err = handler.SubmitTindaklanjutOtherCase(c)

		assert.Equal(t, http.StatusBadRequest, rec.Code)
		mockTindaklanjutUsecase.AssertExpectations(t)
	})
}

func TestTindaklanjutHandler_SubmitTindaklanjutPositiveCase1Tandai(t *testing.T) {
	//requestMock
	request := &models.RequestSubmitTindaklanjutTandai{}
	request = request.MappingExampleData()
	requestBad := request.MappingExampleDataBadRequest()

	//resultMock
	mockResponseDetailNasabah := &models.ResponseDetailNasabah{}
	mockResponseDetailNasabah = mockResponseDetailNasabah.MappingExampleDataNil()

	t.Run("success", func(t *testing.T) {
		//usecaseMock
		mockTindaklanjutUsecase := new(_tindaklanjutUsecaseMock.Usecase)

		mockTindaklanjutUsecase.On("SubmitTindaklanjutTandai", mock.Anything, mock.AnythingOfType("*models.RequestSubmitTindaklanjutTandai")).
			Return(mockResponseDetailNasabah, models2.GeneralSuccess, nil)

		e := echo.New()

		j, err := json.Marshal(request)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, Positive1Tandai, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(Positive1Tandai)
		c.Request().ParseForm()
		handler := tindaklanjutHttp.TindaklanjutHandler{
			TindaklanjutUsecase: mockTindaklanjutUsecase,
			Log:                 l,
		}
		err = handler.SubmitTindaklanjutPositiveCase1Tandai(c)
		require.NoError(t, err)

		assert.Equal(t, http.StatusOK, rec.Code)
		mockTindaklanjutUsecase.AssertExpectations(t)
	})

	t.Run("error-500", func(t *testing.T) {
		//usecaseMock
		mockTindaklanjutUsecase := new(_tindaklanjutUsecaseMock.Usecase)

		mockTindaklanjutUsecase.On("SubmitTindaklanjutTandai", mock.Anything, mock.AnythingOfType("*models.RequestSubmitTindaklanjutTandai")).
			Return(nil, models2.ErrInternalServerError.Error(), models2.ErrInternalServerError)

		e := echo.New()

		j, err := json.Marshal(request)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, Positive1Tandai, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(Positive1Tandai)
		c.Request().ParseForm()
		handler := tindaklanjutHttp.TindaklanjutHandler{
			TindaklanjutUsecase: mockTindaklanjutUsecase,
			Log:                 l,
		}
		err = handler.SubmitTindaklanjutPositiveCase1Tandai(c)

		assert.Equal(t, http.StatusInternalServerError, rec.Code)
		mockTindaklanjutUsecase.AssertExpectations(t)
	})

	t.Run("error-400", func(t *testing.T) {
		//usecaseMock
		mockTindaklanjutUsecase := new(_tindaklanjutUsecaseMock.Usecase)

		// mockTindaklanjutUsecase.On("SubmitTindaklanjutTandai", mock.Anything, mock.AnythingOfType("*models.RequestSubmitTindaklanjutTandai")).
		// 	Return(nil, models2.ErrBadParamInput.Error(), models2.ErrBadParamInput)

		e := echo.New()

		j, err := json.Marshal(requestBad)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, Positive1Tandai, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(Positive1Tandai)
		c.Request().ParseForm()
		handler := tindaklanjutHttp.TindaklanjutHandler{
			TindaklanjutUsecase: mockTindaklanjutUsecase,
			Log:                 l,
		}
		err = handler.SubmitTindaklanjutPositiveCase1Tandai(c)

		assert.Equal(t, http.StatusBadRequest, rec.Code)
		mockTindaklanjutUsecase.AssertExpectations(t)
	})

	t.Run("error-json-decode", func(t *testing.T) {
		//usecaseMock
		mockTindaklanjutUsecase := new(_tindaklanjutUsecaseMock.Usecase)

		// mockTindaklanjutUsecase.On("SubmitTindaklanjutTandai", mock.Anything, mock.AnythingOfType("*models.RequestSubmitTindaklanjutTandai")).
		// 	Return(nil, models2.ErrBadParamInput.Error(), models2.ErrBadParamInput)

		e := echo.New()

		// j, err := json.Marshal(request)
		// assert.NoError(t, err)
		var data []byte
		req, err := http.NewRequest(echo.POST, Positive1Tandai, bytes.NewReader(data))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(Positive1Tandai)
		c.Request().ParseForm()
		handler := tindaklanjutHttp.TindaklanjutHandler{
			TindaklanjutUsecase: mockTindaklanjutUsecase,
			Log:                 l,
		}
		err = handler.SubmitTindaklanjutPositiveCase1Tandai(c)

		assert.Equal(t, http.StatusBadRequest, rec.Code)
		mockTindaklanjutUsecase.AssertExpectations(t)
	})
}

func TestTindaklanjutHandler_SubmitTindaklanjutPositiveOtherCaseTandai(t *testing.T) {
	//requestMock
	request := &models.RequestSubmitTindaklanjutTandai{}
	request = request.MappingExampleData()
	requestBad := request.MappingExampleDataBadRequest()

	//resultMock
	mockResponseDetailNasabah := &models.ResponseDetailNasabah{}
	mockResponseDetailNasabah = mockResponseDetailNasabah.MappingExampleDataNil()

	t.Run("success", func(t *testing.T) {
		//usecaseMock
		mockTindaklanjutUsecase := new(_tindaklanjutUsecaseMock.Usecase)

		mockTindaklanjutUsecase.On("SubmitTindaklanjutTandai", mock.Anything, mock.AnythingOfType("*models.RequestSubmitTindaklanjutTandai")).
			Return(mockResponseDetailNasabah, models2.GeneralSuccess, nil)

		e := echo.New()

		j, err := json.Marshal(request)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, OtherCaseTandai, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(OtherCaseTandai)
		c.Request().ParseForm()
		handler := tindaklanjutHttp.TindaklanjutHandler{
			TindaklanjutUsecase: mockTindaklanjutUsecase,
			Log:                 l,
		}
		err = handler.SubmitTindaklanjutPositiveOtherCaseTandai(c)
		require.NoError(t, err)

		assert.Equal(t, http.StatusOK, rec.Code)
		mockTindaklanjutUsecase.AssertExpectations(t)
	})

	t.Run("error-500", func(t *testing.T) {
		//usecaseMock
		mockTindaklanjutUsecase := new(_tindaklanjutUsecaseMock.Usecase)

		mockTindaklanjutUsecase.On("SubmitTindaklanjutTandai", mock.Anything, mock.AnythingOfType("*models.RequestSubmitTindaklanjutTandai")).
			Return(nil, models2.ErrInternalServerError.Error(), models2.ErrInternalServerError)

		e := echo.New()

		j, err := json.Marshal(request)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, OtherCaseTandai, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(OtherCaseTandai)
		c.Request().ParseForm()
		handler := tindaklanjutHttp.TindaklanjutHandler{
			TindaklanjutUsecase: mockTindaklanjutUsecase,
			Log:                 l,
		}
		err = handler.SubmitTindaklanjutPositiveOtherCaseTandai(c)

		assert.Equal(t, http.StatusInternalServerError, rec.Code)
		mockTindaklanjutUsecase.AssertExpectations(t)
	})

	t.Run("error-400", func(t *testing.T) {
		//usecaseMock
		mockTindaklanjutUsecase := new(_tindaklanjutUsecaseMock.Usecase)

		// mockTindaklanjutUsecase.On("SubmitTindaklanjutTandai", mock.Anything, mock.AnythingOfType("*models.RequestSubmitTindaklanjutTandai")).
		// 	Return(nil, models2.ErrBadParamInput.Error(), models2.ErrBadParamInput)

		e := echo.New()

		j, err := json.Marshal(requestBad)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, OtherCaseTandai, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(OtherCaseTandai)
		c.Request().ParseForm()
		handler := tindaklanjutHttp.TindaklanjutHandler{
			TindaklanjutUsecase: mockTindaklanjutUsecase,
			Log:                 l,
		}
		err = handler.SubmitTindaklanjutPositiveOtherCaseTandai(c)

		assert.Equal(t, http.StatusBadRequest, rec.Code)
		mockTindaklanjutUsecase.AssertExpectations(t)
	})

	t.Run("error-json-decode", func(t *testing.T) {
		//usecaseMock
		mockTindaklanjutUsecase := new(_tindaklanjutUsecaseMock.Usecase)

		// mockTindaklanjutUsecase.On("SubmitTindaklanjutTandai", mock.Anything, mock.AnythingOfType("*models.RequestSubmitTindaklanjutTandai")).
		// 	Return(nil, models2.ErrBadParamInput.Error(), models2.ErrBadParamInput)

		e := echo.New()

		// j, err := json.Marshal(request)
		// assert.NoError(t, err)
		var data []byte
		req, err := http.NewRequest(echo.POST, OtherCaseTandai, bytes.NewReader(data))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(OtherCaseTandai)
		c.Request().ParseForm()
		handler := tindaklanjutHttp.TindaklanjutHandler{
			TindaklanjutUsecase: mockTindaklanjutUsecase,
			Log:                 l,
		}
		err = handler.SubmitTindaklanjutPositiveOtherCaseTandai(c)

		assert.Equal(t, http.StatusBadRequest, rec.Code)
		mockTindaklanjutUsecase.AssertExpectations(t)
	})
}

func TestTindaklanjutHandler_SubmitTindaklanjutNonBri(t *testing.T) {
	//requestMock
	request := &models.RequestSubmitTindaklanjutNonBri{}
	request = request.MappingExampleData()
	requestBad := request.MappingExampleDataBadRequest()

	//resultMock
	mockResponseDetailNasabah := &models.ResponseDetailNasabah{}
	mockResponseDetailNasabah = mockResponseDetailNasabah.MappingExampleDataNil()

	t.Run("success", func(t *testing.T) {
		//usecaseMock
		mockTindaklanjutUsecase := new(_tindaklanjutUsecaseMock.Usecase)

		mockTindaklanjutUsecase.On("SubmitTindaklanjutNonBri", mock.Anything, mock.AnythingOfType("*models.RequestSubmitTindaklanjutNonBri")).
			Return(mockResponseDetailNasabah, models2.GeneralSuccess, nil)

		e := echo.New()

		j, err := json.Marshal(request)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, CaseNonBri, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.Request().Header.Set("Content-Type", "UNIT-TEST")
		c.Request().Header.Set("Authorization", "UNIT_TEST")
		os.Setenv("AUDIT_TRAIL_URL", "https://api-dev.bribrain.com")
		c.SetPath(CaseNonBri)
		c.Request().ParseForm()
		handler := tindaklanjutHttp.TindaklanjutHandler{
			TindaklanjutUsecase: mockTindaklanjutUsecase,
			Log:                 l,
		}
		err = handler.SubmitTindaklanjutNonBri(c)
		require.NoError(t, err)

		assert.Equal(t, http.StatusOK, rec.Code)
		mockTindaklanjutUsecase.AssertExpectations(t)
	})

	t.Run("error-500", func(t *testing.T) {
		//usecaseMock
		mockTindaklanjutUsecase := new(_tindaklanjutUsecaseMock.Usecase)

		mockTindaklanjutUsecase.On("SubmitTindaklanjutNonBri", mock.Anything, mock.AnythingOfType("*models.RequestSubmitTindaklanjutNonBri")).
			Return(nil, models2.ErrInternalServerError.Error(), models2.ErrInternalServerError)

		e := echo.New()

		j, err := json.Marshal(request)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, CaseNonBri, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(CaseNonBri)
		c.Request().ParseForm()
		handler := tindaklanjutHttp.TindaklanjutHandler{
			TindaklanjutUsecase: mockTindaklanjutUsecase,
			Log:                 l,
		}
		err = handler.SubmitTindaklanjutNonBri(c)

		assert.Equal(t, http.StatusInternalServerError, rec.Code)
		mockTindaklanjutUsecase.AssertExpectations(t)
	})

	t.Run("error-400", func(t *testing.T) {
		//usecaseMock
		mockTindaklanjutUsecase := new(_tindaklanjutUsecaseMock.Usecase)

		// mockTindaklanjutUsecase.On("SubmitTindaklanjutNonBri", mock.Anything, mock.AnythingOfType("*models.RequestSubmitTindaklanjutNonBri")).
		// 	Return(nil, models2.ErrBadParamInput.Error(), models2.ErrBadParamInput)

		e := echo.New()

		j, err := json.Marshal(requestBad)
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, CaseNonBri, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(CaseNonBri)
		c.Request().ParseForm()
		handler := tindaklanjutHttp.TindaklanjutHandler{
			TindaklanjutUsecase: mockTindaklanjutUsecase,
			Log:                 l,
		}
		err = handler.SubmitTindaklanjutNonBri(c)

		assert.Equal(t, http.StatusBadRequest, rec.Code)
		mockTindaklanjutUsecase.AssertExpectations(t)
	})

	t.Run("error-json-decode", func(t *testing.T) {
		//usecaseMock
		mockTindaklanjutUsecase := new(_tindaklanjutUsecaseMock.Usecase)

		// mockTindaklanjutUsecase.On("SubmitTindaklanjutNonBri", mock.Anything, mock.AnythingOfType("*models.RequestSubmitTindaklanjutNonBri")).
		// 	Return(nil, models2.ErrBadParamInput.Error(), models2.ErrBadParamInput)

		e := echo.New()

		// j, err := json.Marshal(request)
		// assert.NoError(t, err)
		var data []byte
		req, err := http.NewRequest(echo.POST, CaseNonBri, bytes.NewReader(data))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(CaseNonBri)
		c.Request().ParseForm()
		handler := tindaklanjutHttp.TindaklanjutHandler{
			TindaklanjutUsecase: mockTindaklanjutUsecase,
			Log:                 l,
		}
		err = handler.SubmitTindaklanjutNonBri(c)

		assert.Equal(t, http.StatusBadRequest, rec.Code)
		mockTindaklanjutUsecase.AssertExpectations(t)
	})
}
