package http

import (
	"context"
	"encoding/json"
	"ms-bribrain-link5/helper"
	"ms-bribrain-link5/helper/logger"
	models2 "ms-bribrain-link5/helper/models"
	"ms-bribrain-link5/middleware"
	"ms-bribrain-link5/models"
	"ms-bribrain-link5/modules/tindaklanjut"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
)

type ResponseError struct {
	Message string `json:"message"`
}

type TindaklanjutHandler struct {
	TindaklanjutUsecase tindaklanjut.Usecase
	Log                 logger.Logger
}

func NewTindaklanjutHandler(e *echo.Echo, middL *middleware.GoMiddleware, us tindaklanjut.Usecase, log logger.Logger) {
	handler := &TindaklanjutHandler{
		Log:                 log,
		TindaklanjutUsecase: us,
	}

	e.POST("/submit_tindaklanjut_positive_1", handler.SubmitTindaklanjutPositiveCase1, middL.Authentication)
	e.POST("/submit_tindaklanjut_positive_2", handler.SubmitTindaklanjutPositiveCase2, middL.Authentication)
	e.POST("/submit_tindaklanjut_negative_1", handler.SubmitTindaklanjutNegativeCase1, middL.Authentication)
	e.POST("/submit_tindaklanjut_negative_2", handler.SubmitTindaklanjutNegativeCase2, middL.Authentication)
	e.POST("/submit_tindaklanjut_negative_3", handler.SubmitTindaklanjutNegativeCase3, middL.Authentication)
	e.POST("/submit_tindaklanjut_other", handler.SubmitTindaklanjutOtherCase, middL.Authentication)
	e.POST("/submit_tindaklanjut_positive_1_tandai", handler.SubmitTindaklanjutPositiveCase1Tandai, middL.Authentication)
	e.POST("/submit_tindaklanjut_other_tandai", handler.SubmitTindaklanjutOtherCaseTandai, middL.Authentication)
	e.POST("/submit_tindaklanjut_non_bri", handler.SubmitTindaklanjutNonBri, middL.Authentication)
	e.POST("/submit_ketertarikan_non_bri", handler.SubmitKetertarikanNonBri, middL.Authentication)
	e.POST("/cek_rm_cabang", handler.FindRmInBranch, middL.Authentication)

}

// SubmitTindaklanjutPositiveCase1 godoc
// @Summary Submit Tindaklanjut Positive Case 1
// @Description Submit Tindaklanjut Positive Case 1 (sudah dihubungi, tertarik, akan dikunjungi)
// @Tags SubmitTindakLanjut
// @ID submit-tindaklanjut-positive-case-1
// @Accept  application-bribrain/json
// @Produce  json
// @Param Content-Type header string true "Content-Type" default(application-bribrain/json)
// @Param Authorization header string true "token"
// @Param request body models.SwaggerRequestSubmitTindaklanjutPositive1 true "body"
// @Success 200 {object} models.DefaultResponseSubmitTindaklanjut
// @Failure 401 {object} models.SwaggerErrorExpiredTokenUnAuthorizeAndTokenUnAuthorize
// @Failure 403 {object} models.SwaggerErrorTokenForbiddenPermission
// @Failure 405 {object} models.SwaggerErrorMethodNotAllowed
// @Failure 500 {object} models.SwaggerErrorInternalServerError
// @Failure 400 {object} models.SwaggerErrorBadParamInput
// @Router /submit_tindaklanjut_positive_1 [post]
func (a *TindaklanjutHandler) SubmitTindaklanjutPositiveCase1(c echo.Context) error {
	log := "tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutPositiveCase1: %s"
	response := new(models2.Response)
	requestPositiveCase1 := new(models.RequestSubmitTindaklanjutPositive1)
	// userLogin := c.Get("JWT_CLAIMS").(*models.BRIBrainClaims)

	userLoginTLPositive1 := c.Get("JWT_CLAIMS").(*models.BRIBrainClaims)
	userAuditTrailTLPositive1 := models.BribrainAuditTrail{}
	payloadAuditTrailTLPositive1 := models.PayloadBribrainAuditTrail{}
	headersAuditTrailTLPositive1 := models.HeadersPayloadBribrainAuditTrail{}

	contentTypeHeaderTLPositive1 := c.Request().Header.Get(echo.HeaderContentType)
	authorizationHeaderTLPositive1 := c.Request().Header.Get(echo.HeaderAuthorization)
	urlpayloadTLPositive1 := c.Request().Host + c.Request().URL.Path
	headersAuditTrailTLPositive1 = headersAuditTrailTLPositive1.MappingHeadersPayloadBribrainAuditTrail(contentTypeHeaderTLPositive1, authorizationHeaderTLPositive1)

	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	dec, err := helper.JsonDecode(c, requestPositiveCase1)
	if err != nil {
		response.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), err.Error(), err, c)
		return c.JSON(response.StatusCode, response)
	}
	requestPositiveCase1 = dec.(*models.RequestSubmitTindaklanjutPositive1)
	validate := validator.New()

	reqbodyTLPositive1, err := json.Marshal(requestPositiveCase1)
	if err != nil {
		a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutPositive1.userAuditTrail: %s", err.Error())
		// return nil, nil, err
	}

	errvalidate := validate.Struct(requestPositiveCase1)
	if errvalidate != nil {
		errID := time.Now().Format(helper.DateTimeFormatDefault) + " " + helper.RandomString(20)
		a.Log.Error("["+errID+"]  "+log, errvalidate.Error())
		response.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), models2.ErrInvalidValue.Error(), models2.ErrInvalidValue.Error(), c)

		remarkAuditTrailTLPositive1 := helper.IntToString(response.StatusCode) + " " + errvalidate.Error()
		payloadAuditTrailTLPositive1 = payloadAuditTrailTLPositive1.MappingPayloadBribrainAuditTrail(urlpayloadTLPositive1, headersAuditTrailTLPositive1, string(reqbodyTLPositive1), helper.JsonString(response), response.StatusCode)
		userAuditTrailTLPositive1 = userAuditTrailTLPositive1.MappingBribrainAuditTrail("Submit Tindaklanjut Positive 1", remarkAuditTrailTLPositive1, userLoginTLPositive1.Pernr, userLoginTLPositive1.Role, helper.JsonString(payloadAuditTrailTLPositive1))
		_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailTLPositive1)
		if errAuditTrail != nil {
			a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutNonBri.userAuditTrailTLPositive1: %s", err.Error())

		}

		return c.JSON(response.StatusCode, response)
	}
	// validation := requestPositiveCase1.MappingToGlobalValidation()
	// checkQueryparams, message := helper.GlobalValidationQueryParams(validation)
	// if checkQueryparams == false {
	// 	response.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), message, message)
	// 	return c.JSON(response.StatusCode, response)
	// }

	result, message, errPositive1 := a.TindaklanjutUsecase.SubmitTindaklanjutPositiveCase1(ctx, requestPositiveCase1)
	if errPositive1 != nil {
		errPositive1ID := time.Now().Format(helper.DateTimeFormatDefault) + " " + helper.RandomString(20)
		a.Log.Error("["+errPositive1ID+"]  "+log, errPositive1.Error())
		response.MappingResponseError(helper.GetStatusCode(errPositive1), message, errPositive1ID, c)

		remarkAuditTrailTLPositive1 := helper.IntToString(response.StatusCode) + " " + errPositive1.Error()
		payloadAuditTrailTLPositive1 = payloadAuditTrailTLPositive1.MappingPayloadBribrainAuditTrail(urlpayloadTLPositive1, headersAuditTrailTLPositive1, string(reqbodyTLPositive1), helper.JsonString(response), response.StatusCode)
		userAuditTrailTLPositive1 = userAuditTrailTLPositive1.MappingBribrainAuditTrail("Submit Tindaklanjut Positive 1", remarkAuditTrailTLPositive1, userLoginTLPositive1.Pernr, userLoginTLPositive1.Role, helper.JsonString(payloadAuditTrailTLPositive1))
		_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailTLPositive1)
		if errAuditTrail != nil {
			a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutNonBri.userAuditTrailTLPositive1: %s", err.Error())

		}

		return c.JSON(response.StatusCode, response)
	}

	a.Log.Info(log, helper.JsonString(result))

	response.MappingResponseSuccess(message, result, c)

	payloadAuditTrailTLPositive1 = payloadAuditTrailTLPositive1.MappingPayloadBribrainAuditTrail(urlpayloadTLPositive1, headersAuditTrailTLPositive1, string(reqbodyTLPositive1), helper.JsonString(response), response.StatusCode)
	userAuditTrailTLPositive1 = userAuditTrailTLPositive1.MappingBribrainAuditTrail("Submit Tindaklanjut Positive 1", "200 Success", userLoginTLPositive1.Pernr, userLoginTLPositive1.Role, helper.JsonString(payloadAuditTrailTLPositive1))
	_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailTLPositive1)
	if errAuditTrail != nil {
		a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutNonBri.userAuditTrailTLPositive1: %s", err.Error())

	}

	return c.JSON(response.StatusCode, response)
}

// SubmitTindaklanjutPositiveCase2 godoc
// @Summary Submit Tindaklanjut Positive Case 2
// @Description Submit Tindaklanjut Positive Case 2 (sudah dihubungi, tertarik, tidak akan mengunjungi)
// @Tags SubmitTindakLanjut
// @ID submit-tindaklanjut-positive-case-2
// @Accept  application-bribrain/json
// @Produce  json
// @Param Content-Type header string true "Content-Type" default(application-bribrain/json)
// @Param Authorization header string true "token"
// @Param request body models.SwaggerRequestSubmitTindaklanjutPositive2 true "body"
// @Success 200 {object} models.DefaultResponseSubmitTindaklanjut
// @Failure 401 {object} models.SwaggerErrorExpiredTokenUnAuthorizeAndTokenUnAuthorize
// @Failure 403 {object} models.SwaggerErrorTokenForbiddenPermission
// @Failure 405 {object} models.SwaggerErrorMethodNotAllowed
// @Failure 500 {object} models.SwaggerErrorInternalServerError
// @Failure 400 {object} models.SwaggerErrorBadParamInput
// @Router /submit_tindaklanjut_positive_2 [post]
func (a *TindaklanjutHandler) SubmitTindaklanjutPositiveCase2(c echo.Context) error {
	log := "tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutPositiveCase2: %s"
	response := new(models2.Response)
	request := new(models.RequestSubmitTindaklanjutPositive2)
	// userLogin := c.Get("JWT_CLAIMS").(*models.BRIBrainClaims)

	userLoginTLCase2 := c.Get("JWT_CLAIMS").(*models.BRIBrainClaims)
	userAuditTrailTLCase2 := models.BribrainAuditTrail{}
	payloadAuditTrailTLCase2 := models.PayloadBribrainAuditTrail{}
	headersAuditTrailTLCase2 := models.HeadersPayloadBribrainAuditTrail{}

	contentTypeHeaderTLCase2 := c.Request().Header.Get(echo.HeaderContentType)
	authorizationHeaderTLCase2 := c.Request().Header.Get(echo.HeaderAuthorization)
	urlpayloadTLCase2 := c.Request().Host + c.Request().URL.Path
	headersAuditTrailTLCase2 = headersAuditTrailTLCase2.MappingHeadersPayloadBribrainAuditTrail(contentTypeHeaderTLCase2, authorizationHeaderTLCase2)

	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	dec, err := helper.JsonDecode(c, request)
	if err != nil {
		response.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), err.Error(), err, c)
		return c.JSON(response.StatusCode, response)
	}
	request = dec.(*models.RequestSubmitTindaklanjutPositive2)
	validate := validator.New()

	reqbodyTLCase2, err := json.Marshal(request)
	if err != nil {
		a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutPositive2.userAuditTrail: %s", err.Error())
		// return nil, nil, err
	}

	errvalidate := validate.Struct(request)
	if errvalidate != nil {
		errID := time.Now().Format(helper.DateTimeFormatDefault) + " " + helper.RandomString(20)
		a.Log.Error("["+errID+"]  "+log, errvalidate.Error())
		response.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), models2.ErrInvalidValue.Error(), models2.ErrInvalidValue.Error(), c)

		remarkAuditTrailTLCase2 := helper.IntToString(response.StatusCode) + " " + errvalidate.Error()
		payloadAuditTrailTLCase2 = payloadAuditTrailTLCase2.MappingPayloadBribrainAuditTrail(urlpayloadTLCase2, headersAuditTrailTLCase2, string(reqbodyTLCase2), helper.JsonString(response), response.StatusCode)
		userAuditTrailTLCase2 = userAuditTrailTLCase2.MappingBribrainAuditTrail("Submit Tindaklanjut Positive 2", remarkAuditTrailTLCase2, userLoginTLCase2.Pernr, userLoginTLCase2.Role, helper.JsonString(payloadAuditTrailTLCase2))
		_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailTLCase2)
		if errAuditTrail != nil {
			a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutPositive2.userAuditTrailTLCase2: %s", err.Error())

		}

		return c.JSON(response.StatusCode, response)
	}
	// validation := request.MappingToGlobalValidation()
	// checkQueryparams, message := helper.GlobalValidationQueryParams(validation)
	// if checkQueryparams == false {
	// 	response.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), message, message)
	// 	return c.JSON(response.StatusCode, response)
	// }

	result, message, errorCase2 := a.TindaklanjutUsecase.SubmitTindaklanjutPositiveCase2(ctx, request)
	if errorCase2 != nil {
		errorCase2ID := time.Now().Format(helper.DateTimeFormatDefault) + " " + helper.RandomString(20)
		a.Log.Error("["+errorCase2ID+"]  "+log, errorCase2.Error())
		response.MappingResponseError(helper.GetStatusCode(errorCase2), message, errorCase2ID, c)

		remarkAuditTrailTLCase2 := helper.IntToString(response.StatusCode) + " " + errorCase2.Error()
		payloadAuditTrailTLCase2 = payloadAuditTrailTLCase2.MappingPayloadBribrainAuditTrail(urlpayloadTLCase2, headersAuditTrailTLCase2, string(reqbodyTLCase2), helper.JsonString(response), response.StatusCode)
		userAuditTrailTLCase2 = userAuditTrailTLCase2.MappingBribrainAuditTrail("Submit Tindaklanjut Positive 2", remarkAuditTrailTLCase2, userLoginTLCase2.Pernr, userLoginTLCase2.Role, helper.JsonString(payloadAuditTrailTLCase2))
		_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailTLCase2)
		if errAuditTrail != nil {
			a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutPositive2.userAuditTrailTLCase2: %s", err.Error())

		}

		return c.JSON(response.StatusCode, response)
	}

	a.Log.Info(log, helper.JsonString(result))

	response.MappingResponseSuccess(message, result, c)

	payloadAuditTrailTLCase2 = payloadAuditTrailTLCase2.MappingPayloadBribrainAuditTrail(urlpayloadTLCase2, headersAuditTrailTLCase2, string(reqbodyTLCase2), helper.JsonString(response), response.StatusCode)
	userAuditTrailTLCase2 = userAuditTrailTLCase2.MappingBribrainAuditTrail("Submit Tindaklanjut Positive 2", "200 Success", userLoginTLCase2.Pernr, userLoginTLCase2.Role, helper.JsonString(payloadAuditTrailTLCase2))
	_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailTLCase2)
	if errAuditTrail != nil {
		a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutPositive2.userAuditTrailTLCase2: %s", err.Error())

	}

	return c.JSON(response.StatusCode, response)
}

// SubmitTindaklanjutNegativeCase1 godoc
// @Summary Submit Tindaklanjut Negative Case 1
// @Description Submit Tindaklanjut Negative Case 1 (sudah dihubungi, tidak tertarik)
// @Tags SubmitTindakLanjut
// @ID submit-tindaklanjut-negative-case-1
// @Accept  application-bribrain/json
// @Produce  json
// @Param Content-Type header string true "Content-Type" default(application-bribrain/json)
// @Param Authorization header string true "token"
// @Param request body models.SwaggerRequestSubmitTindaklanjutNegative1 true "body"
// @Success 200 {object} models.DefaultResponseSubmitTindaklanjut
// @Failure 401 {object} models.SwaggerErrorExpiredTokenUnAuthorizeAndTokenUnAuthorize
// @Failure 403 {object} models.SwaggerErrorTokenForbiddenPermission
// @Failure 405 {object} models.SwaggerErrorMethodNotAllowed
// @Failure 500 {object} models.SwaggerErrorInternalServerError
// @Failure 400 {object} models.SwaggerErrorBadParamInput
// @Router /submit_tindaklanjut_negative_1 [post]
func (a *TindaklanjutHandler) SubmitTindaklanjutNegativeCase1(c echo.Context) error {
	log := "tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutNegativeCase1: %s"
	response := new(models2.Response)
	request := new(models.RequestSubmitTindaklanjutNegative1)
	// userLogin := c.Get("JWT_CLAIMS").(*models.BRIBrainClaims)

	userLoginTLNegative1 := c.Get("JWT_CLAIMS").(*models.BRIBrainClaims)
	userAuditTrailTLNegative1 := models.BribrainAuditTrail{}
	payloadAuditTrailTLNegative1 := models.PayloadBribrainAuditTrail{}
	headersAuditTrailTLNegative1 := models.HeadersPayloadBribrainAuditTrail{}

	contentTypeHeaderTLNegative1 := c.Request().Header.Get(echo.HeaderContentType)
	authorizationHeaderTLNegative1 := c.Request().Header.Get(echo.HeaderAuthorization)
	urlpayloadTLNegative1 := c.Request().Host + c.Request().URL.Path
	headersAuditTrailTLNegative1 = headersAuditTrailTLNegative1.MappingHeadersPayloadBribrainAuditTrail(contentTypeHeaderTLNegative1, authorizationHeaderTLNegative1)

	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	dec, err := helper.JsonDecode(c, request)
	if err != nil {
		response.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), err.Error(), err, c)
		return c.JSON(response.StatusCode, response)
	}
	request = dec.(*models.RequestSubmitTindaklanjutNegative1)
	validate := validator.New()

	reqbodyTLNegative2, err := json.Marshal(request)
	if err != nil {
		a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutNegative1.userAuditTrail: %s", err.Error())
		// return nil, nil, err
	}

	errvalidate := validate.Struct(request)
	if errvalidate != nil {
		errID := time.Now().Format(helper.DateTimeFormatDefault) + " " + helper.RandomString(20)
		a.Log.Error("["+errID+"]  "+log, errvalidate.Error())
		response.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), models2.ErrInvalidValue.Error(), models2.ErrInvalidValue.Error(), c)

		remarkAuditTrailTLNegative1 := helper.IntToString(response.StatusCode) + " " + errvalidate.Error()
		payloadAuditTrailTLNegative1 = payloadAuditTrailTLNegative1.MappingPayloadBribrainAuditTrail(urlpayloadTLNegative1, headersAuditTrailTLNegative1, string(reqbodyTLNegative2), helper.JsonString(response), response.StatusCode)
		userAuditTrailTLNegative1 = userAuditTrailTLNegative1.MappingBribrainAuditTrail("Submit Tindaklanjut Negative 1", remarkAuditTrailTLNegative1, userLoginTLNegative1.Pernr, userLoginTLNegative1.Role, helper.JsonString(payloadAuditTrailTLNegative1))
		_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailTLNegative1)
		if errAuditTrail != nil {
			a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutNegative1.userAuditTrail: %s", err.Error())

		}

		return c.JSON(response.StatusCode, response)
	}
	// validation := request.MappingToGlobalValidation()
	// checkQueryparams, message := helper.GlobalValidationQueryParams(validation)
	// if checkQueryparams == false {
	// 	response.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), message, message)
	// 	return c.JSON(response.StatusCode, response)
	// }

	result, message, errNegative1 := a.TindaklanjutUsecase.SubmitTindaklanjutNegativeCase1(ctx, request)
	if errNegative1 != nil {
		errNegative1ID := time.Now().Format(helper.DateTimeFormatDefault) + " " + helper.RandomString(20)
		a.Log.Error("["+errNegative1ID+"]  "+log, errNegative1.Error())
		response.MappingResponseError(helper.GetStatusCode(errNegative1), message, errNegative1ID, c)

		remarkAuditTrailTLNegative1 := helper.IntToString(response.StatusCode) + " " + errNegative1.Error()
		payloadAuditTrailTLNegative1 = payloadAuditTrailTLNegative1.MappingPayloadBribrainAuditTrail(urlpayloadTLNegative1, headersAuditTrailTLNegative1, string(reqbodyTLNegative2), helper.JsonString(response), response.StatusCode)
		userAuditTrailTLNegative1 = userAuditTrailTLNegative1.MappingBribrainAuditTrail("Submit Tindaklanjut Negative 1", remarkAuditTrailTLNegative1, userLoginTLNegative1.Pernr, userLoginTLNegative1.Role, helper.JsonString(payloadAuditTrailTLNegative1))
		_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailTLNegative1)
		if errAuditTrail != nil {
			a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutNegative1.userAuditTrail: %s", err.Error())

		}

		return c.JSON(response.StatusCode, response)
	}

	a.Log.Info(log, helper.JsonString(result))

	response.MappingResponseSuccess(message, result, c)

	payloadAuditTrailTLNegative1 = payloadAuditTrailTLNegative1.MappingPayloadBribrainAuditTrail(urlpayloadTLNegative1, headersAuditTrailTLNegative1, string(reqbodyTLNegative2), helper.JsonString(response), response.StatusCode)
	userAuditTrailTLNegative1 = userAuditTrailTLNegative1.MappingBribrainAuditTrail("Submit Tindaklanjut Negative 1", "200 Success", userLoginTLNegative1.Pernr, userLoginTLNegative1.Role, helper.JsonString(payloadAuditTrailTLNegative1))
	_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailTLNegative1)
	if errAuditTrail != nil {
		a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutNegative1.userAuditTrail: %s", err.Error())

	}

	return c.JSON(response.StatusCode, response)
}

// SubmitTindaklanjutNegativeCase2 godoc
// @Summary Submit Tindaklanjut Negative Case 2
// @Description Submit Tindaklanjut Negative Case 2 (tidak dapat dihubungi, tidak akan mengunjungi)
// @Tags SubmitTindakLanjut
// @ID submit-tindaklanjut-negative-case-2
// @Accept  application-bribrain/json
// @Produce  json
// @Param Content-Type header string true "Content-Type" default(application-bribrain/json)
// @Param Authorization header string true "token"
// @Param request body models.SwaggerRequestSubmitTindaklanjutNegative2 true "body"
// @Success 200 {object} models.DefaultResponseSubmitTindaklanjut
// @Failure 401 {object} models.SwaggerErrorExpiredTokenUnAuthorizeAndTokenUnAuthorize
// @Failure 403 {object} models.SwaggerErrorTokenForbiddenPermission
// @Failure 405 {object} models.SwaggerErrorMethodNotAllowed
// @Failure 500 {object} models.SwaggerErrorInternalServerError
// @Failure 400 {object} models.SwaggerErrorBadParamInput
// @Router /submit_tindaklanjut_negative_2 [post]
func (a *TindaklanjutHandler) SubmitTindaklanjutNegativeCase2(c echo.Context) error {
	log := "tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutNegativeCase2: %s"
	response := new(models2.Response)
	request := new(models.RequestSubmitTindaklanjutNegative2)
	// userLogin := c.Get("JWT_CLAIMS").(*models.BRIBrainClaims)
	userLoginTLNegCase2 := c.Get("JWT_CLAIMS").(*models.BRIBrainClaims)
	userAuditTrailTLNegCase2 := models.BribrainAuditTrail{}
	payloadAuditTrailTLNegCase2 := models.PayloadBribrainAuditTrail{}
	headersAuditTrailTLNegCase2 := models.HeadersPayloadBribrainAuditTrail{}

	contentTypeHeaderTLNegCase2 := c.Request().Header.Get(echo.HeaderContentType)
	authorizationHeaderTLNegCase2 := c.Request().Header.Get(echo.HeaderAuthorization)
	urlpayloadTLNegCase2 := c.Request().Host + c.Request().URL.Path
	headersAuditTrailTLNegCase2 = headersAuditTrailTLNegCase2.MappingHeadersPayloadBribrainAuditTrail(contentTypeHeaderTLNegCase2, authorizationHeaderTLNegCase2)

	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	dec, err := helper.JsonDecode(c, request)
	if err != nil {
		response.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), err.Error(), err, c)
		return c.JSON(response.StatusCode, response)
	}
	request = dec.(*models.RequestSubmitTindaklanjutNegative2)
	validate := validator.New()

	reqbodyTLNegCase2, err := json.Marshal(request)
	if err != nil {
		a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutNegativeCase2.userAuditTrail: %s", err.Error())
		// return nil, nil, err
	}

	errvalidate := validate.Struct(request)
	if errvalidate != nil {
		errID := time.Now().Format(helper.DateTimeFormatDefault) + " " + helper.RandomString(20)
		a.Log.Error("["+errID+"]  "+log, errvalidate.Error())
		response.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), models2.ErrInvalidValue.Error(), models2.ErrInvalidValue.Error(), c)

		remarkAuditTrailTLNegCase2 := helper.IntToString(response.StatusCode) + " " + errvalidate.Error()
		payloadAuditTrailTLNegCase2 = payloadAuditTrailTLNegCase2.MappingPayloadBribrainAuditTrail(urlpayloadTLNegCase2, headersAuditTrailTLNegCase2, string(reqbodyTLNegCase2), helper.JsonString(response), response.StatusCode)
		userAuditTrailTLNegCase2 = userAuditTrailTLNegCase2.MappingBribrainAuditTrail("Submit Tindaklanjut Negative 2", remarkAuditTrailTLNegCase2, userLoginTLNegCase2.Pernr, userLoginTLNegCase2.Role, helper.JsonString(payloadAuditTrailTLNegCase2))
		_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailTLNegCase2)
		if errAuditTrail != nil {
			a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutNegativeCase2.userAuditTrail: %s", err.Error())

		}

		return c.JSON(response.StatusCode, response)
	}
	// validation := request.MappingToGlobalValidation()
	// checkQueryparams, message := helper.GlobalValidationQueryParams(validation)
	// if checkQueryparams == false {
	// 	response.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), message, message)
	// 	return c.JSON(response.StatusCode, response)
	// }

	result, message, errCaseNegative2 := a.TindaklanjutUsecase.SubmitTindaklanjutNegativeCase2(ctx, request)
	if errCaseNegative2 != nil {
		errCaseNegative2ID := time.Now().Format(helper.DateTimeFormatDefault) + " " + helper.RandomString(20)
		a.Log.Error("["+errCaseNegative2ID+"]  "+log, errCaseNegative2.Error())
		response.MappingResponseError(helper.GetStatusCode(errCaseNegative2), message, errCaseNegative2ID, c)

		remarkAuditTrailTLNegCase2 := helper.IntToString(response.StatusCode) + " " + errCaseNegative2.Error()
		payloadAuditTrailTLNegCase2 = payloadAuditTrailTLNegCase2.MappingPayloadBribrainAuditTrail(urlpayloadTLNegCase2, headersAuditTrailTLNegCase2, string(reqbodyTLNegCase2), helper.JsonString(response), response.StatusCode)
		userAuditTrailTLNegCase2 = userAuditTrailTLNegCase2.MappingBribrainAuditTrail("Submit Tindaklanjut Negative 2", remarkAuditTrailTLNegCase2, userLoginTLNegCase2.Pernr, userLoginTLNegCase2.Role, helper.JsonString(payloadAuditTrailTLNegCase2))
		_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailTLNegCase2)
		if errAuditTrail != nil {
			a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutNegativeCase2.userAuditTrail: %s", err.Error())

		}

		return c.JSON(response.StatusCode, response)
	}

	a.Log.Info(log, helper.JsonString(result))

	response.MappingResponseSuccess(message, result, c)

	payloadAuditTrailTLNegCase2 = payloadAuditTrailTLNegCase2.MappingPayloadBribrainAuditTrail(urlpayloadTLNegCase2, headersAuditTrailTLNegCase2, string(reqbodyTLNegCase2), helper.JsonString(response), response.StatusCode)
	userAuditTrailTLNegCase2 = userAuditTrailTLNegCase2.MappingBribrainAuditTrail("Submit Tindaklanjut Negative 2", "200 Success", userLoginTLNegCase2.Pernr, userLoginTLNegCase2.Role, helper.JsonString(payloadAuditTrailTLNegCase2))
	_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailTLNegCase2)
	if errAuditTrail != nil {
		a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutNegativeCase2.userAuditTrail: %s", err.Error())

	}

	return c.JSON(response.StatusCode, response)
}

// SubmitTindaklanjutNegativeCase3 godoc
// @Summary Submit Tindaklanjut Negative Case 3
// @Description Submit Tindaklanjut Negative Case 3 (tidak cocok)
// @Tags SubmitTindakLanjut
// @ID submit-tindaklanjut-negative-case-3
// @Accept  application-bribrain/json
// @Produce  json
// @Param Content-Type header string true "Content-Type" default(application-bribrain/json)
// @Param Authorization header string true "token"
// @Param request body models.SwaggerRequestSubmitTindaklanjutNegative3 true "body"
// @Success 200 {object} models.DefaultResponseSubmitTindaklanjut
// @Failure 401 {object} models.SwaggerErrorExpiredTokenUnAuthorizeAndTokenUnAuthorize
// @Failure 403 {object} models.SwaggerErrorTokenForbiddenPermission
// @Failure 405 {object} models.SwaggerErrorMethodNotAllowed
// @Failure 500 {object} models.SwaggerErrorInternalServerError
// @Failure 400 {object} models.SwaggerErrorBadParamInput
// @Router /submit_tindaklanjut_negative_3 [post]
func (a *TindaklanjutHandler) SubmitTindaklanjutNegativeCase3(c echo.Context) error {
	log := "tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutNegativeCase3: %s"
	response := new(models2.Response)
	request := new(models.RequestSubmitTindaklanjutNegative3)
	// userLogin := c.Get("JWT_CLAIMS").(*models.BRIBrainClaims)

	userLoginTLNegativeCase3 := c.Get("JWT_CLAIMS").(*models.BRIBrainClaims)
	userAuditTrailTLNegativeCase3 := models.BribrainAuditTrail{}
	payloadAuditTrailTLNegativeCase3 := models.PayloadBribrainAuditTrail{}
	headersAuditTrailTLNegativeCase3 := models.HeadersPayloadBribrainAuditTrail{}

	contentTypeHeaderTLNegativeCase3 := c.Request().Header.Get(echo.HeaderContentType)
	authorizationHeaderTLNegativeCase3 := c.Request().Header.Get(echo.HeaderAuthorization)
	urlpayloadTLNegativeCase3 := c.Request().Host + c.Request().URL.Path
	headersAuditTrailTLNegativeCase3 = headersAuditTrailTLNegativeCase3.MappingHeadersPayloadBribrainAuditTrail(contentTypeHeaderTLNegativeCase3, authorizationHeaderTLNegativeCase3)

	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	dec, err := helper.JsonDecode(c, request)
	if err != nil {
		response.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), err.Error(), err, c)
		return c.JSON(response.StatusCode, response)
	}
	request = dec.(*models.RequestSubmitTindaklanjutNegative3)
	validate := validator.New()

	reqbodyTLNegativeCase3, err := json.Marshal(request)
	if err != nil {
		a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutNegative3.userAuditTrail: %s", err.Error())
		// return nil, nil, err
	}

	errvalidate := validate.Struct(request)
	if errvalidate != nil {
		errID := time.Now().Format(helper.DateTimeFormatDefault) + " " + helper.RandomString(20)
		a.Log.Error("["+errID+"]  "+log, errvalidate.Error())
		response.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), models2.ErrInvalidValue.Error(), models2.ErrInvalidValue.Error(), c)

		remarkAuditTrailTLNegativeCase3 := helper.IntToString(response.StatusCode) + " " + errvalidate.Error()
		payloadAuditTrailTLNegativeCase3 = payloadAuditTrailTLNegativeCase3.MappingPayloadBribrainAuditTrail(urlpayloadTLNegativeCase3, headersAuditTrailTLNegativeCase3, string(reqbodyTLNegativeCase3), helper.JsonString(response), response.StatusCode)
		userAuditTrailTLNegativeCase3 = userAuditTrailTLNegativeCase3.MappingBribrainAuditTrail("Submit Tindaklanjut Negative 3", remarkAuditTrailTLNegativeCase3, userLoginTLNegativeCase3.Pernr, userLoginTLNegativeCase3.Role, helper.JsonString(payloadAuditTrailTLNegativeCase3))
		_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailTLNegativeCase3)
		if errAuditTrail != nil {
			a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutNegative3.userAuditTrail: %s", err.Error())

		}

		return c.JSON(response.StatusCode, response)
	}
	// validation := request.MappingToGlobalValidation()
	// checkQueryparams, message := helper.GlobalValidationQueryParams(validation)
	// if checkQueryparams == false {
	// 	response.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), message, message)
	// 	return c.JSON(response.StatusCode, response)
	// }

	result, message, errTLNegative3 := a.TindaklanjutUsecase.SubmitTindaklanjutNegativeCase3(ctx, request)
	if errTLNegative3 != nil {
		errTLNegative3ID := time.Now().Format(helper.DateTimeFormatDefault) + " " + helper.RandomString(20)
		a.Log.Error("["+errTLNegative3ID+"]  "+log, errTLNegative3.Error())
		response.MappingResponseError(helper.GetStatusCode(errTLNegative3), message, errTLNegative3ID, c)

		remarkAuditTrailTLNegativeCase3 := helper.IntToString(response.StatusCode) + " " + errTLNegative3.Error()
		payloadAuditTrailTLNegativeCase3 = payloadAuditTrailTLNegativeCase3.MappingPayloadBribrainAuditTrail(urlpayloadTLNegativeCase3, headersAuditTrailTLNegativeCase3, string(reqbodyTLNegativeCase3), helper.JsonString(response), response.StatusCode)
		userAuditTrailTLNegativeCase3 = userAuditTrailTLNegativeCase3.MappingBribrainAuditTrail("Submit Tindaklanjut Negative 3", remarkAuditTrailTLNegativeCase3, userLoginTLNegativeCase3.Pernr, userLoginTLNegativeCase3.Role, helper.JsonString(payloadAuditTrailTLNegativeCase3))
		_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailTLNegativeCase3)
		if errAuditTrail != nil {
			a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutNegative3.userAuditTrail: %s", err.Error())

		}

		return c.JSON(response.StatusCode, response)
	}

	a.Log.Info(log, helper.JsonString(result))

	response.MappingResponseSuccess(message, result, c)

	payloadAuditTrailTLNegativeCase3 = payloadAuditTrailTLNegativeCase3.MappingPayloadBribrainAuditTrail(urlpayloadTLNegativeCase3, headersAuditTrailTLNegativeCase3, string(reqbodyTLNegativeCase3), helper.JsonString(response), response.StatusCode)
	userAuditTrailTLNegativeCase3 = userAuditTrailTLNegativeCase3.MappingBribrainAuditTrail("Submit Tindaklanjut Negative 3", "200 Success", userLoginTLNegativeCase3.Pernr, userLoginTLNegativeCase3.Role, helper.JsonString(payloadAuditTrailTLNegativeCase3))
	_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailTLNegativeCase3)
	if errAuditTrail != nil {
		a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutNegative3.userAuditTrail: %s", err.Error())

	}

	return c.JSON(response.StatusCode, response)
}

// SubmitTindaklanjutOtherCase godoc
// @Summary Submit Tindaklanjut Other Case
// @Description Submit Tindaklanjut Other Case (tidak dapat dihubungi, saya akan mengunjungi)
// @Tags SubmitTindakLanjut
// @ID submit-tindaklanjut-other-case
// @Accept  application-bribrain/json
// @Produce  json
// @Param Content-Type header string true "Content-Type" default(application-bribrain/json)
// @Param Authorization header string true "token"
// @Param request body models.SwaggerRequestSubmitTindaklanjutOther true "body"
// @Success 200 {object} models.DefaultResponseSubmitTindaklanjut
// @Failure 401 {object} models.SwaggerErrorExpiredTokenUnAuthorizeAndTokenUnAuthorize
// @Failure 403 {object} models.SwaggerErrorTokenForbiddenPermission
// @Failure 405 {object} models.SwaggerErrorMethodNotAllowed
// @Failure 500 {object} models.SwaggerErrorInternalServerError
// @Failure 400 {object} models.SwaggerErrorBadParamInput
// @Router /submit_tindaklanjut_other [post]
func (a *TindaklanjutHandler) SubmitTindaklanjutOtherCase(c echo.Context) error {
	log := "tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutOtherCase: %s"
	response := new(models2.Response)
	request := new(models.RequestSubmitTindaklanjutOther)
	// userLogin := c.Get("JWT_CLAIMS").(*models.BRIBrainClaims)

	userLoginTLOtherCase := c.Get("JWT_CLAIMS").(*models.BRIBrainClaims)
	userAuditTrailTLOtherCase := models.BribrainAuditTrail{}
	payloadAuditTrailTLOtherCase := models.PayloadBribrainAuditTrail{}
	headersAuditTrailTLOtherCase := models.HeadersPayloadBribrainAuditTrail{}

	contentTypeHeaderTLOtherCase := c.Request().Header.Get(echo.HeaderContentType)
	authorizationHeaderTLOtherCase := c.Request().Header.Get(echo.HeaderAuthorization)
	urlpayloadTLOtherCase := c.Request().Host + c.Request().URL.Path
	headersAuditTrailTLOtherCase = headersAuditTrailTLOtherCase.MappingHeadersPayloadBribrainAuditTrail(contentTypeHeaderTLOtherCase, authorizationHeaderTLOtherCase)

	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	dec, err := helper.JsonDecode(c, request)
	if err != nil {
		response.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), err.Error(), err, c)
		return c.JSON(response.StatusCode, response)
	}
	request = dec.(*models.RequestSubmitTindaklanjutOther)
	validate := validator.New()

	reqbodyTLOtherCase, err := json.Marshal(request)
	if err != nil {
		a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutOtherCase.userAuditTrail: %s", err.Error())
		// return nil, nil, err
	}

	errvalidate := validate.Struct(request)
	if errvalidate != nil {
		errID := time.Now().Format(helper.DateTimeFormatDefault) + " " + helper.RandomString(20)
		a.Log.Error("["+errID+"]  "+log, errvalidate.Error())
		response.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), models2.ErrInvalidValue.Error(), models2.ErrInvalidValue.Error(), c)

		remarkAuditTrailTLOtherCase := helper.IntToString(response.StatusCode) + " " + errvalidate.Error()
		payloadAuditTrailTLOtherCase = payloadAuditTrailTLOtherCase.MappingPayloadBribrainAuditTrail(urlpayloadTLOtherCase, headersAuditTrailTLOtherCase, string(reqbodyTLOtherCase), helper.JsonString(response), response.StatusCode)
		userAuditTrailTLOtherCase = userAuditTrailTLOtherCase.MappingBribrainAuditTrail("Submit Tindaklanjut Other Case", remarkAuditTrailTLOtherCase, userLoginTLOtherCase.Pernr, userLoginTLOtherCase.Role, helper.JsonString(payloadAuditTrailTLOtherCase))
		_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailTLOtherCase)
		if errAuditTrail != nil {
			a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutOtherCase.userAuditTrail: %s", err.Error())

		}

		return c.JSON(response.StatusCode, response)
	}
	// validation := request.MappingToGlobalValidation()
	// checkQueryparams, message := helper.GlobalValidationQueryParams(validation)
	// if checkQueryparams == false {
	// 	response.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), message, message)
	// 	return c.JSON(response.StatusCode, response)
	// }

	result, message, errOtherCase := a.TindaklanjutUsecase.SubmitTindaklanjutOtherCase(ctx, request)
	if errOtherCase != nil {
		errOtherCaseID := time.Now().Format(helper.DateTimeFormatDefault) + " " + helper.RandomString(20)
		a.Log.Error("["+errOtherCaseID+"]  "+log, errOtherCase.Error())
		response.MappingResponseError(helper.GetStatusCode(errOtherCase), message, errOtherCaseID, c)

		remarkAuditTrailTLOtherCase := helper.IntToString(response.StatusCode) + " " + errOtherCase.Error()
		payloadAuditTrailTLOtherCase = payloadAuditTrailTLOtherCase.MappingPayloadBribrainAuditTrail(urlpayloadTLOtherCase, headersAuditTrailTLOtherCase, string(reqbodyTLOtherCase), helper.JsonString(response), response.StatusCode)
		userAuditTrailTLOtherCase = userAuditTrailTLOtherCase.MappingBribrainAuditTrail("Submit Tindaklanjut Other Case", remarkAuditTrailTLOtherCase, userLoginTLOtherCase.Pernr, userLoginTLOtherCase.Role, helper.JsonString(payloadAuditTrailTLOtherCase))
		_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailTLOtherCase)
		if errAuditTrail != nil {
			a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutOtherCase.userAuditTrail: %s", err.Error())

		}

		return c.JSON(response.StatusCode, response)
	}

	a.Log.Info(log, helper.JsonString(result))

	response.MappingResponseSuccess(message, result, c)

	payloadAuditTrailTLOtherCase = payloadAuditTrailTLOtherCase.MappingPayloadBribrainAuditTrail(urlpayloadTLOtherCase, headersAuditTrailTLOtherCase, string(reqbodyTLOtherCase), helper.JsonString(response), response.StatusCode)
	userAuditTrailTLOtherCase = userAuditTrailTLOtherCase.MappingBribrainAuditTrail("Submit Tindaklanjut Other Case", "200 Success", userLoginTLOtherCase.Pernr, userLoginTLOtherCase.Role, helper.JsonString(payloadAuditTrailTLOtherCase))
	_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailTLOtherCase)
	if errAuditTrail != nil {
		a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutOtherCase.userAuditTrail: %s", err.Error())

	}

	return c.JSON(response.StatusCode, response)
}

// SubmitTindaklanjutPositiveCase1Tandai godoc
// @Summary Submit Tindaklanjut Tandai Sudah Dikunjungi
// @Description Submit Tindaklanjut Tandai Sudah Dikunjungi (tandai sudah dikunjungi, untuk positive case 1 dan other case)
// @Tags SubmitTindakLanjut
// @ID submit-tindaklanjut-tandai-sudah-dikunjungi-1
// @Accept  application-bribrain/json
// @Produce  json
// @Param Content-Type header string true "Content-Type" default(application-bribrain/json)
// @Param Authorization header string true "token"
// @Param request body models.SwaggerRequestSubmitTindaklanjutTandai true "body"
// @Success 200 {object} models.DefaultResponseSubmitTindaklanjut
// @Failure 401 {object} models.SwaggerErrorExpiredTokenUnAuthorizeAndTokenUnAuthorize
// @Failure 403 {object} models.SwaggerErrorTokenForbiddenPermission
// @Failure 405 {object} models.SwaggerErrorMethodNotAllowed
// @Failure 500 {object} models.SwaggerErrorInternalServerError
// @Failure 400 {object} models.SwaggerErrorBadParamInput
// @Router /submit_tindaklanjut_positive_1_tandai [post]
func (a *TindaklanjutHandler) SubmitTindaklanjutPositiveCase1Tandai(c echo.Context) error {
	log := "tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutPositiveCase1Tandai: %s"
	responsePositive1Tandai := new(models2.Response)
	requestPositive1Tandai := new(models.RequestSubmitTindaklanjutTandai)
	// userLogin := c.Get("JWT_CLAIMS").(*models.BRIBrainClaims)

	userLoginPosCase1Tandai := c.Get("JWT_CLAIMS").(*models.BRIBrainClaims)
	userAuditTrailPosCase1Tandai := models.BribrainAuditTrail{}
	payloadAuditTrailPosCase1Tandai := models.PayloadBribrainAuditTrail{}
	headersAuditTrailPosCase1Tandai := models.HeadersPayloadBribrainAuditTrail{}

	contentTypeHeaderPosCase1Tandai := c.Request().Header.Get(echo.HeaderContentType)
	authorizationHeaderPosCase1Tandai := c.Request().Header.Get(echo.HeaderAuthorization)
	urlpayloadPosCase1Tandai := c.Request().Host + c.Request().URL.Path
	headersAuditTrailPosCase1Tandai = headersAuditTrailPosCase1Tandai.MappingHeadersPayloadBribrainAuditTrail(contentTypeHeaderPosCase1Tandai, authorizationHeaderPosCase1Tandai)

	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	dec, err := helper.JsonDecode(c, requestPositive1Tandai)
	if err != nil {
		responsePositive1Tandai.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), err.Error(), err, c)
		return c.JSON(responsePositive1Tandai.StatusCode, responsePositive1Tandai)
	}
	requestPositive1Tandai = dec.(*models.RequestSubmitTindaklanjutTandai)
	validate := validator.New()

	reqbodyPosCase1Tandai, err := json.Marshal(requestPositive1Tandai)
	if err != nil {
		a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutPositiveCase1Tandai.userAuditTrail: %s", err.Error())
		// return nil, nil, err
	}

	errvalidate := validate.Struct(requestPositive1Tandai)
	if errvalidate != nil {
		errID := time.Now().Format(helper.DateTimeFormatDefault) + " " + helper.RandomString(20)
		a.Log.Error("["+errID+"]  "+log, errvalidate.Error())
		responsePositive1Tandai.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), models2.ErrInvalidValue.Error(), models2.ErrInvalidValue.Error(), c)

		remarkAuditTrailPosCase1Tandai := helper.IntToString(responsePositive1Tandai.StatusCode) + " " + errvalidate.Error()
		payloadAuditTrailPosCase1Tandai = payloadAuditTrailPosCase1Tandai.MappingPayloadBribrainAuditTrail(urlpayloadPosCase1Tandai, headersAuditTrailPosCase1Tandai, string(reqbodyPosCase1Tandai), helper.JsonString(responsePositive1Tandai), responsePositive1Tandai.StatusCode)
		userAuditTrailPosCase1Tandai = userAuditTrailPosCase1Tandai.MappingBribrainAuditTrail("Submit Tindaklanjut Positive 1 Tandai", remarkAuditTrailPosCase1Tandai, userLoginPosCase1Tandai.Pernr, userLoginPosCase1Tandai.Role, helper.JsonString(payloadAuditTrailPosCase1Tandai))
		_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailPosCase1Tandai)
		if errAuditTrail != nil {
			a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutPositiveCase1Tandai.userAuditTrail: %s", err.Error())

		}

		return c.JSON(responsePositive1Tandai.StatusCode, responsePositive1Tandai)
	}
	// validation := requestPositive1Tandai.MappingToGlobalValidation()
	// checkQueryparams, message := helper.GlobalValidationQueryParams(validation)
	// if checkQueryparams == false {
	// 	responsePositive1Tandai.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), message, message)
	// 	return c.JSON(responsePositive1Tandai.StatusCode, responsePositive1Tandai)
	// }

	result, message, errCaseTandai1 := a.TindaklanjutUsecase.SubmitTindaklanjutTandai(ctx, requestPositive1Tandai)
	if errCaseTandai1 != nil {
		errCaseTandai1ID := time.Now().Format(helper.DateTimeFormatDefault) + " " + helper.RandomString(20)
		a.Log.Error("["+errCaseTandai1ID+"]  "+log, errCaseTandai1.Error())
		responsePositive1Tandai.MappingResponseError(helper.GetStatusCode(errCaseTandai1), message, errCaseTandai1ID, c)

		remarkAuditTrailPosCase1Tandai := helper.IntToString(responsePositive1Tandai.StatusCode) + " " + errCaseTandai1.Error()
		payloadAuditTrailPosCase1Tandai = payloadAuditTrailPosCase1Tandai.MappingPayloadBribrainAuditTrail(urlpayloadPosCase1Tandai, headersAuditTrailPosCase1Tandai, string(reqbodyPosCase1Tandai), helper.JsonString(responsePositive1Tandai), responsePositive1Tandai.StatusCode)
		userAuditTrailPosCase1Tandai = userAuditTrailPosCase1Tandai.MappingBribrainAuditTrail("Submit Tindaklanjut Positive 1 Tandai", remarkAuditTrailPosCase1Tandai, userLoginPosCase1Tandai.Pernr, userLoginPosCase1Tandai.Role, helper.JsonString(payloadAuditTrailPosCase1Tandai))
		_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailPosCase1Tandai)
		if errAuditTrail != nil {
			a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutPositiveCase1Tandai.userAuditTrail: %s", err.Error())

		}

		return c.JSON(responsePositive1Tandai.StatusCode, responsePositive1Tandai)
	}

	a.Log.Info(log, helper.JsonString(result))

	responsePositive1Tandai.MappingResponseSuccess(message, result, c)

	payloadAuditTrailPosCase1Tandai = payloadAuditTrailPosCase1Tandai.MappingPayloadBribrainAuditTrail(urlpayloadPosCase1Tandai, headersAuditTrailPosCase1Tandai, string(reqbodyPosCase1Tandai), helper.JsonString(responsePositive1Tandai), responsePositive1Tandai.StatusCode)
	userAuditTrailPosCase1Tandai = userAuditTrailPosCase1Tandai.MappingBribrainAuditTrail("Submit Tindaklanjut Positive 1 Tandai", "200 Success", userLoginPosCase1Tandai.Pernr, userLoginPosCase1Tandai.Role, helper.JsonString(payloadAuditTrailPosCase1Tandai))
	_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailPosCase1Tandai)
	if errAuditTrail != nil {
		a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutPositiveCase1Tandai.userAuditTrail: %s", err.Error())

	}

	return c.JSON(responsePositive1Tandai.StatusCode, responsePositive1Tandai)
}

// SubmitTindaklanjutOtherCaseTandai godoc
// @Summary Submit Tindaklanjut Tandai Sudah Dikunjungi
// @Description Submit Tindaklanjut Tandai Sudah Dikunjungi (tandai sudah dikunjungi, untuk positive case 1 dan other case)
// @Tags SubmitTindakLanjut
// @ID submit-tindaklanjut-tandai-sudah-dikunjungi-other
// @Accept  application-bribrain/json
// @Produce  json
// @Param Content-Type header string true "Content-Type" default(application-bribrain/json)
// @Param Authorization header string true "token"
// @Param request body models.SwaggerRequestSubmitTindaklanjutTandai true "body"
// @Success 200 {object} models.DefaultResponseSubmitTindaklanjut
// @Failure 401 {object} models.SwaggerErrorExpiredTokenUnAuthorizeAndTokenUnAuthorize
// @Failure 403 {object} models.SwaggerErrorTokenForbiddenPermission
// @Failure 405 {object} models.SwaggerErrorMethodNotAllowed
// @Failure 500 {object} models.SwaggerErrorInternalServerError
// @Failure 400 {object} models.SwaggerErrorBadParamInput
// @Router /submit_tindaklanjut_other_tandai [post]
func (a *TindaklanjutHandler) SubmitTindaklanjutOtherCaseTandai(c echo.Context) error {
	log := "tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutOtherCaseTandai: %s"
	responseTandaiOtherCase := new(models2.Response)
	requestTandaiOtherCase := new(models.RequestSubmitTindaklanjutTandai)
	// userLogin := c.Get("JWT_CLAIMS").(*models.BRIBrainClaims)

	userLoginOtherTandai := c.Get("JWT_CLAIMS").(*models.BRIBrainClaims)
	userAuditTrailOtherTandai := models.BribrainAuditTrail{}
	payloadAuditTrailOtherTandai := models.PayloadBribrainAuditTrail{}
	headersAuditTrailOtherTandai := models.HeadersPayloadBribrainAuditTrail{}

	contentTypeHeaderOtherTandai := c.Request().Header.Get(echo.HeaderContentType)
	authorizationHeaderOtherTandai := c.Request().Header.Get(echo.HeaderAuthorization)
	urlpayloadOtherTandai := c.Request().Host + c.Request().URL.Path
	headersAuditTrailOtherTandai = headersAuditTrailOtherTandai.MappingHeadersPayloadBribrainAuditTrail(contentTypeHeaderOtherTandai, authorizationHeaderOtherTandai)

	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	dec, err := helper.JsonDecode(c, requestTandaiOtherCase)
	if err != nil {
		responseTandaiOtherCase.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), err.Error(), err, c)
		return c.JSON(responseTandaiOtherCase.StatusCode, responseTandaiOtherCase)
	}
	requestTandaiOtherCase = dec.(*models.RequestSubmitTindaklanjutTandai)
	validate := validator.New()

	reqbodyOtherTandai, err := json.Marshal(requestTandaiOtherCase)
	if err != nil {
		a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutOtherCaseTandai.userAuditTrail: %s", err.Error())
		// return nil, nil, err
	}

	errvalidate := validate.Struct(requestTandaiOtherCase)
	if errvalidate != nil {
		errID := time.Now().Format(helper.DateTimeFormatDefault) + " " + helper.RandomString(20)
		a.Log.Error("["+errID+"]  "+log, errvalidate.Error())
		responseTandaiOtherCase.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), models2.ErrInvalidValue.Error(), models2.ErrInvalidValue.Error(), c)

		remarkAuditTrailOtherTandai := helper.IntToString(responseTandaiOtherCase.StatusCode) + " " + errvalidate.Error()
		payloadAuditTrailOtherTandai = payloadAuditTrailOtherTandai.MappingPayloadBribrainAuditTrail(urlpayloadOtherTandai, headersAuditTrailOtherTandai, string(reqbodyOtherTandai), helper.JsonString(responseTandaiOtherCase), responseTandaiOtherCase.StatusCode)
		userAuditTrailOtherTandai = userAuditTrailOtherTandai.MappingBribrainAuditTrail("Submit Tindaklanjut Positive Other Tandai", remarkAuditTrailOtherTandai, userLoginOtherTandai.Pernr, userLoginOtherTandai.Role, helper.JsonString(payloadAuditTrailOtherTandai))
		_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailOtherTandai)
		if errAuditTrail != nil {
			a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutOtherCaseTandai.userAuditTrail: %s", err.Error())

		}

		return c.JSON(responseTandaiOtherCase.StatusCode, responseTandaiOtherCase)
	}
	// validation := requestTandaiOtherCase.MappingToGlobalValidation()
	// checkQueryparams, message := helper.GlobalValidationQueryParams(validation)
	// if checkQueryparams == false {
	// 	responseTandaiOtherCase.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), message, message)
	// 	return c.JSON(responseTandaiOtherCase.StatusCode, responseTandaiOtherCase)
	// }

	result, message, errTandaiOtherCase := a.TindaklanjutUsecase.SubmitTindaklanjutTandai(ctx, requestTandaiOtherCase)
	if errTandaiOtherCase != nil {
		errTandaiOtherCaseID := time.Now().Format(helper.DateTimeFormatDefault) + " " + helper.RandomString(20)
		a.Log.Error("["+errTandaiOtherCaseID+"]  "+log, errTandaiOtherCase.Error())
		responseTandaiOtherCase.MappingResponseError(helper.GetStatusCode(errTandaiOtherCase), message, errTandaiOtherCaseID, c)

		remarkAuditTrailOtherTandai := helper.IntToString(responseTandaiOtherCase.StatusCode) + " " + errTandaiOtherCase.Error()
		payloadAuditTrailOtherTandai = payloadAuditTrailOtherTandai.MappingPayloadBribrainAuditTrail(urlpayloadOtherTandai, headersAuditTrailOtherTandai, string(reqbodyOtherTandai), helper.JsonString(responseTandaiOtherCase), responseTandaiOtherCase.StatusCode)
		userAuditTrailOtherTandai = userAuditTrailOtherTandai.MappingBribrainAuditTrail("Submit Tindaklanjut Positive Other Tandai", remarkAuditTrailOtherTandai, userLoginOtherTandai.Pernr, userLoginOtherTandai.Role, helper.JsonString(payloadAuditTrailOtherTandai))
		_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailOtherTandai)
		if errAuditTrail != nil {
			a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutOtherCaseTandai.userAuditTrail: %s", err.Error())

		}

		return c.JSON(responseTandaiOtherCase.StatusCode, responseTandaiOtherCase)
	}

	a.Log.Info(log, helper.JsonString(result))

	responseTandaiOtherCase.MappingResponseSuccess(message, result, c)

	payloadAuditTrailOtherTandai = payloadAuditTrailOtherTandai.MappingPayloadBribrainAuditTrail(urlpayloadOtherTandai, headersAuditTrailOtherTandai, string(reqbodyOtherTandai), helper.JsonString(responseTandaiOtherCase), responseTandaiOtherCase.StatusCode)
	userAuditTrailOtherTandai = userAuditTrailOtherTandai.MappingBribrainAuditTrail("Submit Tindaklanjut Positive Other Tandai", "200 Success", userLoginOtherTandai.Pernr, userLoginOtherTandai.Role, helper.JsonString(payloadAuditTrailOtherTandai))
	_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailOtherTandai)
	if errAuditTrail != nil {
		a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutOtherCaseTandai.userAuditTrail: %s", err.Error())

	}

	return c.JSON(responseTandaiOtherCase.StatusCode, responseTandaiOtherCase)
}

// SubmitTindaklanjutNonBri godoc
// @Summary Submit Tindaklanjut Non Bri
// @Description Submit Tindaklanjut Non Bri
// @Tags SubmitTindakLanjut
// @ID submit-tindaklanjut-non-bri
// @Accept  application-bribrain/json
// @Produce  json
// @Param Content-Type header string true "Content-Type" default(application-bribrain/json)
// @Param Authorization header string true "token"
// @Param request body models.SwaggerRequestSubmitTindaklanjutNonBri true "body"
// @Success 200 {object} models.DefaultResponseSubmitTindaklanjut
// @Failure 401 {object} models.SwaggerErrorExpiredTokenUnAuthorizeAndTokenUnAuthorize
// @Failure 403 {object} models.SwaggerErrorTokenForbiddenPermission
// @Failure 405 {object} models.SwaggerErrorMethodNotAllowed
// @Failure 500 {object} models.SwaggerErrorInternalServerError
// @Failure 400 {object} models.SwaggerErrorBadParamInput
// @Router /submit_tindaklanjut_non_bri [post]
func (a *TindaklanjutHandler) SubmitTindaklanjutNonBri(c echo.Context) error {
	log := "tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutNonBri: %s"
	response := new(models2.Response)
	request := new(models.RequestSubmitTindaklanjutNonBri)
	userLoginTLNonBri := c.Get("JWT_CLAIMS").(*models.BRIBrainClaims)
	userAuditTrailTLNonBri := models.BribrainAuditTrail{}
	payloadAuditTrailTLNonBri := models.PayloadBribrainAuditTrail{}
	headersAuditTrailTLNonBri := models.HeadersPayloadBribrainAuditTrail{}

	contentTypeHeaderTLNonBri := c.Request().Header.Get(echo.HeaderContentType)
	authorizationHeaderTLNonBri := c.Request().Header.Get(echo.HeaderAuthorization)
	urlpayloadTLNonBri := c.Request().Host + c.Request().URL.Path
	headersAuditTrailTLNonBri = headersAuditTrailTLNonBri.MappingHeadersPayloadBribrainAuditTrail(contentTypeHeaderTLNonBri, authorizationHeaderTLNonBri)

	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	dec, err := helper.JsonDecode(c, request)
	if err != nil {
		response.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), err.Error(), err, c)
		return c.JSON(response.StatusCode, response)
	}
	request = dec.(*models.RequestSubmitTindaklanjutNonBri)
	validateTLNonBri := validator.New()

	reqbodyTLNonBri, err := json.Marshal(request)
	if err != nil {
		a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutNonBri.userAuditTrailTLNonBri: %s", err.Error())
		// return nil, nil, err
	}

	errvalidateTLNonBri := validateTLNonBri.Struct(request)
	if errvalidateTLNonBri != nil {
		errID := time.Now().Format(helper.DateTimeFormatDefault) + " " + helper.RandomString(20)
		a.Log.Error("["+errID+"]  "+log, errvalidateTLNonBri.Error())
		response.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), models2.ErrInvalidValue.Error(), models2.ErrInvalidValue.Error(), c)

		remarkAuditTrail := helper.IntToString(response.StatusCode) + " " + errvalidateTLNonBri.Error()
		payloadAuditTrailTLNonBri = payloadAuditTrailTLNonBri.MappingPayloadBribrainAuditTrail(urlpayloadTLNonBri, headersAuditTrailTLNonBri, string(reqbodyTLNonBri), helper.JsonString(response), response.StatusCode)
		userAuditTrailTLNonBri = userAuditTrailTLNonBri.MappingBribrainAuditTrail("Submit Tindaklanjut Non BRI", remarkAuditTrail, userLoginTLNonBri.Pernr, userLoginTLNonBri.Role, helper.JsonString(payloadAuditTrailTLNonBri))
		_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailTLNonBri)
		if errAuditTrail != nil {
			a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutNonBri.userAuditTrailTLNonBri: %s", err.Error())

		}

		return c.JSON(response.StatusCode, response)
	}
	// validation := request.MappingToGlobalValidation()
	// checkQueryparams, message := helper.GlobalValidationQueryParams(validation)
	// if checkQueryparams == false {
	// 	response.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), message, message)
	// 	return c.JSON(response.StatusCode, response)
	// }

	result, message, errTLNonBri := a.TindaklanjutUsecase.SubmitTindaklanjutNonBri(ctx, request)
	if errTLNonBri != nil {
		errTLNonBriID := time.Now().Format(helper.DateTimeFormatDefault) + " " + helper.RandomString(20)
		a.Log.Error("["+errTLNonBriID+"]  "+log, errTLNonBri.Error())
		response.MappingResponseError(helper.GetStatusCode(errTLNonBri), message, errTLNonBriID, c)

		remarkAuditTrail := helper.IntToString(response.StatusCode) + " " + errTLNonBri.Error()
		payloadAuditTrailTLNonBri = payloadAuditTrailTLNonBri.MappingPayloadBribrainAuditTrail(urlpayloadTLNonBri, headersAuditTrailTLNonBri, string(reqbodyTLNonBri), helper.JsonString(response), response.StatusCode)
		userAuditTrailTLNonBri = userAuditTrailTLNonBri.MappingBribrainAuditTrail("Submit Tindaklanjut Non BRI", remarkAuditTrail, userLoginTLNonBri.Pernr, userLoginTLNonBri.Role, helper.JsonString(payloadAuditTrailTLNonBri))
		_, errTLNonBriAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailTLNonBri)
		if errTLNonBriAuditTrail != nil {
			a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutNonBri.userAuditTrailTLNonBri: %s", errTLNonBri.Error())

		}

		return c.JSON(response.StatusCode, response)
	}

	a.Log.Info(log, helper.JsonString(result))

	response.MappingResponseSuccess(message, result, c)

	remarkAuditTrail := helper.IntToString(response.StatusCode) + " " + "Success"
	payloadAuditTrailTLNonBri = payloadAuditTrailTLNonBri.MappingPayloadBribrainAuditTrail(urlpayloadTLNonBri, headersAuditTrailTLNonBri, string(reqbodyTLNonBri), helper.JsonString(response), response.StatusCode)
	userAuditTrailTLNonBri = userAuditTrailTLNonBri.MappingBribrainAuditTrail("Submit Tindaklanjut Non BRI", remarkAuditTrail, userLoginTLNonBri.Pernr, userLoginTLNonBri.Role, helper.JsonString(payloadAuditTrailTLNonBri))
	_, errTLNonBriAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailTLNonBri)
	if errTLNonBriAuditTrail != nil {
		a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.SubmitTindaklanjutNonBri.userAuditTrailTLNonBri: %s", errTLNonBri.Error())

	}

	return c.JSON(response.StatusCode, response)
}

func (a *TindaklanjutHandler) SubmitKetertarikanNonBri(c echo.Context) error {
	log := "tindaklanjut.delivery.http.TindaklanjutHandler.SubmitKetertarikanNonBri: %s"
	response := new(models2.Response)
	request := new(models.RequestSubmitKetertarikanNonBri)
	userLogin := c.Get("JWT_CLAIMS").(*models.BRIBrainClaims)
	userAuditTrail := models.BribrainAuditTrail{}
	payloadAuditTrail := models.PayloadBribrainAuditTrail{}
	headersAuditTrail := models.HeadersPayloadBribrainAuditTrail{}

	contentTypeHeader := c.Request().Header.Get(echo.HeaderContentType)
	authorizationHeader := c.Request().Header.Get(echo.HeaderAuthorization)
	urlpayload := c.Request().Host + c.Request().URL.Path
	headersAuditTrail = headersAuditTrail.MappingHeadersPayloadBribrainAuditTrail(contentTypeHeader, authorizationHeader)

	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	dec, err := helper.JsonDecode(c, request)
	if err != nil {
		response.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), err.Error(), err, c)
		return c.JSON(response.StatusCode, response)
	}
	request = dec.(*models.RequestSubmitKetertarikanNonBri)
	validate := validator.New()

	reqbody, err := json.Marshal(request)
	if err != nil {
		a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.SubmitKetertarikanNonBri.userAuditTrail: %s", err.Error())
		// return nil, nil, err
	}

	errvalidate := validate.Struct(request)
	if errvalidate != nil {
		errID := time.Now().Format(helper.DateTimeFormatDefault) + " " + helper.RandomString(20)
		a.Log.Error("["+errID+"]  "+log, errvalidate.Error())
		response.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), models2.ErrInvalidValue.Error(), models2.ErrInvalidValue.Error(), c)

		remarkAuditTrail := helper.IntToString(response.StatusCode) + " " + errvalidate.Error()
		payloadAuditTrail = payloadAuditTrail.MappingPayloadBribrainAuditTrail(urlpayload, headersAuditTrail, string(reqbody), helper.JsonString(response), response.StatusCode)
		userAuditTrail = userAuditTrail.MappingBribrainAuditTrail("Submit Ketertarikan Non BRI", remarkAuditTrail, userLogin.Pernr, userLogin.Role, helper.JsonString(payloadAuditTrail))
		_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrail)
		if errAuditTrail != nil {
			a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.SubmitKetertarikanNonBri.userAuditTrail: %s", err.Error())

		}

		return c.JSON(response.StatusCode, response)
	}
	// validation := request.MappingToGlobalValidation()
	// checkQueryparams, message := helper.GlobalValidationQueryParams(validation)
	// if checkQueryparams == false {
	// 	response.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), message, message)
	// 	return c.JSON(response.StatusCode, response)
	// }

	result, message, errKetertarikan := a.TindaklanjutUsecase.SubmitKetertarikanNonBri(ctx, request)
	if errKetertarikan != nil {
		errKetertarikanID := time.Now().Format(helper.DateTimeFormatDefault) + " " + helper.RandomString(20)
		a.Log.Error("["+errKetertarikanID+"]  "+log, errKetertarikan.Error())
		response.MappingResponseError(helper.GetStatusCode(errKetertarikan), message, errKetertarikanID, c)

		remarkAuditTrail := helper.IntToString(response.StatusCode) + " " + errKetertarikan.Error()
		payloadAuditTrail = payloadAuditTrail.MappingPayloadBribrainAuditTrail(urlpayload, headersAuditTrail, string(reqbody), helper.JsonString(response), response.StatusCode)
		userAuditTrail = userAuditTrail.MappingBribrainAuditTrail("Submit Ketertarikan Non BRI", remarkAuditTrail, userLogin.Pernr, userLogin.Role, helper.JsonString(payloadAuditTrail))
		_, errKetertarikanAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrail)
		if errKetertarikanAuditTrail != nil {
			a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.SubmitKetertarikanNonBri.userAuditTrail: %s", errKetertarikan.Error())

		}

		return c.JSON(response.StatusCode, response)
	}

	a.Log.Info(log, helper.JsonString(result))

	response.MappingResponseSuccess(message, result, c)

	remarkAuditTrail := helper.IntToString(response.StatusCode) + " " + "Success"
	payloadAuditTrail = payloadAuditTrail.MappingPayloadBribrainAuditTrail(urlpayload, headersAuditTrail, string(reqbody), helper.JsonString(response), response.StatusCode)
	userAuditTrail = userAuditTrail.MappingBribrainAuditTrail("Submit Ketertarikan Non BRI", remarkAuditTrail, userLogin.Pernr, userLogin.Role, helper.JsonString(payloadAuditTrail))
	_, errKetertarikanAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrail)
	if errKetertarikanAuditTrail != nil {
		a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.SubmitKetertarikanNonBri.userAuditTrail: %s", errKetertarikan.Error())

	}

	return c.JSON(response.StatusCode, response)
}

func (a *TindaklanjutHandler) FindRmInBranch(c echo.Context) error {
	log := "tindaklanjut.delivery.http.TindaklanjutHandler.FindRmInBranch: %s"
	response := new(models2.Response)
	request := new(models.RequestFindRmInBranch)
	userLogin := c.Get("JWT_CLAIMS").(*models.BRIBrainClaims)
	userAuditTrailFindRM := models.BribrainAuditTrail{}
	payloadAuditTrailFindRM := models.PayloadBribrainAuditTrail{}
	headersAuditTrailFindRM := models.HeadersPayloadBribrainAuditTrail{}

	contentTypeHeader := c.Request().Header.Get(echo.HeaderContentType)
	authorizationHeader := c.Request().Header.Get(echo.HeaderAuthorization)
	urlpayload := c.Request().Host + c.Request().URL.Path
	headersAuditTrailFindRM = headersAuditTrailFindRM.MappingHeadersPayloadBribrainAuditTrail(contentTypeHeader, authorizationHeader)

	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	dec, err := helper.JsonDecode(c, request)
	if err != nil {
		response.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), err.Error(), err, c)
		return c.JSON(response.StatusCode, response)
	}
	request = dec.(*models.RequestFindRmInBranch)
	validate := validator.New()

	reqbody, err := json.Marshal(request)
	if err != nil {
		a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.FindRmInBranch.userAuditTrail: %s", err.Error())
		// return nil, nil, err
	}

	errvalidate := validate.Struct(request)
	if errvalidate != nil {
		errID := time.Now().Format(helper.DateTimeFormatDefault) + " " + helper.RandomString(20)
		a.Log.Error("["+errID+"]  "+log, errvalidate.Error())
		response.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), models2.ErrInvalidValue.Error(), models2.ErrInvalidValue.Error(), c)

		remarkAuditTrail := helper.IntToString(response.StatusCode) + " " + errvalidate.Error()
		payloadAuditTrailFindRM = payloadAuditTrailFindRM.MappingPayloadBribrainAuditTrail(urlpayload, headersAuditTrailFindRM, string(reqbody), helper.JsonString(response), response.StatusCode)
		userAuditTrailFindRM = userAuditTrailFindRM.MappingBribrainAuditTrail("Submit Ketertarikan Non BRI", remarkAuditTrail, userLogin.Pernr, userLogin.Role, helper.JsonString(payloadAuditTrailFindRM))
		_, errAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailFindRM)
		if errAuditTrail != nil {
			a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.FindRmInBranch.userAuditTrail: %s", err.Error())

		}

		return c.JSON(response.StatusCode, response)
	}
	// validation := request.MappingToGlobalValidation()
	// checkQueryparams, message := helper.GlobalValidationQueryParams(validation)
	// if checkQueryparams == false {
	// 	response.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), message, message)
	// 	return c.JSON(response.StatusCode, response)
	// }

	result, message, errKetertarikan := a.TindaklanjutUsecase.FindRmInBranch(ctx, request.KodeCabangReferal)
	if errKetertarikan != nil {
		errKetertarikanID := time.Now().Format(helper.DateTimeFormatDefault) + " " + helper.RandomString(20)
		a.Log.Error("["+errKetertarikanID+"]  "+log, errKetertarikan.Error())
		response.MappingResponseError(helper.GetStatusCode(errKetertarikan), message, errKetertarikanID, c)

		remarkAuditTrail := helper.IntToString(response.StatusCode) + " " + errKetertarikan.Error()
		payloadAuditTrailFindRM = payloadAuditTrailFindRM.MappingPayloadBribrainAuditTrail(urlpayload, headersAuditTrailFindRM, string(reqbody), helper.JsonString(response), response.StatusCode)
		userAuditTrailFindRM = userAuditTrailFindRM.MappingBribrainAuditTrail("Submit Ketertarikan Non BRI", remarkAuditTrail, userLogin.Pernr, userLogin.Role, helper.JsonString(payloadAuditTrailFindRM))
		_, errKetertarikanAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailFindRM)
		if errKetertarikanAuditTrail != nil {
			a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.FindRmInBranch.userAuditTrail: %s", errKetertarikan.Error())

		}

		return c.JSON(response.StatusCode, response)
	}

	a.Log.Info(log, helper.JsonString(result))

	response.MappingResponseSuccess(message, result, c)

	remarkAuditTrail := helper.IntToString(response.StatusCode) + " " + "Success"
	payloadAuditTrailFindRM = payloadAuditTrailFindRM.MappingPayloadBribrainAuditTrail(urlpayload, headersAuditTrailFindRM, string(reqbody), helper.JsonString(response), response.StatusCode)
	userAuditTrailFindRM = userAuditTrailFindRM.MappingBribrainAuditTrail("Submit Ketertarikan Non BRI", remarkAuditTrail, userLogin.Pernr, userLogin.Role, helper.JsonString(payloadAuditTrailFindRM))
	_, errKetertarikanAuditTrail := middleware.InitMiddleware().StoreAuditTrail(ctx, userAuditTrailFindRM)
	if errKetertarikanAuditTrail != nil {
		a.Log.Error("tindaklanjut.delivery.http.TindaklanjutHandler.FindRmInBranch.userAuditTrail: %s", errKetertarikan.Error())

	}

	return c.JSON(response.StatusCode, response)
}
