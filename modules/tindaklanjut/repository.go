package tindaklanjut

import (
	"context"
	"ms-bribrain-link5/models"
)

type Repository interface {
	FindNasabahBRI(ctx context.Context, param string) (res *models.GetDetailQueryStruct, err error)
	FindNasabahNonBRI(ctx context.Context, param string) (res *models.GetDetailQueryStruct, err error)
	FindRmWithLeastNonBriCustomer(ctx context.Context, param string) (res *models.QueryGetInfoRM, err error)
	GetInfoRMReferal(ctx context.Context, kodeMainbranch, pnRm string) (res *models.QueryGetInfoRM, err error)
	FindTindaklanjutNasabahBRI(ctx context.Context, param string) (res *models.GetDetailQueryStruct, err error)
	FindTindaklanjutNasabahNonBRI(ctx context.Context, param string) (res *models.GetDetailQueryStruct, err error)
	UpdateStatusTabelNasabahBRI(ctx context.Context, cifno *string, idStatus *int64) (err error)
	UpdateStatusTabelNasabahNonBRI(ctx context.Context, kodenonbri *string, idStatus *int64) (err error)
	UpdateStatusTabelNasabahNonBRISendiri(ctx context.Context, kodenonbri *string, param *models.QueryUpdateStatusNasabah) (err error)
	UpdateStatusTabelNasabahNonBRIReferal(ctx context.Context, kodenonbri *string, param *models.QueryUpdateStatusNasabah) (err error)
	UpdateTabelStatusTindaklanjutNonBRIReferal(ctx context.Context, kodenonbri *string, param *models.QueryGetInfoRM) (err error)
	SubmitTindaklanjutPositiveCase1(ctx context.Context, param *models.QuerySubmitTindaklanjut) (err error)
	SubmitTindaklanjutPositiveCase2(ctx context.Context, param *models.QuerySubmitTindaklanjut) (err error)
	SubmitTindaklanjutNegativeCase1(ctx context.Context, param *models.QuerySubmitTindaklanjut) (err error)
	SubmitTindaklanjutNegativeCase2(ctx context.Context, param *models.QuerySubmitTindaklanjut) (err error)
	SubmitTindaklanjutNegativeCase3(ctx context.Context, param *models.QuerySubmitTindaklanjut) (err error)
	SubmitTindaklanjutOtherCase(ctx context.Context, param *models.QuerySubmitTindaklanjut) (err error)
	SubmitTindaklanjutTandai(ctx context.Context, param *models.QuerySubmitTindaklanjut) (err error)
	SubmitTindaklanjutNonBriCase2Dan3(ctx context.Context, param *models.QuerySubmitTindaklanjut) (err error)
	SubmitTindaklanjutNonBriCase1(ctx context.Context, param *models.QuerySubmitTindaklanjut) (err error)
	SubmitTindaklanjutNonBriTertarik(ctx context.Context, param *models.QuerySubmitTindaklanjut) (err error)
	SubmitTindaklanjutNonBriTidakTertarik(ctx context.Context, param *models.QuerySubmitTindaklanjut) (err error)
	InsertHistoriReferal(ctx context.Context, kodenonbri *string, dataRMSebelum *models.GetDetailQueryStruct, dataRMSesudah *models.QueryGetInfoRM, dataNasabah *models.QueryUpdateStatusNasabah) (err error)
}
