package usecase_test

import (
	"context"
	"ms-bribrain-link5/helper/logger"
	models2 "ms-bribrain-link5/helper/models"
	"ms-bribrain-link5/models"
	_tindaklanjutRepositoryMock "ms-bribrain-link5/modules/tindaklanjut/mocks"
	"ms-bribrain-link5/modules/tindaklanjut/usecase"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

var (
	l              = logger.L
	timeoutContext = time.Second * 30
)

func TestTindaklanjutUsecase_SubmitTindaklanjutPositiveCase1(t *testing.T) {
	//requestMock
	request := &models.RequestSubmitTindaklanjutPositive1{}
	request = request.MappingExampleData()

	//resultMock
	mockResponseRepoGetDetail := &models.GetDetailQueryStruct{}
	mockResponseRepoGetDetail = mockResponseRepoGetDetail.MappingExampleData()

	t.Run("success", func(t *testing.T) {
		//repositoryMock
		mockTindaklanjutRepo := new(_tindaklanjutRepositoryMock.Repository)

		mockTindaklanjutRepo.On("FindNasabahBRI", mock.Anything, mock.AnythingOfType("string")).Return(mockResponseRepoGetDetail, nil).Once()
		mockTindaklanjutRepo.On("SubmitTindaklanjutPositiveCase1", mock.Anything, mock.AnythingOfType("*models.QuerySubmitTindaklanjut")).Return(nil).Once()
		mockTindaklanjutRepo.On("UpdateStatusTabelNasabahBRI", mock.Anything, mock.AnythingOfType("*string"), mock.AnythingOfType("*int64")).Return(nil).Once()

		u := usecase.NewTindaklanjutUsecase(mockTindaklanjutRepo, l, timeoutContext)

		a, _, err := u.SubmitTindaklanjutPositiveCase1(context.TODO(), request)

		assert.NoError(t, err)
		assert.Nil(t, a)

		mockTindaklanjutRepo.AssertExpectations(t)
	})

	t.Run("error-findnasabah", func(t *testing.T) {
		//repositoryMock
		mockTindaklanjutRepo := new(_tindaklanjutRepositoryMock.Repository)

		mockTindaklanjutRepo.On("FindNasabahBRI", mock.Anything, mock.AnythingOfType("string")).Return(nil, models2.ErrInternalServerError).Once()

		u := usecase.NewTindaklanjutUsecase(mockTindaklanjutRepo, l, timeoutContext)

		a, _, err := u.SubmitTindaklanjutPositiveCase1(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockTindaklanjutRepo.AssertExpectations(t)
	})

	t.Run("error-submittindaklanjut", func(t *testing.T) {
		//repositoryMock
		mockTindaklanjutRepo := new(_tindaklanjutRepositoryMock.Repository)

		mockTindaklanjutRepo.On("FindNasabahBRI", mock.Anything, mock.AnythingOfType("string")).Return(mockResponseRepoGetDetail, nil).Once()
		mockTindaklanjutRepo.On("SubmitTindaklanjutPositiveCase1", mock.Anything, mock.AnythingOfType("*models.QuerySubmitTindaklanjut")).Return(models2.ErrInternalServerError).Once()

		u := usecase.NewTindaklanjutUsecase(mockTindaklanjutRepo, l, timeoutContext)

		a, _, err := u.SubmitTindaklanjutPositiveCase1(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockTindaklanjutRepo.AssertExpectations(t)
	})

	t.Run("error-updatetabelnasabah", func(t *testing.T) {
		//repositoryMock
		mockTindaklanjutRepo := new(_tindaklanjutRepositoryMock.Repository)

		mockTindaklanjutRepo.On("FindNasabahBRI", mock.Anything, mock.AnythingOfType("string")).Return(mockResponseRepoGetDetail, nil).Once()
		mockTindaklanjutRepo.On("SubmitTindaklanjutPositiveCase1", mock.Anything, mock.AnythingOfType("*models.QuerySubmitTindaklanjut")).Return(nil).Once()
		mockTindaklanjutRepo.On("UpdateStatusTabelNasabahBRI", mock.Anything, mock.AnythingOfType("*string"), mock.AnythingOfType("*int64")).Return(models2.ErrInternalServerError).Once()

		u := usecase.NewTindaklanjutUsecase(mockTindaklanjutRepo, l, timeoutContext)

		a, _, err := u.SubmitTindaklanjutPositiveCase1(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockTindaklanjutRepo.AssertExpectations(t)
	})
}

func TestTindaklanjutUsecase_SubmitTindaklanjutPositiveCase2(t *testing.T) {
	//requestMock
	request := &models.RequestSubmitTindaklanjutPositive2{}
	request = request.MappingExampleData()

	//resultMock
	mockResponseRepoGetDetail := &models.GetDetailQueryStruct{}
	mockResponseRepoGetDetail = mockResponseRepoGetDetail.MappingExampleData()

	t.Run("success", func(t *testing.T) {
		//repositoryMock
		mockTindaklanjutRepo := new(_tindaklanjutRepositoryMock.Repository)

		mockTindaklanjutRepo.On("FindNasabahBRI", mock.Anything, mock.AnythingOfType("string")).Return(mockResponseRepoGetDetail, nil).Once()
		mockTindaklanjutRepo.On("SubmitTindaklanjutPositiveCase2", mock.Anything, mock.AnythingOfType("*models.QuerySubmitTindaklanjut")).Return(nil).Once()
		mockTindaklanjutRepo.On("UpdateStatusTabelNasabahBRI", mock.Anything, mock.AnythingOfType("*string"), mock.AnythingOfType("*int64")).Return(nil).Once()

		u := usecase.NewTindaklanjutUsecase(mockTindaklanjutRepo, l, timeoutContext)

		a, _, err := u.SubmitTindaklanjutPositiveCase2(context.TODO(), request)

		assert.NoError(t, err)
		assert.Nil(t, a)

		mockTindaklanjutRepo.AssertExpectations(t)
	})

	t.Run("error-findnasabah", func(t *testing.T) {
		//repositoryMock
		mockTindaklanjutRepo := new(_tindaklanjutRepositoryMock.Repository)

		mockTindaklanjutRepo.On("FindNasabahBRI", mock.Anything, mock.AnythingOfType("string")).Return(nil, models2.ErrInternalServerError).Once()

		u := usecase.NewTindaklanjutUsecase(mockTindaklanjutRepo, l, timeoutContext)

		a, _, err := u.SubmitTindaklanjutPositiveCase2(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockTindaklanjutRepo.AssertExpectations(t)
	})

	t.Run("error-submittindaklanjut", func(t *testing.T) {
		//repositoryMock
		mockTindaklanjutRepo := new(_tindaklanjutRepositoryMock.Repository)

		mockTindaklanjutRepo.On("FindNasabahBRI", mock.Anything, mock.AnythingOfType("string")).Return(mockResponseRepoGetDetail, nil).Once()
		mockTindaklanjutRepo.On("SubmitTindaklanjutPositiveCase2", mock.Anything, mock.AnythingOfType("*models.QuerySubmitTindaklanjut")).Return(models2.ErrInternalServerError).Once()

		u := usecase.NewTindaklanjutUsecase(mockTindaklanjutRepo, l, timeoutContext)

		a, _, err := u.SubmitTindaklanjutPositiveCase2(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockTindaklanjutRepo.AssertExpectations(t)
	})

	t.Run("error-updatetabelnasabah", func(t *testing.T) {
		//repositoryMock
		mockTindaklanjutRepo := new(_tindaklanjutRepositoryMock.Repository)

		mockTindaklanjutRepo.On("FindNasabahBRI", mock.Anything, mock.AnythingOfType("string")).Return(mockResponseRepoGetDetail, nil).Once()
		mockTindaklanjutRepo.On("SubmitTindaklanjutPositiveCase2", mock.Anything, mock.AnythingOfType("*models.QuerySubmitTindaklanjut")).Return(nil).Once()
		mockTindaklanjutRepo.On("UpdateStatusTabelNasabahBRI", mock.Anything, mock.AnythingOfType("*string"), mock.AnythingOfType("*int64")).Return(models2.ErrInternalServerError).Once()

		u := usecase.NewTindaklanjutUsecase(mockTindaklanjutRepo, l, timeoutContext)

		a, _, err := u.SubmitTindaklanjutPositiveCase2(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockTindaklanjutRepo.AssertExpectations(t)
	})
}

func TestTindaklanjutUsecase_SubmitTindaklanjutNegativeCase1(t *testing.T) {
	//requestMock
	request := &models.RequestSubmitTindaklanjutNegative1{}
	request = request.MappingExampleData()

	//resultMock
	mockResponseRepoGetDetail := &models.GetDetailQueryStruct{}
	mockResponseRepoGetDetail = mockResponseRepoGetDetail.MappingExampleData()

	t.Run("success", func(t *testing.T) {
		//repositoryMock
		mockTindaklanjutRepo := new(_tindaklanjutRepositoryMock.Repository)

		mockTindaklanjutRepo.On("FindNasabahBRI", mock.Anything, mock.AnythingOfType("string")).Return(mockResponseRepoGetDetail, nil).Once()
		mockTindaklanjutRepo.On("SubmitTindaklanjutNegativeCase1", mock.Anything, mock.AnythingOfType("*models.QuerySubmitTindaklanjut")).Return(nil).Once()
		mockTindaklanjutRepo.On("UpdateStatusTabelNasabahBRI", mock.Anything, mock.AnythingOfType("*string"), mock.AnythingOfType("*int64")).Return(nil).Once()

		u := usecase.NewTindaklanjutUsecase(mockTindaklanjutRepo, l, timeoutContext)

		a, _, err := u.SubmitTindaklanjutNegativeCase1(context.TODO(), request)

		assert.NoError(t, err)
		assert.Nil(t, a)

		mockTindaklanjutRepo.AssertExpectations(t)
	})

	t.Run("error-findnasabah", func(t *testing.T) {
		//repositoryMock
		mockTindaklanjutRepo := new(_tindaklanjutRepositoryMock.Repository)

		mockTindaklanjutRepo.On("FindNasabahBRI", mock.Anything, mock.AnythingOfType("string")).Return(nil, models2.ErrInternalServerError).Once()

		u := usecase.NewTindaklanjutUsecase(mockTindaklanjutRepo, l, timeoutContext)

		a, _, err := u.SubmitTindaklanjutNegativeCase1(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockTindaklanjutRepo.AssertExpectations(t)
	})

	t.Run("error-submittindaklanjut", func(t *testing.T) {
		//repositoryMock
		mockTindaklanjutRepo := new(_tindaklanjutRepositoryMock.Repository)

		mockTindaklanjutRepo.On("FindNasabahBRI", mock.Anything, mock.AnythingOfType("string")).Return(mockResponseRepoGetDetail, nil).Once()
		mockTindaklanjutRepo.On("SubmitTindaklanjutNegativeCase1", mock.Anything, mock.AnythingOfType("*models.QuerySubmitTindaklanjut")).Return(models2.ErrInternalServerError).Once()

		u := usecase.NewTindaklanjutUsecase(mockTindaklanjutRepo, l, timeoutContext)

		a, _, err := u.SubmitTindaklanjutNegativeCase1(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockTindaklanjutRepo.AssertExpectations(t)
	})

	t.Run("error-updatetabelnasabah", func(t *testing.T) {
		//repositoryMock
		mockTindaklanjutRepo := new(_tindaklanjutRepositoryMock.Repository)

		mockTindaklanjutRepo.On("FindNasabahBRI", mock.Anything, mock.AnythingOfType("string")).Return(mockResponseRepoGetDetail, nil).Once()
		mockTindaklanjutRepo.On("SubmitTindaklanjutNegativeCase1", mock.Anything, mock.AnythingOfType("*models.QuerySubmitTindaklanjut")).Return(nil).Once()
		mockTindaklanjutRepo.On("UpdateStatusTabelNasabahBRI", mock.Anything, mock.AnythingOfType("*string"), mock.AnythingOfType("*int64")).Return(models2.ErrInternalServerError).Once()

		u := usecase.NewTindaklanjutUsecase(mockTindaklanjutRepo, l, timeoutContext)

		a, _, err := u.SubmitTindaklanjutNegativeCase1(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockTindaklanjutRepo.AssertExpectations(t)
	})
}

func TestTindaklanjutUsecase_SubmitTindaklanjutNegativeCase2(t *testing.T) {
	//requestMock
	request := &models.RequestSubmitTindaklanjutNegative2{}
	request = request.MappingExampleData()

	//resultMock
	mockResponseRepoGetDetail := &models.GetDetailQueryStruct{}
	mockResponseRepoGetDetail = mockResponseRepoGetDetail.MappingExampleData()

	t.Run("success", func(t *testing.T) {
		//repositoryMock
		mockTindaklanjutRepo := new(_tindaklanjutRepositoryMock.Repository)

		mockTindaklanjutRepo.On("FindNasabahBRI", mock.Anything, mock.AnythingOfType("string")).Return(mockResponseRepoGetDetail, nil).Once()
		mockTindaklanjutRepo.On("SubmitTindaklanjutNegativeCase2", mock.Anything, mock.AnythingOfType("*models.QuerySubmitTindaklanjut")).Return(nil).Once()
		mockTindaklanjutRepo.On("UpdateStatusTabelNasabahBRI", mock.Anything, mock.AnythingOfType("*string"), mock.AnythingOfType("*int64")).Return(nil).Once()

		u := usecase.NewTindaklanjutUsecase(mockTindaklanjutRepo, l, timeoutContext)

		a, _, err := u.SubmitTindaklanjutNegativeCase2(context.TODO(), request)

		assert.NoError(t, err)
		assert.Nil(t, a)

		mockTindaklanjutRepo.AssertExpectations(t)
	})

	t.Run("error-findnasabah", func(t *testing.T) {
		//repositoryMock
		mockTindaklanjutRepo := new(_tindaklanjutRepositoryMock.Repository)

		mockTindaklanjutRepo.On("FindNasabahBRI", mock.Anything, mock.AnythingOfType("string")).Return(nil, models2.ErrInternalServerError).Once()

		u := usecase.NewTindaklanjutUsecase(mockTindaklanjutRepo, l, timeoutContext)

		a, _, err := u.SubmitTindaklanjutNegativeCase2(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockTindaklanjutRepo.AssertExpectations(t)
	})

	t.Run("error-submittindaklanjut", func(t *testing.T) {
		//repositoryMock
		mockTindaklanjutRepo := new(_tindaklanjutRepositoryMock.Repository)

		mockTindaklanjutRepo.On("FindNasabahBRI", mock.Anything, mock.AnythingOfType("string")).Return(mockResponseRepoGetDetail, nil).Once()
		mockTindaklanjutRepo.On("SubmitTindaklanjutNegativeCase2", mock.Anything, mock.AnythingOfType("*models.QuerySubmitTindaklanjut")).Return(models2.ErrInternalServerError).Once()

		u := usecase.NewTindaklanjutUsecase(mockTindaklanjutRepo, l, timeoutContext)

		a, _, err := u.SubmitTindaklanjutNegativeCase2(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockTindaklanjutRepo.AssertExpectations(t)
	})

	t.Run("error-updatetabelnasabah", func(t *testing.T) {
		//repositoryMock
		mockTindaklanjutRepo := new(_tindaklanjutRepositoryMock.Repository)

		mockTindaklanjutRepo.On("FindNasabahBRI", mock.Anything, mock.AnythingOfType("string")).Return(mockResponseRepoGetDetail, nil).Once()
		mockTindaklanjutRepo.On("SubmitTindaklanjutNegativeCase2", mock.Anything, mock.AnythingOfType("*models.QuerySubmitTindaklanjut")).Return(nil).Once()
		mockTindaklanjutRepo.On("UpdateStatusTabelNasabahBRI", mock.Anything, mock.AnythingOfType("*string"), mock.AnythingOfType("*int64")).Return(models2.ErrInternalServerError).Once()

		u := usecase.NewTindaklanjutUsecase(mockTindaklanjutRepo, l, timeoutContext)

		a, _, err := u.SubmitTindaklanjutNegativeCase2(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockTindaklanjutRepo.AssertExpectations(t)
	})
}

func TestTindaklanjutUsecase_SubmitTindaklanjutNegativeCase3(t *testing.T) {
	//requestMock
	request := &models.RequestSubmitTindaklanjutNegative3{}
	request = request.MappingExampleData()

	//resultMock
	mockResponseRepoGetDetail := &models.GetDetailQueryStruct{}
	mockResponseRepoGetDetail = mockResponseRepoGetDetail.MappingExampleData()

	t.Run("success", func(t *testing.T) {
		//repositoryMock
		mockTindaklanjutRepo := new(_tindaklanjutRepositoryMock.Repository)

		mockTindaklanjutRepo.On("FindNasabahBRI", mock.Anything, mock.AnythingOfType("string")).Return(mockResponseRepoGetDetail, nil).Once()
		mockTindaklanjutRepo.On("SubmitTindaklanjutNegativeCase3", mock.Anything, mock.AnythingOfType("*models.QuerySubmitTindaklanjut")).Return(nil).Once()
		mockTindaklanjutRepo.On("UpdateStatusTabelNasabahBRI", mock.Anything, mock.AnythingOfType("*string"), mock.AnythingOfType("*int64")).Return(nil).Once()

		u := usecase.NewTindaklanjutUsecase(mockTindaklanjutRepo, l, timeoutContext)

		a, _, err := u.SubmitTindaklanjutNegativeCase3(context.TODO(), request)

		assert.NoError(t, err)
		assert.Nil(t, a)

		mockTindaklanjutRepo.AssertExpectations(t)
	})

	t.Run("error-findnasabah", func(t *testing.T) {
		//repositoryMock
		mockTindaklanjutRepo := new(_tindaklanjutRepositoryMock.Repository)

		mockTindaklanjutRepo.On("FindNasabahBRI", mock.Anything, mock.AnythingOfType("string")).Return(nil, models2.ErrInternalServerError).Once()

		u := usecase.NewTindaklanjutUsecase(mockTindaklanjutRepo, l, timeoutContext)

		a, _, err := u.SubmitTindaklanjutNegativeCase3(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockTindaklanjutRepo.AssertExpectations(t)
	})

	t.Run("error-submittindaklanjut", func(t *testing.T) {
		//repositoryMock
		mockTindaklanjutRepo := new(_tindaklanjutRepositoryMock.Repository)

		mockTindaklanjutRepo.On("FindNasabahBRI", mock.Anything, mock.AnythingOfType("string")).Return(mockResponseRepoGetDetail, nil).Once()
		mockTindaklanjutRepo.On("SubmitTindaklanjutNegativeCase3", mock.Anything, mock.AnythingOfType("*models.QuerySubmitTindaklanjut")).Return(models2.ErrInternalServerError).Once()

		u := usecase.NewTindaklanjutUsecase(mockTindaklanjutRepo, l, timeoutContext)

		a, _, err := u.SubmitTindaklanjutNegativeCase3(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockTindaklanjutRepo.AssertExpectations(t)
	})

	t.Run("error-updatetabelnasabah", func(t *testing.T) {
		//repositoryMock
		mockTindaklanjutRepo := new(_tindaklanjutRepositoryMock.Repository)

		mockTindaklanjutRepo.On("FindNasabahBRI", mock.Anything, mock.AnythingOfType("string")).Return(mockResponseRepoGetDetail, nil).Once()
		mockTindaklanjutRepo.On("SubmitTindaklanjutNegativeCase3", mock.Anything, mock.AnythingOfType("*models.QuerySubmitTindaklanjut")).Return(nil).Once()
		mockTindaklanjutRepo.On("UpdateStatusTabelNasabahBRI", mock.Anything, mock.AnythingOfType("*string"), mock.AnythingOfType("*int64")).Return(models2.ErrInternalServerError).Once()

		u := usecase.NewTindaklanjutUsecase(mockTindaklanjutRepo, l, timeoutContext)

		a, _, err := u.SubmitTindaklanjutNegativeCase3(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockTindaklanjutRepo.AssertExpectations(t)
	})
}

func TestTindaklanjutUsecase_SubmitTindaklanjutOtherCase(t *testing.T) {
	//requestMock
	request := &models.RequestSubmitTindaklanjutOther{}
	request = request.MappingExampleData()

	//resultMock
	mockResponseRepoGetDetail := &models.GetDetailQueryStruct{}
	mockResponseRepoGetDetail = mockResponseRepoGetDetail.MappingExampleData()

	t.Run("success", func(t *testing.T) {
		//repositoryMock
		mockTindaklanjutRepo := new(_tindaklanjutRepositoryMock.Repository)

		mockTindaklanjutRepo.On("FindNasabahBRI", mock.Anything, mock.AnythingOfType("string")).Return(mockResponseRepoGetDetail, nil).Once()
		mockTindaklanjutRepo.On("SubmitTindaklanjutOtherCase", mock.Anything, mock.AnythingOfType("*models.QuerySubmitTindaklanjut")).Return(nil).Once()
		mockTindaklanjutRepo.On("UpdateStatusTabelNasabahBRI", mock.Anything, mock.AnythingOfType("*string"), mock.AnythingOfType("*int64")).Return(nil).Once()

		u := usecase.NewTindaklanjutUsecase(mockTindaklanjutRepo, l, timeoutContext)

		a, _, err := u.SubmitTindaklanjutOtherCase(context.TODO(), request)

		assert.NoError(t, err)
		assert.Nil(t, a)

		mockTindaklanjutRepo.AssertExpectations(t)
	})

	t.Run("error-findnasabah", func(t *testing.T) {
		//repositoryMock
		mockTindaklanjutRepo := new(_tindaklanjutRepositoryMock.Repository)

		mockTindaklanjutRepo.On("FindNasabahBRI", mock.Anything, mock.AnythingOfType("string")).Return(nil, models2.ErrInternalServerError).Once()

		u := usecase.NewTindaklanjutUsecase(mockTindaklanjutRepo, l, timeoutContext)

		a, _, err := u.SubmitTindaklanjutOtherCase(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockTindaklanjutRepo.AssertExpectations(t)
	})

	t.Run("error-submittindaklanjut", func(t *testing.T) {
		//repositoryMock
		mockTindaklanjutRepo := new(_tindaklanjutRepositoryMock.Repository)

		mockTindaklanjutRepo.On("FindNasabahBRI", mock.Anything, mock.AnythingOfType("string")).Return(mockResponseRepoGetDetail, nil).Once()
		mockTindaklanjutRepo.On("SubmitTindaklanjutOtherCase", mock.Anything, mock.AnythingOfType("*models.QuerySubmitTindaklanjut")).Return(models2.ErrInternalServerError).Once()

		u := usecase.NewTindaklanjutUsecase(mockTindaklanjutRepo, l, timeoutContext)

		a, _, err := u.SubmitTindaklanjutOtherCase(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockTindaklanjutRepo.AssertExpectations(t)
	})

	t.Run("error-updatetabelnasabah", func(t *testing.T) {
		//repositoryMock
		mockTindaklanjutRepo := new(_tindaklanjutRepositoryMock.Repository)

		mockTindaklanjutRepo.On("FindNasabahBRI", mock.Anything, mock.AnythingOfType("string")).Return(mockResponseRepoGetDetail, nil).Once()
		mockTindaklanjutRepo.On("SubmitTindaklanjutOtherCase", mock.Anything, mock.AnythingOfType("*models.QuerySubmitTindaklanjut")).Return(nil).Once()
		mockTindaklanjutRepo.On("UpdateStatusTabelNasabahBRI", mock.Anything, mock.AnythingOfType("*string"), mock.AnythingOfType("*int64")).Return(models2.ErrInternalServerError).Once()

		u := usecase.NewTindaklanjutUsecase(mockTindaklanjutRepo, l, timeoutContext)

		a, _, err := u.SubmitTindaklanjutOtherCase(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockTindaklanjutRepo.AssertExpectations(t)
	})
}

func TestTindaklanjutUsecase_SubmitTindaklanjutTandai(t *testing.T) {
	//requestMock
	request := &models.RequestSubmitTindaklanjutTandai{}
	request = request.MappingExampleData()

	//resultMock
	mockResponseRepoGetDetail := &models.GetDetailQueryStruct{}
	mockResponseRepoGetDetail = mockResponseRepoGetDetail.MappingExampleData()

	t.Run("success", func(t *testing.T) {
		//repositoryMock
		mockTindaklanjutRepo := new(_tindaklanjutRepositoryMock.Repository)

		mockTindaklanjutRepo.On("FindNasabahBRI", mock.Anything, mock.AnythingOfType("string")).Return(mockResponseRepoGetDetail, nil).Once()
		mockTindaklanjutRepo.On("SubmitTindaklanjutTandai", mock.Anything, mock.AnythingOfType("*models.QuerySubmitTindaklanjut")).Return(nil).Once()

		u := usecase.NewTindaklanjutUsecase(mockTindaklanjutRepo, l, timeoutContext)

		a, _, err := u.SubmitTindaklanjutTandai(context.TODO(), request)

		assert.NoError(t, err)
		assert.Nil(t, a)

		mockTindaklanjutRepo.AssertExpectations(t)
	})

	t.Run("error-findnasabah", func(t *testing.T) {
		//repositoryMock
		mockTindaklanjutRepo := new(_tindaklanjutRepositoryMock.Repository)

		mockTindaklanjutRepo.On("FindNasabahBRI", mock.Anything, mock.AnythingOfType("string")).Return(nil, models2.ErrInternalServerError).Once()

		u := usecase.NewTindaklanjutUsecase(mockTindaklanjutRepo, l, timeoutContext)

		a, _, err := u.SubmitTindaklanjutTandai(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockTindaklanjutRepo.AssertExpectations(t)
	})

	t.Run("error-submittindaklanjut", func(t *testing.T) {
		//repositoryMock
		mockTindaklanjutRepo := new(_tindaklanjutRepositoryMock.Repository)

		mockTindaklanjutRepo.On("FindNasabahBRI", mock.Anything, mock.AnythingOfType("string")).Return(mockResponseRepoGetDetail, nil).Once()
		mockTindaklanjutRepo.On("SubmitTindaklanjutTandai", mock.Anything, mock.AnythingOfType("*models.QuerySubmitTindaklanjut")).Return(models2.ErrInternalServerError).Once()

		u := usecase.NewTindaklanjutUsecase(mockTindaklanjutRepo, l, timeoutContext)

		a, _, err := u.SubmitTindaklanjutTandai(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockTindaklanjutRepo.AssertExpectations(t)
	})
}

func TestTindaklanjutUsecase_SubmitTindaklanjutNonBri(t *testing.T) {
	//requestMock
	request := &models.RequestSubmitTindaklanjutNonBri{}
	request = request.MappingExampleData()
	requestCase2 := request.MappingExampleDataCase2()
	requestTindaklanjutSendiri := request.MappingExampleDataTLSendiri()

	//resultMock
	mockResponseRepoGetDetail := &models.GetDetailQueryStruct{}
	mockResponseRepoGetDetail = mockResponseRepoGetDetail.MappingExampleData()

	mockResponseSearchRM := &models.QueryGetInfoRM{}
	mockResponseSearchRM = mockResponseSearchRM.MappingExampleData()

	t.Run("success-TL-sendiri", func(t *testing.T) {
		//repositoryMock
		mockTindaklanjutRepo := new(_tindaklanjutRepositoryMock.Repository)

		mockTindaklanjutRepo.On("FindNasabahNonBRI", mock.Anything, mock.AnythingOfType("string")).Return(mockResponseRepoGetDetail, nil).Once()
		mockTindaklanjutRepo.On("SubmitTindaklanjutNonBriCase1", mock.Anything, mock.AnythingOfType("*models.QuerySubmitTindaklanjut")).Return(nil).Once()
		mockTindaklanjutRepo.On("UpdateStatusTabelNasabahNonBRISendiri", mock.Anything, mock.Anything, mock.Anything).Return(nil).Once()

		u := usecase.NewTindaklanjutUsecase(mockTindaklanjutRepo, l, timeoutContext)

		a, _, err := u.SubmitTindaklanjutNonBri(context.TODO(), requestTindaklanjutSendiri)

		assert.NoError(t, err)
		assert.Nil(t, a)

		mockTindaklanjutRepo.AssertExpectations(t)
	})

	t.Run("success-TL-referal", func(t *testing.T) {
		//repositoryMock
		mockTindaklanjutRepo := new(_tindaklanjutRepositoryMock.Repository)

		mockTindaklanjutRepo.On("FindNasabahNonBRI", mock.Anything, mock.AnythingOfType("string")).Return(mockResponseRepoGetDetail, nil).Once()
		mockTindaklanjutRepo.On("SubmitTindaklanjutNonBriCase1", mock.Anything, mock.AnythingOfType("*models.QuerySubmitTindaklanjut")).Return(nil).Once()
		mockTindaklanjutRepo.On("FindRmWithLeastNonBriCustomer", mock.Anything, mock.AnythingOfType("string")).Return(mockResponseSearchRM, nil).Once()
		mockTindaklanjutRepo.On("GetInfoRMReferal", mock.Anything, mock.Anything, mock.AnythingOfType("string")).Return(mockResponseSearchRM, nil).Once()
		mockTindaklanjutRepo.On("UpdateStatusTabelNasabahNonBRIReferal", mock.Anything, mock.Anything, mock.Anything).Return(nil).Once()
		mockTindaklanjutRepo.On("UpdateTabelStatusTindaklanjutNonBRIReferal", mock.Anything, mock.Anything, mock.Anything).Return(nil).Once()
		mockTindaklanjutRepo.On("InsertHistoriReferal", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil).Once()

		u := usecase.NewTindaklanjutUsecase(mockTindaklanjutRepo, l, timeoutContext)

		a, _, err := u.SubmitTindaklanjutNonBri(context.TODO(), request)

		assert.NoError(t, err)
		assert.Nil(t, a)

		mockTindaklanjutRepo.AssertExpectations(t)
	})

	t.Run("success-TL-case23", func(t *testing.T) {
		//repositoryMock
		mockTindaklanjutRepo := new(_tindaklanjutRepositoryMock.Repository)

		mockTindaklanjutRepo.On("FindNasabahNonBRI", mock.Anything, mock.AnythingOfType("string")).Return(mockResponseRepoGetDetail, nil).Once()
		mockTindaklanjutRepo.On("SubmitTindaklanjutNonBriCase2Dan3", mock.Anything, mock.AnythingOfType("*models.QuerySubmitTindaklanjut")).Return(nil).Once()
		mockTindaklanjutRepo.On("UpdateStatusTabelNasabahNonBRI", mock.Anything, mock.AnythingOfType("*string"), mock.AnythingOfType("*int64")).Return(nil).Once()

		u := usecase.NewTindaklanjutUsecase(mockTindaklanjutRepo, l, timeoutContext)

		a, _, err := u.SubmitTindaklanjutNonBri(context.TODO(), requestCase2)

		assert.NoError(t, err)
		assert.Nil(t, a)

		mockTindaklanjutRepo.AssertExpectations(t)
	})

	t.Run("error-findnasabah", func(t *testing.T) {
		//repositoryMock
		mockTindaklanjutRepo := new(_tindaklanjutRepositoryMock.Repository)

		mockTindaklanjutRepo.On("FindNasabahNonBRI", mock.Anything, mock.AnythingOfType("string")).Return(nil, models2.ErrInternalServerError).Once()

		u := usecase.NewTindaklanjutUsecase(mockTindaklanjutRepo, l, timeoutContext)

		a, _, err := u.SubmitTindaklanjutNonBri(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockTindaklanjutRepo.AssertExpectations(t)
	})

	t.Run("error-submittindaklanjut", func(t *testing.T) {
		//repositoryMock
		mockTindaklanjutRepo := new(_tindaklanjutRepositoryMock.Repository)

		mockTindaklanjutRepo.On("FindNasabahNonBRI", mock.Anything, mock.AnythingOfType("string")).Return(mockResponseRepoGetDetail, nil).Once()
		mockTindaklanjutRepo.On("SubmitTindaklanjutNonBriCase1", mock.Anything, mock.AnythingOfType("*models.QuerySubmitTindaklanjut")).Return(models2.ErrInternalServerError).Once()

		u := usecase.NewTindaklanjutUsecase(mockTindaklanjutRepo, l, timeoutContext)

		a, _, err := u.SubmitTindaklanjutNonBri(context.TODO(), request)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockTindaklanjutRepo.AssertExpectations(t)
	})

	t.Run("error-updatetabelnasabah", func(t *testing.T) {
		//repositoryMock
		mockTindaklanjutRepo := new(_tindaklanjutRepositoryMock.Repository)

		mockTindaklanjutRepo.On("FindNasabahNonBRI", mock.Anything, mock.AnythingOfType("string")).Return(mockResponseRepoGetDetail, nil).Once()
		mockTindaklanjutRepo.On("SubmitTindaklanjutNonBriCase1", mock.Anything, mock.AnythingOfType("*models.QuerySubmitTindaklanjut")).Return(nil).Once()
		mockTindaklanjutRepo.On("UpdateStatusTabelNasabahNonBRISendiri", mock.Anything, mock.Anything, mock.Anything).Return(models2.ErrInternalServerError).Once()

		u := usecase.NewTindaklanjutUsecase(mockTindaklanjutRepo, l, timeoutContext)

		a, _, err := u.SubmitTindaklanjutNonBri(context.TODO(), requestTindaklanjutSendiri)

		assert.Error(t, err)
		assert.Nil(t, a)

		mockTindaklanjutRepo.AssertExpectations(t)
	})
}
