package usecase

import (
	"context"
	"fmt"
	"ms-bribrain-link5/helper"
	"ms-bribrain-link5/helper/logger"
	models2 "ms-bribrain-link5/helper/models"
	"ms-bribrain-link5/models"
	"ms-bribrain-link5/modules/tindaklanjut"
	"time"
)

type TindaklanjutUsecase struct {
	tindaklanjutRepo tindaklanjut.Repository
	contextTimeout   time.Duration
	log              logger.Logger
}

func NewTindaklanjutUsecase(tindaklanjutRepo tindaklanjut.Repository, log logger.Logger, timeout time.Duration) tindaklanjut.Usecase {
	return &TindaklanjutUsecase{
		tindaklanjutRepo: tindaklanjutRepo,
		contextTimeout:   timeout,
		log:              log,
	}
}

func (s TindaklanjutUsecase) SubmitTindaklanjutPositiveCase1(c context.Context, param *models.RequestSubmitTindaklanjutPositive1) (res *models.ResponseDetailNasabah, message string, err error) {
	ctx, cancel := context.WithTimeout(c, s.contextTimeout)
	defer cancel()
	log := "tindaklanjut.usecase.TindaklanjutUsecase.SubmitTindaklanjutPositiveCase1: %s"

	//cek nasabah
	detail, err := s.tindaklanjutRepo.FindNasabahBRI(ctx, helper.StringNullableToString(param.Cif))
	if err != nil {
		s.log.Error(log, err.Error())
		return nil, models2.ErrNotFound.Error(), models2.ErrNotFound
	}

	//submit tindaklanjut
	submittindaklanjut := models.MappingToQuerySubmitTindaklanjut(param)
	err = s.tindaklanjutRepo.SubmitTindaklanjutPositiveCase1(ctx, submittindaklanjut)
	if err != nil {
		s.log.Error(log, err.Error())
		return nil, models2.ErrGeneralMessage.Error(), err
	}
	//update id tindaklanjut tabel nasabah
	err = s.tindaklanjutRepo.UpdateStatusTabelNasabahBRI(ctx, param.Cif, detail.Id)
	if err != nil {
		s.log.Error(log, err.Error())
		return nil, models2.ErrGeneralMessage.Error(), err
	}

	// res = models.MappingResponseDetailNasabah(detail)

	s.log.Info(log, helper.JsonString(detail))
	return res, models2.GeneralSuccess, nil
}

func (s TindaklanjutUsecase) SubmitTindaklanjutPositiveCase2(c context.Context, param *models.RequestSubmitTindaklanjutPositive2) (res *models.ResponseDetailNasabah, message string, err error) {
	ctx, cancel := context.WithTimeout(c, s.contextTimeout)
	defer cancel()
	log := "tindaklanjut.usecase.TindaklanjutUsecase.SubmitTindaklanjutPositiveCase2: %s"

	//cek nasabah
	detail, err := s.tindaklanjutRepo.FindNasabahBRI(ctx, helper.StringNullableToString(param.Cif))
	if err != nil {
		s.log.Error(log, err.Error())
		return nil, models2.ErrNotFound.Error(), models2.ErrNotFound
	}

	//submit tindaklanjut
	submittindaklanjut := models.MappingToQuerySubmitTindaklanjutPositive2(param)
	err = s.tindaklanjutRepo.SubmitTindaklanjutPositiveCase2(ctx, submittindaklanjut)
	if err != nil {
		s.log.Error(log, err.Error())
		return nil, models2.ErrGeneralMessage.Error(), err
	}

	//update id tindaklanjut tabel nasabah
	err = s.tindaklanjutRepo.UpdateStatusTabelNasabahBRI(ctx, param.Cif, detail.Id)
	if err != nil {
		s.log.Error(log, err.Error())
		return nil, models2.ErrGeneralMessage.Error(), err
	}

	// res = models.MappingResponseDetailNasabah(detail)

	s.log.Info(log, helper.JsonString(detail))
	return res, models2.GeneralSuccess, nil
}

func (s TindaklanjutUsecase) SubmitTindaklanjutNegativeCase1(c context.Context, param *models.RequestSubmitTindaklanjutNegative1) (res *models.ResponseDetailNasabah, message string, err error) {
	ctx, cancel := context.WithTimeout(c, s.contextTimeout)
	defer cancel()
	log := "tindaklanjut.usecase.TindaklanjutUsecase.SubmitTindaklanjutNegativeCase1: %s"

	//cek nasabah
	detail, err := s.tindaklanjutRepo.FindNasabahBRI(ctx, helper.StringNullableToString(param.Cif))
	if err != nil {
		s.log.Error(log, err.Error())
		return nil, models2.ErrNotFound.Error(), models2.ErrNotFound
	}

	//submit tindaklanjut
	submittindaklanjut := models.MappingToQuerySubmitTindaklanjutNegative1(param)
	err = s.tindaklanjutRepo.SubmitTindaklanjutNegativeCase1(ctx, submittindaklanjut)
	if err != nil {
		s.log.Error(log, err.Error())
		return nil, models2.ErrGeneralMessage.Error(), err
	}

	//update id tindaklanjut tabel nasabah
	err = s.tindaklanjutRepo.UpdateStatusTabelNasabahBRI(ctx, param.Cif, detail.Id)
	if err != nil {
		s.log.Error(log, err.Error())
		return nil, models2.ErrGeneralMessage.Error(), err
	}

	// res = models.MappingResponseDetailNasabah(detail)

	s.log.Info(log, helper.JsonString(detail))
	return res, models2.GeneralSuccess, nil
}

func (s TindaklanjutUsecase) SubmitTindaklanjutNegativeCase2(c context.Context, param *models.RequestSubmitTindaklanjutNegative2) (res *models.ResponseDetailNasabah, message string, err error) {
	ctx, cancel := context.WithTimeout(c, s.contextTimeout)
	defer cancel()
	log := "tindaklanjut.usecase.TindaklanjutUsecase.SubmitTindaklanjutNegativeCase2: %s"

	//cek nasabah
	detail, err := s.tindaklanjutRepo.FindNasabahBRI(ctx, helper.StringNullableToString(param.Cif))
	if err != nil {
		s.log.Error(log, err.Error())
		return nil, models2.ErrNotFound.Error(), models2.ErrNotFound
	}

	//submit tindaklanjut
	submittindaklanjut := models.MappingToQuerySubmitTindaklanjutNegative2(param)
	err = s.tindaklanjutRepo.SubmitTindaklanjutNegativeCase2(ctx, submittindaklanjut)
	if err != nil {
		s.log.Error(log, err.Error())
		return nil, models2.ErrGeneralMessage.Error(), err
	}

	//update id tindaklanjut tabel nasabah
	err = s.tindaklanjutRepo.UpdateStatusTabelNasabahBRI(ctx, param.Cif, detail.Id)
	if err != nil {
		s.log.Error(log, err.Error())
		return nil, models2.ErrGeneralMessage.Error(), err
	}

	// res = models.MappingResponseDetailNasabah(detail)

	s.log.Info(log, helper.JsonString(detail))
	return res, models2.GeneralSuccess, nil
}

func (s TindaklanjutUsecase) SubmitTindaklanjutNegativeCase3(c context.Context, param *models.RequestSubmitTindaklanjutNegative3) (res *models.ResponseDetailNasabah, message string, err error) {
	ctx, cancel := context.WithTimeout(c, s.contextTimeout)
	defer cancel()
	log := "tindaklanjut.usecase.TindaklanjutUsecase.SubmitTindaklanjutNegativeCase3: %s"

	//cek nasabah
	detail, err := s.tindaklanjutRepo.FindNasabahBRI(ctx, helper.StringNullableToString(param.Cif))
	if err != nil {
		s.log.Error(log, err.Error())
		return nil, models2.ErrNotFound.Error(), models2.ErrNotFound
	}

	//submit tindaklanjut
	submittindaklanjut := models.MappingToQuerySubmitTindaklanjutNegative3(param)
	err = s.tindaklanjutRepo.SubmitTindaklanjutNegativeCase3(ctx, submittindaklanjut)
	if err != nil {
		s.log.Error(log, err.Error())
		return nil, models2.ErrGeneralMessage.Error(), err
	}

	//update id tindaklanjut tabel nasabah
	err = s.tindaklanjutRepo.UpdateStatusTabelNasabahBRI(ctx, param.Cif, detail.Id)
	if err != nil {
		s.log.Error(log, err.Error())
		return nil, models2.ErrGeneralMessage.Error(), err
	}

	// res = models.MappingResponseDetailNasabah(detail)

	s.log.Info(log, helper.JsonString(detail))
	return res, models2.GeneralSuccess, nil
}

func (s TindaklanjutUsecase) SubmitTindaklanjutOtherCase(c context.Context, param *models.RequestSubmitTindaklanjutOther) (res *models.ResponseDetailNasabah, message string, err error) {
	ctx, cancel := context.WithTimeout(c, s.contextTimeout)
	defer cancel()
	log := "tindaklanjut.usecase.TindaklanjutUsecase.SubmitTindaklanjutOtherCase: %s"

	//cek nasabah
	detail, err := s.tindaklanjutRepo.FindNasabahBRI(ctx, helper.StringNullableToString(param.Cif))
	if err != nil {
		s.log.Error(log, err.Error())
		return nil, models2.ErrNotFound.Error(), models2.ErrNotFound
	}

	//submit tindaklanjut
	submittindaklanjut := models.MappingToQuerySubmitTindaklanjutOther(param)
	err = s.tindaklanjutRepo.SubmitTindaklanjutOtherCase(ctx, submittindaklanjut)
	if err != nil {
		s.log.Error(log, err.Error())
		return nil, models2.ErrGeneralMessage.Error(), err
	}

	//update id tindaklanjut tabel nasabah
	err = s.tindaklanjutRepo.UpdateStatusTabelNasabahBRI(ctx, param.Cif, detail.Id)
	if err != nil {
		s.log.Error(log, err.Error())
		return nil, models2.ErrGeneralMessage.Error(), err
	}

	// res = models.MappingResponseDetailNasabah(detail)

	s.log.Info(log, helper.JsonString(detail))
	return res, models2.GeneralSuccess, nil
}

func (s TindaklanjutUsecase) SubmitTindaklanjutTandai(c context.Context, param *models.RequestSubmitTindaklanjutTandai) (res *models.ResponseDetailNasabah, message string, err error) {
	ctx, cancel := context.WithTimeout(c, s.contextTimeout)
	defer cancel()
	log := "tindaklanjut.usecase.TindaklanjutUsecase.SubmitTindaklanjutTandai: %s"

	//cek nasabah
	detail, err := s.tindaklanjutRepo.FindNasabahBRI(ctx, helper.StringNullableToString(param.Cif))
	if err != nil {
		s.log.Error(log, err.Error())
		return nil, models2.ErrNotFound.Error(), models2.ErrNotFound
	}

	//submit tindaklanjut
	submittindaklanjut := models.MappingToQuerySubmitTindaklanjutTandai(param)
	err = s.tindaklanjutRepo.SubmitTindaklanjutTandai(ctx, submittindaklanjut)
	if err != nil {
		s.log.Error(log, err.Error())
		return nil, models2.ErrGeneralMessage.Error(), err
	}

	// res = models.MappingResponseDetailNasabah(detail)

	s.log.Info(log, helper.JsonString(detail))
	return res, models2.GeneralSuccess, nil
}

func (s TindaklanjutUsecase) SubmitTindaklanjutNonBri(c context.Context, param *models.RequestSubmitTindaklanjutNonBri) (res *models.ResponseDetailNasabah, message string, err error) {
	ctx, cancel := context.WithTimeout(c, s.contextTimeout)
	defer cancel()
	log := "tindaklanjut.usecase.TindaklanjutUsecase.SubmitTindaklanjutNonBri: %s"

	//cek nasabah
	detail, err := s.tindaklanjutRepo.FindNasabahNonBRI(ctx, helper.StringNullableToString(param.KodeNonBri))
	if err != nil {
		s.log.Error(log, err.Error())
		return nil, models2.ErrNotFound.Error(), models2.ErrNotFound
	}

	//submit tindaklanjut
	submittindaklanjut := models.MappingToQuerySubmitTindaklanjutNonBri(param)
	if param.KodeHubungi == "1" {
		// if param.TglHubungi != nil {
		err = s.tindaklanjutRepo.SubmitTindaklanjutNonBriCase1(ctx, submittindaklanjut)
	} else {
		err = s.tindaklanjutRepo.SubmitTindaklanjutNonBriCase2Dan3(ctx, submittindaklanjut)

		if err != nil {
			s.log.Error(log, err.Error())
			return nil, models2.ErrGeneralMessage.Error(), err
		}

		err = s.tindaklanjutRepo.UpdateStatusTabelNasabahNonBRI(ctx, param.KodeNonBri, detail.Id)
	}

	if err != nil {
		s.log.Error(log, err.Error())
		return nil, models2.ErrGeneralMessage.Error(), err
	}

	updatetabelnasabah := models.MappingToQueryUpdateStatusNasabahNonBri(param)
	//update no telpon, alamat pada tabel nasabah non bri
	//update referal
	if param.KodeHubungi == "1" && param.KodeCabangReferal != "" {
		searchRMReferal, err := s.tindaklanjutRepo.FindRmWithLeastNonBriCustomer(ctx, param.KodeCabangReferal)
		if err != nil {
			s.log.Error(log, err.Error())
			return nil, models2.ErrNotFound.Error(), models2.ErrNotFound
		}

		// fmt.Println("RM_TOBEASSIGNED -> ", helper.JsonString(searchRMReferal))

		getInfoRMReferal, err := s.tindaklanjutRepo.GetInfoRMReferal(ctx, param.KodeCabangReferal, helper.StringNullableToString(searchRMReferal.PnRm))
		if err != nil {
			s.log.Error(log, err.Error())
			return nil, models2.ErrNotFound.Error(), models2.ErrNotFound
		}

		fmt.Println("RM_TOBEASSIGNED -> ", helper.JsonString(getInfoRMReferal))

		updatereferaltabelnasabah := models.MappingToQueryUpdateStatusReferalNasabahNonBri(getInfoRMReferal, param)

		err = s.tindaklanjutRepo.UpdateStatusTabelNasabahNonBRIReferal(ctx, param.KodeNonBri, updatereferaltabelnasabah)
		if err != nil {
			s.log.Error(log, err.Error())
			return nil, models2.ErrGeneralMessage.Error(), err
		}

		err = s.tindaklanjutRepo.UpdateTabelStatusTindaklanjutNonBRIReferal(ctx, param.KodeNonBri, getInfoRMReferal)
		if err != nil {
			s.log.Error(log, err.Error())
			return nil, models2.ErrGeneralMessage.Error(), err
		}

		err = s.tindaklanjutRepo.InsertHistoriReferal(ctx, param.KodeNonBri, detail, getInfoRMReferal, updatereferaltabelnasabah)

		//update sendiri
	} else if param.KodeHubungi == "1" && param.KodeCabangReferal == "" {
		err = s.tindaklanjutRepo.UpdateStatusTabelNasabahNonBRISendiri(ctx, param.KodeNonBri, updatetabelnasabah)
	}

	if err != nil {
		s.log.Error(log, err.Error())
		return nil, models2.ErrGeneralMessage.Error(), err
	}

	// res = models.MappingResponseDetailNasabah(detail)

	s.log.Info(log, helper.JsonString(detail))
	return res, models2.GeneralSuccess, nil
}

func (s TindaklanjutUsecase) SubmitKetertarikanNonBri(c context.Context, param *models.RequestSubmitKetertarikanNonBri) (res *models.ResponseDetailNasabah, message string, err error) {
	ctx, cancel := context.WithTimeout(c, s.contextTimeout)
	defer cancel()
	log := "tindaklanjut.usecase.TindaklanjutUsecase.SubmitKetertarikanNonBri: %s"

	//cek nasabah
	detail, err := s.tindaklanjutRepo.FindNasabahNonBRI(ctx, helper.StringNullableToString(param.KodeNonBri))
	if err != nil {
		s.log.Error(log, err.Error())
		return nil, models2.ErrNotFound.Error(), models2.ErrNotFound
	}

	detailTL, errTL := s.tindaklanjutRepo.FindTindaklanjutNasabahNonBRI(ctx, helper.StringNullableToString(param.KodeNonBri))
	if errTL != nil {
		s.log.Error(log, err.Error())
		return nil, models2.ErrNotFound.Error(), models2.ErrNotFound
	}

	if helper.Int64NullableToInt(detailTL.KodeHubungi) != 1 {
		return nil, models2.ErrBadParamInput.Error(), models2.ErrBadParamInput
	}

	//submit tindaklanjut
	submittindaklanjut := models.MappingToQuerySubmitKetertarikanNonBri(param)
	if param.KodeKetertarikan == "1" {
		// if param.TglHubungi != nil {
		err = s.tindaklanjutRepo.SubmitTindaklanjutNonBriTertarik(ctx, submittindaklanjut)
	} else {
		err = s.tindaklanjutRepo.SubmitTindaklanjutNonBriTidakTertarik(ctx, submittindaklanjut)
	}

	if err != nil {
		s.log.Error(log, err.Error())
		return nil, models2.ErrGeneralMessage.Error(), err
	}

	//update id tindaklanjut tabel nasabah
	err = s.tindaklanjutRepo.UpdateStatusTabelNasabahNonBRI(ctx, param.KodeNonBri, detail.Id)
	if err != nil {
		s.log.Error(log, err.Error())
		return nil, models2.ErrGeneralMessage.Error(), err
	}

	// res = models.MappingResponseDetailNasabah(detail)

	s.log.Info(log, helper.JsonString(detail))
	return res, models2.GeneralSuccess, nil
}

func (s TindaklanjutUsecase) FindRmInBranch(c context.Context, kodecabang string) (res string, message string, err error) {
	ctx, cancel := context.WithTimeout(c, s.contextTimeout)
	defer cancel()
	log := "tindaklanjut.usecase.TindaklanjutUsecase.SubmitTindaklanjutPositiveCase1: %s"

	//cek nasabah
	detail, err := s.tindaklanjutRepo.FindRmWithLeastNonBriCustomer(ctx, kodecabang)
	if err != nil {
		s.log.Error(log, err.Error())
		res = "RM Tidak Ditemukan"
		return res, models2.GeneralSuccess, nil
	}

	s.log.Info(log, helper.JsonString(detail))
	res = "RM Ditemukan"
	return res, models2.GeneralSuccess, nil
}
