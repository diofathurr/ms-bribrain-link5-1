package repository

import (
	"context"
	"errors"
	"fmt"
	"ms-bribrain-link5/helper/logger"
	"ms-bribrain-link5/models"
	"ms-bribrain-link5/modules/tindaklanjut"
	"time"

	"gorm.io/gorm"
)

type TindaklanjutRepository struct {
	Conn  *gorm.DB
	log   logger.Logger
	table *models.NasabahTable
}

func NewTindaklanjutRepository(Conn *gorm.DB, log logger.Logger) tindaklanjut.Repository {
	table := &models.NasabahTable{}
	table = table.MappingTable()
	return &TindaklanjutRepository{Conn, log, table}
}

func (s TindaklanjutRepository) FindNasabahBRI(ctx context.Context, param string) (res *models.GetDetailQueryStruct, err error) {
	var hasil models.GetDetailQueryStruct
	query := s.Conn.WithContext(ctx).Table(s.table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table).
		Select(s.table.SelectAllColumnBribrainLink5DashboardDetailInformasiNasabahBRITable())
	// query = query.Joins(`LEFT JOIN ` + n.table.BribrainLink5StatusTindaklanjut.Table + ` ON ` + n.table.BribrainLink5DashboardDetailInformasiNasabahBRI.CifNasabahRekan + ` = ` + n.table.BribrainLink5StatusTindaklanjut.Cifno)
	query = query.Where(s.table.BribrainLink5DashboardDetailInformasiNasabahBRI.CifNasabahRekan+`= ?`, param)

	result := query.Limit(1).Scan(&hasil)

	rnf := result.RowsAffected
	fmt.Println("not found status -> ", rnf)

	if rnf == 0 {
		err := errors.New("record not found")
		s.log.Error("tindaklanjut.repository.TindaklanjutRepository.FindNasabahBRI: %s", err.Error())
		return nil, err
	}

	res = &hasil
	return res, nil
}

func (s TindaklanjutRepository) FindNasabahNonBRI(ctx context.Context, param string) (res *models.GetDetailQueryStruct, err error) {
	var hasil models.GetDetailQueryStruct
	query := s.Conn.WithContext(ctx).Table(s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.Table).
		Select(s.table.SelectAllColumnBribrainLink5DashboardDetailInformasiNasabahNonBRITable())
	// query = query.Joins(`LEFT JOIN ` + n.table.BribrainLink5StatusTindaklanjut.Table + ` ON ` + n.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.CifNasabahRekan + ` = ` + n.table.BribrainLink5StatusTindaklanjut.Cifno)
	query = query.Where(s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.KodeNonBri+`= ?`, param)

	result := query.Limit(1).Scan(&hasil)

	rnf := result.RowsAffected
	fmt.Println("not found status -> ", rnf)

	if rnf == 0 {
		err := errors.New("record not found")
		s.log.Error("tindaklanjut.repository.TindaklanjutRepository.FindNasabahNonBRI: %s", err.Error())
		return nil, err
	}

	res = &hasil
	return res, nil
}

func (s TindaklanjutRepository) FindTindaklanjutNasabahBRI(ctx context.Context, param string) (res *models.GetDetailQueryStruct, err error) {
	var hasil models.GetDetailQueryStruct
	query := s.Conn.WithContext(ctx).Table(s.table.BribrainLink5StatusTindaklanjut.Table).
		Select(s.table.SelectAllColumnBribrainLink5StatusTindaklanjutTable())
	// query = query.Joins(`LEFT JOIN ` + n.table.BribrainLink5StatusTindaklanjut.Table + ` ON ` + n.table.BribrainLink5StatusTindaklanjut.CifNasabahRekan + ` = ` + n.table.BribrainLink5StatusTindaklanjut.Cifno)
	query = query.Where(s.table.BribrainLink5StatusTindaklanjut.Cifno+`= ?`, param)

	result := query.Limit(1).Scan(&hasil)

	err = result.Error
	if err != nil {
		s.log.Error("tindaklanjut.repository.TindaklanjutRepository.FindTindaklanjutNasabahBRI: %s", err.Error())
		if err.Error() != gorm.ErrRecordNotFound.Error() {
			return nil, err
		}
		return nil, nil
	}

	res = &hasil
	return res, nil
}

func (s TindaklanjutRepository) FindTindaklanjutNasabahNonBRI(ctx context.Context, param string) (res *models.GetDetailQueryStruct, err error) {
	var hasil models.GetDetailQueryStruct
	query := s.Conn.WithContext(ctx).Table(s.table.BribrainLink5StatusTindaklanjut.Table).
		Select(s.table.SelectAllColumnBribrainLink5StatusTindaklanjutTable())
	// query = query.Joins(`LEFT JOIN ` + n.table.BribrainLink5StatusTindaklanjut.Table + ` ON ` + n.table.BribrainLink5StatusTindaklanjut.CifNasabahRekan + ` = ` + n.table.BribrainLink5StatusTindaklanjut.Cifno)
	query = query.Where(s.table.BribrainLink5StatusTindaklanjut.KodeNonBri+`= ?`, param)

	result := query.Limit(1).Scan(&hasil)

	err = result.Error
	if err != nil {
		s.log.Error("tindaklanjut.repository.TindaklanjutRepository.FindTindaklanjutNasabahNonBRI: %s", err.Error())
		if err.Error() != gorm.ErrRecordNotFound.Error() {
			return nil, err
		}
		return nil, nil
	}

	res = &hasil
	return res, nil
}

func (s TindaklanjutRepository) FindRmWithLeastNonBriCustomer(ctx context.Context, param string) (res *models.QueryGetInfoRM, err error) {
	var hasil models.QueryGetInfoRM
	query := s.Conn.WithContext(ctx).Table(s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.Table).
		Select(s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.KodeMainbranch + `,` +
			s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.PnRm + `, COUNT(*)`)
	query = query.Where(s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.KodeMainbranch+`= ?`, param)
	query = query.Group(s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.KodeMainbranch + `,` + s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.PnRm)
	query = query.Order(`COUNT(*) ASC, ` + s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.PnRm)

	result := query.Limit(1).Scan(&hasil)

	rnf := result.RowsAffected
	fmt.Println("returned row -> ", rnf)

	if rnf == 0 {
		err := errors.New("record not found")
		s.log.Error("tindaklanjut.repository.TindaklanjutRepository.FindNasabahNonBRI: %s", err.Error())
		return nil, err
	}

	res = &hasil
	return res, nil
}

func (s TindaklanjutRepository) GetInfoRMReferal(ctx context.Context, kodeMainbranch, pnRm string) (res *models.QueryGetInfoRM, err error) {
	var hasil models.QueryGetInfoRM
	query := s.Conn.WithContext(ctx).Table(s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.Table).
		Select(s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.KodeMainbranch + `,` +
			s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.Mbdesc + `,` +
			s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.PnRm + `,` +
			s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.NamaRm + `,` +
			s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.BranchRm + `,` +
			s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.PhonenumberRm + `,` +
			s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.KodeRegion + `,` +
			s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.Rgdesc)
	query = query.Where(s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.KodeMainbranch+`= ?`, kodeMainbranch)
	query = query.Where(s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.PnRm+`= ?`, pnRm)
	// query = query.Group(s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.KodeMainbranch + `,` + s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.PnRm)
	// query = query.Order(`COUNT(*) ASC, ` + s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.PnRm)

	result := query.Limit(1).Scan(&hasil)

	rnf := result.RowsAffected
	fmt.Println("not found status -> ", rnf)

	if rnf == 0 {
		err := errors.New("record not found")
		s.log.Error("tindaklanjut.repository.TindaklanjutRepository.FindNasabahNonBRI: %s", err.Error())
		return nil, err
	}

	res = &hasil
	return res, nil
}

func (s TindaklanjutRepository) SubmitTindaklanjutPositiveCase1(ctx context.Context, param *models.QuerySubmitTindaklanjut) (err error) {
	//code tabel baru
	updatequery := s.Conn.WithContext(ctx).Table(s.table.BribrainLink5StatusTindaklanjut.Table)
	updatequery = updatequery.Where(s.table.BribrainLink5StatusTindaklanjut.Cifno+` = ?`, param.Cifno)
	updatequery = updatequery.Where(s.table.BribrainLink5StatusTindaklanjut.TipeCustomer+` = ?`, "BRI")
	updatequery = updatequery.Updates(map[string]interface{}{
		s.table.BribrainLink5StatusTindaklanjut.KodeHubungi:             param.KodeHubungi,
		s.table.BribrainLink5StatusTindaklanjut.KodeKunjungan:           param.KodeKunjungan,
		s.table.BribrainLink5StatusTindaklanjut.KodeKetertarikan:        param.KodeKetertarikan,
		s.table.BribrainLink5StatusTindaklanjut.TglHubungi:              param.TglHubungi,
		s.table.BribrainLink5StatusTindaklanjut.TglKunjungan:            param.TglKunjungan,
		s.table.BribrainLink5StatusTindaklanjut.VolumePotensiRmSimpanan: param.VolumePotensiRmSimpanan,
		s.table.BribrainLink5StatusTindaklanjut.VolumePotensiRmPinjaman: param.VolumePotensiRmPinjaman,
		s.table.BribrainLink5StatusTindaklanjut.UpdatedDate:             param.UpdateDate,
		s.table.BribrainLink5StatusTindaklanjut.StatusHubungi:           param.StatusHubungi,
		s.table.BribrainLink5StatusTindaklanjut.StatusKunjungan:         param.StatusKunjungan,
		s.table.BribrainLink5StatusTindaklanjut.StatusKetertarikan:      param.StatusKetertarikan,
		s.table.BribrainLink5StatusTindaklanjut.InputPotensiRmSimpanan:  param.InputPotensiRmSimpanan,
		s.table.BribrainLink5StatusTindaklanjut.InputPotensiRmPinjaman:  param.InputPotensiRmPinjaman,
	})
	err = updatequery.Error

	if err != nil {
		s.log.Error("tindaklanjut.repository.TindaklanjutRepository.SubmitTindaklanjutPositiveCase1: %s", err.Error())
		return err
	}
	return nil
}

func (s TindaklanjutRepository) UpdateStatusTabelNasabahBRI(ctx context.Context, cifno *string, idStatus *int64) (err error) {
	//code tabel baru
	updatequery := s.Conn.WithContext(ctx).Table(s.table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table)
	updatequery = updatequery.Where(s.table.BribrainLink5DashboardDetailInformasiNasabahBRI.CifNasabahRekan+` = ?`, cifno)
	updatequery = updatequery.Updates(map[string]interface{}{
		s.table.BribrainLink5DashboardDetailInformasiNasabahBRI.StatusTindaklanjut: 1,
		s.table.BribrainLink5DashboardDetailInformasiNasabahBRI.UpdatedDate:        time.Now(),
	})
	err = updatequery.Error

	if err != nil {
		s.log.Error("tindaklanjut.repository.TindaklanjutRepository.UpdateStatusTabelNasabahBRI: %s", err.Error())
		return err
	}
	return nil
}

func (s TindaklanjutRepository) UpdateStatusTabelNasabahNonBRI(ctx context.Context, kodenonbri *string, idStatus *int64) (err error) {
	updatequery := s.Conn.WithContext(ctx).Table(s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.Table)
	updatequery = updatequery.Where(s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.KodeNonBri+` = ?`, kodenonbri)
	updatequery = updatequery.Updates(map[string]interface{}{
		s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.StatusTindaklanjut: 1,
		s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.UpdatedDate:        time.Now(),
	})

	err = updatequery.Error
	if err != nil {
		s.log.Error("tindaklanjut.repository.TindaklanjutRepository.UpdateStatusTabelNasabahNonBRI: %s", err.Error())
		return err
	}
	return nil
}

func (s TindaklanjutRepository) UpdateStatusTabelNasabahNonBRISendiri(ctx context.Context, kodenonbri *string, param *models.QueryUpdateStatusNasabah) (err error) {
	updatequery := s.Conn.WithContext(ctx).Table(s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.Table)
	updatequery = updatequery.Where(s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.KodeNonBri+` = ?`, kodenonbri)
	updatequery = updatequery.Updates(map[string]interface{}{
		s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.StatusTindaklanjut: 1,
		s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.InputAlamat:        param.InputAlamat,
		s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.InputPhonenumber:   param.InputPhonenumber,
		s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.UpdatedDate:        time.Now(),
	})

	err = updatequery.Error
	if err != nil {
		s.log.Error("tindaklanjut.repository.TindaklanjutRepository.UpdateStatusTabelNasabahNonBRI: %s", err.Error())
		return err
	}
	return nil
}

func (s TindaklanjutRepository) UpdateStatusTabelNasabahNonBRIReferal(ctx context.Context, kodenonbri *string, param *models.QueryUpdateStatusNasabah) (err error) {
	updatequery := s.Conn.WithContext(ctx).Table(s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.Table)
	updatequery = updatequery.Where(s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.KodeNonBri+` = ?`, kodenonbri)
	updatequery = updatequery.Updates(map[string]interface{}{
		s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.StatusTindaklanjut: 0,
		s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.InputAlamat:        param.InputAlamat,
		s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.InputPhonenumber:   param.InputPhonenumber,
		s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.KodeRegion:         param.KodeRegion,
		s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.Rgdesc:             param.Rgdesc,
		s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.KodeMainbranch:     param.KodeMainbranch,
		s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.Mbdesc:             param.Mbdesc,
		s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.PnRm:               param.PnRm,
		s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.NamaRm:             param.NamaRm,
		s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.BranchRm:           param.BranchRm,
		s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.PhonenumberRm:      param.PhonenumberRm,
		s.table.BribrainLink5DashboardDetailInformasiNasabahNonBRI.UpdatedDate:        time.Now(),
	})

	err = updatequery.Error
	if err != nil {
		s.log.Error("tindaklanjut.repository.TindaklanjutRepository.UpdateStatusTabelNasabahNonBRI: %s", err.Error())
		return err
	}
	return nil
}

func (s TindaklanjutRepository) UpdateTabelStatusTindaklanjutNonBRIReferal(ctx context.Context, kodenonbri *string, param *models.QueryGetInfoRM) (err error) {
	updatequery := s.Conn.WithContext(ctx).Table(s.table.BribrainLink5StatusTindaklanjut.Table)
	updatequery = updatequery.Where(s.table.BribrainLink5StatusTindaklanjut.KodeNonBri+` = ?`, kodenonbri)
	updatequery = updatequery.Where(s.table.BribrainLink5StatusTindaklanjut.TipeCustomer+` = ?`, "NONBRI")
	updatequery = updatequery.Updates(map[string]interface{}{
		s.table.BribrainLink5StatusTindaklanjut.KodeHubungi:    "0",
		s.table.BribrainLink5StatusTindaklanjut.TglHubungi:     "1900-01-01",
		s.table.BribrainLink5StatusTindaklanjut.StatusHubungi:  "Belum",
		s.table.BribrainLink5StatusTindaklanjut.PnRm:           param.PnRm,
		s.table.BribrainLink5StatusTindaklanjut.KodeRegion:     param.KodeRegion,
		s.table.BribrainLink5StatusTindaklanjut.Rgdesc:         param.Rgdesc,
		s.table.BribrainLink5StatusTindaklanjut.KodeMainbranch: param.KodeMainbranch,
		s.table.BribrainLink5StatusTindaklanjut.Mbdesc:         param.Mbdesc,
		s.table.BribrainLink5StatusTindaklanjut.UpdatedDate:    time.Now(),
	})

	err = updatequery.Error
	if err != nil {
		s.log.Error("tindaklanjut.repository.TindaklanjutRepository.UpdateStatusTabelNasabahNonBRI: %s", err.Error())
		return err
	}
	return nil
}

func (s TindaklanjutRepository) SubmitTindaklanjutPositiveCase2(ctx context.Context, param *models.QuerySubmitTindaklanjut) (err error) {

	//code tabel baru
	updatequery := s.Conn.WithContext(ctx).Table(s.table.BribrainLink5StatusTindaklanjut.Table)
	updatequery = updatequery.Where(s.table.BribrainLink5StatusTindaklanjut.Cifno+` = ?`, param.Cifno)
	updatequery = updatequery.Where(s.table.BribrainLink5StatusTindaklanjut.TipeCustomer+` = ?`, "BRI")
	updatequery = updatequery.Updates(map[string]interface{}{
		s.table.BribrainLink5StatusTindaklanjut.KodeHubungi:             param.KodeHubungi,
		s.table.BribrainLink5StatusTindaklanjut.KodeKunjungan:           param.KodeKunjungan,
		s.table.BribrainLink5StatusTindaklanjut.KodeKetertarikan:        param.KodeKetertarikan,
		s.table.BribrainLink5StatusTindaklanjut.TglHubungi:              param.TglHubungi,
		s.table.BribrainLink5StatusTindaklanjut.VolumePotensiRmSimpanan: param.VolumePotensiRmSimpanan,
		s.table.BribrainLink5StatusTindaklanjut.VolumePotensiRmPinjaman: param.VolumePotensiRmPinjaman,
		s.table.BribrainLink5StatusTindaklanjut.CatatanKunjungan:        param.CatatanKunjungan,
		s.table.BribrainLink5StatusTindaklanjut.UpdatedDate:             param.UpdateDate,
		s.table.BribrainLink5StatusTindaklanjut.StatusHubungi:           param.StatusHubungi,
		s.table.BribrainLink5StatusTindaklanjut.StatusKunjungan:         param.StatusKunjungan,
		s.table.BribrainLink5StatusTindaklanjut.StatusKetertarikan:      param.StatusKetertarikan,
		s.table.BribrainLink5StatusTindaklanjut.InputPotensiRmSimpanan:  param.InputPotensiRmSimpanan,
		s.table.BribrainLink5StatusTindaklanjut.InputPotensiRmPinjaman:  param.InputPotensiRmPinjaman,
	})
	err = updatequery.Error
	if err != nil {
		s.log.Error("tindaklanjut.repository.TindaklanjutRepository.SubmitTindaklanjutPositiveCase2: %s", err.Error())
		return err
	}
	return nil
}

func (s TindaklanjutRepository) SubmitTindaklanjutNegativeCase1(ctx context.Context, param *models.QuerySubmitTindaklanjut) (err error) {

	//code tabel baru
	updatequery := s.Conn.WithContext(ctx).Table(s.table.BribrainLink5StatusTindaklanjut.Table)
	updatequery = updatequery.Where(s.table.BribrainLink5StatusTindaklanjut.Cifno+` = ?`, param.Cifno)
	updatequery = updatequery.Where(s.table.BribrainLink5StatusTindaklanjut.TipeCustomer+` = ?`, "BRI")
	updatequery = updatequery.Updates(map[string]interface{}{
		s.table.BribrainLink5StatusTindaklanjut.KodeHubungi:         param.KodeHubungi,
		s.table.BribrainLink5StatusTindaklanjut.KodeKetertarikan:    param.KodeKetertarikan,
		s.table.BribrainLink5StatusTindaklanjut.TglHubungi:          param.TglHubungi,
		s.table.BribrainLink5StatusTindaklanjut.CatatanKetertarikan: param.CatatanKetertarikan,
		s.table.BribrainLink5StatusTindaklanjut.UpdatedDate:         param.UpdateDate,
		s.table.BribrainLink5StatusTindaklanjut.StatusHubungi:       param.StatusHubungi,
		s.table.BribrainLink5StatusTindaklanjut.StatusKetertarikan:  param.StatusKetertarikan,
	})
	err = updatequery.Error
	if err != nil {
		s.log.Error("tindaklanjut.repository.TindaklanjutRepository.SubmitTindaklanjutNegativeCase1: %s", err.Error())
		return err
	}
	return nil
}

func (s TindaklanjutRepository) SubmitTindaklanjutNegativeCase2(ctx context.Context, param *models.QuerySubmitTindaklanjut) (err error) {

	//code tabel baru
	updatequery := s.Conn.WithContext(ctx).Table(s.table.BribrainLink5StatusTindaklanjut.Table)
	updatequery = updatequery.Where(s.table.BribrainLink5StatusTindaklanjut.Cifno+` = ?`, param.Cifno)
	updatequery = updatequery.Where(s.table.BribrainLink5StatusTindaklanjut.TipeCustomer+` = ?`, "BRI")
	updatequery = updatequery.Updates(map[string]interface{}{
		s.table.BribrainLink5StatusTindaklanjut.KodeHubungi:      param.KodeHubungi,
		s.table.BribrainLink5StatusTindaklanjut.KodeKunjungan:    param.KodeKunjungan,
		s.table.BribrainLink5StatusTindaklanjut.CatatanHubungi:   param.CatatanHubungi,
		s.table.BribrainLink5StatusTindaklanjut.CatatanKunjungan: param.CatatanKunjungan,
		s.table.BribrainLink5StatusTindaklanjut.UpdatedDate:      param.UpdateDate,
		s.table.BribrainLink5StatusTindaklanjut.StatusHubungi:    param.StatusHubungi,
		s.table.BribrainLink5StatusTindaklanjut.StatusKunjungan:  param.StatusKunjungan,
	})
	err = updatequery.Error
	if err != nil {
		s.log.Error("tindaklanjut.repository.TindaklanjutRepository.SubmitTindaklanjutNegativeCase2: %s", err.Error())
		return err
	}
	return nil
}

func (s TindaklanjutRepository) SubmitTindaklanjutNegativeCase3(ctx context.Context, param *models.QuerySubmitTindaklanjut) (err error) {

	//code tabel baru
	updatequery := s.Conn.WithContext(ctx).Table(s.table.BribrainLink5StatusTindaklanjut.Table)
	updatequery = updatequery.Where(s.table.BribrainLink5StatusTindaklanjut.Cifno+` = ?`, param.Cifno)
	updatequery = updatequery.Where(s.table.BribrainLink5StatusTindaklanjut.TipeCustomer+` = ?`, "BRI")
	updatequery = updatequery.Updates(map[string]interface{}{
		s.table.BribrainLink5StatusTindaklanjut.KodeHubungi:    param.KodeHubungi,
		s.table.BribrainLink5StatusTindaklanjut.CatatanHubungi: param.CatatanHubungi,
		s.table.BribrainLink5StatusTindaklanjut.UpdatedDate:    param.UpdateDate,
		s.table.BribrainLink5StatusTindaklanjut.StatusHubungi:  param.StatusHubungi,
	})
	err = updatequery.Error
	if err != nil {
		s.log.Error("tindaklanjut.repository.TindaklanjutRepository.SubmitTindaklanjutNegativeCase3: %s", err.Error())
		return err
	}
	return nil
}

func (s TindaklanjutRepository) SubmitTindaklanjutOtherCase(ctx context.Context, param *models.QuerySubmitTindaklanjut) (err error) {

	updatequery := s.Conn.WithContext(ctx).Table(s.table.BribrainLink5StatusTindaklanjut.Table)
	updatequery = updatequery.Where(s.table.BribrainLink5StatusTindaklanjut.Cifno+` = ?`, param.Cifno)
	updatequery = updatequery.Where(s.table.BribrainLink5StatusTindaklanjut.TipeCustomer+` = ?`, "BRI")
	updatequery = updatequery.Updates(map[string]interface{}{
		s.table.BribrainLink5StatusTindaklanjut.KodeHubungi:        param.KodeHubungi,
		s.table.BribrainLink5StatusTindaklanjut.KodeKunjungan:      param.KodeKunjungan,
		s.table.BribrainLink5StatusTindaklanjut.KodeKetertarikan:   param.KodeKetertarikan,
		s.table.BribrainLink5StatusTindaklanjut.CatatanHubungi:     param.CatatanHubungi,
		s.table.BribrainLink5StatusTindaklanjut.TglKunjungan:       param.TglKunjungan,
		s.table.BribrainLink5StatusTindaklanjut.UpdatedDate:        param.UpdateDate,
		s.table.BribrainLink5StatusTindaklanjut.StatusHubungi:      param.StatusHubungi,
		s.table.BribrainLink5StatusTindaklanjut.StatusKunjungan:    param.StatusKunjungan,
		s.table.BribrainLink5StatusTindaklanjut.StatusKetertarikan: param.StatusKetertarikan,
	})
	err = updatequery.Error
	if err != nil {
		s.log.Error("tindaklanjut.repository.TindaklanjutRepository.SubmitTindaklanjutOtherCase: %s", err.Error())
		return err
	}
	return nil
}

func (s TindaklanjutRepository) SubmitTindaklanjutTandai(ctx context.Context, param *models.QuerySubmitTindaklanjut) (err error) {

	//code tabel baru
	updatequery := s.Conn.WithContext(ctx).Table(s.table.BribrainLink5StatusTindaklanjut.Table)
	updatequery = updatequery.Where(s.table.BribrainLink5StatusTindaklanjut.Cifno+` = ?`, param.Cifno)
	updatequery = updatequery.Where(s.table.BribrainLink5StatusTindaklanjut.TipeCustomer+` = ?`, "BRI")
	updatequery = updatequery.Updates(map[string]interface{}{
		s.table.BribrainLink5StatusTindaklanjut.KodeKunjungan:           param.KodeKunjungan,
		s.table.BribrainLink5StatusTindaklanjut.KodeKetertarikan:        param.KodeKetertarikan,
		s.table.BribrainLink5StatusTindaklanjut.CatatanKetertarikan:     param.CatatanKetertarikan,
		s.table.BribrainLink5StatusTindaklanjut.VolumePotensiRmSimpanan: param.VolumePotensiRmSimpanan,
		s.table.BribrainLink5StatusTindaklanjut.VolumePotensiRmPinjaman: param.VolumePotensiRmPinjaman,
		s.table.BribrainLink5StatusTindaklanjut.TglKunjungan:            param.TglKunjungan,
		s.table.BribrainLink5StatusTindaklanjut.UpdatedDate:             param.UpdateDate,
		s.table.BribrainLink5StatusTindaklanjut.StatusKunjungan:         param.StatusKunjungan,
		s.table.BribrainLink5StatusTindaklanjut.StatusKetertarikan:      param.StatusKetertarikan,
		s.table.BribrainLink5StatusTindaklanjut.InputPotensiRmSimpanan:  param.InputPotensiRmSimpanan,
		s.table.BribrainLink5StatusTindaklanjut.InputPotensiRmPinjaman:  param.InputPotensiRmPinjaman,
	})
	err = updatequery.Error
	if err != nil {
		s.log.Error("tindaklanjut.repository.TindaklanjutRepository.SubmitTindaklanjutTandai: %s", err.Error())
		return err
	}
	return nil
}

func (s TindaklanjutRepository) SubmitTindaklanjutNonBriCase2Dan3(ctx context.Context, param *models.QuerySubmitTindaklanjut) (err error) {

	//code tabel baru
	updatequery := s.Conn.WithContext(ctx).Table(s.table.BribrainLink5StatusTindaklanjut.Table)
	updatequery = updatequery.Where(s.table.BribrainLink5StatusTindaklanjut.KodeNonBri+` = ?`, param.KodeNonBri)
	updatequery = updatequery.Where(s.table.BribrainLink5StatusTindaklanjut.TipeCustomer+` = ?`, "NONBRI")
	updatequery = updatequery.Updates(map[string]interface{}{
		s.table.BribrainLink5StatusTindaklanjut.KodeHubungi:    param.KodeHubungi,
		s.table.BribrainLink5StatusTindaklanjut.CatatanHubungi: param.CatatanHubungi,
		s.table.BribrainLink5StatusTindaklanjut.UpdatedDate:    param.UpdateDate,
		s.table.BribrainLink5StatusTindaklanjut.StatusHubungi:  param.StatusHubungi,
	})
	err = updatequery.Error
	if err != nil {
		s.log.Error("tindaklanjut.repository.TindaklanjutRepository.SubmitTindaklanjutNonBriCase2Dan3: %s", err.Error())
		return err
	}
	return nil
}

func (s TindaklanjutRepository) SubmitTindaklanjutNonBriCase1(ctx context.Context, param *models.QuerySubmitTindaklanjut) (err error) {
	//code tabel baru
	updatequery := s.Conn.WithContext(ctx).Table(s.table.BribrainLink5StatusTindaklanjut.Table)
	updatequery = updatequery.Where(s.table.BribrainLink5StatusTindaklanjut.KodeNonBri+` = ?`, param.KodeNonBri)
	updatequery = updatequery.Where(s.table.BribrainLink5StatusTindaklanjut.TipeCustomer+` = ?`, "NONBRI")
	updatequery = updatequery.Updates(map[string]interface{}{
		s.table.BribrainLink5StatusTindaklanjut.KodeHubungi:   param.KodeHubungi,
		s.table.BribrainLink5StatusTindaklanjut.TglHubungi:    param.TglHubungi,
		s.table.BribrainLink5StatusTindaklanjut.UpdatedDate:   param.UpdateDate,
		s.table.BribrainLink5StatusTindaklanjut.StatusHubungi: param.StatusHubungi,
	})
	err = updatequery.Error
	if err != nil {
		s.log.Error("tindaklanjut.repository.TindaklanjutRepository.SubmitTindaklanjutNonBriCase1: %s", err.Error())
		return err
	}
	return nil
}

func (s TindaklanjutRepository) SubmitTindaklanjutNonBriTertarik(ctx context.Context, param *models.QuerySubmitTindaklanjut) (err error) {
	//code tabel baru
	updatequery := s.Conn.WithContext(ctx).Table(s.table.BribrainLink5StatusTindaklanjut.Table)
	updatequery = updatequery.Where(s.table.BribrainLink5StatusTindaklanjut.KodeNonBri+` = ?`, param.KodeNonBri)
	updatequery = updatequery.Where(s.table.BribrainLink5StatusTindaklanjut.TipeCustomer+` = ?`, "NONBRI")
	updatequery = updatequery.Updates(map[string]interface{}{
		s.table.BribrainLink5StatusTindaklanjut.KodeKetertarikan:   param.KodeKetertarikan,
		s.table.BribrainLink5StatusTindaklanjut.InputNorek:         param.InputNorek,
		s.table.BribrainLink5StatusTindaklanjut.UpdatedDate:        param.UpdateDate,
		s.table.BribrainLink5StatusTindaklanjut.StatusKetertarikan: param.StatusKetertarikan,
	})
	err = updatequery.Error
	if err != nil {
		s.log.Error("tindaklanjut.repository.TindaklanjutRepository.SubmitTindaklanjutNonBriTertarik: %s", err.Error())
		return err
	}
	return nil
}

func (s TindaklanjutRepository) SubmitTindaklanjutNonBriTidakTertarik(ctx context.Context, param *models.QuerySubmitTindaklanjut) (err error) {
	//code tabel baru
	updatequery := s.Conn.WithContext(ctx).Table(s.table.BribrainLink5StatusTindaklanjut.Table)
	updatequery = updatequery.Where(s.table.BribrainLink5StatusTindaklanjut.KodeNonBri+` = ?`, param.KodeNonBri)
	updatequery = updatequery.Where(s.table.BribrainLink5StatusTindaklanjut.TipeCustomer+` = ?`, "NONBRI")
	updatequery = updatequery.Updates(map[string]interface{}{
		s.table.BribrainLink5StatusTindaklanjut.KodeKetertarikan:    param.KodeKetertarikan,
		s.table.BribrainLink5StatusTindaklanjut.UpdatedDate:         param.UpdateDate,
		s.table.BribrainLink5StatusTindaklanjut.StatusKetertarikan:  param.StatusKetertarikan,
		s.table.BribrainLink5StatusTindaklanjut.CatatanKetertarikan: param.CatatanKetertarikan,
	})
	err = updatequery.Error
	if err != nil {
		s.log.Error("tindaklanjut.repository.TindaklanjutRepository.SubmitTindaklanjutNonBriTidakTertarik: %s", err.Error())
		return err
	}
	return nil
}

func (s TindaklanjutRepository) InsertHistoriReferal(ctx context.Context, kodenonbri *string, dataRMSebelum *models.GetDetailQueryStruct, dataRMSesudah *models.QueryGetInfoRM, dataNasabah *models.QueryUpdateStatusNasabah) (err error) {
	insertquery := s.Conn.WithContext(ctx).Table(s.table.BribrainLink5Referal.Table)
	insertquery = insertquery.Create(map[string]interface{}{
		s.table.BribrainLink5Referal.KodeNonBri:            kodenonbri,
		s.table.BribrainLink5Referal.PnRmPengirim:          dataRMSebelum.PnRm,
		s.table.BribrainLink5Referal.PnRmPenerima:          dataRMSesudah.PnRm,
		s.table.BribrainLink5Referal.MainbranchCodeSebelum: dataRMSebelum.KodeMainbranch,
		s.table.BribrainLink5Referal.MainbranchCodeSesudah: dataRMSesudah.KodeMainbranch,
		s.table.BribrainLink5Referal.Alamat:                dataNasabah.InputAlamat,
		s.table.BribrainLink5Referal.Phonenumber:           dataNasabah.InputPhonenumber,
		s.table.BribrainLink5Referal.CreatedDate:           time.Now(),
	})

	err = insertquery.Error
	if err != nil {
		s.log.Error("tindaklanjut.repository.TindaklanjutRepository.UpdateStatusTabelNasabahNonBRI: %s", err.Error())
		return err
	}
	return nil
}
