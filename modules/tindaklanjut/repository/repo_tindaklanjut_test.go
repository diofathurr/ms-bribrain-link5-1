package repository_test

import (
	"context"
	"ms-bribrain-link5/helper"
	"ms-bribrain-link5/helper/logger"
	"ms-bribrain-link5/models"
	tindaklanjutRepo "ms-bribrain-link5/modules/tindaklanjut/repository"
	"regexp"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
)

func TestTindaklanjutRepository_FindNasabahBRI(t *testing.T) {
	err := helper.NewMockEnv()
	db, mock, err := helper.NewMockDB()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	//requestMock
	request := &models.RequestDetailNasabahEncrypted{}
	request = request.MappingExampleData()

	//resultMock
	mockDetailQueryStruct := &models.GetDetailQueryStruct{}
	mockDetailQueryStruct = mockDetailQueryStruct.MappingExampleData()
	pnPengelola := "00999951"
	// role := "RM"

	//rowAndColumnMock
	table := &models.NasabahTable{}
	table = table.MappingTable()
	//rowAndColumnMock
	row, fields := helper.GetValueAndColumnStructToDriverValue(mockDetailQueryStruct)

	// querySelect := table.BribrainLink5DashboardDetailInformasiNasabahBRI.UpdatedDate + ` AS UPDATED_DATE`

	// queryJoin := `INNER join ` + table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table + ` on ` + table.BribrainLink5DashboardDetailInformasiNasabahBRI.IdRekomendasi + ` = ` + table.BribrainLink5DashboardDetailInformasiNasabahBRI.Id

	// queryWhere := table.BribrainLink5DashboardDetailInformasiNasabahBRI.PnRm + " =?"

	// queryOrder := table.BribrainLink5DashboardDetailInformasiNasabahBRI.UpdatedDate + " desc"
	// t.Run("success", func(t *testing.T) {
	// 	rows := sqlmock.NewRows(fields).
	// 		AddRow(row...)
	// 	// query := "SELECT " + querySelect + " FROM `" + table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table + "` " + " WHERE " + queryWhere + " ORDER BY " + queryOrder
	// 	query := `SELECT . AS CIF_NASABAH_REKAN,. AS NAMA_NASABAH_REKAN,. AS NAMA_NASABAH_UTAMA,. AS POSISI_TIER,. AS USIA,. AS NOMOR_KTP,. AS PN_RM,. AS JENIS_KELAMIN,. AS SEKTOR_EKONOMI,. AS TIER_DIATASNYA,. AS RM_DIATASNYA,. AS BRANCH,. AS RGDESC,. AS MBDESC,. AS SBDESC,. AS BRDESC,. AS PHONENUMBER,. AS ALAMAT_ID,. AS ALAMAT_DOMISILI,. AS ALAMAT_PINJAMAN,. AS USIA_REKENING_BULAN,. AS PRODUK_SIMPANAN,. AS SNAME_RM_SIMPANAN,. AS PRODUK_PINJAMAN,. AS SNAME_RM_PINJAMAN,. AS SEGMEN,. AS PHONENUMBER_RM_PINJAMAN,. AS JENIS_PEKERJAAN,. AS BIDANG_PEKERJAAN,. AS KOLEKTABILITAS,. AS TOTAL_PLAFON,. AS TOTAL_OUTSTANDING,. AS INET_BANKING,. AS SMS_BANKING,. AS TOTAL_CREDIT_BRI,. AS TOTAL_DEBIT_BRI,. AS TOTAL_KREDIT_NON_BRI,. AS TOTAL_DEBIT_NON_BRI,. AS TOTAL_KREDIT_DEBIT_NON_BRI,. AS NAMA_NASABAH_NON_BRI,. AS NAMA_BANK_LAWAN,. AS RATAS_SALDO_SIMPANAN_BRI,. AS TOTAL_CREDIT_KESELURUHAN,. AS TOTAL_DEBIT_KESELURUHAN,. AS CIF_NASABAH_TIER_1,. AS NAMA_NASABAH_TIER_1,. AS SNAME_RM_PINJAMAN_TIER_1,. AS UNIT_RM_TIER_1,. AS PHONENUMBER_RM_TIER_1,. AS CIF_NASABAH_TIER_2,. AS NAMA_NASABAH_TIER_2,. AS SNAME_RM_PINJAMAN_TIER_2,. AS UNIT_RM_TIER_2,. AS PHONENUMBER_TIER_2,. AS CIF_NASABAH_TIER_3,. AS NAMA_NASABAH_TIER_3,. AS SNAME_RM_PINJAMAN_TIER_3,. AS UNIT_RM_TIER_3,. AS PHONENUMBER_TIER_3,. AS CIF_NASABAH_TIER_4,. AS NAMA_NASABAH_TIER_4,. AS SNAME_RM_PINJAMAN_TIER_4,. AS UNIT_RM_TIER_4,. AS PHONENUMBER_TIER_4,. AS STATUS_REKENING,. AS AKUISISI_PINJAMAN,. AS LAMA_NASABAH_BULAN,. AS TANGGAL_LAHIR,. AS DS,. AS CREATED_DATE,. AS STATUS_TINDAKLANJUT,. AS TOPUP_SIMPANAN,. AS SUPLESI_PINJAMAN,. AS VOLUME_POTENSI_PINJAMAN,. AS VOLUME_POTENSI_SIMPANAN, case when .=0 then` + "Belum" + ` when .=1 then ` + "Sudah" + ` END AS STATUS_AKUISISI_SIMPANAN, case when .=0 AND .=0 then ` + "Belum" + ` when .=1 OR .=1 then ` + "Sudah" + ` END AS STATUS_AKUISISI_PINJAMAN FROM ` + ` WHERE .= ? LIMIT 1`
	// 	queryRegex := regexp.QuoteMeta(query)
	// 	mock.ExpectQuery(queryRegex).
	// 		WithArgs(pnPengelola).
	// 		WillReturnRows(rows)
	// 	a := nasabahRepo.NewNasabahRepository(db, logger.L)

	// 	detail, err := a.GetDetail(context.TODO(), request)
	// 	assert.NoError(t, err)
	// 	assert.NotNil(t, detail)
	// })

	t.Run("success", func(t *testing.T) {
		rows := sqlmock.NewRows(fields).
			AddRow(row...)
		// query := "SELECT " + querySelect + " FROM `" + table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table + "` " + " WHERE " + queryWhere + " ORDER BY " + queryOrder
		query := `SELECT . AS CIF_NASABAH_REKAN,. AS NAMA_NASABAH_REKAN,. AS NAMA_NASABAH_UTAMA,. AS POSISI_TIER,. AS USIA,. AS NOMOR_KTP,. AS PN_RM,. AS JENIS_KELAMIN,. AS SEKTOR_EKONOMI,. AS TIER_DIATASNYA,. AS RM_DIATASNYA,. AS BRANCH,. AS RGDESC,. AS MBDESC,. AS SBDESC,. AS BRDESC,. AS PHONENUMBER,. AS ALAMAT_ID,. AS ALAMAT_DOMISILI,. AS ALAMAT_PINJAMAN,. AS USIA_REKENING_BULAN,. AS PRODUK_SIMPANAN,. AS SNAME_RM_SIMPANAN,. AS PRODUK_PINJAMAN,. AS SNAME_RM_PINJAMAN,. AS SEGMEN,. AS PHONENUMBER_RM_PINJAMAN,. AS JENIS_PEKERJAAN,. AS BIDANG_PEKERJAAN,. AS KOLEKTABILITAS,. AS TOTAL_PLAFON,. AS TOTAL_OUTSTANDING,. AS INET_BANKING,. AS SMS_BANKING,. AS TOTAL_CREDIT_BRI,. AS TOTAL_DEBIT_BRI,. AS TOTAL_KREDIT_NON_BRI,. AS TOTAL_DEBIT_NON_BRI,. AS TOTAL_KREDIT_DEBIT_NON_BRI,. AS NAMA_NASABAH_NON_BRI,. AS NAMA_BANK_LAWAN,. AS RATAS_SALDO_SIMPANAN_BRI,. AS TOTAL_CREDIT_KESELURUHAN,. AS TOTAL_DEBIT_KESELURUHAN,. AS CIF_NASABAH_TIER_1,. AS NAMA_NASABAH_TIER_1,. AS SNAME_RM_PINJAMAN_TIER_1,. AS UNIT_RM_TIER_1,. AS PHONENUMBER_RM_TIER_1,. AS CIF_NASABAH_TIER_2,. AS NAMA_NASABAH_TIER_2,. AS SNAME_RM_PINJAMAN_TIER_2,. AS UNIT_RM_TIER_2,. AS PHONENUMBER_TIER_2,. AS CIF_NASABAH_TIER_3,. AS NAMA_NASABAH_TIER_3,. AS SNAME_RM_PINJAMAN_TIER_3,. AS UNIT_RM_TIER_3,. AS PHONENUMBER_TIER_3,. AS CIF_NASABAH_TIER_4,. AS NAMA_NASABAH_TIER_4,. AS SNAME_RM_PINJAMAN_TIER_4,. AS UNIT_RM_TIER_4,. AS PHONENUMBER_TIER_4,. AS STATUS_REKENING,. AS AKUISISI_PINJAMAN,. AS LAMA_NASABAH_BULAN,. AS TANGGAL_LAHIR,. AS DS,. AS CREATED_DATE,. AS STATUS_TINDAKLANJUT,. AS TOPUP_SIMPANAN,. AS SUPLESI_PINJAMAN,. AS VOLUME_POTENSI_PINJAMAN,. AS VOLUME_POTENSI_SIMPANAN, case when .=0 then` + "Belum" + ` when .=1 then ` + "Sudah" + ` END AS STATUS_AKUISISI_SIMPANAN, case when .=0 AND .=0 then ` + "Belum" + ` when .=1 OR .=1 then ` + "Sudah" + ` END AS STATUS_AKUISISI_PINJAMAN FROM ` + ` WHERE .= ? LIMIT 1`
		queryRegex := regexp.QuoteMeta(query)
		mock.ExpectQuery(queryRegex).
			WithArgs(pnPengelola).
			WillReturnRows(rows)
		a := tindaklanjutRepo.NewTindaklanjutRepository(db, logger.L)

		detail, err := a.FindNasabahBRI(context.TODO(), pnPengelola)
		assert.Nil(t, err)
		assert.NotNil(t, detail)
	})
}

func TestTindaklanjutRepository_FindNasabahNonBRI(t *testing.T) {
	err := helper.NewMockEnv()
	db, mock, err := helper.NewMockDB()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	//requestMock
	request := &models.RequestDetailNasabahEncrypted{}
	request = request.MappingExampleData()

	//resultMock
	mockDetailQueryStruct := &models.GetDetailQueryStruct{}
	mockDetailQueryStruct = mockDetailQueryStruct.MappingExampleData()
	pnPengelola := "00999951"
	// role := "RM"

	//rowAndColumnMock
	table := &models.NasabahTable{}
	table = table.MappingTable()
	//rowAndColumnMock
	row, fields := helper.GetValueAndColumnStructToDriverValue(mockDetailQueryStruct)

	t.Run("success", func(t *testing.T) {
		rows := sqlmock.NewRows(fields).
			AddRow(row...)
		// query := "SELECT " + querySelect + " FROM `" + table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table + "` " + " WHERE " + queryWhere + " ORDER BY " + queryOrder
		query := `SELECT . AS CIF_NASABAH_REKAN,. AS NAMA_NASABAH_REKAN,. AS NAMA_NASABAH_UTAMA,. AS POSISI_TIER,. AS USIA,. AS NOMOR_KTP,. AS PN_RM,. AS JENIS_KELAMIN,. AS SEKTOR_EKONOMI,. AS TIER_DIATASNYA,. AS RM_DIATASNYA,. AS BRANCH,. AS RGDESC,. AS MBDESC,. AS SBDESC,. AS BRDESC,. AS PHONENUMBER,. AS ALAMAT_ID,. AS ALAMAT_DOMISILI,. AS ALAMAT_PINJAMAN,. AS USIA_REKENING_BULAN,. AS PRODUK_SIMPANAN,. AS SNAME_RM_SIMPANAN,. AS PRODUK_PINJAMAN,. AS SNAME_RM_PINJAMAN,. AS SEGMEN,. AS PHONENUMBER_RM_PINJAMAN,. AS JENIS_PEKERJAAN,. AS BIDANG_PEKERJAAN,. AS KOLEKTABILITAS,. AS TOTAL_PLAFON,. AS TOTAL_OUTSTANDING,. AS INET_BANKING,. AS SMS_BANKING,. AS TOTAL_CREDIT_BRI,. AS TOTAL_DEBIT_BRI,. AS TOTAL_KREDIT_NON_BRI,. AS TOTAL_DEBIT_NON_BRI,. AS TOTAL_KREDIT_DEBIT_NON_BRI,. AS NAMA_NASABAH_NON_BRI,. AS NAMA_BANK_LAWAN,. AS RATAS_SALDO_SIMPANAN_BRI,. AS TOTAL_CREDIT_KESELURUHAN,. AS TOTAL_DEBIT_KESELURUHAN,. AS CIF_NASABAH_TIER_1,. AS NAMA_NASABAH_TIER_1,. AS SNAME_RM_PINJAMAN_TIER_1,. AS UNIT_RM_TIER_1,. AS PHONENUMBER_RM_TIER_1,. AS CIF_NASABAH_TIER_2,. AS NAMA_NASABAH_TIER_2,. AS SNAME_RM_PINJAMAN_TIER_2,. AS UNIT_RM_TIER_2,. AS PHONENUMBER_TIER_2,. AS CIF_NASABAH_TIER_3,. AS NAMA_NASABAH_TIER_3,. AS SNAME_RM_PINJAMAN_TIER_3,. AS UNIT_RM_TIER_3,. AS PHONENUMBER_TIER_3,. AS CIF_NASABAH_TIER_4,. AS NAMA_NASABAH_TIER_4,. AS SNAME_RM_PINJAMAN_TIER_4,. AS UNIT_RM_TIER_4,. AS PHONENUMBER_TIER_4,. AS STATUS_REKENING,. AS AKUISISI_PINJAMAN,. AS LAMA_NASABAH_BULAN,. AS TANGGAL_LAHIR,. AS DS,. AS CREATED_DATE,. AS STATUS_TINDAKLANJUT,. AS TOPUP_SIMPANAN,. AS SUPLESI_PINJAMAN,. AS VOLUME_POTENSI_PINJAMAN,. AS VOLUME_POTENSI_SIMPANAN, case when .=0 then` + "Belum" + ` when .=1 then ` + "Sudah" + ` END AS STATUS_AKUISISI_SIMPANAN, case when .=0 AND .=0 then ` + "Belum" + ` when .=1 OR .=1 then ` + "Sudah" + ` END AS STATUS_AKUISISI_PINJAMAN FROM ` + ` WHERE .= ? LIMIT 1`
		queryRegex := regexp.QuoteMeta(query)
		mock.ExpectQuery(queryRegex).
			WithArgs(pnPengelola).
			WillReturnRows(rows)
		a := tindaklanjutRepo.NewTindaklanjutRepository(db, logger.L)

		detail, err := a.FindNasabahNonBRI(context.TODO(), pnPengelola)
		assert.Nil(t, err)
		assert.NotNil(t, detail)
	})
}

func TestTindaklanjutRepository_FindTindaklanjutNasabahNonBRI(t *testing.T) {
	err := helper.NewMockEnv()
	db, mock, err := helper.NewMockDB()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	//requestMock
	request := &models.RequestDetailNasabahEncrypted{}
	request = request.MappingExampleData()

	//resultMock
	mockDetailQueryStruct := &models.GetDetailQueryStruct{}
	mockDetailQueryStruct = mockDetailQueryStruct.MappingExampleData()
	pnPengelola := "00999951"
	// role := "RM"

	//rowAndColumnMock
	table := &models.NasabahTable{}
	table = table.MappingTable()
	//rowAndColumnMock
	row, fields := helper.GetValueAndColumnStructToDriverValue(mockDetailQueryStruct)

	t.Run("success", func(t *testing.T) {
		rows := sqlmock.NewRows(fields).
			AddRow(row...)
		// query := "SELECT " + querySelect + " FROM `" + table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table + "` " + " WHERE " + queryWhere + " ORDER BY " + queryOrder
		query := `SELECT . AS CIF_NASABAH_REKAN,. AS NAMA_NASABAH_REKAN,. AS NAMA_NASABAH_UTAMA,. AS POSISI_TIER,. AS USIA,. AS NOMOR_KTP,. AS PN_RM,. AS JENIS_KELAMIN,. AS SEKTOR_EKONOMI,. AS TIER_DIATASNYA,. AS RM_DIATASNYA,. AS BRANCH,. AS RGDESC,. AS MBDESC,. AS SBDESC,. AS BRDESC,. AS PHONENUMBER,. AS ALAMAT_ID,. AS ALAMAT_DOMISILI,. AS ALAMAT_PINJAMAN,. AS USIA_REKENING_BULAN,. AS PRODUK_SIMPANAN,. AS SNAME_RM_SIMPANAN,. AS PRODUK_PINJAMAN,. AS SNAME_RM_PINJAMAN,. AS SEGMEN,. AS PHONENUMBER_RM_PINJAMAN,. AS JENIS_PEKERJAAN,. AS BIDANG_PEKERJAAN,. AS KOLEKTABILITAS,. AS TOTAL_PLAFON,. AS TOTAL_OUTSTANDING,. AS INET_BANKING,. AS SMS_BANKING,. AS TOTAL_CREDIT_BRI,. AS TOTAL_DEBIT_BRI,. AS TOTAL_KREDIT_NON_BRI,. AS TOTAL_DEBIT_NON_BRI,. AS TOTAL_KREDIT_DEBIT_NON_BRI,. AS NAMA_NASABAH_NON_BRI,. AS NAMA_BANK_LAWAN,. AS RATAS_SALDO_SIMPANAN_BRI,. AS TOTAL_CREDIT_KESELURUHAN,. AS TOTAL_DEBIT_KESELURUHAN,. AS CIF_NASABAH_TIER_1,. AS NAMA_NASABAH_TIER_1,. AS SNAME_RM_PINJAMAN_TIER_1,. AS UNIT_RM_TIER_1,. AS PHONENUMBER_RM_TIER_1,. AS CIF_NASABAH_TIER_2,. AS NAMA_NASABAH_TIER_2,. AS SNAME_RM_PINJAMAN_TIER_2,. AS UNIT_RM_TIER_2,. AS PHONENUMBER_TIER_2,. AS CIF_NASABAH_TIER_3,. AS NAMA_NASABAH_TIER_3,. AS SNAME_RM_PINJAMAN_TIER_3,. AS UNIT_RM_TIER_3,. AS PHONENUMBER_TIER_3,. AS CIF_NASABAH_TIER_4,. AS NAMA_NASABAH_TIER_4,. AS SNAME_RM_PINJAMAN_TIER_4,. AS UNIT_RM_TIER_4,. AS PHONENUMBER_TIER_4,. AS STATUS_REKENING,. AS AKUISISI_PINJAMAN,. AS LAMA_NASABAH_BULAN,. AS TANGGAL_LAHIR,. AS DS,. AS CREATED_DATE,. AS STATUS_TINDAKLANJUT,. AS TOPUP_SIMPANAN,. AS SUPLESI_PINJAMAN,. AS VOLUME_POTENSI_PINJAMAN,. AS VOLUME_POTENSI_SIMPANAN, case when .=0 then` + "Belum" + ` when .=1 then ` + "Sudah" + ` END AS STATUS_AKUISISI_SIMPANAN, case when .=0 AND .=0 then ` + "Belum" + ` when .=1 OR .=1 then ` + "Sudah" + ` END AS STATUS_AKUISISI_PINJAMAN FROM ` + ` WHERE .= ? LIMIT 1`
		queryRegex := regexp.QuoteMeta(query)
		mock.ExpectQuery(queryRegex).
			WithArgs(pnPengelola).
			WillReturnRows(rows)
		a := tindaklanjutRepo.NewTindaklanjutRepository(db, logger.L)

		detail, err := a.FindTindaklanjutNasabahNonBRI(context.TODO(), pnPengelola)
		assert.NotNil(t, err)
		assert.Nil(t, detail)
	})
}

func TestTindaklanjutRepository_FindTindaklanjutNasabahBRI(t *testing.T) {
	err := helper.NewMockEnv()
	db, mock, err := helper.NewMockDB()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	//requestMock
	request := &models.RequestDetailNasabahEncrypted{}
	request = request.MappingExampleData()

	//resultMock
	mockDetailQueryStruct := &models.GetDetailQueryStruct{}
	mockDetailQueryStruct = mockDetailQueryStruct.MappingExampleData()
	pnPengelola := "00999951"
	// role := "RM"

	//rowAndColumnMock
	table := &models.NasabahTable{}
	table = table.MappingTable()
	//rowAndColumnMock
	row, fields := helper.GetValueAndColumnStructToDriverValue(mockDetailQueryStruct)

	t.Run("success", func(t *testing.T) {
		rows := sqlmock.NewRows(fields).
			AddRow(row...)
		// query := "SELECT " + querySelect + " FROM `" + table.BribrainLink5DashboardDetailInformasiNasabahBRI.Table + "` " + " WHERE " + queryWhere + " ORDER BY " + queryOrder
		query := `SELECT . AS CIF_NASABAH_REKAN,. AS NAMA_NASABAH_REKAN,. AS NAMA_NASABAH_UTAMA,. AS POSISI_TIER,. AS USIA,. AS NOMOR_KTP,. AS PN_RM,. AS JENIS_KELAMIN,. AS SEKTOR_EKONOMI,. AS TIER_DIATASNYA,. AS RM_DIATASNYA,. AS BRANCH,. AS RGDESC,. AS MBDESC,. AS SBDESC,. AS BRDESC,. AS PHONENUMBER,. AS ALAMAT_ID,. AS ALAMAT_DOMISILI,. AS ALAMAT_PINJAMAN,. AS USIA_REKENING_BULAN,. AS PRODUK_SIMPANAN,. AS SNAME_RM_SIMPANAN,. AS PRODUK_PINJAMAN,. AS SNAME_RM_PINJAMAN,. AS SEGMEN,. AS PHONENUMBER_RM_PINJAMAN,. AS JENIS_PEKERJAAN,. AS BIDANG_PEKERJAAN,. AS KOLEKTABILITAS,. AS TOTAL_PLAFON,. AS TOTAL_OUTSTANDING,. AS INET_BANKING,. AS SMS_BANKING,. AS TOTAL_CREDIT_BRI,. AS TOTAL_DEBIT_BRI,. AS TOTAL_KREDIT_NON_BRI,. AS TOTAL_DEBIT_NON_BRI,. AS TOTAL_KREDIT_DEBIT_NON_BRI,. AS NAMA_NASABAH_NON_BRI,. AS NAMA_BANK_LAWAN,. AS RATAS_SALDO_SIMPANAN_BRI,. AS TOTAL_CREDIT_KESELURUHAN,. AS TOTAL_DEBIT_KESELURUHAN,. AS CIF_NASABAH_TIER_1,. AS NAMA_NASABAH_TIER_1,. AS SNAME_RM_PINJAMAN_TIER_1,. AS UNIT_RM_TIER_1,. AS PHONENUMBER_RM_TIER_1,. AS CIF_NASABAH_TIER_2,. AS NAMA_NASABAH_TIER_2,. AS SNAME_RM_PINJAMAN_TIER_2,. AS UNIT_RM_TIER_2,. AS PHONENUMBER_TIER_2,. AS CIF_NASABAH_TIER_3,. AS NAMA_NASABAH_TIER_3,. AS SNAME_RM_PINJAMAN_TIER_3,. AS UNIT_RM_TIER_3,. AS PHONENUMBER_TIER_3,. AS CIF_NASABAH_TIER_4,. AS NAMA_NASABAH_TIER_4,. AS SNAME_RM_PINJAMAN_TIER_4,. AS UNIT_RM_TIER_4,. AS PHONENUMBER_TIER_4,. AS STATUS_REKENING,. AS AKUISISI_PINJAMAN,. AS LAMA_NASABAH_BULAN,. AS TANGGAL_LAHIR,. AS DS,. AS CREATED_DATE,. AS STATUS_TINDAKLANJUT,. AS TOPUP_SIMPANAN,. AS SUPLESI_PINJAMAN,. AS VOLUME_POTENSI_PINJAMAN,. AS VOLUME_POTENSI_SIMPANAN, case when .=0 then` + "Belum" + ` when .=1 then ` + "Sudah" + ` END AS STATUS_AKUISISI_SIMPANAN, case when .=0 AND .=0 then ` + "Belum" + ` when .=1 OR .=1 then ` + "Sudah" + ` END AS STATUS_AKUISISI_PINJAMAN FROM ` + ` WHERE .= ? LIMIT 1`
		queryRegex := regexp.QuoteMeta(query)
		mock.ExpectQuery(queryRegex).
			WithArgs(pnPengelola).
			WillReturnRows(rows)
		a := tindaklanjutRepo.NewTindaklanjutRepository(db, logger.L)

		detail, err := a.FindTindaklanjutNasabahBRI(context.TODO(), pnPengelola)
		assert.NotNil(t, err)
		assert.Nil(t, detail)
	})
}
