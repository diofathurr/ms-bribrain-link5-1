package tindaklanjut

import (
	"context"
	"ms-bribrain-link5/models"
)

type Usecase interface {
	SubmitTindaklanjutPositiveCase1(c context.Context, param *models.RequestSubmitTindaklanjutPositive1) (res *models.ResponseDetailNasabah, message string, err error)
	SubmitTindaklanjutPositiveCase2(c context.Context, param *models.RequestSubmitTindaklanjutPositive2) (res *models.ResponseDetailNasabah, message string, err error)
	SubmitTindaklanjutNegativeCase1(c context.Context, param *models.RequestSubmitTindaklanjutNegative1) (res *models.ResponseDetailNasabah, message string, err error)
	SubmitTindaklanjutNegativeCase2(c context.Context, param *models.RequestSubmitTindaklanjutNegative2) (res *models.ResponseDetailNasabah, message string, err error)
	SubmitTindaklanjutNegativeCase3(c context.Context, param *models.RequestSubmitTindaklanjutNegative3) (res *models.ResponseDetailNasabah, message string, err error)
	SubmitTindaklanjutOtherCase(c context.Context, param *models.RequestSubmitTindaklanjutOther) (res *models.ResponseDetailNasabah, message string, err error)
	SubmitTindaklanjutTandai(c context.Context, param *models.RequestSubmitTindaklanjutTandai) (res *models.ResponseDetailNasabah, message string, err error)
	SubmitTindaklanjutNonBri(c context.Context, param *models.RequestSubmitTindaklanjutNonBri) (res *models.ResponseDetailNasabah, message string, err error)
	SubmitKetertarikanNonBri(c context.Context, param *models.RequestSubmitKetertarikanNonBri) (res *models.ResponseDetailNasabah, message string, err error)
	FindRmInBranch(c context.Context, kodecabang string) (res string, message string, err error)
}
