package http_test

import (
	"encoding/json"
	"ms-bribrain-link5/helper/logger"
	"ms-bribrain-link5/middleware"
	"ms-bribrain-link5/models"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	models2 "ms-bribrain-link5/helper/models"
	masterdataHttp "ms-bribrain-link5/modules/master_data/delivery/http"
	masterdataHttpHandler "ms-bribrain-link5/modules/master_data/delivery/http"
	_masterdataUsecaseMock "ms-bribrain-link5/modules/master_data/mocks"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

var (
	l                         = logger.L
	Onboarding                = "/link5/onboarding"
	AlasanTidakDapatDihubungi = "/alasan_tidak_dapat_dihubungi"
	AlasanTidakCocok          = "/alasan_tidak_cocok"
	jwtClaimsstring           = `{"access_level":"","branch":806,"brdesc":"UNIT GUNUNG PUTRI CIBUBUR","description_1":"Jakarta","description_2":"2","exp":1630401199,"expired_token":1630318399,"iat":1630314799,"iss":"BRIBRAIN","jenis_kelamin":"P","jti":"234f3706-7df8-4c18-88c1-7a80a90d23b1","mainbr":384,"mbdesc":"Kantor Cabang Cibubur","orgeh_tx":"BRI UNIT GUNUNG PUTRI","pernr":"00999950","region":"I","rgdesc":"Kantor Wilayah Jakarta 2","role":"RM","sname":"User RM BRIBrain","stell_tx":"RM"}`
)

func TestMasterDataHandler_LoadHandler(t *testing.T) {
	mockMasterDataUsecase := new(_masterdataUsecaseMock.Usecase)
	e := echo.New()
	middL := middleware.InitMiddleware()
	masterdataHttpHandler.NewmasterDataHandler(e, middL, mockMasterDataUsecase, l)

	mockMasterDataUsecase.AssertExpectations(t)
}

func TestMasterDataHandler_GetOnboardingText(t *testing.T) {
	//requestMock

	//resultMock
	mockOnboardingText := models.OnboardingText{}
	mockOnboardingText = mockOnboardingText.MappingExampleDataNil()

	t.Run("success", func(t *testing.T) {
		//usecaseMock
		mockMasterDataUsecase := new(_masterdataUsecaseMock.Usecase)

		mockMasterDataUsecase.On("GetOnboardingText", mock.Anything).
			Return(mockOnboardingText, models2.GeneralSuccess, nil)

		e := echo.New()

		j, err := json.Marshal("")
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, Onboarding, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(Onboarding)
		c.Request().ParseForm()
		handler := masterdataHttp.MasterDataHandler{
			MasterDataUsecase: mockMasterDataUsecase,
			Log:               l,
		}
		err = handler.GetOnboardingText(c)
		require.NoError(t, err)

		assert.Equal(t, http.StatusOK, rec.Code)
		mockMasterDataUsecase.AssertExpectations(t)
	})

	t.Run("error-500", func(t *testing.T) {
		//usecaseMock
		mockMasterDataUsecase := new(_masterdataUsecaseMock.Usecase)

		mockMasterDataUsecase.On("GetOnboardingText", mock.Anything).
			Return(mockOnboardingText, models2.ErrInternalServerError.Error(), models2.ErrInternalServerError)

		e := echo.New()

		j, err := json.Marshal("")
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, Onboarding, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(Onboarding)
		c.Request().ParseForm()
		handler := masterdataHttp.MasterDataHandler{
			MasterDataUsecase: mockMasterDataUsecase,
			Log:               l,
		}
		err = handler.GetOnboardingText(c)

		assert.Equal(t, http.StatusInternalServerError, rec.Code)
		mockMasterDataUsecase.AssertExpectations(t)
	})
}

func TestMasterDataHandler_GetAlasanTidakDapatDihubungi(t *testing.T) {
	//requestMock

	//resultMock
	mockResponsePilihanAlasan := []models.PilihanAlasan{}
	mockResponsePilihanAlasanItem := models.PilihanAlasan{}
	mockResponsePilihanAlasanItem = mockResponsePilihanAlasanItem.MappingExampleData()
	mockResponsePilihanAlasan = append(mockResponsePilihanAlasan, mockResponsePilihanAlasanItem)

	t.Run("success", func(t *testing.T) {
		//usecaseMock
		mockMasterDataUsecase := new(_masterdataUsecaseMock.Usecase)

		mockMasterDataUsecase.On("GetAlasanTidakDapatDihubungi", mock.Anything).
			Return(mockResponsePilihanAlasan, models2.GeneralSuccess, nil)

		e := echo.New()

		j, err := json.Marshal("")
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, AlasanTidakDapatDihubungi, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(AlasanTidakDapatDihubungi)
		c.Request().ParseForm()
		handler := masterdataHttp.MasterDataHandler{
			MasterDataUsecase: mockMasterDataUsecase,
			Log:               l,
		}
		err = handler.GetAlasanTidakDapatDihubungi(c)
		require.NoError(t, err)

		assert.Equal(t, http.StatusOK, rec.Code)
		mockMasterDataUsecase.AssertExpectations(t)
	})

	t.Run("error-500", func(t *testing.T) {
		//usecaseMock
		mockMasterDataUsecase := new(_masterdataUsecaseMock.Usecase)

		mockMasterDataUsecase.On("GetAlasanTidakDapatDihubungi", mock.Anything).
			Return(mockResponsePilihanAlasan, models2.ErrInternalServerError.Error(), models2.ErrInternalServerError)

		e := echo.New()

		j, err := json.Marshal("")
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, AlasanTidakDapatDihubungi, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(AlasanTidakDapatDihubungi)
		c.Request().ParseForm()
		handler := masterdataHttp.MasterDataHandler{
			MasterDataUsecase: mockMasterDataUsecase,
			Log:               l,
		}
		err = handler.GetAlasanTidakDapatDihubungi(c)

		assert.Equal(t, http.StatusInternalServerError, rec.Code)
		mockMasterDataUsecase.AssertExpectations(t)
	})
}

func TestMasterDataHandler_GetAlasanTidakCocok(t *testing.T) {
	//requestMock

	//resultMock
	mockResponsePilihanAlasan := []models.PilihanAlasan{}
	mockResponsePilihanAlasanItem := models.PilihanAlasan{}
	mockResponsePilihanAlasanItem = mockResponsePilihanAlasanItem.MappingExampleData()
	mockResponsePilihanAlasan = append(mockResponsePilihanAlasan, mockResponsePilihanAlasanItem)

	t.Run("success", func(t *testing.T) {
		//usecaseMock
		mockMasterDataUsecase := new(_masterdataUsecaseMock.Usecase)

		mockMasterDataUsecase.On("GetAlasanTidakCocok", mock.Anything).
			Return(mockResponsePilihanAlasan, models2.GeneralSuccess, nil)

		e := echo.New()

		j, err := json.Marshal("")
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, AlasanTidakCocok, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(AlasanTidakCocok)
		c.Request().ParseForm()
		handler := masterdataHttp.MasterDataHandler{
			MasterDataUsecase: mockMasterDataUsecase,
			Log:               l,
		}
		err = handler.GetAlasanTidakCocok(c)
		require.NoError(t, err)

		assert.Equal(t, http.StatusOK, rec.Code)
		mockMasterDataUsecase.AssertExpectations(t)
	})

	t.Run("error-500", func(t *testing.T) {
		//usecaseMock
		mockMasterDataUsecase := new(_masterdataUsecaseMock.Usecase)

		mockMasterDataUsecase.On("GetAlasanTidakCocok", mock.Anything).
			Return(mockResponsePilihanAlasan, models2.ErrInternalServerError.Error(), models2.ErrInternalServerError)

		e := echo.New()

		j, err := json.Marshal("")
		assert.NoError(t, err)
		req, err := http.NewRequest(echo.POST, AlasanTidakCocok, strings.NewReader(string(j)))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		assert.NoError(t, err)

		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		var jwtClaims *models.BRIBrainClaims
		json.Unmarshal([]byte(string(jwtClaimsstring)), &jwtClaims)
		c.Set("JWT_CLAIMS", jwtClaims)
		c.SetPath(AlasanTidakCocok)
		c.Request().ParseForm()
		handler := masterdataHttp.MasterDataHandler{
			MasterDataUsecase: mockMasterDataUsecase,
			Log:               l,
		}
		err = handler.GetAlasanTidakCocok(c)

		assert.Equal(t, http.StatusInternalServerError, rec.Code)
		mockMasterDataUsecase.AssertExpectations(t)
	})
}
