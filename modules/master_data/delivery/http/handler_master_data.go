package http

import (
	"context"
	"ms-bribrain-link5/helper"
	"ms-bribrain-link5/helper/logger"
	models2 "ms-bribrain-link5/helper/models"
	"ms-bribrain-link5/middleware"
	"ms-bribrain-link5/models"
	"ms-bribrain-link5/modules/master_data"
	_ "ms-bribrain-link5/swagger/models"
	"regexp"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
)

// ResponseError represent the response error struct
type ResponseError struct {
	Message string `json:"message"`
}

// MasterDataHandler  represent the http handler
type MasterDataHandler struct {
	MasterDataUsecase master_data.Usecase
	Log               logger.Logger
}

// NewmasterDataHandler will initialize the countrys/ resources endpoint
func NewmasterDataHandler(e *echo.Echo, middL *middleware.GoMiddleware, us master_data.Usecase, log logger.Logger) {
	handler := &MasterDataHandler{
		Log:               log,
		MasterDataUsecase: us,
	}

	gapis := e.Group("/link5")
	gapis.POST("/onboarding", handler.GetOnboardingText, middL.Authentication)

	e.POST("/alasan_tidak_dapat_dihubungi", handler.GetAlasanTidakDapatDihubungi, middL.Authentication)
	e.POST("/alasan_tidak_cocok", handler.GetAlasanTidakCocok, middL.Authentication)
	e.POST("/list_kc", handler.SearchCabang, middL.Authentication)
	e.POST("/terms", handler.GetTermsAndConditionText, middL.AuthenticationUsingSecret)
	e.POST("/search_kecamatan", handler.SearchKotaKecamatan, middL.Authentication)
	e.POST("/list_kelurahan", handler.GetListKelurahan, middL.Authentication)

}

// GetOnboardingText godoc
// @Summary GetOnboardingText.
// @Description GetOnboardingText.
// @Tags MasterData
// @Accept  application-bribrain/json
// @Produce  json
// @Param Content-Type header string true "Content-Type" default(application-bribrain/json)
// @Param Authorization header string true "token" default(QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUp3WlhKdWNpSTZJakF3T1RrNU9UazVJaXdpYzI1aGJXVWlPaUpWYzJWeUlFMWhiblJ5YVNCQ1VrbENjbUZwYmlJc0luSnZiR1VpT2lKTlFVNVVVa2tpTENKa1pYTmpjbWx3ZEdsdmJsOHhJam9pU21GcllYSjBZU0lzSW1SbGMyTnlhWEIwYVc5dVh6SWlPaUl5SWl3aWIzSm5aV2hmZEhnaU9pSlZUa2xVSUUxQlRWQkJUa2NnVUZKQlVFRlVRVTRpTENKemRHVnNiRjkwZUNJNklrcFZUa2xQVWlCQlUxTlBRMGxCVkVVZ1RVRk9WRkpKSWl3aWNtZGtaWE5qSWpvaVMyRnVkRzl5SUZkcGJHRjVZV2dnUzJGdWRHOXlJRmRwYkdGNVlXZ2dTbUZyWVhKMFlTQXlJaXdpYldKa1pYTmpJam9pUzJGdWRHOXlJRU5oWW1GdVp5QkxZVzUwYjNJZ1EyRmlZVzVuSUVwaGEyRnlkR0VnVUdGellYSWdUV2x1WjJkMUlpd2lZbkprWlhOaklqb2lWVTVKVkNCQ1JVNUVRU0JTUVZsQklGQkJVMEZTSUUxSlRrZEhWU0lzSW5KbFoybHZiaUk2SWtraUxDSnRZV2x1WW5JaU9qTXpPU3dpWW5KaGJtTm9Jam80TXpRc0ltcGxibWx6WDJ0bGJHRnRhVzRpT2lKUUlpd2lZV05qWlhOelgyeGxkbVZzSWpvaVRsVk1UQ0lzSW1WNGNHbHlaV1JmZEc5clpXNGlPakUyTWprek9ERTBNellzSW1WNGNDSTZNVFl5T1RRMk5ESXpOaXdpYW5ScElqb2lOakF6TVRrME1HSXRNelF3WmkwMFl6QTFMV0UyTm1JdE9EQXdaVEZpWkRCbE56WmpJaXdpYVdGMElqb3hOakk1TXpjM09ETTJMQ0pwYzNNaU9pSkNVa2xDVWtGSlRpSjkuam5BQjUyZFNzTk9jRC1HLWpYeDdKZFhtZHpLeFhTVm9EVk5Kbmh4aGtTZw==)
// @Param request body interface{} false "request"
// @Success 200 {object} models.SwaggerResponseOnboardingTextSuccess
// @Failure 500 {object} models.SwaggerErrorInternalServerError
// @Failure 405 {object} models.SwaggerErrorMethodNotAllowed
// @Failure 400 {object} models.SwaggerErrorInvalidMethod
// @Failure 404 {object} models.SwaggerErrorNotFoundRoutes
// @Failure 401 {object} models.SwaggerErrorExpiredTokenUnAuthorizeAndTokenUnAuthorize
// @Router /link5/onboarding [post]
func (a *MasterDataHandler) GetOnboardingText(c echo.Context) error {
	response := new(models2.Response)
	_ = c.Get("JWT_CLAIMS").(*models.BRIBrainClaims)
	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}
	result, message, err := a.MasterDataUsecase.GetOnboardingText(ctx)
	if err != nil {
		errId := time.Now().Format(helper.DateTimeFormatDefault) + " " + helper.RandomString(20)
		a.Log.Error("["+errId+"]  master_data.delivery.http.MasterDataHandler.GetOnboardingText: %s", err.Error())
		response.MappingResponseError(helper.GetStatusCode(err), message, errId, c)
		return c.JSON(response.StatusCode, response)
	}
	response.MappingResponseSuccess(message, result, c)
	return c.JSON(response.StatusCode, response)
}

// GetAlasanTidakDapatDihubungi godoc
// @Summary GetAlasanTidakDapatDihubungi.
// @Description GetAlasanTidakDapatDihubungi.
// @Tags MasterData
// @Accept  application-bribrain/json
// @Produce  json
// @Param Content-Type header string true "Content-Type" default(application-bribrain/json)
// @Param Authorization header string true "token" default(QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUp3WlhKdWNpSTZJakF3T1RrNU9UazVJaXdpYzI1aGJXVWlPaUpWYzJWeUlFMWhiblJ5YVNCQ1VrbENjbUZwYmlJc0luSnZiR1VpT2lKTlFVNVVVa2tpTENKa1pYTmpjbWx3ZEdsdmJsOHhJam9pU21GcllYSjBZU0lzSW1SbGMyTnlhWEIwYVc5dVh6SWlPaUl5SWl3aWIzSm5aV2hmZEhnaU9pSlZUa2xVSUUxQlRWQkJUa2NnVUZKQlVFRlVRVTRpTENKemRHVnNiRjkwZUNJNklrcFZUa2xQVWlCQlUxTlBRMGxCVkVVZ1RVRk9WRkpKSWl3aWNtZGtaWE5qSWpvaVMyRnVkRzl5SUZkcGJHRjVZV2dnUzJGdWRHOXlJRmRwYkdGNVlXZ2dTbUZyWVhKMFlTQXlJaXdpYldKa1pYTmpJam9pUzJGdWRHOXlJRU5oWW1GdVp5QkxZVzUwYjNJZ1EyRmlZVzVuSUVwaGEyRnlkR0VnVUdGellYSWdUV2x1WjJkMUlpd2lZbkprWlhOaklqb2lWVTVKVkNCQ1JVNUVRU0JTUVZsQklGQkJVMEZTSUUxSlRrZEhWU0lzSW5KbFoybHZiaUk2SWtraUxDSnRZV2x1WW5JaU9qTXpPU3dpWW5KaGJtTm9Jam80TXpRc0ltcGxibWx6WDJ0bGJHRnRhVzRpT2lKUUlpd2lZV05qWlhOelgyeGxkbVZzSWpvaVRsVk1UQ0lzSW1WNGNHbHlaV1JmZEc5clpXNGlPakUyTWprek9ERTBNellzSW1WNGNDSTZNVFl5T1RRMk5ESXpOaXdpYW5ScElqb2lOakF6TVRrME1HSXRNelF3WmkwMFl6QTFMV0UyTm1JdE9EQXdaVEZpWkRCbE56WmpJaXdpYVdGMElqb3hOakk1TXpjM09ETTJMQ0pwYzNNaU9pSkNVa2xDVWtGSlRpSjkuam5BQjUyZFNzTk9jRC1HLWpYeDdKZFhtZHpLeFhTVm9EVk5Kbmh4aGtTZw==)
// @Param request body interface{} false "request"
// @Success 200 {object} models.SwaggerGetAlasanTidakDapatDihubungiSuccess
// @Failure 500 {object} models.SwaggerErrorInternalServerError
// @Failure 405 {object} models.SwaggerErrorMethodNotAllowed
// @Failure 400 {object} models.SwaggerErrorInvalidMethod
// @Failure 404 {object} models.SwaggerErrorNotFoundRoutes
// @Failure 401 {object} models.SwaggerErrorExpiredTokenUnAuthorizeAndTokenUnAuthorize
// @Router /alasan_tidak_dapat_dihubungi [post]
func (a *MasterDataHandler) GetAlasanTidakDapatDihubungi(c echo.Context) error {
	response := new(models2.Response)
	_ = c.Get("JWT_CLAIMS").(*models.BRIBrainClaims)
	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}
	result, message, err := a.MasterDataUsecase.GetAlasanTidakDapatDihubungi(ctx)
	if err != nil {
		errId := time.Now().Format(helper.DateTimeFormatDefault) + " " + helper.RandomString(20)
		a.Log.Error("["+errId+"]  master_data.delivery.http.MasterDataHandler.GetAlasanTidakDapatDihubungi: %s", err.Error())
		response.MappingResponseError(helper.GetStatusCode(err), message, errId, c)
		return c.JSON(response.StatusCode, response)
	}
	response.MappingResponseSuccess(message, result, c)
	return c.JSON(response.StatusCode, response)
}

// GetAlasanTidakCocok godoc
// @Summary GetAlasanTidakCocok.
// @Description GetAlasanTidakCocok.
// @Tags MasterData
// @Accept  application-bribrain/json
// @Produce  json
// @Param Content-Type header string true "Content-Type" default(application-bribrain/json)
// @Param Authorization header string true "token" default(QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUp3WlhKdWNpSTZJakF3T1RrNU9UazVJaXdpYzI1aGJXVWlPaUpWYzJWeUlFMWhiblJ5YVNCQ1VrbENjbUZwYmlJc0luSnZiR1VpT2lKTlFVNVVVa2tpTENKa1pYTmpjbWx3ZEdsdmJsOHhJam9pU21GcllYSjBZU0lzSW1SbGMyTnlhWEIwYVc5dVh6SWlPaUl5SWl3aWIzSm5aV2hmZEhnaU9pSlZUa2xVSUUxQlRWQkJUa2NnVUZKQlVFRlVRVTRpTENKemRHVnNiRjkwZUNJNklrcFZUa2xQVWlCQlUxTlBRMGxCVkVVZ1RVRk9WRkpKSWl3aWNtZGtaWE5qSWpvaVMyRnVkRzl5SUZkcGJHRjVZV2dnUzJGdWRHOXlJRmRwYkdGNVlXZ2dTbUZyWVhKMFlTQXlJaXdpYldKa1pYTmpJam9pUzJGdWRHOXlJRU5oWW1GdVp5QkxZVzUwYjNJZ1EyRmlZVzVuSUVwaGEyRnlkR0VnVUdGellYSWdUV2x1WjJkMUlpd2lZbkprWlhOaklqb2lWVTVKVkNCQ1JVNUVRU0JTUVZsQklGQkJVMEZTSUUxSlRrZEhWU0lzSW5KbFoybHZiaUk2SWtraUxDSnRZV2x1WW5JaU9qTXpPU3dpWW5KaGJtTm9Jam80TXpRc0ltcGxibWx6WDJ0bGJHRnRhVzRpT2lKUUlpd2lZV05qWlhOelgyeGxkbVZzSWpvaVRsVk1UQ0lzSW1WNGNHbHlaV1JmZEc5clpXNGlPakUyTWprek9ERTBNellzSW1WNGNDSTZNVFl5T1RRMk5ESXpOaXdpYW5ScElqb2lOakF6TVRrME1HSXRNelF3WmkwMFl6QTFMV0UyTm1JdE9EQXdaVEZpWkRCbE56WmpJaXdpYVdGMElqb3hOakk1TXpjM09ETTJMQ0pwYzNNaU9pSkNVa2xDVWtGSlRpSjkuam5BQjUyZFNzTk9jRC1HLWpYeDdKZFhtZHpLeFhTVm9EVk5Kbmh4aGtTZw==)
// @Param request body interface{} false "request"
// @Success 200 {object} models.SwaggerGetAlasanTidakCocokSuccess
// @Failure 500 {object} models.SwaggerErrorInternalServerError
// @Failure 405 {object} models.SwaggerErrorMethodNotAllowed
// @Failure 400 {object} models.SwaggerErrorInvalidMethod
// @Failure 404 {object} models.SwaggerErrorNotFoundRoutes
// @Failure 401 {object} models.SwaggerErrorExpiredTokenUnAuthorizeAndTokenUnAuthorize
// @Router /alasan_tidak_cocok [post]
func (a *MasterDataHandler) GetAlasanTidakCocok(c echo.Context) error {
	response := new(models2.Response)
	_ = c.Get("JWT_CLAIMS").(*models.BRIBrainClaims)
	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}
	result, message, err := a.MasterDataUsecase.GetAlasanTidakCocok(ctx)
	if err != nil {
		errId := time.Now().Format(helper.DateTimeFormatDefault) + " " + helper.RandomString(20)
		a.Log.Error("["+errId+"]  master_data.delivery.http.MasterDataHandler.GetAlasanTidakCocok: %s", err.Error())
		response.MappingResponseError(helper.GetStatusCode(err), message, errId, c)
		return c.JSON(response.StatusCode, response)
	}
	response.MappingResponseSuccess(message, result, c)
	return c.JSON(response.StatusCode, response)
}

func (a *MasterDataHandler) SearchCabang(c echo.Context) error {
	log := "master_data.delivery.http.MasterDataHandler.SearchCabang: %s"
	response := new(models2.Response)
	request := new(models.RequestListCabangPointer)
	_ = c.Get("JWT_CLAIMS").(*models.BRIBrainClaims)
	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	dec, err := helper.JsonDecode(c, request)
	if err != nil {
		response.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), err.Error(), err, c)
		return c.JSON(response.StatusCode, response)
	}

	request = dec.(*models.RequestListCabangPointer)
	validate := validator.New()
	validate.RegisterValidation("allowempty", allowEmptyField)
	errvalidate := validate.Struct(request)
	if errvalidate != nil {
		errID := time.Now().Format(helper.DateTimeFormatDefault) + " " + helper.RandomString(20)
		a.Log.Error("["+errID+"]  "+log, errvalidate.Error())
		response.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), models2.ErrInvalidValue.Error(), models2.ErrInvalidValue.Error(), c)
		return c.JSON(response.StatusCode, response)
	}

	result, message, err := a.MasterDataUsecase.SearchCabang(ctx, request)
	if err != nil {
		errId := time.Now().Format(helper.DateTimeFormatDefault) + " " + helper.RandomString(20)
		a.Log.Error("["+errId+"]  "+log, err.Error())
		response.MappingResponseError(helper.GetStatusCode(err), message, errId, c)
		return c.JSON(response.StatusCode, response)
	}
	response.MappingResponseSuccess(message, result, c)
	return c.JSON(response.StatusCode, response)
}

func allowEmptyField(fl validator.FieldLevel) bool {

	emptyRegex := "^$"
	emptyStringRegex := regexp.MustCompile(emptyRegex)
	// fmt.Println("tag ->", fl.GetTag())
	// fmt.Println("field name ->", fl.FieldName())
	// fmt.Println("struct field name ->", fl.StructFieldName())
	// fmt.Println("param ->", fl.Param())
	// fmt.Println("field ->", fl.Field())
	return emptyStringRegex.MatchString(fl.Field().String())
}

func (a *MasterDataHandler) GetTermsAndConditionText(c echo.Context) error {
	response := new(models2.Response)
	// _ = c.Get("JWT_CLAIMS").(*models.BRIBrainClaims)
	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}
	result, message, err := a.MasterDataUsecase.GetTermsAndConditionText(ctx)
	if err != nil {
		errId := time.Now().Format(helper.DateTimeFormatDefault) + " " + helper.RandomString(20)
		a.Log.Error("["+errId+"]  master_data.delivery.http.MasterDataHandler.GetTermsAndConditionText: %s", err.Error())
		response.MappingResponseError(helper.GetStatusCode(err), message, errId, c)
		return c.JSON(response.StatusCode, response)
	}
	response.MappingResponseSuccess(message, result, c)
	return c.JSON(response.StatusCode, response)
}

func (a *MasterDataHandler) SearchKotaKecamatan(c echo.Context) error {
	log := "master_data.delivery.http.MasterDataHandler.SearchKotaKecamatan: %s"
	response := new(models2.Response)
	request := new(models.RequestListKotaKecamatanPointer)
	_ = c.Get("JWT_CLAIMS").(*models.BRIBrainClaims)
	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	dec, err := helper.JsonDecode(c, request)
	if err != nil {
		response.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), err.Error(), err, c)
		return c.JSON(response.StatusCode, response)
	}

	request = dec.(*models.RequestListKotaKecamatanPointer)
	validate := validator.New()
	validate.RegisterValidation("allowempty", allowEmptyField)
	errvalidate := validate.Struct(request)
	if errvalidate != nil {
		errID := time.Now().Format(helper.DateTimeFormatDefault) + " " + helper.RandomString(20)
		a.Log.Error("["+errID+"]  "+log, errvalidate.Error())
		response.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), models2.ErrInvalidValue.Error(), models2.ErrInvalidValue.Error(), c)
		return c.JSON(response.StatusCode, response)
	}

	result, message, err := a.MasterDataUsecase.SearchKotaKecamatan(ctx, request)
	if err != nil {
		errId := time.Now().Format(helper.DateTimeFormatDefault) + " " + helper.RandomString(20)
		a.Log.Error("["+errId+"]  "+log, err.Error())
		response.MappingResponseError(helper.GetStatusCode(err), message, errId, c)
		return c.JSON(response.StatusCode, response)
	}
	response.MappingResponseSuccess(message, result, c)
	return c.JSON(response.StatusCode, response)
}

func (a *MasterDataHandler) GetListKelurahan(c echo.Context) error {
	log := "master_data.delivery.http.MasterDataHandler.GetListKelurahan: %s"
	response := new(models2.Response)
	request := new(models.RequestListKelurahan)
	_ = c.Get("JWT_CLAIMS").(*models.BRIBrainClaims)
	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	dec, err := helper.JsonDecode(c, request)
	if err != nil {
		response.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), err.Error(), err, c)
		return c.JSON(response.StatusCode, response)
	}

	request = dec.(*models.RequestListKelurahan)
	validate := validator.New()
	validate.RegisterValidation("allowempty", allowEmptyField)
	errvalidate := validate.Struct(request)
	if errvalidate != nil {
		errID := time.Now().Format(helper.DateTimeFormatDefault) + " " + helper.RandomString(20)
		a.Log.Error("["+errID+"]  "+log, errvalidate.Error())
		response.MappingResponseError(helper.GetStatusCode(models2.ErrBadParamInput), models2.ErrInvalidValue.Error(), models2.ErrInvalidValue.Error(), c)
		return c.JSON(response.StatusCode, response)
	}

	result, message, err := a.MasterDataUsecase.GetListKelurahan(ctx, request)
	if err != nil {
		errId := time.Now().Format(helper.DateTimeFormatDefault) + " " + helper.RandomString(20)
		a.Log.Error("["+errId+"]  "+log, err.Error())
		response.MappingResponseError(helper.GetStatusCode(err), message, errId, c)
		return c.JSON(response.StatusCode, response)
	}
	response.MappingResponseSuccess(message, result, c)
	return c.JSON(response.StatusCode, response)
}
