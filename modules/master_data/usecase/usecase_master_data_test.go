package usecase_test

import (
	"context"
	"ms-bribrain-link5/helper/logger"
	models2 "ms-bribrain-link5/helper/models"
	"ms-bribrain-link5/models"
	_masterDataRepositoryMock "ms-bribrain-link5/modules/master_data/mocks"
	"ms-bribrain-link5/modules/master_data/usecase"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

var (
	l              = logger.L
	timeoutContext = time.Second * 30
)

func TestMasterDataUsecase_GetOnboardingText(t *testing.T) {
	//requestMock

	//resultMock
	mockResponseRepoMasterData := models.OnboardingText{}
	mockResponseRepoMasterData = mockResponseRepoMasterData.MappingExampleDataNil()

	t.Run("success", func(t *testing.T) {
		//repositoryMock
		mockMasterDataRepo := new(_masterDataRepositoryMock.Repository)

		mockMasterDataRepo.On("GetOnboardingText", mock.Anything).Return(mockResponseRepoMasterData, nil).Once()

		u := usecase.NewMasterDataUsecase(mockMasterDataRepo, l, timeoutContext)

		a, _, err := u.GetOnboardingText(context.TODO())

		assert.NoError(t, err)
		assert.NotNil(t, a)

		mockMasterDataRepo.AssertExpectations(t)
	})

	t.Run("error", func(t *testing.T) {
		//repositoryMock
		mockMasterDataRepo := new(_masterDataRepositoryMock.Repository)

		mockMasterDataRepo.On("GetOnboardingText", mock.Anything).Return(mockResponseRepoMasterData, models2.ErrInternalServerError).Once()

		u := usecase.NewMasterDataUsecase(mockMasterDataRepo, l, timeoutContext)

		a, _, err := u.GetOnboardingText(context.TODO())

		assert.Error(t, err)
		assert.NotNil(t, a)

		mockMasterDataRepo.AssertExpectations(t)
	})
}

func TestMasterDataUsecase_GetAlasanTidakDapatDihubungi(t *testing.T) {
	//requestMock

	//resultMock
	mockResponsePilihanAlasan := []models.PilihanAlasan{}
	mockResponsePilihanAlasanItem := models.PilihanAlasan{}
	mockResponsePilihanAlasanItem = mockResponsePilihanAlasanItem.MappingExampleData()
	mockResponsePilihanAlasan = append(mockResponsePilihanAlasan, mockResponsePilihanAlasanItem)

	t.Run("success", func(t *testing.T) {
		//repositoryMock
		mockMasterDataRepo := new(_masterDataRepositoryMock.Repository)

		mockMasterDataRepo.On("GetAlasanTidakDapatDihubungi", mock.Anything).Return(mockResponsePilihanAlasan, nil).Once()

		u := usecase.NewMasterDataUsecase(mockMasterDataRepo, l, timeoutContext)

		a, _, err := u.GetAlasanTidakDapatDihubungi(context.TODO())

		assert.NoError(t, err)
		assert.NotNil(t, a)

		mockMasterDataRepo.AssertExpectations(t)
	})

	t.Run("error", func(t *testing.T) {
		//repositoryMock
		mockMasterDataRepo := new(_masterDataRepositoryMock.Repository)

		mockMasterDataRepo.On("GetAlasanTidakDapatDihubungi", mock.Anything).Return(mockResponsePilihanAlasan, models2.ErrInternalServerError).Once()

		u := usecase.NewMasterDataUsecase(mockMasterDataRepo, l, timeoutContext)

		a, _, err := u.GetAlasanTidakDapatDihubungi(context.TODO())

		assert.Error(t, err)
		assert.NotNil(t, a)

		mockMasterDataRepo.AssertExpectations(t)
	})
}

func TestMasterDataUsecase_GetAlasanTidakCocok(t *testing.T) {
	//requestMock

	//resultMock
	mockResponsePilihanAlasan := []models.PilihanAlasan{}
	mockResponsePilihanAlasanItem := models.PilihanAlasan{}
	mockResponsePilihanAlasanItem = mockResponsePilihanAlasanItem.MappingExampleData()
	mockResponsePilihanAlasan = append(mockResponsePilihanAlasan, mockResponsePilihanAlasanItem)

	t.Run("success", func(t *testing.T) {
		//repositoryMock
		mockMasterDataRepo := new(_masterDataRepositoryMock.Repository)

		mockMasterDataRepo.On("GetAlasanTidakCocok", mock.Anything).Return(mockResponsePilihanAlasan, nil).Once()

		u := usecase.NewMasterDataUsecase(mockMasterDataRepo, l, timeoutContext)

		a, _, err := u.GetAlasanTidakCocok(context.TODO())

		assert.NoError(t, err)
		assert.NotNil(t, a)

		mockMasterDataRepo.AssertExpectations(t)
	})

	t.Run("error", func(t *testing.T) {
		//repositoryMock
		mockMasterDataRepo := new(_masterDataRepositoryMock.Repository)

		mockMasterDataRepo.On("GetAlasanTidakCocok", mock.Anything).Return(mockResponsePilihanAlasan, models2.ErrInternalServerError).Once()

		u := usecase.NewMasterDataUsecase(mockMasterDataRepo, l, timeoutContext)

		a, _, err := u.GetAlasanTidakCocok(context.TODO())

		assert.Error(t, err)
		assert.NotNil(t, a)

		mockMasterDataRepo.AssertExpectations(t)
	})
}
