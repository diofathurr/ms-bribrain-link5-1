package usecase

import (
	"context"
	"ms-bribrain-link5/helper"
	"ms-bribrain-link5/helper/logger"
	models2 "ms-bribrain-link5/helper/models"
	"ms-bribrain-link5/models"
	"ms-bribrain-link5/modules/master_data"
	"time"
)

type MasterDataUsecase struct {
	masterDataRepo master_data.Repository
	contextTimeout time.Duration
	log            logger.Logger
}

// NewMasterDataUsecase will create new an MasterDataUsecase object representation of master_data.Usecase interface
func NewMasterDataUsecase(masterDataRepo master_data.Repository, log logger.Logger, timeout time.Duration) master_data.Usecase {
	return &MasterDataUsecase{
		masterDataRepo: masterDataRepo,
		contextTimeout: timeout,
		log:            log,
	}
}

func (c MasterDataUsecase) GetOnboardingText(ctx context.Context) (res models.OnboardingText, message string, err error) {
	ctx, cancel := context.WithTimeout(ctx, c.contextTimeout)
	defer cancel()

	onboardingText, err := c.masterDataRepo.GetOnboardingText(ctx)
	if err != nil {
		c.log.Error("master_data.usecase.MasterDataUsecase.GetOnboardingText: %s", err.Error())
		// return nil, models2.ErrGeneralMessage.Error(), err
		return onboardingText, models2.ErrGeneralMessage.Error(), err
	}

	return onboardingText, models2.GeneralSuccess, nil
}

func (c MasterDataUsecase) GetAlasanTidakDapatDihubungi(ctx context.Context) (res []models.PilihanAlasan, message string, err error) {
	ctx, cancel := context.WithTimeout(ctx, c.contextTimeout)
	defer cancel()

	pilihanalasan, err := c.masterDataRepo.GetAlasanTidakDapatDihubungi(ctx)
	if err != nil {
		c.log.Error("master_data.usecase.MasterDataUsecase.GetAlasanTidakDapatDihubungi: %s", err.Error())
		// return nil, models2.ErrGeneralMessage.Error(), err
		return pilihanalasan, models2.ErrGeneralMessage.Error(), err
	}

	return pilihanalasan, models2.GeneralSuccess, nil
}

func (c MasterDataUsecase) GetAlasanTidakCocok(ctx context.Context) (res []models.PilihanAlasan, message string, err error) {
	ctx, cancel := context.WithTimeout(ctx, c.contextTimeout)
	defer cancel()

	pilihanalasan, err := c.masterDataRepo.GetAlasanTidakCocok(ctx)
	if err != nil {
		c.log.Error("master_data.usecase.MasterDataUsecase.GetAlasanTidakCocok: %s", err.Error())
		// return nil, models2.ErrGeneralMessage.Error(), err
		return pilihanalasan, models2.ErrGeneralMessage.Error(), err
	}

	return pilihanalasan, models2.GeneralSuccess, nil
}

func (c MasterDataUsecase) SearchCabang(ctx context.Context, request *models.RequestListCabangPointer) (res []models.ListCabang, message string, err error) {
	ctx, cancel := context.WithTimeout(ctx, c.contextTimeout)
	defer cancel()
	var listCabang []models.ListCabang

	requestString := helper.StringNullableToString(request.Search)

	if requestString != "" {
		listCabang, err = c.masterDataRepo.SearchCabang(ctx, requestString)
	} else {
		listCabang = nil
	}

	if err != nil {
		c.log.Error("master_data.usecase.MasterDataUsecase.SearchCabang: %s", err.Error())
		// return nil, models2.ErrGeneralMessage.Error(), err
		return listCabang, models2.ErrGeneralMessage.Error(), err
	}

	if listCabang == nil {
		listCabang = make([]models.ListCabang, 0)
	}

	return listCabang, models2.GeneralSuccess, nil
}

func (c MasterDataUsecase) GetTermsAndConditionText(ctx context.Context) (res string, message string, err error) {
	ctx, cancel := context.WithTimeout(ctx, c.contextTimeout)
	defer cancel()

	res, err = c.masterDataRepo.GetTermsAndConditionText(ctx)
	if err != nil {
		c.log.Error("master_data.usecase.MasterDataUsecase.GetTermsAndConditionText: %s", err.Error())
		// return nil, models2.ErrGeneralMessage.Error(), err
		return res, models2.ErrGeneralMessage.Error(), err
	}

	return res, models2.GeneralSuccess, nil
}

func (c MasterDataUsecase) SearchKotaKecamatan(ctx context.Context, request *models.RequestListKotaKecamatanPointer) (res []models.ResponseListKotaKecamatan, message string, err error) {
	ctx, cancel := context.WithTimeout(ctx, c.contextTimeout)
	defer cancel()
	var responseKotaKecamatan []models.ResponseListKotaKecamatan
	var kotaKecamatan []models.QueryStructKotaKecamatan

	requestString := helper.StringNullableToString(request.Search)

	if requestString != "" {
		kotaKecamatan, err = c.masterDataRepo.SearchKotaKecamatan(ctx, requestString)
	} else {
		kotaKecamatan = nil
	}

	if err != nil {
		c.log.Error("master_data.usecase.MasterDataUsecase.SearchKotaKecamatan: %s", err.Error())
		// return nil, models2.ErrGeneralMessage.Error(), err
		return responseKotaKecamatan, models2.ErrGeneralMessage.Error(), err
	}

	if kotaKecamatan == nil {
		responseKotaKecamatan = make([]models.ResponseListKotaKecamatan, 0)
	} else {
		responseKotaKecamatan = make([]models.ResponseListKotaKecamatan, len(kotaKecamatan))
		// fmt.Println("len list ->", len(list))
		for i, val := range kotaKecamatan {
			responseKotaKecamatan[i] = val.MappingResponseListKotaKecamatan()
			// fmt.Println("data -> ", data[i])
		}
	}

	// fmt.Println("kota_kecamatan", kotaKecamatan)

	return responseKotaKecamatan, models2.GeneralSuccess, nil
}

func (c MasterDataUsecase) GetListKelurahan(ctx context.Context, request *models.RequestListKelurahan) (res []models.ResponseListKelurahan, message string, err error) {
	ctx, cancel := context.WithTimeout(ctx, c.contextTimeout)
	defer cancel()

	listkelurahan, err := c.masterDataRepo.GetListKelurahan(ctx, request.KodeKecamatan)

	responseListKelurahan := make([]models.ResponseListKelurahan, len(listkelurahan))
	// fmt.Println("len list ->", len(list))
	for i, val := range listkelurahan {
		responseListKelurahan[i] = val.MappingResponseListKelurahan()
		// fmt.Println("data -> ", data[i])
	}

	if err != nil {
		c.log.Error("master_data.usecase.MasterDataUsecase.GetListKelurahan: %s", err.Error())
		// return nil, models2.ErrGeneralMessage.Error(), err
		return responseListKelurahan, models2.ErrGeneralMessage.Error(), err
	}

	return responseListKelurahan, models2.GeneralSuccess, nil
}
