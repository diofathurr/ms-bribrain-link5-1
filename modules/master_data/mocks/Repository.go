// Code generated by mockery v0.0.0-dev. DO NOT EDIT.

package mocks

import (
	context "context"

	mock "github.com/stretchr/testify/mock"

	models "ms-bribrain-link5/models"
)

// Repository is an autogenerated mock type for the Repository type
type Repository struct {
	mock.Mock
}

// GetAlasanTidakCocok provides a mock function with given fields: ctx
func (_m *Repository) GetAlasanTidakCocok(ctx context.Context) ([]models.PilihanAlasan, error) {
	ret := _m.Called(ctx)

	var r0 []models.PilihanAlasan
	if rf, ok := ret.Get(0).(func(context.Context) []models.PilihanAlasan); ok {
		r0 = rf(ctx)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]models.PilihanAlasan)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context) error); ok {
		r1 = rf(ctx)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetAlasanTidakDapatDihubungi provides a mock function with given fields: ctx
func (_m *Repository) GetAlasanTidakDapatDihubungi(ctx context.Context) ([]models.PilihanAlasan, error) {
	ret := _m.Called(ctx)

	var r0 []models.PilihanAlasan
	if rf, ok := ret.Get(0).(func(context.Context) []models.PilihanAlasan); ok {
		r0 = rf(ctx)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]models.PilihanAlasan)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context) error); ok {
		r1 = rf(ctx)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetOnboardingText provides a mock function with given fields: ctx
func (_m *Repository) GetOnboardingText(ctx context.Context) (models.OnboardingText, error) {
	ret := _m.Called(ctx)

	var r0 models.OnboardingText
	if rf, ok := ret.Get(0).(func(context.Context) models.OnboardingText); ok {
		r0 = rf(ctx)
	} else {
		r0 = ret.Get(0).(models.OnboardingText)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context) error); ok {
		r1 = rf(ctx)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetTermsAndConditionText provides a mock function with given fields: ctx
func (_m *Repository) GetTermsAndConditionText(ctx context.Context) (string, error) {
	ret := _m.Called(ctx)

	var r0 string
	if rf, ok := ret.Get(0).(func(context.Context) string); ok {
		r0 = rf(ctx)
	} else {
		r0 = ret.Get(0).(string)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context) error); ok {
		r1 = rf(ctx)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// SearchCabang provides a mock function with given fields: ctx, request
func (_m *Repository) SearchCabang(ctx context.Context, request string) ([]models.ListCabang, error) {
	ret := _m.Called(ctx, request)

	var r0 []models.ListCabang
	if rf, ok := ret.Get(0).(func(context.Context, string) []models.ListCabang); ok {
		r0 = rf(ctx, request)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]models.ListCabang)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string) error); ok {
		r1 = rf(ctx, request)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
