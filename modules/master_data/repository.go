package master_data

import (
	"ms-bribrain-link5/models"

	"golang.org/x/net/context"
)

type Repository interface {
	GetOnboardingText(ctx context.Context) (res models.OnboardingText, err error)
	GetAlasanTidakDapatDihubungi(ctx context.Context) (res []models.PilihanAlasan, err error)
	GetAlasanTidakCocok(ctx context.Context) (res []models.PilihanAlasan, err error)
	SearchCabang(ctx context.Context, request string) (res []models.ListCabang, err error)
	GetTermsAndConditionText(ctx context.Context) (res string, err error)
	SearchKotaKecamatan(ctx context.Context, request string) (res []models.QueryStructKotaKecamatan, err error)
	GetListKelurahan(ctx context.Context, request string) (res []models.QueryStructListKelurahan, err error)
}
