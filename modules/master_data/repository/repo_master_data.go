package repository

import (
	"context"
	"ms-bribrain-link5/helper/logger"
	"ms-bribrain-link5/models"

	"gorm.io/gorm"

	"ms-bribrain-link5/modules/master_data"
)

type MasterDataRepository struct {
	Conn  *gorm.DB
	Conn2 *gorm.DB
	log   logger.Logger
	table *models.MasterDataTable
}

// NewMasterDataRepository will create an object that represent the master_data.Repository interface
func NewMasterDataRepository(Conn *gorm.DB, Conn2 *gorm.DB, log logger.Logger) master_data.Repository {
	table := &models.MasterDataTable{}
	table = table.MappingTable()
	return &MasterDataRepository{Conn, Conn2, log, table}
}

//query
//func (n MasterDataRepository) query(ctx context.Context) {
//	panic("implement me")
//}
//
////messaging
//func (n MasterDataRepository) messaging(ctx context.Context) {
//	panic("implement me")
//}
//
////grpc
//func (n MasterDataRepository) grpc(ctx context.Context) {
//	panic("implement me")
//}

//functional
func (n MasterDataRepository) GetOnboardingText(ctx context.Context) (res models.OnboardingText, err error) {
	var onboardingtext models.OnboardingText
	query := n.Conn2.WithContext(ctx).Table(n.table.AppsOnboarding.Table).Select(n.table.SelectAllColumnAppsOnboardingTable())
	query = query.Where(n.table.AppsOnboarding.ProductName+` = ?`, "link5")

	err = query.Limit(1).Scan(&onboardingtext).Error

	if err != nil {
		n.log.Error("rekomendasi.repository.MasterDataRepository.GetOnboardingText: %s", err.Error())
		if err.Error() != gorm.ErrRecordNotFound.Error() {
			// return nil, err
			return onboardingtext, err
		}
	}
	res = onboardingtext
	return res, nil
}

func (n MasterDataRepository) queryGetOptions(ctx context.Context, tabel string, opsiTindaklanjut string) (res []models.PilihanAlasan, err error) {

	var options []models.PilihanAlasan

	query := n.Conn.WithContext(ctx).Table(tabel).Select(
		n.table.BribrainLink5MasterOpsiTindaklanjut.Kode + ` AS KODE,` +
			n.table.BribrainLink5MasterOpsiTindaklanjut.Deskripsi + ` AS DESKRIPSI`)

	query = query.Where(n.table.BribrainLink5MasterOpsiTindaklanjut.OpsiTindaklanjut+" = ?", opsiTindaklanjut)

	err = query.Scan(&options).Error

	if err != nil {
		n.log.Error("rekomendasi.repository.MasterDataRepository.queryGetOptions: %s", err.Error())
		if err.Error() != gorm.ErrRecordNotFound.Error() {
			// return nil, err
			return options, err
		}
	}

	res = options

	// fmt.Println("repo -> ", res)

	return res, nil
}

func (n MasterDataRepository) GetAlasanTidakDapatDihubungi(ctx context.Context) (res []models.PilihanAlasan, err error) {

	res, err = n.queryGetOptions(ctx, n.table.BribrainLink5MasterOpsiTindaklanjut.Table, "Tidak Dapat Dihubungi")

	if err != nil {
		n.log.Error("rekomendasi.repository.MasterDataRepository.GetAlasanTidakDapatDihubungi: %s", err.Error())
		if err.Error() != gorm.ErrRecordNotFound.Error() {
			// return nil, err
			return res, err
		}
	}

	return res, nil
}

func (n MasterDataRepository) GetAlasanTidakCocok(ctx context.Context) (res []models.PilihanAlasan, err error) {

	res, err = n.queryGetOptions(ctx, n.table.BribrainLink5MasterOpsiTindaklanjut.Table, "Tidak Cocok")

	if err != nil {
		n.log.Error("rekomendasi.repository.MasterDataRepository.GetAlasanTidakCocok: %s", err.Error())
		if err.Error() != gorm.ErrRecordNotFound.Error() {
			// return nil, err
			return res, err
		}
	}

	// fmt.Println("respon tidak pilihan tidak cocok ->", res)

	return res, nil
}

func (n MasterDataRepository) SearchCabang(ctx context.Context, request string) (res []models.ListCabang, err error) {

	var listCabang []models.ListCabang

	query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5UserFilter.Table).Select(
		n.table.SelectAllColumnBribrainLink5UserFilterTable())

	query = query.Where(n.table.BribrainLink5UserFilter.ParentName+` like ? OR `+n.table.BribrainLink5UserFilter.UnitKerja+` like ?`, "%"+request+"%", "%"+request+"%")
	query = query.Where(n.table.BribrainLink5UserFilter.TipeUker + `= "Kantor Cabang"`)

	err = query.Scan(&listCabang).Error

	if err != nil {
		n.log.Error("rekomendasi.repository.MasterDataRepository.SearchCabang: %s", err.Error())
		if err.Error() != gorm.ErrRecordNotFound.Error() {
			// return nil, err
			return listCabang, err
		}
	}

	res = listCabang

	// fmt.Println("repo -> ", res)

	return res, nil
}

func (n MasterDataRepository) GetTermsAndConditionText(ctx context.Context) (res string, err error) {
	var termsandconditiontext string
	query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5SystemVariable.Table).Select(n.table.BribrainLink5SystemVariable.Value + ` AS VALUE`)
	query = query.Where(n.table.BribrainLink5SystemVariable.Variable+` = ?`, "tnc")

	err = query.Limit(1).Scan(&termsandconditiontext).Error

	if err != nil {
		n.log.Error("rekomendasi.repository.MasterDataRepository.GetTermsAndConditionText: %s", err.Error())
		if err.Error() != gorm.ErrRecordNotFound.Error() {
			// return nil, err
			return termsandconditiontext, err
		}
	}
	res = termsandconditiontext
	return res, nil
}

func (n MasterDataRepository) SearchKotaKecamatan(ctx context.Context, request string) (res []models.QueryStructKotaKecamatan, err error) {

	var kotaKecamatan []models.QueryStructKotaKecamatan

	query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5MasterWilayah.Table).Distinct(
		n.table.SelectProvinsiKotaKecamatanBribrainLink5MasterWilayahTable())

	query = query.Where(n.table.BribrainLink5MasterWilayah.Kecamatan+` like ? OR `+n.table.BribrainLink5MasterWilayah.Kota+` like ?`, "%"+request+"%", "%"+request+"%")
	// query = query.Where(n.table.BribrainLink5MasterWilayah.TipeUker + `= "Kantor Cabang"`)

	err = query.
		Order(n.table.BribrainLink5MasterWilayah.Provinsi + ` ASC`).
		Order(n.table.BribrainLink5MasterWilayah.Kota + ` ASC`).
		Order(n.table.BribrainLink5MasterWilayah.Kecamatan + ` ASC`).
		Scan(&kotaKecamatan).Error

	if err != nil {
		n.log.Error("rekomendasi.repository.MasterDataRepository.SearchKotaKecamatan: %s", err.Error())
		if err.Error() != gorm.ErrRecordNotFound.Error() {
			// return nil, err
			return kotaKecamatan, err
		}
	}

	res = kotaKecamatan

	// fmt.Println("repo -> ", res)

	return res, nil
}

func (n MasterDataRepository) GetListKelurahan(ctx context.Context, request string) (res []models.QueryStructListKelurahan, err error) {

	var kelurahan []models.QueryStructListKelurahan

	query := n.Conn.WithContext(ctx).Table(n.table.BribrainLink5MasterWilayah.Table).Select(
		n.table.SelectKelurahanKotaKecamatanBribrainLink5MasterWilayahTable())

	query = query.Where(n.table.BribrainLink5MasterWilayah.KodeKecamatan+` = ?`, request)
	// query = query.Where(n.table.BribrainLink5MasterWilayah.TipeUker + `= "Kantor Cabang"`)

	err = query.
		Order(n.table.BribrainLink5MasterWilayah.Kelurahan + ` ASC`).
		Order(n.table.BribrainLink5MasterWilayah.Kota + ` ASC`).
		Order(n.table.BribrainLink5MasterWilayah.Kecamatan + ` ASC`).
		Scan(&kelurahan).Error

	if err != nil {
		n.log.Error("rekomendasi.repository.MasterDataRepository.GetListKelurahan: %s", err.Error())
		if err.Error() != gorm.ErrRecordNotFound.Error() {
			// return nil, err
			return kelurahan, err
		}
	}

	res = kelurahan

	// fmt.Println("repo -> ", res)

	return res, nil
}
