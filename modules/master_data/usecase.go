package master_data

import (
	"ms-bribrain-link5/models"

	"golang.org/x/net/context"
)

type Usecase interface {
	GetOnboardingText(ctx context.Context) (res models.OnboardingText, message string, err error)
	GetAlasanTidakDapatDihubungi(ctx context.Context) (res []models.PilihanAlasan, message string, err error)
	GetAlasanTidakCocok(ctx context.Context) (res []models.PilihanAlasan, message string, err error)
	SearchCabang(ctx context.Context, request *models.RequestListCabangPointer) (res []models.ListCabang, message string, err error)
	GetTermsAndConditionText(ctx context.Context) (res string, message string, err error)
	SearchKotaKecamatan(ctx context.Context, request *models.RequestListKotaKecamatanPointer) (res []models.ResponseListKotaKecamatan, message string, err error)
	GetListKelurahan(ctx context.Context, request *models.RequestListKelurahan) (res []models.ResponseListKelurahan, message string, err error)
}
