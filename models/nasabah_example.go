package models

import (
	"ms-bribrain-link5/helper"
	"ms-bribrain-link5/helper/models"
	"time"
)

func (a *RequestDetailNasabahEncrypted) MappingExampleData() *RequestDetailNasabahEncrypted {
	res := &RequestDetailNasabahEncrypted{
		Cifno: "ABC2345",
	}
	return res
}

func (a *RequestDetailNasabahEncrypted) MappingExampleDataBadRequest() *RequestDetailNasabahEncrypted {
	res := &RequestDetailNasabahEncrypted{
		Cifno: "%^&*",
	}
	return res
}

func (a *ResponseDetailNasabah) MappingExampleDataNil() *ResponseDetailNasabah {
	res := &ResponseDetailNasabah{
		DetailCard:         DetailCard{},
		DetailTier:         DetailTier{},
		DetailInfoPersonal: DetailInfoPersonal{},
		DetailInfoRekening: DetailInfoRekening{},
		DetailInfoSimpanan: &DetailInfoSimpanan{},
		DetailInfoPinjaman: &DetailInfoPinjaman{},
	}

	return res
}

func (a *ResponseDetailNasabahDitindaklanjuti) MappingExampleDataNil() *ResponseDetailNasabahDitindaklanjuti {
	res := &ResponseDetailNasabahDitindaklanjuti{
		DetailCard:                DetailCard{},
		DetailTier:                DetailTier{},
		DetailCatatanTindaklanjut: []DetailCatatanTindaklanjut{},
		DetailInfoPersonal:        DetailInfoPersonal{},
		DetailInfoRekening:        DetailInfoRekening{},
		DetailInfoSimpanan:        &DetailInfoSimpanan{},
		DetailInfoPinjaman:        &DetailInfoPinjaman{},
	}

	return res
}

func (a *RequestListNasabah) MappingExampleData() *RequestListNasabah {
	res := &RequestListNasabah{
		Page:   "1",
		Limit:  "5",
		Search: "",
	}
	return res
}

func (a *RequestListNasabah) MappingExampleDataBadRequest() *RequestListNasabah {
	res := &RequestListNasabah{
		Page:   "a",
		Limit:  "5",
		Search: "",
	}
	return res
}

func (a *ResponseListRekomendasiNasabahPaginationDto) MappingExampleDataNil() *ResponseListRekomendasiNasabahPaginationDto {
	res := &ResponseListRekomendasiNasabahPaginationDto{
		Data:                []*ResponseListRekomendasiNasabah{},
		DataTerakhirUpdated: a.DataTerakhirUpdated,
		Paginator:           models.Paginator{},
		TabCounter:          TabCounter{},
	}

	return res
}

func (a *ResponseListRekomendasiNasabahNonBRIPaginationDto) MappingExampleDataNil() *ResponseListRekomendasiNasabahNonBRIPaginationDto {
	res := &ResponseListRekomendasiNasabahNonBRIPaginationDto{
		Data:                []*ResponseListRekomendasiNasabahNonBRI{},
		DataTerakhirUpdated: a.DataTerakhirUpdated,
		Paginator:           models.Paginator{},
		TabCounter:          TabCounter{},
	}

	return res
}

func (a *ResponseListDitindaklanjutiPaginationDto) MappingExampleDataNil() *ResponseListDitindaklanjutiPaginationDto {
	res := &ResponseListDitindaklanjutiPaginationDto{
		Data:                []*ResponseListDitindaklanjuti{},
		DataTerakhirUpdated: a.DataTerakhirUpdated,
		Paginator:           models.Paginator{},
		TabCounter:          TabCounter{},
	}

	return res
}

func (a *ResponseInfoTierNasabahV110Dto) MappingExampleDataNil() *ResponseInfoTierNasabahV110Dto {
	res := &ResponseInfoTierNasabahV110Dto{
		HierarkiTier: []*ResponseInfoTierNasabahV110{},
	}

	return res
}

func (a *QueryGetInfoRM) MappingExampleData() *QueryGetInfoRM {
	res := &QueryGetInfoRM{
		KodeRegion:     helper.StringToStringNullable("A"),
		Rgdesc:         helper.StringToStringNullable("DKI2"),
		KodeMainbranch: helper.StringToStringNullable("345"),
		Mbdesc:         helper.StringToStringNullable("Bendungan Hilir"),
		PnRm:           helper.StringToStringNullable("00999950"),
		NamaRm:         helper.StringToStringNullable("Dodi"),
		BranchRm:       helper.StringToStringNullable("Bendungan Hilir"),
		PhonenumberRm:  helper.StringToStringNullable("0856565656"),
	}
	return res
}

func (a *GetDetailQueryStruct) MappingExampleData() *GetDetailQueryStruct {
	res := &GetDetailQueryStruct{
		CifNasabahRekan:        helper.StringToStringNullable("ABC1234"),
		NamaNasabahRekan:       helper.StringToStringNullable("Anto"),
		NamaNasabahUtama:       helper.StringToStringNullable("Budi"),
		PosisiTier:             helper.IntToInt64Nullable(1),
		Usia:                   helper.IntToInt64Nullable(40),
		NomorKtp:               helper.StringToStringNullable("343323232323"),
		PnRm:                   helper.StringToStringNullable("00999950"),
		JenisKelamin:           helper.StringToStringNullable("Laki-Laki"),
		SektorEkonomi:          helper.StringToStringNullable("Pedagang"),
		TierDiatasnya:          helper.StringToStringNullable("Charlie"),
		RmDiatasnya:            helper.StringToStringNullable("Donovan"),
		SegmenPinjaman:         helper.StringToStringNullable("Evan"),
		Branch:                 helper.StringToStringNullable("Kecil"),
		Rgdesc:                 helper.StringToStringNullable("345"),
		Mbdesc:                 helper.StringToStringNullable("DKI2"),
		Sbdesc:                 helper.StringToStringNullable("KC Jakarta"),
		Brdesc:                 helper.StringToStringNullable("Unit Jakarta"),
		Phonenumber:            helper.StringToStringNullable("Unit Jakarta"),
		AlamatId:               helper.StringToStringNullable("Jalan Bendungan 1"),
		AlamatDomisili:         helper.StringToStringNullable("Jalan Bendungan 2"),
		AlamatPinjaman:         helper.StringToStringNullable("Jalan Bendungan 3"),
		UsiaRekeningBulan:      helper.StringToStringNullable("30"),
		ProdukSimpanan:         helper.StringToStringNullable("BRITAMA"),
		SnameRmSimpanan:        helper.StringToStringNullable("Fany"),
		ProdukPinjaman:         helper.StringToStringNullable("Kupedes"),
		SnameRmPinjaman:        helper.StringToStringNullable("Gigih"),
		Segmen:                 helper.StringToStringNullable("Mikro"),
		PhonenumberRmPinjaman:  helper.StringToStringNullable("085656565656"),
		GroupUsaha:             helper.StringToStringNullable("Pedagang"),
		JenisPekerjaan:         helper.StringToStringNullable("Pedagang"),
		BidangPekerjaan:        helper.StringToStringNullable("Pedagang"),
		Kolektabilitas:         helper.StringToStringNullable("Lancar"),
		TotalPlafon:            helper.FloatToFloatNullable(30000),
		TotalOutstanding:       helper.FloatToFloatNullable(30000),
		InetBanking:            helper.StringToStringNullable("Ya"),
		SmsBanking:             helper.StringToStringNullable("Ya"),
		TotalCreditBri:         helper.FloatToFloatNullable(30000),
		TotalDebitBri:          helper.FloatToFloatNullable(30000),
		TotalKreditNonBri:      helper.FloatToFloatNullable(30000),
		TotalDebitNonBri:       helper.FloatToFloatNullable(30000),
		TotalKreditDebitNonBri: helper.FloatToFloatNullable(30000),
		NamaNasabahNonBri:      helper.StringToStringNullable("Hasan"),
		NamaBankLawan:          helper.StringToStringNullable("BNI"),
		RatasSaldoSimpananBri:  helper.FloatToFloatNullable(30000),
		TotalCreditKeseluruhan: helper.FloatToFloatNullable(30000),
		TotalDebitKeseluruhan:  helper.FloatToFloatNullable(30000),
		CifNasabahTier1:        helper.StringToStringNullable("ABC2345"),
		NamaNasabahTier1:       helper.StringToStringNullable("Ilham"),
		SnameRmPinjamanTier1:   helper.StringToStringNullable("Jono"),
		UnitRmTier1:            helper.StringToStringNullable("KC Jakarta"),
		PhonenumberRmTier1:     helper.StringToStringNullable("085656565656"),
		CifNasabahTier2:        helper.StringToStringNullable("ABC2345"),
		NamaNasabahTier2:       helper.StringToStringNullable("Ilham"),
		SnameRmPinjamanTier2:   helper.StringToStringNullable("Jono"),
		UnitRmTier2:            helper.StringToStringNullable("KC Jakarta"),
		PhonenumberTier2:       helper.StringToStringNullable("085656565656"),
		CifNasabahTier3:        helper.StringToStringNullable("ABC2345"),
		NamaNasabahTier3:       helper.StringToStringNullable("Ilham"),
		SnameRmPinjamanTier3:   helper.StringToStringNullable("Jono"),
		UnitRmTier3:            helper.StringToStringNullable("KC Jakarta"),
		PhonenumberTier3:       helper.StringToStringNullable("085656565656"),
		CifNasabahTier4:        helper.StringToStringNullable("ABC2345"),
		NamaNasabahTier4:       helper.StringToStringNullable("Ilham"),
		SnameRmPinjamanTier4:   helper.StringToStringNullable("Jono"),
		UnitRmTier4:            helper.StringToStringNullable("KC Jakarta"),
		PhonenumberTier4:       helper.StringToStringNullable("085656565656"),
		StatusRekening:         helper.StringToStringNullable("Aktif"),
		AkuisisiSimpanan:       helper.StringToStringNullable("1"),
		AkuisisiPinjaman:       helper.StringToStringNullable("1"),
		LamaNasabahBulan:       helper.StringToStringNullable("30"),
		TanggalLahir:           helper.StringToStringNullable("12 Januari 1990"),
		TotalMutasiDebit:       helper.FloatToFloatNullable(30000),
		TotalMutasiKredit:      helper.FloatToFloatNullable(30000),
		Ds:                     helper.StringToStringNullable("202010"),
		IdStatus:               helper.IntToInt64Nullable(1),
		StatusAkuisisiSimpanan: helper.StringToStringNullable("Sudah"),
		StatusAkuisisiPinjaman: helper.StringToStringNullable("Sudah"),
		VolumePotensiPinjaman:  helper.FloatToFloatNullable(30000),
		VolumePotensiSimpanan:  helper.FloatToFloatNullable(30000),

		Id:                      helper.IntToInt64Nullable(1),
		Cifno:                   helper.StringToStringNullable("VCB656"),
		KodeNonBri:              helper.StringToStringNullable("BRINON666"),
		TipeCustomer:            helper.StringToStringNullable("BRI"),
		StatusHubungi:           helper.StringToStringNullable("Sudah Dihubungi"),
		KodeHubungi:             helper.IntToInt64Nullable(1),
		TglHubungi:              helper.DateTimeToDateTimeNUllable(helper.StringToDateTimeNullable("2021-01-02")),
		CatatanHubungi:          helper.StringToStringNullable("catatan"),
		StatusKetertarikan:      helper.StringToStringNullable("Tertarik"),
		KodeKetertarikan:        helper.IntToInt64Nullable(1),
		CatatanKetertarikan:     helper.StringToStringNullable("catatan"),
		StatusKunjungan:         helper.StringToStringNullable("Sudah Dikunjungi"),
		KodeKunjungan:           helper.IntToInt64Nullable(1),
		TglKunjungan:            helper.DateTimeToDateTimeNUllable(helper.StringToDateTimeNullable("2021-01-02")),
		CatatanKunjungan:        helper.StringToStringNullable("catatab"),
		CreatedDate:             helper.DateTimeToDateTimeNUllable(helper.StringToDateTimeNullable("2021-01-02")),
		UpdatedDate:             helper.DateTimeToDateTimeNUllable(helper.StringToDateTimeNullable("2021-01-02")),
		IdStatusTabelNon:        helper.IntToInt64Nullable(1),
		VolumePotensiRmPinjaman: helper.FloatToFloatNullable(30000),
		VolumePotensiRmSimpanan: helper.FloatToFloatNullable(30000),
	}
	return res
}

func (a *GetDetailQueryStruct) MappingExampleDataTier2() *GetDetailQueryStruct {
	res := &GetDetailQueryStruct{
		CifNasabahRekan:        helper.StringToStringNullable("ABC1234"),
		NamaNasabahRekan:       helper.StringToStringNullable("Anto"),
		NamaNasabahUtama:       helper.StringToStringNullable("Budi"),
		PosisiTier:             helper.IntToInt64Nullable(2),
		Usia:                   helper.IntToInt64Nullable(40),
		NomorKtp:               helper.StringToStringNullable("343323232323"),
		PnRm:                   helper.StringToStringNullable("00999950"),
		JenisKelamin:           helper.StringToStringNullable("Laki-Laki"),
		SektorEkonomi:          helper.StringToStringNullable("Pedagang"),
		TierDiatasnya:          helper.StringToStringNullable("Charlie"),
		RmDiatasnya:            helper.StringToStringNullable("Donovan"),
		SegmenPinjaman:         helper.StringToStringNullable("Evan"),
		Branch:                 helper.StringToStringNullable("Kecil"),
		Rgdesc:                 helper.StringToStringNullable("345"),
		Mbdesc:                 helper.StringToStringNullable("DKI2"),
		Sbdesc:                 helper.StringToStringNullable("KC Jakarta"),
		Brdesc:                 helper.StringToStringNullable("Unit Jakarta"),
		Phonenumber:            helper.StringToStringNullable("Unit Jakarta"),
		AlamatId:               helper.StringToStringNullable("Jalan Bendungan 1"),
		AlamatDomisili:         helper.StringToStringNullable("Jalan Bendungan 2"),
		AlamatPinjaman:         helper.StringToStringNullable("Jalan Bendungan 3"),
		UsiaRekeningBulan:      helper.StringToStringNullable("30"),
		ProdukSimpanan:         helper.StringToStringNullable("BRITAMA"),
		SnameRmSimpanan:        helper.StringToStringNullable("Fany"),
		ProdukPinjaman:         helper.StringToStringNullable("Kupedes"),
		SnameRmPinjaman:        helper.StringToStringNullable("Gigih"),
		Segmen:                 helper.StringToStringNullable("Mikro"),
		PhonenumberRmPinjaman:  helper.StringToStringNullable("085656565656"),
		GroupUsaha:             helper.StringToStringNullable("Pedagang"),
		JenisPekerjaan:         helper.StringToStringNullable("Pedagang"),
		BidangPekerjaan:        helper.StringToStringNullable("Pedagang"),
		Kolektabilitas:         helper.StringToStringNullable("Lancar"),
		TotalPlafon:            helper.FloatToFloatNullable(30000),
		TotalOutstanding:       helper.FloatToFloatNullable(30000),
		InetBanking:            helper.StringToStringNullable("Ya"),
		SmsBanking:             helper.StringToStringNullable("Ya"),
		TotalCreditBri:         helper.FloatToFloatNullable(30000),
		TotalDebitBri:          helper.FloatToFloatNullable(30000),
		TotalKreditNonBri:      helper.FloatToFloatNullable(30000),
		TotalDebitNonBri:       helper.FloatToFloatNullable(30000),
		TotalKreditDebitNonBri: helper.FloatToFloatNullable(30000),
		NamaNasabahNonBri:      helper.StringToStringNullable("Hasan"),
		NamaBankLawan:          helper.StringToStringNullable("BNI"),
		RatasSaldoSimpananBri:  helper.FloatToFloatNullable(30000),
		TotalCreditKeseluruhan: helper.FloatToFloatNullable(30000),
		TotalDebitKeseluruhan:  helper.FloatToFloatNullable(30000),
		CifNasabahTier1:        helper.StringToStringNullable("ABC2345"),
		NamaNasabahTier1:       helper.StringToStringNullable("Ilham"),
		SnameRmPinjamanTier1:   helper.StringToStringNullable("Jono"),
		UnitRmTier1:            helper.StringToStringNullable("KC Jakarta"),
		PhonenumberRmTier1:     helper.StringToStringNullable("085656565656"),
		CifNasabahTier2:        helper.StringToStringNullable("ABC2345"),
		NamaNasabahTier2:       helper.StringToStringNullable("Ilham"),
		SnameRmPinjamanTier2:   helper.StringToStringNullable("Jono"),
		UnitRmTier2:            helper.StringToStringNullable("KC Jakarta"),
		PhonenumberTier2:       helper.StringToStringNullable("085656565656"),
		CifNasabahTier3:        helper.StringToStringNullable("ABC2345"),
		NamaNasabahTier3:       helper.StringToStringNullable("Ilham"),
		SnameRmPinjamanTier3:   helper.StringToStringNullable("Jono"),
		UnitRmTier3:            helper.StringToStringNullable("KC Jakarta"),
		PhonenumberTier3:       helper.StringToStringNullable("085656565656"),
		CifNasabahTier4:        helper.StringToStringNullable("ABC2345"),
		NamaNasabahTier4:       helper.StringToStringNullable("Ilham"),
		SnameRmPinjamanTier4:   helper.StringToStringNullable("Jono"),
		UnitRmTier4:            helper.StringToStringNullable("KC Jakarta"),
		PhonenumberTier4:       helper.StringToStringNullable("085656565656"),
		StatusRekening:         helper.StringToStringNullable("Aktif"),
		AkuisisiSimpanan:       helper.StringToStringNullable("1"),
		AkuisisiPinjaman:       helper.StringToStringNullable("1"),
		LamaNasabahBulan:       helper.StringToStringNullable("30"),
		TanggalLahir:           helper.StringToStringNullable("12 Januari 1990"),
		TotalMutasiDebit:       helper.FloatToFloatNullable(30000),
		TotalMutasiKredit:      helper.FloatToFloatNullable(30000),
		Ds:                     helper.StringToStringNullable("202010"),
		IdStatus:               helper.IntToInt64Nullable(1),
		StatusAkuisisiSimpanan: helper.StringToStringNullable("Sudah"),
		StatusAkuisisiPinjaman: helper.StringToStringNullable("Sudah"),
		VolumePotensiPinjaman:  helper.FloatToFloatNullable(30000),
		VolumePotensiSimpanan:  helper.FloatToFloatNullable(30000),

		Id:                      helper.IntToInt64Nullable(1),
		Cifno:                   helper.StringToStringNullable("VCB656"),
		KodeNonBri:              helper.StringToStringNullable("BRINON666"),
		TipeCustomer:            helper.StringToStringNullable("BRI"),
		StatusHubungi:           helper.StringToStringNullable("Sudah Dihubungi"),
		KodeHubungi:             helper.IntToInt64Nullable(1),
		TglHubungi:              helper.DateTimeToDateTimeNUllable(helper.StringToDateTimeNullable("2021-01-02")),
		CatatanHubungi:          helper.StringToStringNullable("catatan"),
		StatusKetertarikan:      helper.StringToStringNullable("Tertarik"),
		KodeKetertarikan:        helper.IntToInt64Nullable(1),
		CatatanKetertarikan:     helper.StringToStringNullable("catatan"),
		StatusKunjungan:         helper.StringToStringNullable("Sudah Dikunjungi"),
		KodeKunjungan:           helper.IntToInt64Nullable(1),
		TglKunjungan:            helper.DateTimeToDateTimeNUllable(helper.StringToDateTimeNullable("2021-01-02")),
		CatatanKunjungan:        helper.StringToStringNullable("catatab"),
		CreatedDate:             helper.DateTimeToDateTimeNUllable(helper.StringToDateTimeNullable("2021-01-02")),
		UpdatedDate:             helper.DateTimeToDateTimeNUllable(helper.StringToDateTimeNullable("2021-01-02")),
		IdStatusTabelNon:        helper.IntToInt64Nullable(1),
		VolumePotensiRmPinjaman: helper.FloatToFloatNullable(30000),
		VolumePotensiRmSimpanan: helper.FloatToFloatNullable(30000),
	}
	return res
}

func (a *GetDetailQueryStruct) MappingExampleDataTier3() *GetDetailQueryStruct {
	res := &GetDetailQueryStruct{
		CifNasabahRekan:        helper.StringToStringNullable("ABC1234"),
		NamaNasabahRekan:       helper.StringToStringNullable("Anto"),
		NamaNasabahUtama:       helper.StringToStringNullable("Budi"),
		PosisiTier:             helper.IntToInt64Nullable(3),
		Usia:                   helper.IntToInt64Nullable(40),
		NomorKtp:               helper.StringToStringNullable("343323232323"),
		PnRm:                   helper.StringToStringNullable("00999950"),
		JenisKelamin:           helper.StringToStringNullable("Laki-Laki"),
		SektorEkonomi:          helper.StringToStringNullable("Pedagang"),
		TierDiatasnya:          helper.StringToStringNullable("Charlie"),
		RmDiatasnya:            helper.StringToStringNullable("Donovan"),
		SegmenPinjaman:         helper.StringToStringNullable("Evan"),
		Branch:                 helper.StringToStringNullable("Kecil"),
		Rgdesc:                 helper.StringToStringNullable("345"),
		Mbdesc:                 helper.StringToStringNullable("DKI2"),
		Sbdesc:                 helper.StringToStringNullable("KC Jakarta"),
		Brdesc:                 helper.StringToStringNullable("Unit Jakarta"),
		Phonenumber:            helper.StringToStringNullable("Unit Jakarta"),
		AlamatId:               helper.StringToStringNullable("Jalan Bendungan 1"),
		AlamatDomisili:         helper.StringToStringNullable("Jalan Bendungan 2"),
		AlamatPinjaman:         helper.StringToStringNullable("Jalan Bendungan 3"),
		UsiaRekeningBulan:      helper.StringToStringNullable("30"),
		ProdukSimpanan:         helper.StringToStringNullable("BRITAMA"),
		SnameRmSimpanan:        helper.StringToStringNullable("Fany"),
		ProdukPinjaman:         helper.StringToStringNullable("Kupedes"),
		SnameRmPinjaman:        helper.StringToStringNullable("Gigih"),
		Segmen:                 helper.StringToStringNullable("Mikro"),
		PhonenumberRmPinjaman:  helper.StringToStringNullable("085656565656"),
		GroupUsaha:             helper.StringToStringNullable("Pedagang"),
		JenisPekerjaan:         helper.StringToStringNullable("Pedagang"),
		BidangPekerjaan:        helper.StringToStringNullable("Pedagang"),
		Kolektabilitas:         helper.StringToStringNullable("Lancar"),
		TotalPlafon:            helper.FloatToFloatNullable(30000),
		TotalOutstanding:       helper.FloatToFloatNullable(30000),
		InetBanking:            helper.StringToStringNullable("Ya"),
		SmsBanking:             helper.StringToStringNullable("Ya"),
		TotalCreditBri:         helper.FloatToFloatNullable(30000),
		TotalDebitBri:          helper.FloatToFloatNullable(30000),
		TotalKreditNonBri:      helper.FloatToFloatNullable(30000),
		TotalDebitNonBri:       helper.FloatToFloatNullable(30000),
		TotalKreditDebitNonBri: helper.FloatToFloatNullable(30000),
		NamaNasabahNonBri:      helper.StringToStringNullable("Hasan"),
		NamaBankLawan:          helper.StringToStringNullable("BNI"),
		RatasSaldoSimpananBri:  helper.FloatToFloatNullable(30000),
		TotalCreditKeseluruhan: helper.FloatToFloatNullable(30000),
		TotalDebitKeseluruhan:  helper.FloatToFloatNullable(30000),
		CifNasabahTier1:        helper.StringToStringNullable("ABC2345"),
		NamaNasabahTier1:       helper.StringToStringNullable("Ilham"),
		SnameRmPinjamanTier1:   helper.StringToStringNullable("Jono"),
		UnitRmTier1:            helper.StringToStringNullable("KC Jakarta"),
		PhonenumberRmTier1:     helper.StringToStringNullable("085656565656"),
		CifNasabahTier2:        helper.StringToStringNullable("ABC2345"),
		NamaNasabahTier2:       helper.StringToStringNullable("Ilham"),
		SnameRmPinjamanTier2:   helper.StringToStringNullable("Jono"),
		UnitRmTier2:            helper.StringToStringNullable("KC Jakarta"),
		PhonenumberTier2:       helper.StringToStringNullable("085656565656"),
		CifNasabahTier3:        helper.StringToStringNullable("ABC2345"),
		NamaNasabahTier3:       helper.StringToStringNullable("Ilham"),
		SnameRmPinjamanTier3:   helper.StringToStringNullable("Jono"),
		UnitRmTier3:            helper.StringToStringNullable("KC Jakarta"),
		PhonenumberTier3:       helper.StringToStringNullable("085656565656"),
		CifNasabahTier4:        helper.StringToStringNullable("ABC2345"),
		NamaNasabahTier4:       helper.StringToStringNullable("Ilham"),
		SnameRmPinjamanTier4:   helper.StringToStringNullable("Jono"),
		UnitRmTier4:            helper.StringToStringNullable("KC Jakarta"),
		PhonenumberTier4:       helper.StringToStringNullable("085656565656"),
		StatusRekening:         helper.StringToStringNullable("Aktif"),
		AkuisisiSimpanan:       helper.StringToStringNullable("1"),
		AkuisisiPinjaman:       helper.StringToStringNullable("1"),
		LamaNasabahBulan:       helper.StringToStringNullable("30"),
		TanggalLahir:           helper.StringToStringNullable("12 Januari 1990"),
		TotalMutasiDebit:       helper.FloatToFloatNullable(30000),
		TotalMutasiKredit:      helper.FloatToFloatNullable(30000),
		Ds:                     helper.StringToStringNullable("202010"),
		IdStatus:               helper.IntToInt64Nullable(1),
		StatusAkuisisiSimpanan: helper.StringToStringNullable("Sudah"),
		StatusAkuisisiPinjaman: helper.StringToStringNullable("Sudah"),
		VolumePotensiPinjaman:  helper.FloatToFloatNullable(30000),
		VolumePotensiSimpanan:  helper.FloatToFloatNullable(30000),

		Id:                      helper.IntToInt64Nullable(1),
		Cifno:                   helper.StringToStringNullable("VCB656"),
		KodeNonBri:              helper.StringToStringNullable("BRINON666"),
		TipeCustomer:            helper.StringToStringNullable("BRI"),
		StatusHubungi:           helper.StringToStringNullable("Sudah Dihubungi"),
		KodeHubungi:             helper.IntToInt64Nullable(1),
		TglHubungi:              helper.DateTimeToDateTimeNUllable(helper.StringToDateTimeNullable("2021-01-02")),
		CatatanHubungi:          helper.StringToStringNullable("catatan"),
		StatusKetertarikan:      helper.StringToStringNullable("Tertarik"),
		KodeKetertarikan:        helper.IntToInt64Nullable(1),
		CatatanKetertarikan:     helper.StringToStringNullable("catatan"),
		StatusKunjungan:         helper.StringToStringNullable("Sudah Dikunjungi"),
		KodeKunjungan:           helper.IntToInt64Nullable(1),
		TglKunjungan:            helper.DateTimeToDateTimeNUllable(helper.StringToDateTimeNullable("2021-01-02")),
		CatatanKunjungan:        helper.StringToStringNullable("catatab"),
		CreatedDate:             helper.DateTimeToDateTimeNUllable(helper.StringToDateTimeNullable("2021-01-02")),
		UpdatedDate:             helper.DateTimeToDateTimeNUllable(helper.StringToDateTimeNullable("2021-01-02")),
		IdStatusTabelNon:        helper.IntToInt64Nullable(1),
		VolumePotensiRmPinjaman: helper.FloatToFloatNullable(30000),
		VolumePotensiRmSimpanan: helper.FloatToFloatNullable(30000),
	}
	return res
}

func (a *GetDetailQueryStruct) MappingExampleDataTier4() *GetDetailQueryStruct {
	res := &GetDetailQueryStruct{
		CifNasabahRekan:        helper.StringToStringNullable("ABC1234"),
		NamaNasabahRekan:       helper.StringToStringNullable("Anto"),
		NamaNasabahUtama:       helper.StringToStringNullable("Budi"),
		PosisiTier:             helper.IntToInt64Nullable(4),
		Usia:                   helper.IntToInt64Nullable(40),
		NomorKtp:               helper.StringToStringNullable("343323232323"),
		PnRm:                   helper.StringToStringNullable("00999950"),
		JenisKelamin:           helper.StringToStringNullable("Laki-Laki"),
		SektorEkonomi:          helper.StringToStringNullable("Pedagang"),
		TierDiatasnya:          helper.StringToStringNullable("Charlie"),
		RmDiatasnya:            helper.StringToStringNullable("Donovan"),
		SegmenPinjaman:         helper.StringToStringNullable("Evan"),
		Branch:                 helper.StringToStringNullable("Kecil"),
		Rgdesc:                 helper.StringToStringNullable("345"),
		Mbdesc:                 helper.StringToStringNullable("DKI2"),
		Sbdesc:                 helper.StringToStringNullable("KC Jakarta"),
		Brdesc:                 helper.StringToStringNullable("Unit Jakarta"),
		Phonenumber:            helper.StringToStringNullable("Unit Jakarta"),
		AlamatId:               helper.StringToStringNullable("Jalan Bendungan 1"),
		AlamatDomisili:         helper.StringToStringNullable("Jalan Bendungan 2"),
		AlamatPinjaman:         helper.StringToStringNullable("Jalan Bendungan 3"),
		UsiaRekeningBulan:      helper.StringToStringNullable("30"),
		ProdukSimpanan:         helper.StringToStringNullable("BRITAMA"),
		SnameRmSimpanan:        helper.StringToStringNullable("Fany"),
		ProdukPinjaman:         helper.StringToStringNullable("Kupedes"),
		SnameRmPinjaman:        helper.StringToStringNullable("Gigih"),
		Segmen:                 helper.StringToStringNullable("Mikro"),
		PhonenumberRmPinjaman:  helper.StringToStringNullable("085656565656"),
		GroupUsaha:             helper.StringToStringNullable("Pedagang"),
		JenisPekerjaan:         helper.StringToStringNullable("Pedagang"),
		BidangPekerjaan:        helper.StringToStringNullable("Pedagang"),
		Kolektabilitas:         helper.StringToStringNullable("Lancar"),
		TotalPlafon:            helper.FloatToFloatNullable(30000),
		TotalOutstanding:       helper.FloatToFloatNullable(30000),
		InetBanking:            helper.StringToStringNullable("Ya"),
		SmsBanking:             helper.StringToStringNullable("Ya"),
		TotalCreditBri:         helper.FloatToFloatNullable(30000),
		TotalDebitBri:          helper.FloatToFloatNullable(30000),
		TotalKreditNonBri:      helper.FloatToFloatNullable(30000),
		TotalDebitNonBri:       helper.FloatToFloatNullable(30000),
		TotalKreditDebitNonBri: helper.FloatToFloatNullable(30000),
		NamaNasabahNonBri:      helper.StringToStringNullable("Hasan"),
		NamaBankLawan:          helper.StringToStringNullable("BNI"),
		RatasSaldoSimpananBri:  helper.FloatToFloatNullable(30000),
		TotalCreditKeseluruhan: helper.FloatToFloatNullable(30000),
		TotalDebitKeseluruhan:  helper.FloatToFloatNullable(30000),
		CifNasabahTier1:        helper.StringToStringNullable("ABC2345"),
		NamaNasabahTier1:       helper.StringToStringNullable("Ilham"),
		SnameRmPinjamanTier1:   helper.StringToStringNullable("Jono"),
		UnitRmTier1:            helper.StringToStringNullable("KC Jakarta"),
		PhonenumberRmTier1:     helper.StringToStringNullable("085656565656"),
		CifNasabahTier2:        helper.StringToStringNullable("ABC2345"),
		NamaNasabahTier2:       helper.StringToStringNullable("Ilham"),
		SnameRmPinjamanTier2:   helper.StringToStringNullable("Jono"),
		UnitRmTier2:            helper.StringToStringNullable("KC Jakarta"),
		PhonenumberTier2:       helper.StringToStringNullable("085656565656"),
		CifNasabahTier3:        helper.StringToStringNullable("ABC2345"),
		NamaNasabahTier3:       helper.StringToStringNullable("Ilham"),
		SnameRmPinjamanTier3:   helper.StringToStringNullable("Jono"),
		UnitRmTier3:            helper.StringToStringNullable("KC Jakarta"),
		PhonenumberTier3:       helper.StringToStringNullable("085656565656"),
		CifNasabahTier4:        helper.StringToStringNullable("ABC2345"),
		NamaNasabahTier4:       helper.StringToStringNullable("Ilham"),
		SnameRmPinjamanTier4:   helper.StringToStringNullable("Jono"),
		UnitRmTier4:            helper.StringToStringNullable("KC Jakarta"),
		PhonenumberTier4:       helper.StringToStringNullable("085656565656"),
		StatusRekening:         helper.StringToStringNullable("Aktif"),
		AkuisisiSimpanan:       helper.StringToStringNullable("1"),
		AkuisisiPinjaman:       helper.StringToStringNullable("1"),
		LamaNasabahBulan:       helper.StringToStringNullable("30"),
		TanggalLahir:           helper.StringToStringNullable("12 Januari 1990"),
		TotalMutasiDebit:       helper.FloatToFloatNullable(30000),
		TotalMutasiKredit:      helper.FloatToFloatNullable(30000),
		Ds:                     helper.StringToStringNullable("202010"),
		IdStatus:               helper.IntToInt64Nullable(1),
		StatusAkuisisiSimpanan: helper.StringToStringNullable("Sudah"),
		StatusAkuisisiPinjaman: helper.StringToStringNullable("Sudah"),
		VolumePotensiPinjaman:  helper.FloatToFloatNullable(30000),
		VolumePotensiSimpanan:  helper.FloatToFloatNullable(30000),

		Id:                      helper.IntToInt64Nullable(1),
		Cifno:                   helper.StringToStringNullable("VCB656"),
		KodeNonBri:              helper.StringToStringNullable("BRINON666"),
		TipeCustomer:            helper.StringToStringNullable("BRI"),
		StatusHubungi:           helper.StringToStringNullable("Sudah Dihubungi"),
		KodeHubungi:             helper.IntToInt64Nullable(1),
		TglHubungi:              helper.DateTimeToDateTimeNUllable(helper.StringToDateTimeNullable("2021-01-02")),
		CatatanHubungi:          helper.StringToStringNullable("catatan"),
		StatusKetertarikan:      helper.StringToStringNullable("Tertarik"),
		KodeKetertarikan:        helper.IntToInt64Nullable(1),
		CatatanKetertarikan:     helper.StringToStringNullable("catatan"),
		StatusKunjungan:         helper.StringToStringNullable("Sudah Dikunjungi"),
		KodeKunjungan:           helper.IntToInt64Nullable(1),
		TglKunjungan:            helper.DateTimeToDateTimeNUllable(helper.StringToDateTimeNullable("2021-01-02")),
		CatatanKunjungan:        helper.StringToStringNullable("catatab"),
		CreatedDate:             helper.DateTimeToDateTimeNUllable(helper.StringToDateTimeNullable("2021-01-02")),
		UpdatedDate:             helper.DateTimeToDateTimeNUllable(helper.StringToDateTimeNullable("2021-01-02")),
		IdStatusTabelNon:        helper.IntToInt64Nullable(1),
		VolumePotensiRmPinjaman: helper.FloatToFloatNullable(30000),
		VolumePotensiRmSimpanan: helper.FloatToFloatNullable(30000),
	}
	return res
}

func (a *GetDetailQueryStruct) MappingExampleDataTier5() *GetDetailQueryStruct {
	res := &GetDetailQueryStruct{
		CifNasabahRekan:        helper.StringToStringNullable("ABC1234"),
		NamaNasabahRekan:       helper.StringToStringNullable("Anto"),
		NamaNasabahUtama:       helper.StringToStringNullable("Budi"),
		PosisiTier:             helper.IntToInt64Nullable(5),
		Usia:                   helper.IntToInt64Nullable(40),
		NomorKtp:               helper.StringToStringNullable("343323232323"),
		PnRm:                   helper.StringToStringNullable("00999950"),
		JenisKelamin:           helper.StringToStringNullable("Laki-Laki"),
		SektorEkonomi:          helper.StringToStringNullable("Pedagang"),
		TierDiatasnya:          helper.StringToStringNullable("Charlie"),
		RmDiatasnya:            helper.StringToStringNullable("Donovan"),
		SegmenPinjaman:         helper.StringToStringNullable("Evan"),
		Branch:                 helper.StringToStringNullable("Kecil"),
		Rgdesc:                 helper.StringToStringNullable("345"),
		Mbdesc:                 helper.StringToStringNullable("DKI2"),
		Sbdesc:                 helper.StringToStringNullable("KC Jakarta"),
		Brdesc:                 helper.StringToStringNullable("Unit Jakarta"),
		Phonenumber:            helper.StringToStringNullable("Unit Jakarta"),
		AlamatId:               helper.StringToStringNullable("Jalan Bendungan 1"),
		AlamatDomisili:         helper.StringToStringNullable("Jalan Bendungan 2"),
		AlamatPinjaman:         helper.StringToStringNullable("Jalan Bendungan 3"),
		UsiaRekeningBulan:      helper.StringToStringNullable("30"),
		ProdukSimpanan:         helper.StringToStringNullable("BRITAMA"),
		SnameRmSimpanan:        helper.StringToStringNullable("Fany"),
		ProdukPinjaman:         helper.StringToStringNullable("Kupedes"),
		SnameRmPinjaman:        helper.StringToStringNullable("Gigih"),
		Segmen:                 helper.StringToStringNullable("Mikro"),
		PhonenumberRmPinjaman:  helper.StringToStringNullable("085656565656"),
		GroupUsaha:             helper.StringToStringNullable("Pedagang"),
		JenisPekerjaan:         helper.StringToStringNullable("Pedagang"),
		BidangPekerjaan:        helper.StringToStringNullable("Pedagang"),
		Kolektabilitas:         helper.StringToStringNullable("Lancar"),
		TotalPlafon:            helper.FloatToFloatNullable(30000),
		TotalOutstanding:       helper.FloatToFloatNullable(30000),
		InetBanking:            helper.StringToStringNullable("Ya"),
		SmsBanking:             helper.StringToStringNullable("Ya"),
		TotalCreditBri:         helper.FloatToFloatNullable(30000),
		TotalDebitBri:          helper.FloatToFloatNullable(30000),
		TotalKreditNonBri:      helper.FloatToFloatNullable(30000),
		TotalDebitNonBri:       helper.FloatToFloatNullable(30000),
		TotalKreditDebitNonBri: helper.FloatToFloatNullable(30000),
		NamaNasabahNonBri:      helper.StringToStringNullable("Hasan"),
		NamaBankLawan:          helper.StringToStringNullable("BNI"),
		RatasSaldoSimpananBri:  helper.FloatToFloatNullable(30000),
		TotalCreditKeseluruhan: helper.FloatToFloatNullable(30000),
		TotalDebitKeseluruhan:  helper.FloatToFloatNullable(30000),
		CifNasabahTier1:        helper.StringToStringNullable("ABC2345"),
		NamaNasabahTier1:       helper.StringToStringNullable("Ilham"),
		SnameRmPinjamanTier1:   helper.StringToStringNullable("Jono"),
		UnitRmTier1:            helper.StringToStringNullable("KC Jakarta"),
		PhonenumberRmTier1:     helper.StringToStringNullable("085656565656"),
		CifNasabahTier2:        helper.StringToStringNullable("ABC2345"),
		NamaNasabahTier2:       helper.StringToStringNullable("Ilham"),
		SnameRmPinjamanTier2:   helper.StringToStringNullable("Jono"),
		UnitRmTier2:            helper.StringToStringNullable("KC Jakarta"),
		PhonenumberTier2:       helper.StringToStringNullable("085656565656"),
		CifNasabahTier3:        helper.StringToStringNullable("ABC2345"),
		NamaNasabahTier3:       helper.StringToStringNullable("Ilham"),
		SnameRmPinjamanTier3:   helper.StringToStringNullable("Jono"),
		UnitRmTier3:            helper.StringToStringNullable("KC Jakarta"),
		PhonenumberTier3:       helper.StringToStringNullable("085656565656"),
		CifNasabahTier4:        helper.StringToStringNullable("ABC2345"),
		NamaNasabahTier4:       helper.StringToStringNullable("Ilham"),
		SnameRmPinjamanTier4:   helper.StringToStringNullable("Jono"),
		UnitRmTier4:            helper.StringToStringNullable("KC Jakarta"),
		PhonenumberTier4:       helper.StringToStringNullable("085656565656"),
		StatusRekening:         helper.StringToStringNullable("Aktif"),
		AkuisisiSimpanan:       helper.StringToStringNullable("1"),
		AkuisisiPinjaman:       helper.StringToStringNullable("1"),
		LamaNasabahBulan:       helper.StringToStringNullable("30"),
		TanggalLahir:           helper.StringToStringNullable("12 Januari 1990"),
		TotalMutasiDebit:       helper.FloatToFloatNullable(30000),
		TotalMutasiKredit:      helper.FloatToFloatNullable(30000),
		Ds:                     helper.StringToStringNullable("202010"),
		IdStatus:               helper.IntToInt64Nullable(1),
		StatusAkuisisiSimpanan: helper.StringToStringNullable("Sudah"),
		StatusAkuisisiPinjaman: helper.StringToStringNullable("Sudah"),
		VolumePotensiPinjaman:  helper.FloatToFloatNullable(30000),
		VolumePotensiSimpanan:  helper.FloatToFloatNullable(30000),

		Id:                      helper.IntToInt64Nullable(1),
		Cifno:                   helper.StringToStringNullable("VCB656"),
		KodeNonBri:              helper.StringToStringNullable("BRINON666"),
		TipeCustomer:            helper.StringToStringNullable("BRI"),
		StatusHubungi:           helper.StringToStringNullable("Sudah Dihubungi"),
		KodeHubungi:             helper.IntToInt64Nullable(1),
		TglHubungi:              helper.DateTimeToDateTimeNUllable(helper.StringToDateTimeNullable("2021-01-02")),
		CatatanHubungi:          helper.StringToStringNullable("catatan"),
		StatusKetertarikan:      helper.StringToStringNullable("Tertarik"),
		KodeKetertarikan:        helper.IntToInt64Nullable(1),
		CatatanKetertarikan:     helper.StringToStringNullable("catatan"),
		StatusKunjungan:         helper.StringToStringNullable("Sudah Dikunjungi"),
		KodeKunjungan:           helper.IntToInt64Nullable(1),
		TglKunjungan:            helper.DateTimeToDateTimeNUllable(helper.StringToDateTimeNullable("2021-01-02")),
		CatatanKunjungan:        helper.StringToStringNullable("catatab"),
		CreatedDate:             helper.DateTimeToDateTimeNUllable(helper.StringToDateTimeNullable("2021-01-02")),
		UpdatedDate:             helper.DateTimeToDateTimeNUllable(helper.StringToDateTimeNullable("2021-01-02")),
		IdStatusTabelNon:        helper.IntToInt64Nullable(1),
		VolumePotensiRmPinjaman: helper.FloatToFloatNullable(30000),
		VolumePotensiRmSimpanan: helper.FloatToFloatNullable(30000),
	}
	return res
}

func (a *ParamRequestListNasabah) MappingExampleData() *ParamRequestListNasabah {
	res := &ParamRequestListNasabah{
		Offset:      0,
		Limit:       5,
		Page:        1,
		Search:      "a",
		PnPengelola: "00999950",
		Role:        "RM",
	}
	return res
}

func (a *GetListQueryStruct) MappingExampleData() GetListQueryStruct {
	res := GetListQueryStruct{
		CifNasabahRekan:        helper.StringToStringNullable("ABC2345"),
		KeteranganInformasi:    helper.StringToStringNullable("CIF"),
		NamaNasabahRekan:       helper.StringToStringNullable("Anton"),
		Phonenumber:            helper.StringToStringNullable("085656565656"),
		GroupUsaha:             helper.StringToStringNullable("Perdagangan"),
		SektorEkonomi:          helper.StringToStringNullable("Perdagangan"),
		RatasSaldoSimpananBri:  helper.FloatToFloatNullable(5000000),
		TotalKreditNonBri:      helper.FloatToFloatNullable(5000000),
		TotalDebitNonBri:       helper.FloatToFloatNullable(5000000),
		PnRm:                   helper.StringToStringNullable("00999950"),
		TotalCreditKeseluruhan: helper.FloatToFloatNullable(5000000),
		TotalDebitKeseluruhan:  helper.FloatToFloatNullable(5000000),

		KodeHubungi:        helper.IntToInt64Nullable(1),
		KodeKunjungan:      helper.IntToInt64Nullable(1),
		KodeKetertarikan:   helper.IntToInt64Nullable(1),
		StatusHubungi:      helper.StringToStringNullable("Sudah Dihubungi"),
		StatusKunjungan:    helper.StringToStringNullable("Sudah Dikunjungi"),
		StatusKetertarikan: helper.StringToStringNullable("Tertarik"),

		CifNasabahRekanTabelNon:   helper.StringToStringNullable("ABC2345"),
		NamaNasabahRekanTabelNon:  helper.StringToStringNullable("Anton"),
		NamaBankLawan:             helper.StringToStringNullable("BNI"),
		NamaNasabahNonBRI:         helper.StringToStringNullable("Budi"),
		TotalDebitNonBRITabelNon:  helper.FloatToFloatNullable(5000000),
		TotalCreditNonBRITabelNon: helper.FloatToFloatNullable(5000000),
		TipeCustomer:              helper.StringToStringNullable("NONBRI"),
		KodeNonBri:                helper.StringToStringNullable("BRINON3434"),
		AkuisisiSimpananTabelNon:  helper.StringToStringNullable("1"),
	}
	return res
}

func (a *TanggalUpdateStruct) MappingExampleData() *TanggalUpdateStruct {
	res := &TanggalUpdateStruct{

		TanggalUpdate: helper.DateTimeToDateTimeNUllable(time.Now()),
	}
	return res
}

func (a *RequestListNasabahTop20) MappingExampleData() *RequestListNasabahTop20 {
	res := &RequestListNasabahTop20{
		Page:   "1",
		Limit:  "20",
		Search: "",
	}
	return res
}

func (a *RequestListNasabahTop20) MappingExampleDataBadRequest() *RequestListNasabahTop20 {
	res := &RequestListNasabahTop20{
		Page:   "a",
		Limit:  "5",
		Search: "",
	}
	return res
}
