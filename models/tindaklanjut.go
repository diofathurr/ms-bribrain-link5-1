package models

import (
	"ms-bribrain-link5/helper"
	"time"
)

type QuerySubmitTindaklanjut struct {
	Cifno                      *string    `json:"cifno"`
	KodeHubungi                *string    `json:"kode_hubungi"`
	KodeAlasanHubungi          *string    `json:"kode_alasan_hubungi"`
	KodeKunjungan              *string    `json:"kode_kunjungan"`
	KodeKetertarikan           *string    `json:"kode_ketertarikan"`
	TglHubungi                 *string    `json:"tanggal_hubungi"`
	TglKunjungan               *string    `json:"tanggal_kunjungan"`
	StatusHubungi              *string    `json:"status_hubungi"`
	StatusKunjungan            *string    `json:"status_kunjungan"`
	StatusKetertarikan         *string    `json:"status_ketertarikan"`
	CatatanHubungi             *string    `json:"catatan_hubungi"`
	CatatanKunjungan           *string    `json:"catatan_kunjungan"`
	CatatanKetertarikan        *string    `json:"catatan_ketertarikan"`
	CreatedDate                *time.Time `json:"created_date"`
	UpdateDate                 *time.Time `json:"update_date"`
	TipeCustomer               *string    `json:"tipe_customer"`
	IdStatus                   *int64     `json:"id_status"`
	IdStatusTabelNon           *int64     `json:"id_status_tabel_non"`
	KodeNonBri                 *string    `json:"kode_non_bri"`
	VolumePotensiRmPinjaman    *string    `json:"volume_potensi_rm_pinjaman"`
	VolumePotensiRmSimpanan    *string    `json:"volume_potensi_rm_simpanan"`
	InputPotensiRmPinjaman     *float64   `json:"input_potensi_rm_pinjaman"`
	InputPotensiRmSimpanan     *float64   `json:"input_potensi_rm_simpanan"`
	StatusTindaklanjut         *float64   `json:"status_tindaklanjut"`
	StatusTindaklanjutTabelNon *float64   `json:"status_tindaklanjut_tabel_non"`
	InputNorek                 *string    `json:"input_norek"`
	InputPhonenumber           *string    `json:"input_phonenumber"`
	InputAlamat                *string    `json:"input_alamat"`
	KodeRegion                 *string    `json:"kode_region"`
	Rgdesc                     *string    `json:"rgdesc"`
	KodeMainbranch             *string    `json:"kode_mainbranch"`
	Mbdesc                     *string    `json:"mbdesc"`
	PnRm                       *string    `json:"pn_rm"`
}

type QueryUpdateStatusNasabah struct {
	InputPhonenumber *string `json:"input_phonenumber"`
	InputAlamat      *string `json:"input_alamat"`
	KodeRegion       *string `json:"kode_region"`
	Rgdesc           *string `json:"rgdesc"`
	KodeMainbranch   *string `json:"kode_mainbranch"`
	Mbdesc           *string `json:"mbdesc"`
	PnRm             *string `json:"pn_rm"`
	NamaRm           *string `json:"nama_rm"`
	BranchRm         *string `json:"branch_rm"`
	PhonenumberRm    *string `json:"phonenumber_rm"`
}

type RequestSubmitTindaklanjutPositive1 struct {
	Cif                     *string `json:"cif" validate:"required,alphanum"`
	KodeHubungi             *string `json:"kode_hubungi" validate:"required,eq=1"`
	KodeKunjungan           *string `json:"kode_kunjungan" validate:"required,eq=1"`
	KodeKetertarikan        *string `json:"kode_ketertarikan" validate:"required,eq=1"`
	TglHubungi              *string `json:"tanggal_hubungi" validate:"required,datetime=2006-01-02"`
	TglKunjungan            *string `json:"tanggal_kunjungan" validate:"required,datetime=2006-01-02"`
	VolumePotensiRmSimpanan *string `json:"volume_potensi_rm_simpanan" validate:"omitempty,numeric,max=12"`
	VolumePotensiRmPinjaman *string `json:"volume_potensi_rm_pinjaman" validate:"omitempty,numeric,max=12"`
	// CatatanKetertarikan *string `json:"catatan_ketertarikan"`
}

func MappingToQuerySubmitTindaklanjut(data *RequestSubmitTindaklanjutPositive1) *QuerySubmitTindaklanjut {
	statusHubungi, statusKunjungan, statusKetertarikan := mappingKodeHubungiToStatus(helper.StringNullableToString(data.KodeHubungi), helper.StringNullableToString(data.KodeKunjungan), helper.StringNullableToString(data.KodeKetertarikan))
	inputPotensiSimpanan, inputPotensiPinjaman := mappingInputPotensi(data.VolumePotensiRmSimpanan, data.VolumePotensiRmPinjaman)

	res := &QuerySubmitTindaklanjut{
		Cifno:                   data.Cif,
		KodeHubungi:             data.KodeHubungi,
		KodeKunjungan:           data.KodeKunjungan,
		KodeKetertarikan:        data.KodeKetertarikan,
		TglHubungi:              data.TglHubungi,
		TglKunjungan:            data.TglKunjungan,
		VolumePotensiRmSimpanan: data.VolumePotensiRmSimpanan,
		VolumePotensiRmPinjaman: data.VolumePotensiRmPinjaman,
		CreatedDate:             helper.DateTimeToDateTimeNUllable(time.Now()),
		UpdateDate:              helper.DateTimeToDateTimeNUllable(time.Now()),
		StatusHubungi:           helper.StringToStringNullable(statusHubungi),
		StatusKunjungan:         helper.StringToStringNullable(statusKunjungan),
		StatusKetertarikan:      helper.StringToStringNullable(statusKetertarikan),
		TipeCustomer:            helper.StringToStringNullable("BRI"),
		InputPotensiRmSimpanan:  inputPotensiSimpanan,
		InputPotensiRmPinjaman:  inputPotensiPinjaman,
	}

	return res
}

func mappingInputPotensi(volumePotensiSimpanan, volumePotensiPinjaman *string) (inputPotensiSimpanan, inputPotensiPinjaman *float64) {
	if volumePotensiPinjaman == nil && volumePotensiSimpanan != nil {
		inputPotensiPinjaman = helper.FloatToFloatNullable(0)
		inputPotensiSimpanan = helper.FloatToFloatNullable(1)
	} else if volumePotensiPinjaman != nil && volumePotensiSimpanan == nil {
		inputPotensiPinjaman = helper.FloatToFloatNullable(1)
		inputPotensiSimpanan = helper.FloatToFloatNullable(0)
	} else if volumePotensiPinjaman != nil && volumePotensiSimpanan != nil {
		inputPotensiPinjaman = helper.FloatToFloatNullable(1)
		inputPotensiSimpanan = helper.FloatToFloatNullable(1)
	} else {
		inputPotensiPinjaman = helper.FloatToFloatNullable(0)
		inputPotensiSimpanan = helper.FloatToFloatNullable(0)
	}

	return inputPotensiSimpanan, inputPotensiPinjaman
}

func mappingKodeHubungiToStatus(kodeHubungi string, kodeKunjungan string, kodeKetertarikan string) (statusHubungi string, statusKunjungan string, statusKetertarikan string) {
	switch kodeHubungi {
	case "1":
		statusHubungi = "Sudah Dihubungi"
	case "2":
		statusHubungi = "Tidak Dapat Dihubungi"
	case "3":
		statusHubungi = "Tidak Cocok"
	}

	switch kodeKunjungan {
	case "1":
		statusKunjungan = "Akan Dikunjungi"
	case "2":
		statusKunjungan = "Tidak Akan Dikunjungi"
	case "3":
		statusKunjungan = "Sudah Dikunjungi"
	}

	switch kodeKetertarikan {
	case "1":
		statusKetertarikan = "Tertarik"
	case "2":
		statusKetertarikan = "Tidak Tertarik"
	case "3":
		statusKetertarikan = "Pending"
	}

	return statusHubungi, statusKunjungan, statusKetertarikan
}

type RequestSubmitTindaklanjutPositive2 struct {
	Cif                     *string `json:"cif" validate:"required,alphanum"`
	KodeHubungi             *string `json:"kode_hubungi" validate:"required,eq=1"`
	KodeKunjungan           *string `json:"kode_kunjungan" validate:"required,eq=2"`
	KodeKetertarikan        *string `json:"kode_ketertarikan" validate:"required,eq=1"`
	TglHubungi              *string `json:"tanggal_hubungi" validate:"required,datetime=2006-01-02"`
	VolumePotensiRmSimpanan *string `json:"volume_potensi_rm_simpanan" validate:"omitempty,numeric,max=12"`
	VolumePotensiRmPinjaman *string `json:"volume_potensi_rm_pinjaman" validate:"omitempty,numeric,max=12"`
	CatatanKunjungan        *string `json:"catatan_kunjungan" validate:"required,min=1"`
	// CatatanKetertarikan *string `json:"catatan_ketertarikan" validate:"required"`
}

func MappingToQuerySubmitTindaklanjutPositive2(data *RequestSubmitTindaklanjutPositive2) *QuerySubmitTindaklanjut {
	statusHubungi, statusKunjungan, statusKetertarikan := mappingKodeHubungiToStatus(helper.StringNullableToString(data.KodeHubungi), helper.StringNullableToString(data.KodeKunjungan), helper.StringNullableToString(data.KodeKetertarikan))
	inputPotensiSimpanan, inputPotensiPinjaman := mappingInputPotensi(data.VolumePotensiRmSimpanan, data.VolumePotensiRmPinjaman)
	res := &QuerySubmitTindaklanjut{
		Cifno:                   data.Cif,
		KodeHubungi:             data.KodeHubungi,
		KodeKunjungan:           data.KodeKunjungan,
		KodeKetertarikan:        data.KodeKetertarikan,
		TglHubungi:              data.TglHubungi,
		VolumePotensiRmSimpanan: data.VolumePotensiRmSimpanan,
		VolumePotensiRmPinjaman: data.VolumePotensiRmPinjaman,
		CatatanKunjungan:        data.CatatanKunjungan,
		CreatedDate:             helper.DateTimeToDateTimeNUllable(time.Now()),
		UpdateDate:              helper.DateTimeToDateTimeNUllable(time.Now()),
		StatusHubungi:           helper.StringToStringNullable(statusHubungi),
		StatusKunjungan:         helper.StringToStringNullable(statusKunjungan),
		StatusKetertarikan:      helper.StringToStringNullable(statusKetertarikan),
		TipeCustomer:            helper.StringToStringNullable("BRI"),
		InputPotensiRmSimpanan:  inputPotensiSimpanan,
		InputPotensiRmPinjaman:  inputPotensiPinjaman,
	}

	return res
}

type RequestSubmitTindaklanjutNegative1 struct {
	Cif                 *string `json:"cif" validate:"required,alphanum"`
	KodeHubungi         *string `json:"kode_hubungi" validate:"required,eq=1"`
	KodeKetertarikan    *string `json:"kode_ketertarikan" validate:"required,eq=2"`
	TglHubungi          *string `json:"tanggal_hubungi" validate:"required,datetime=2006-01-02"`
	CatatanKetertarikan *string `json:"catatan_ketertarikan" validate:"required,min=1"`
}

func MappingToQuerySubmitTindaklanjutNegative1(data *RequestSubmitTindaklanjutNegative1) *QuerySubmitTindaklanjut {
	statusHubungi, _, statusKetertarikan := mappingKodeHubungiToStatus(helper.StringNullableToString(data.KodeHubungi), helper.StringNullableToString(nil), helper.StringNullableToString(data.KodeKetertarikan))

	res := &QuerySubmitTindaklanjut{
		Cifno:               data.Cif,
		KodeHubungi:         data.KodeHubungi,
		KodeKetertarikan:    data.KodeKetertarikan,
		TglHubungi:          data.TglHubungi,
		CatatanKetertarikan: data.CatatanKetertarikan,
		CreatedDate:         helper.DateTimeToDateTimeNUllable(time.Now()),
		UpdateDate:          helper.DateTimeToDateTimeNUllable(time.Now()),
		StatusHubungi:       helper.StringToStringNullable(statusHubungi),
		StatusKetertarikan:  helper.StringToStringNullable(statusKetertarikan),
		TipeCustomer:        helper.StringToStringNullable("BRI"),
	}

	return res
}

type RequestSubmitTindaklanjutNegative2 struct {
	Cif              *string `json:"cif" validate:"required,alphanum"`
	KodeHubungi      *string `json:"kode_hubungi" validate:"required,eq=2"`
	KodeKunjungan    *string `json:"kode_kunjungan" validate:"required,eq=2"`
	CatatanHubungi   *string `json:"catatan_hubungi" validate:"required,min=1"`
	CatatanKunjungan *string `json:"catatan_kunjungan" validate:"required,min=1"`
}

func MappingToQuerySubmitTindaklanjutNegative2(data *RequestSubmitTindaklanjutNegative2) *QuerySubmitTindaklanjut {
	statusHubungi, statusKunjungan, _ := mappingKodeHubungiToStatus(helper.StringNullableToString(data.KodeHubungi), helper.StringNullableToString(data.KodeKunjungan), helper.StringNullableToString(nil))

	res := &QuerySubmitTindaklanjut{
		Cifno:            data.Cif,
		KodeHubungi:      data.KodeHubungi,
		KodeKunjungan:    data.KodeKunjungan,
		CatatanHubungi:   data.CatatanHubungi,
		CatatanKunjungan: data.CatatanKunjungan,
		CreatedDate:      helper.DateTimeToDateTimeNUllable(time.Now()),
		UpdateDate:       helper.DateTimeToDateTimeNUllable(time.Now()),
		StatusHubungi:    helper.StringToStringNullable(statusHubungi),
		StatusKunjungan:  helper.StringToStringNullable(statusKunjungan),
		TipeCustomer:     helper.StringToStringNullable("BRI"),
	}

	return res
}

type RequestSubmitTindaklanjutNegative3 struct {
	Cif            *string `json:"cif" validate:"required,alphanum"`
	KodeHubungi    *string `json:"kode_hubungi" validate:"required,eq=3"`
	CatatanHubungi *string `json:"catatan_hubungi" validate:"required,min=1"`
}

func MappingToQuerySubmitTindaklanjutNegative3(data *RequestSubmitTindaklanjutNegative3) *QuerySubmitTindaklanjut {
	statusHubungi, _, _ := mappingKodeHubungiToStatus(helper.StringNullableToString(data.KodeHubungi), helper.StringNullableToString(nil), helper.StringNullableToString(nil))

	res := &QuerySubmitTindaklanjut{
		Cifno:          data.Cif,
		KodeHubungi:    data.KodeHubungi,
		CatatanHubungi: data.CatatanHubungi,
		CreatedDate:    helper.DateTimeToDateTimeNUllable(time.Now()),
		UpdateDate:     helper.DateTimeToDateTimeNUllable(time.Now()),
		StatusHubungi:  helper.StringToStringNullable(statusHubungi),
		TipeCustomer:   helper.StringToStringNullable("BRI"),
	}

	return res
}

type RequestSubmitTindaklanjutOther struct {
	Cif              *string `json:"cif" validate:"required,alphanum"`
	KodeHubungi      *string `json:"kode_hubungi" validate:"required,eq=2"`
	KodeKunjungan    *string `json:"kode_kunjungan" validate:"required,eq=1"`
	KodeKetertarikan *string `json:"kode_ketertarikan" validate:"required,eq=3"`
	CatatanHubungi   *string `json:"catatan_hubungi" validate:"required,min=1"`
	TglKunjungan     *string `json:"tanggal_kunjungan" validate:"required,datetime=2006-01-02"`
}

func MappingToQuerySubmitTindaklanjutOther(data *RequestSubmitTindaklanjutOther) *QuerySubmitTindaklanjut {
	statusHubungi, statusKunjungan, statusKetertarikan := mappingKodeHubungiToStatus(helper.StringNullableToString(data.KodeHubungi), helper.StringNullableToString(data.KodeKunjungan), helper.StringNullableToString(data.KodeKetertarikan))

	res := &QuerySubmitTindaklanjut{
		Cifno:              data.Cif,
		KodeHubungi:        data.KodeHubungi,
		KodeKunjungan:      data.KodeKunjungan,
		KodeKetertarikan:   data.KodeKetertarikan,
		CatatanHubungi:     data.CatatanHubungi,
		TglKunjungan:       data.TglKunjungan,
		CreatedDate:        helper.DateTimeToDateTimeNUllable(time.Now()),
		UpdateDate:         helper.DateTimeToDateTimeNUllable(time.Now()),
		StatusHubungi:      helper.StringToStringNullable(statusHubungi),
		StatusKunjungan:    helper.StringToStringNullable(statusKunjungan),
		StatusKetertarikan: helper.StringToStringNullable(statusKetertarikan),
		TipeCustomer:       helper.StringToStringNullable("BRI"),
	}

	return res
}

type RequestSubmitTindaklanjutTandai struct {
	Cif                     *string `json:"cif" validate:"required,alphanum"`
	KodeKunjungan           *string `json:"kode_kunjungan" validate:"required,eq=3"`
	KodeKetertarikan        *string `json:"kode_ketertarikan" validate:"required,oneof=1 2"`
	VolumePotensiRmSimpanan *string `json:"volume_potensi_rm_simpanan" validate:"required_without_all=VolumePotensiRmPinjaman CatatanKetertarikan,omitempty,required_if=KodeKetertarikan 1,numeric,max=12"`
	VolumePotensiRmPinjaman *string `json:"volume_potensi_rm_pinjaman" validate:"required_without_all=VolumePotensiRmSimpanan CatatanKetertarikan,omitempty,required_if=KodeKetertarikan 1,numeric,max=12"`
	CatatanKetertarikan     string  `json:"catatan_ketertarikan" validate:"required_if=KodeKetertarikan 2,omitempty,min=1"`
	// VolumePotensiRm     *string `json:"volume_potensi" validate:"required_if=KodeKetertarikan 1,numeric"`
}

func MappingToQuerySubmitTindaklanjutTandai(data *RequestSubmitTindaklanjutTandai) *QuerySubmitTindaklanjut {
	_, statusKunjungan, statusKetertarikan := mappingKodeHubungiToStatus(helper.StringNullableToString(nil), helper.StringNullableToString(data.KodeKunjungan), helper.StringNullableToString(data.KodeKetertarikan))
	// _, statusKunjungan, _ := mappingKodeHubungiToStatus(helper.StringNullableToString(nil), helper.StringNullableToString(data.KodeKunjungan), helper.StringNullableToString(nil))
	inputPotensiSimpanan, inputPotensiPinjaman := mappingInputPotensi(data.VolumePotensiRmSimpanan, data.VolumePotensiRmPinjaman)

	// var statusKetertarikanString string
	// if data.KodeKetertarikan == "1" {
	// 	statusKetertarikanString = "Tertarik"
	// } else if data.KodeKetertarikan == "2" {
	// 	statusKetertarikanString = "Tidak Tertarik"
	// }

	res := &QuerySubmitTindaklanjut{
		Cifno:         data.Cif,
		KodeKunjungan: data.KodeKunjungan,
		// KodeKetertarikan: helper.StringToStringNullable(data.KodeKetertarikan),
		KodeKetertarikan:        data.KodeKetertarikan,
		VolumePotensiRmSimpanan: data.VolumePotensiRmSimpanan,
		VolumePotensiRmPinjaman: data.VolumePotensiRmPinjaman,
		CatatanKetertarikan:     helper.StringToStringNullable(data.CatatanKetertarikan),
		// CatatanKetertarikan:     data.CatatanKetertarikan,
		TglKunjungan:    helper.DateTimeToStringNullable(time.Now()),
		CreatedDate:     helper.DateTimeToDateTimeNUllable(time.Now()),
		UpdateDate:      helper.DateTimeToDateTimeNUllable(time.Now()),
		StatusKunjungan: helper.StringToStringNullable(statusKunjungan),
		// StatusKetertarikan: helper.StringToStringNullable(statusKetertarikanString),
		StatusKetertarikan:     helper.StringToStringNullable(statusKetertarikan),
		TipeCustomer:           helper.StringToStringNullable("BRI"),
		InputPotensiRmSimpanan: inputPotensiSimpanan,
		InputPotensiRmPinjaman: inputPotensiPinjaman,
	}

	return res
}

type QueryGetInfoRM struct {
	KodeRegion     *string `json:"kode_region"  gorm:"column:KODE_REGION"`
	Rgdesc         *string `json:"rgdesc"  gorm:"column:RGDESC"`
	KodeMainbranch *string `json:"kode_mainbranch"  gorm:"column:KODE_MAINBRANCH"`
	Mbdesc         *string `json:"mbdesc"  gorm:"column:MBDESC"`
	PnRm           *string `json:"pn_rm"  gorm:"column:PN_RM"`
	NamaRm         *string `json:"nama_rm" gorm:"column:NAMA_RM"`
	BranchRm       *string `json:"branch_rm" gorm:"column:BRANCH_RM"`
	PhonenumberRm  *string `json:"phonenumber_rm" gorm:"column:PHONENUMBER_RM"`
}

type RequestSubmitTindaklanjutNonBri struct {
	KodeNonBri        *string `json:"kode_non_bri" validate:"required,alphanum"`
	KodeHubungi       string  `json:"kode_hubungi" validate:"required,oneof=1 2 3"`
	TglHubungi        string  `json:"tanggal_hubungi" validate:"required_if=KodeHubungi 1,omitempty,datetime=2006-01-02"`
	CatatanHubungi    string  `json:"catatan_hubungi" validate:"required_unless=KodeHubungi 1,omitempty,min=1,max=150"`
	NoTelepon         string  `json:"no_telepon" validate:"required_if=KodeHubungi 1,omitempty,numeric,max=14"`
	Alamat            string  `json:"alamat" validate:"required_if=KodeHubungi 1,omitempty,max=255"`
	KodeCabangReferal string  `json:"kode_cabang_referal" validate:"omitempty,numeric"`
}

func MappingToQuerySubmitTindaklanjutNonBri(data *RequestSubmitTindaklanjutNonBri) *QuerySubmitTindaklanjut {
	// statusHubungi, _, _ := mappingKodeHubungiToStatus(helper.StringNullableToString(data.KodeHubungi), helper.StringNullableToString(nil), helper.StringNullableToString(nil))
	var statusHubungi string
	if data.KodeHubungi == "1" {
		statusHubungi = "Sudah Dihubungi"
	} else if data.KodeHubungi == "2" {
		statusHubungi = "Tidak Dapat Dihubungi"
	} else if data.KodeHubungi == "3" {
		statusHubungi = "Tidak Cocok"
	}
	// res := &QuerySubmitTindaklanjut{
	// 	KodeNonBri:     data.KodeNonBri,
	// 	KodeHubungi:    data.KodeHubungi,
	// 	TglHubungi:     data.TglHubungi,
	// 	CatatanHubungi: data.CatatanHubungi,
	// 	CreatedDate:    helper.DateTimeToDateTimeNUllable(time.Now()),
	// 	UpdateDate:     helper.DateTimeToDateTimeNUllable(time.Now()),
	// 	StatusHubungi:  helper.StringToStringNullable(statusHubungi),
	// 	TipeCustomer:   helper.StringToStringNullable("NON BRI"),
	// }

	res := &QuerySubmitTindaklanjut{
		KodeNonBri:       data.KodeNonBri,
		KodeHubungi:      helper.StringToStringNullable(data.KodeHubungi),
		TglHubungi:       helper.StringToStringNullable(data.TglHubungi),
		CatatanHubungi:   helper.StringToStringNullable(data.CatatanHubungi),
		CreatedDate:      helper.DateTimeToDateTimeNUllable(time.Now()),
		UpdateDate:       helper.DateTimeToDateTimeNUllable(time.Now()),
		StatusHubungi:    helper.StringToStringNullable(statusHubungi),
		TipeCustomer:     helper.StringToStringNullable("NON BRI"),
		InputPhonenumber: helper.StringToStringNullable(data.NoTelepon),
		InputAlamat:      helper.StringToStringNullable(data.Alamat),
	}

	return res
}

func MappingToQueryUpdateStatusNasabahNonBri(data *RequestSubmitTindaklanjutNonBri) *QueryUpdateStatusNasabah {

	res := &QueryUpdateStatusNasabah{

		InputPhonenumber: helper.StringToStringNullable(data.NoTelepon),
		InputAlamat:      helper.StringToStringNullable(data.Alamat),
		KodeRegion:       helper.StringToStringNullable("tbd"),
		Rgdesc:           helper.StringToStringNullable("tbd"),
		KodeMainbranch:   helper.StringToStringNullable("tbd"),
		Mbdesc:           helper.StringToStringNullable("tbd"),
		PnRm:             helper.StringToStringNullable("tbd"),
	}

	return res
}

func MappingToQueryUpdateStatusReferalNasabahNonBri(dataRM *QueryGetInfoRM, dataNasabah *RequestSubmitTindaklanjutNonBri) *QueryUpdateStatusNasabah {

	res := &QueryUpdateStatusNasabah{

		InputPhonenumber: helper.StringToStringNullable(dataNasabah.NoTelepon),
		InputAlamat:      helper.StringToStringNullable(dataNasabah.Alamat),
		KodeRegion:       dataRM.KodeRegion,
		Rgdesc:           dataRM.Rgdesc,
		KodeMainbranch:   dataRM.KodeMainbranch,
		Mbdesc:           dataRM.Mbdesc,
		PnRm:             dataRM.PnRm,
		NamaRm:           dataRM.NamaRm,
		BranchRm:         dataRM.BranchRm,
		PhonenumberRm:    dataRM.PhonenumberRm,
	}

	return res
}

type RequestSubmitKetertarikanNonBri struct {
	KodeNonBri          *string `json:"kode_non_bri" validate:"required,alphanum"`
	KodeKetertarikan    string  `json:"kode_ketertarikan" validate:"required,oneof=1 2"`
	CatatanKetertarikan string  `json:"catatan_ketertarikan" validate:"required_unless=KodeKetertarikan 1,omitempty,min=1,max=150"`
	NomorRekening       string  `json:"nomor_rekening" validate:"required_if=KodeKetertarikan 1,omitempty,min=1,max=16"`
}

func MappingToQuerySubmitKetertarikanNonBri(data *RequestSubmitKetertarikanNonBri) *QuerySubmitTindaklanjut {

	var statusKetertarikan string
	if data.KodeKetertarikan == "1" {
		statusKetertarikan = "Tertarik"
	} else if data.KodeKetertarikan == "2" {
		statusKetertarikan = "Tidak Tertarik"
	}

	res := &QuerySubmitTindaklanjut{
		KodeNonBri:          data.KodeNonBri,
		KodeKetertarikan:    helper.StringToStringNullable(data.KodeKetertarikan),
		StatusKetertarikan:  helper.StringToStringNullable(statusKetertarikan),
		CatatanKetertarikan: helper.StringToStringNullable(data.CatatanKetertarikan),
		InputNorek:          helper.StringToStringNullable(data.NomorRekening),
		UpdateDate:          helper.DateTimeToDateTimeNUllable(time.Now()),
	}

	return res
}

type RequestFindRmInBranch struct {
	KodeCabangReferal string `json:"kode_cabang_referal" validate:"numeric"`
}
