package models

import (
	"ms-bribrain-link5/helper"
	"strings"
)

type OnboardingText struct {
	OnBoardingTitle       string `json:"onboarding_title" gorm:"column:onboarding_title"`
	OnBoardingDescription string `json:"onboarding_description" gorm:"column:onboarding_description"`
}

type RequestOnboardingText struct {
	// Data OnboardingText `json:"data"`
}

type PilihanAlasan struct {
	Kode      string `json:"id" gorm:"column:KODE"`
	Deskripsi string `json:"description" gorm:"column:DESKRIPSI"`
}

type RequestListCabang struct {
	// Search string `json:"search" validate:"min=1"`
	Search string `json:"search" validate:"allowempty|alphanum"`
}

type RequestListCabangPointer struct {
	// Search string `json:"search" validate:"min=1"`
	Search *string `json:"search" validate:"allowempty|excludesall=!@#$%^&*()+_{};:'<>?/"`
}

type ListCabang struct {
	KodeMainbranch string `json:"kode_mainbranch" gorm:"column:KODE_UNIT_KERJA"`
	Mainbranch     string `json:"mainbranch" gorm:"column:UNIT_KERJA"`
	Region         string `json:"region" gorm:"column:PARENT_NAME"`
}

type TermsAndConditionText struct {
	Title string `json:"title" gorm:"column:title"`
	Value string `json:"description" gorm:"column:description"`
}

type RequestListKotaKecamatanPointer struct {
	// Search string `json:"search" validate:"min=1"`
	Search *string `json:"search" validate:"allowempty|excludesall=!@#$%^&*()+_{};:'<>?/"`
}

type ResponseListKotaKecamatan struct {
	Provinsi      string `json:"provinsi"`
	KotaKecamatan string `json:"kota_kecamatan"`
	KodeKecamatan string `json:"kode_kecamatan"`
}

type QueryStructKotaKecamatan struct {
	Provinsi      string `json:"provinsi" gorm:"column:PROVINSI"`
	Kota          string `json:"kota" gorm:"column:KOTA"`
	Kecamatan     string `json:"kecamatan" gorm:"column:KECAMATAN"`
	KodeKecamatan int    `json:"kode_kecamatan" gorm:"column:KODE_KECAMATAN"`
}

func (data *QueryStructKotaKecamatan) MappingResponseListKotaKecamatan() ResponseListKotaKecamatan {

	kotaKecamatan := strings.Title(strings.ToLower(data.Kota)) + `, ` + strings.Title(strings.ToLower(data.Kecamatan))

	res := ResponseListKotaKecamatan{
		Provinsi:      strings.Title(strings.ToLower(data.Provinsi)),
		KotaKecamatan: kotaKecamatan,
		KodeKecamatan: helper.IntToString(data.KodeKecamatan),
	}
	return res
}

type RequestListKelurahan struct {
	KodeKecamatan string `json:"kode_kecamatan" validate:"numeric"`
}

type ResponseListKelurahan struct {
	Kelurahan       string `json:"kelurahan"`
	KotaKecamatan   string `json:"kota_kecamatan"`
	StringKelurahan string `json:"string_kelurahan"`
}

type QueryStructListKelurahan struct {
	Kelurahan string `json:"kelurahan" gorm:"column:KELURAHAN"`
	Kota      string `json:"kota" gorm:"column:KOTA"`
	Kecamatan string `json:"kecamatan" gorm:"column:KECAMATAN"`
}

func (data *QueryStructListKelurahan) MappingResponseListKelurahan() ResponseListKelurahan {

	kotaKecamatan := strings.Title(strings.ToLower(data.Kota)) + `, ` + strings.Title(strings.ToLower(data.Kecamatan))

	res := ResponseListKelurahan{
		Kelurahan:       "Kelurahan " + strings.Title(strings.ToLower(data.Kelurahan)),
		KotaKecamatan:   kotaKecamatan,
		StringKelurahan: strings.Title(strings.ToLower(data.Kelurahan)),
	}
	return res
}
