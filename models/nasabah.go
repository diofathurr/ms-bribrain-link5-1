package models

import (
	"fmt"
	"ms-bribrain-link5/helper"
	"strings"
	"time"
)

type RequestDetailNasabahEncrypted struct {
	Cifno string `json:"cif" validate:"required,alphanum"`
}

type GetDetailQueryStruct struct {
	CifNasabahRekan        *string  `json:"cif_nasabah_rekan" gorm:"column:CIF_NASABAH_REKAN"`
	NamaNasabahRekan       *string  `json:"nama_nasabah_rekan" gorm:"column:NAMA_NASABAH_REKAN"`
	NamaNasabahUtama       *string  `json:"nama_nasabah_utama" gorm:"column:NAMA_NASABAH_UTAMA"`
	PosisiTier             *int64   `json:"posisi_tier" gorm:"column:POSISI_TIER"`
	Usia                   *int64   `json:"usia" gorm:"column:USIA"`
	NomorKtp               *string  `json:"nomor_ktp" gorm:"column:NOMOR_KTP"`
	PnRm                   *string  `json:"pn_rm" gorm:"column:PN_RM"`
	JenisKelamin           *string  `json:"jenis_kelamin" gorm:"column:JENIS_KELAMIN"`
	SektorEkonomi          *string  `json:"sektor_ekonomi" gorm:"column:SEKTOR_EKONOMI"`
	TierDiatasnya          *string  `json:"tier_diatasnya" gorm:"column:TIER_DIATASNYA"`
	RmDiatasnya            *string  `json:"rm_diatasnya" gorm:"column:RM_DIATASNYA"`
	SegmenPinjaman         *string  `json:"segmen_pinjaman" gorm:"column:SEGMEN_PINJAMAN"`
	Branch                 *string  `json:"branch" gorm:"column:BRANCH"`
	Rgdesc                 *string  `json:"rgdesc" gorm:"column:RGDESC"`
	Mbdesc                 *string  `json:"mbdesc" gorm:"column:MBDESC"`
	Sbdesc                 *string  `json:"sbdesc" gorm:"column:SBDESC"`
	Brdesc                 *string  `json:"brdesc" gorm:"column:BRDESC"`
	Phonenumber            *string  `json:"phonenumber" gorm:"column:PHONENUMBER"`
	AlamatId               *string  `json:"alamat_id" gorm:"column:ALAMAT_ID"`
	AlamatDomisili         *string  `json:"alamat_domisili" gorm:"column:ALAMAT_DOMISILI"`
	AlamatPinjaman         *string  `json:"alamat_pinjaman" gorm:"column:ALAMAT_PINJAMAN"`
	UsiaRekeningBulan      *string  `json:"usia_rekening_bulan" gorm:"column:USIA_REKENING_BULAN"`
	ProdukSimpanan         *string  `json:"produk_simpanan" gorm:"column:PRODUK_SIMPANAN"`
	SnameRmSimpanan        *string  `json:"sname_rm_simpanan" gorm:"column:SNAME_RM_SIMPANAN"`
	ProdukPinjaman         *string  `json:"produk_pinjaman" gorm:"column:PRODUK_PINJAMAN"`
	SnameRmPinjaman        *string  `json:"sname_rm_pinjaman" gorm:"column:SNAME_RM_PINJAMAN"`
	UnitRmPinjaman         *string  `json:"unit_rm_pinjaman" gorm:"column:UNIT_RM_PINJAMAN"`
	FlagPinjaman           *int64   `json:"flag_pinjaman" gorm:"column:FLAG_PINJAMAN"`
	Segmen                 *string  `json:"segmen" gorm:"column:SEGMEN"`
	PhonenumberRmPinjaman  *string  `json:"phonenumber_rm_pinjaman" gorm:"column:PHONENUMBER_RM_PINJAMAN"`
	GroupUsaha             *string  `json:"group_usaha" gorm:"column:GROUP_USAHA"`
	JenisPekerjaan         *string  `json:"jenis_pekerjaan" gorm:"column:JENIS_PEKERJAAN"`
	BidangPekerjaan        *string  `json:"bidang_pekerjaan" gorm:"column:BIDANG_PEKERJAAN"`
	Kolektabilitas         *string  `json:"kolektabilitas" gorm:"column:KOLEKTABILITAS"`
	TotalPlafon            *float64 `json:"total_plafon" gorm:"column:TOTAL_PLAFON"`
	TotalOutstanding       *float64 `json:"total_outstanding" gorm:"column:TOTAL_OUTSTANDING"`
	InetBanking            *string  `json:"inet_banking" gorm:"column:INET_BANKING"`
	SmsBanking             *string  `json:"sms_banking" gorm:"column:SMS_BANKING"`
	TotalCreditBri         *float64 `json:"total_credit_bri" gorm:"column:TOTAL_CREDIT_BRI"`
	TotalDebitBri          *float64 `json:"total_debit_bri" gorm:"column:TOTAL_DEBIT_BRI"`
	TotalKreditNonBri      *float64 `json:"total_kredit_non_bri" gorm:"column:TOTAL_KREDIT_NON_BRI"`
	TotalDebitNonBri       *float64 `json:"total_debit_non_bri" gorm:"column:TOTAL_DEBIT_NON_BRI"`
	TotalKreditDebitNonBri *float64 `json:"total_kredit_debit_non_bri" gorm:"column:TOTAL_KREDIT_DEBIT_NON_BRI"`
	NamaNasabahNonBri      *string  `json:"nama_nasabah_non_bri" gorm:"column:NAMA_NASABAH_NON_BRI"`
	NamaBankLawan          *string  `json:"nama_bank_lawan" gorm:"column:NAMA_BANK_LAWAN"`
	RatasSaldoSimpananBri  *float64 `json:"ratas_saldo_simpanan_bri" gorm:"column:RATAS_SALDO_SIMPANAN_BRI"`
	TotalCreditKeseluruhan *float64 `json:"total_credit_keseluruhan" gorm:"column:TOTAL_CREDIT_KESELURUHAN"`
	TotalDebitKeseluruhan  *float64 `json:"total_debit_keseluruhan" gorm:"column:TOTAL_DEBIT_KESELURUHAN"`
	CifNasabahTier1        *string  `json:"cif_nasabah_tier1" gorm:"column:CIF_NASABAH_TIER_1"`
	FlagPinjamanTier1      *int64   `json:"flag_pinjaman_tier1" gorm:"column:FLAG_PINJAMAN_TIER_1"`
	NamaNasabahTier1       *string  `json:"nama_nasabah_tier1" gorm:"column:NAMA_NASABAH_TIER_1"`
	SnameRmPinjamanTier1   *string  `json:"sname_rm_pinjaman_tier1" gorm:"column:SNAME_RM_PINJAMAN_TIER_1"`
	UnitRmTier1            *string  `json:"unit_rm_tier1" gorm:"column:UNIT_RM_TIER_1"`
	PhonenumberRmTier1     *string  `json:"phonenumber_rm_tier1" gorm:"column:PHONENUMBER_RM_TIER_1"`
	CifNasabahTier2        *string  `json:"cif_nasabah_tier2" gorm:"column:CIF_NASABAH_TIER_2"`
	FlagPinjamanTier2      *int64   `json:"flag_pinjaman_tier2" gorm:"column:FLAG_PINJAMAN_TIER_2"`
	NamaNasabahTier2       *string  `json:"nama_nasabah_tier2" gorm:"column:NAMA_NASABAH_TIER_2"`
	SnameRmPinjamanTier2   *string  `json:"sname_rm_pinjaman_tier2" gorm:"column:SNAME_RM_PINJAMAN_TIER_2"`
	UnitRmTier2            *string  `json:"unit_rm_tier2" gorm:"column:UNIT_RM_TIER_2"`
	PhonenumberTier2       *string  `json:"phonenumber_tier2" gorm:"column:PHONENUMBER_TIER_2"`
	CifNasabahTier3        *string  `json:"cif_nasabah_tier3" gorm:"column:CIF_NASABAH_TIER_3"`
	FlagPinjamanTier3      *int64   `json:"flag_pinjaman_tier3" gorm:"column:FLAG_PINJAMAN_TIER_3"`
	NamaNasabahTier3       *string  `json:"nama_nasabah_tier3" gorm:"column:NAMA_NASABAH_TIER_3"`
	SnameRmPinjamanTier3   *string  `json:"sname_rm_pinjaman_tier3" gorm:"column:SNAME_RM_PINJAMAN_TIER_3"`
	UnitRmTier3            *string  `json:"unit_rm_tier3" gorm:"column:UNIT_RM_TIER_3"`
	PhonenumberTier3       *string  `json:"phonenumber_tier3" gorm:"column:PHONENUMBER_TIER_3"`
	CifNasabahTier4        *string  `json:"cif_nasabah_tier4" gorm:"column:CIF_NASABAH_TIER_4"`
	FlagPinjamanTier4      *int64   `json:"flag_pinjaman_tier4" gorm:"column:FLAG_PINJAMAN_TIER_4"`
	NamaNasabahTier4       *string  `json:"nama_nasabah_tier4" gorm:"column:NAMA_NASABAH_TIER_4"`
	SnameRmPinjamanTier4   *string  `json:"sname_rm_pinjaman_tier4" gorm:"column:SNAME_RM_PINJAMAN_TIER_4"`
	UnitRmTier4            *string  `json:"unit_rm_tier4" gorm:"column:UNIT_RM_TIER_4"`
	PhonenumberTier4       *string  `json:"phonenumber_tier4" gorm:"column:PHONENUMBER_TIER_4"`
	StatusRekening         *string  `json:"status_rekening" gorm:"column:STATUS_REKENING"`
	AkuisisiSimpanan       *string  `json:"akuisisi_simpanan" gorm:"column:AKUISISI_SIMPANAN"`
	AkuisisiPinjaman       *string  `json:"akuisisi_pinjaman" gorm:"column:AKUISISI_PINJAMAN"`
	LamaNasabahBulan       *string  `json:"lama_nasabah_bulan" gorm:"column:LAMA_NASABAH_BULAN"`
	TanggalLahir           *string  `json:"tanggal_lahir" gorm:"column:TANGGAL_LAHIR"`
	TotalMutasiDebit       *float64 `json:"total_mutasi_debit" gorm:"column:TOTAL_MUTASI_DEBIT"`
	TotalMutasiKredit      *float64 `json:"total_mutasi_kredit" gorm:"column:TOTAL_MUTASI_KREDIT"`
	Ds                     *string  `json:"ds" gorm:"column:DS"`
	IdStatus               *int64   `json:"id_status" gorm:"column:ID_STATUS"`
	StatusAkuisisiSimpanan *string  `json:"status_akuisisi_simpanan" gorm:"column:STATUS_AKUISISI_SIMPANAN"`
	StatusAkuisisiPinjaman *string  `json:"status_akuisisi_pinjaman" gorm:"column:STATUS_AKUISISI_PINJAMAN"`
	VolumePotensiPinjaman  *float64 `json:"volume_potensi_pinjaman" gorm:"column:VOLUME_POTENSI_PINJAMAN"`
	VolumePotensiSimpanan  *float64 `json:"volume_potensi_simpanan" gorm:"column:VOLUME_POTENSI_SIMPANAN"`
	FreqDebitKeseluruhan   *int64   `json:"freq_debit_keseluruhan" gorm:"column:FREQ_DEBIT_KESELURUHAN"`
	FreqCreditKeseluruhan  *int64   `json:"freq_credit_keseluruhan" gorm:"column:FREQ_CREDIT_KESELURUHAN"`

	Id                      *int64     `json:"id" gorm:"column:ID"`
	Cifno                   *string    `json:"cifno" gorm:"column:CIFNO"`
	KodeNonBri              *string    `json:"kode_non_bri" gorm:"column:KODE_NON_BRI"`
	TipeCustomer            *string    `json:"tipe_customer" gorm:"column:TIPE_CUSTOMER"`
	StatusHubungi           *string    `json:"status_hubungi" gorm:"column:STATUS_HUBUNGI"`
	KodeHubungi             *int64     `json:"kode_hubungi" gorm:"column:KODE_HUBUNGI"`
	TglHubungi              *time.Time `json:"tgl_hubungi" gorm:"column:TGL_HUBUNGI"`
	CatatanHubungi          *string    `json:"catatan_hubungi" gorm:"column:CATATAN_HUBUNGI"`
	StatusKetertarikan      *string    `json:"status_ketertarikan" gorm:"column:STATUS_KETERTARIKAN"`
	KodeKetertarikan        *int64     `json:"kode_ketertarikan" gorm:"column:KODE_KETERTARIKAN"`
	CatatanKetertarikan     *string    `json:"catatan_ketertarikan" gorm:"column:CATATAN_KETERTARIKAN"`
	StatusKunjungan         *string    `json:"status_kunjungan" gorm:"column:STATUS_KUNJUNGAN"`
	KodeKunjungan           *int64     `json:"kode_kunjungan" gorm:"column:KODE_KUNJUNGAN"`
	TglKunjungan            *time.Time `json:"tgl_kunjungan" gorm:"column:TGL_KUNJUNGAN"`
	CatatanKunjungan        *string    `json:"catatan_kunjungan" gorm:"column:CATATAN_KUNJUNGAN"`
	CreatedDate             *time.Time `json:"created_date" gorm:"column:CREATED_DATE"`
	UpdatedDate             *time.Time `json:"updated_date" gorm:"column:UPDATED_DATE"`
	IdStatusTabelNon        *int64     `json:"id_status_tabel_non" gorm:"column:ID_STATUS_TABEL_NON"`
	VolumePotensiRmPinjaman *float64   `json:"volume_potensi_rm_pinjaman" gorm:"column:VOLUME_POTENSI_RM_PINJAMAN"`
	VolumePotensiRmSimpanan *float64   `json:"volume_potensi_rm_simpanan" gorm:"column:VOLUME_POTENSI_RM_SIMPANAN"`
	KodeMainbranch          *string    `json:"kode_mainbranch" gorm:"column:KODE_MAINBRANCH_TABEL_NON"`
	// NamaRm                  *string    `json:"nama_rm" gorm:"column:NAMA_RM"`
	// BranchRm                *string    `json:"branch_rm" gorm:"column:BRANCH_RM"`
	// PhonenumberRm           *string    `json:"phonenumber_rm" gorm:"column:PHONENUMBER_RM"`
}

type ResponseDetailNasabah struct {
	DetailCard          DetailCard          `json:"card"`
	DetailTier          DetailTier          `json:"posisi_tier_nasabah"`
	DetailInfoPersonal  DetailInfoPersonal  `json:"informasi_personal"`
	DetailInfoRekening  DetailInfoRekening  `json:"informasi_rekening"`
	DetailInfoTransaksi DetailInfoTransaksi `json:"informasi_transaksi"`
	DetailInfoSimpanan  *DetailInfoSimpanan `json:"informasi_simpanan,omitempty"`
	DetailInfoPinjaman  *DetailInfoPinjaman `json:"informasi_pinjaman,omitempty"`
}

//DetailCard ...
type DetailCard struct {
	NamaNasabahRekanan     *string `json:"nama"`
	CIF                    *string `json:"cif"`
	KeteranganInformasi    *string `json:"keterangan_informasi"`
	Inisial                *string `json:"inisial"`
	StatusTindakLanjut     *string `json:"status_tindak_lanjut"`
	StatusKetertarikan     *string `json:"status_ketertarikan,omitempty"`
	RatasSaldo             *string `json:"ratas_saldo"`
	TotalKreditKeseluruhan *string `json:"total_kredit_keseluruhan"`
	TransaksiDebitNonBRI   *string `json:"transaksi_debit_non_bri"`
	AkuisisiSimpanan       *string `json:"akuisisi_simpanan,omitempty"`
	AkuisisiPinjaman       *string `json:"akuisisi_pinjaman,omitempty"`
	SektorIndustri         *string `json:"sektor_industri"`
	TanggalHubungi         *string `json:"tanggal_hubungi,omitempty"`
}

//DetailScore ...
type DetailTier struct {
	PosisiTier *string `json:"tier" gorm:"column:posisi_tier"`
}

//DetailInfoPersonal ...
type DetailInfoPersonal struct {
	// TanggalLahir *string `json:"tanggal_lahir" gorm:"column:tanggal_lahir"`
	Usia         *string `json:"usia" gorm:"column:usia"`
	JenisKelamin *string `json:"jenis_kelamin" gorm:"column:usia"`
	PhoneNumber  *string `json:"no_telepon" gorm:"column:phonenumber"`
	Pekerjaan    *string `json:"pekerjaan" gorm:"column:jenis_pekerjaan"`
	// LayananDigital []*string `json:"layanan_digital"`
	SektorEkonomi  *string `json:"sektor_ekonomi"`
	AlamatPinjaman *string `json:"alamat_pinjaman" gorm:"column:alamat_pinjaman"`
	AlamatDomisili *string `json:"alamat_domisili" gorm:"column:alamat_domisili"`
	AlamatKTP      *string `json:"alamat_ktp" gorm:"column:alamat_id"`
}

//DetailInfoRekening ...
type DetailInfoRekening struct {
	CIF                   *string   `json:"cif" gorm:"column:cifno"`
	StatusRekening        *string   `json:"status_rekening"`
	NamaUnitKerja         *string   `json:"nama_unit_kerja" gorm:"column:columnx"`
	ProdukBRIYangDimiliki *[]string `json:"produk_bri_yang_dimiliki" gorm:"column:columnx"`
	LayananDigital        []*string `json:"layanan_digital"`
	// TotalTransaksiKreditBRI    *string   `json:"total_transaksi_kredit_bri" gorm:"column:total_transaksi_credit"`
	// TotalTransaksiDebitBRI     *string   `json:"total_transaksi_debit_bri" gorm:"column:total_transaksi_debit"`
	// TotalTransaksiKreditNonBRI *string   `json:"total_transaksi_kredit_non_bri" gorm:"column:total_credit_non_bri"`
	// TotalTransaksiDebitNonBRI  *string   `json:"total_transaksi_debit_non_bri" gorm:"column:total_debit_non_bri"`
	// TotalKreditKeseluruhan     *string   `json:"total_kredit_keseluruhan" gorm:"column:columnx"`
	// TotalDebitKeseluruhan      *string   `json:"total_debit_keseluruhan" gorm:"column:columnx"`
	// UangKeluar                 *string   `json:"uang_keluar"`
	// FrekuensiTransaksiDebit    *string   `json:"frekuensi_transaksi_debit"`
	// UangMasuk                  *string   `json:"uang_masuk"`
	// FrekuensiTransaksiKredit   *string   `json:"frekuensi_transaksi_kredit"`
}

//DetailInfoTransaksi ...
type DetailInfoTransaksi struct {
	TotalDebitKeseluruhan      *string `json:"total_debit_keseluruhan" gorm:"column:columnx"`
	FrekuensiTransaksiDebit    *string `json:"frekuensi_transaksi_debit"`
	TotalKreditKeseluruhan     *string `json:"total_kredit_keseluruhan" gorm:"column:columnx"`
	FrekuensiTransaksiKredit   *string `json:"frekuensi_transaksi_kredit"`
	TotalTransaksiDebitBRI     *string `json:"total_transaksi_debit_bri" gorm:"column:total_transaksi_debit"`
	TotalTransaksiKreditBRI    *string `json:"total_transaksi_kredit_bri" gorm:"column:total_transaksi_credit"`
	TotalTransaksiDebitNonBRI  *string `json:"total_transaksi_debit_non_bri" gorm:"column:total_debit_non_bri"`
	TotalTransaksiKreditNonBRI *string `json:"total_transaksi_kredit_non_bri" gorm:"column:total_credit_non_bri"`
}

//DetailInfoSimpanan ...
type DetailInfoSimpanan struct {
	// StatusRekening        *string `json:"status_rekening"`
	TierDiatasnya      *string `json:"tier_diatasnya"`
	AkuisisiSimpanan   *string `json:"potensi_simpanan"`
	RatasSaldoSimpanan *string `json:"ratas_saldo_simpanan"`
	// UangMasukNonBRI       *string `json:"uang_masuk_non_bri"`
	// SegmenPinjaman        *string `json:"segmen_pinjaman"`
	// TotalDebitKeseluruhan *string `json:"total_debit_keseluruhan"`
	// SektorEkonomi         *string `json:"sektor_ekonomi"`
	RMDiatasnya *string `json:"rm_diatasnya"`
	// DebitNonBRI           *string `json:"debit_non_bri"`
	LamaMenjadiNasabah *string `json:"lama_menjadi_nasabah"`
}

//DetailInfoPinjaman ...
type DetailInfoPinjaman struct {
	Kolektibilitas   *string `json:"kolektibilitas"`
	TotalOutstanding *string `json:"total_outstanding"`
	SegmenPinjaman   *string `json:"segmen_pinjaman"`
	// UangMasukDariNonBRI *string `json:"uang_masuk_dari_non_bri"`
	AkuisisiPinjaman *string `json:"potensi_pinjaman"`
	TotalPlafon      *string `json:"total_plafon"`
	// TotalMutasiDebit    *string `json:"total_mutasi_debit"`
	// TotalMutasiCredit   *string `json:"total_mutasi_credit"`
}

func getInitialDetail(nama string) (res string) {

	fullname := strings.TrimSpace(nama)
	fmt.Println("fullname ->", nama)
	result := strings.Split(fullname, " ")
	lensize := len(result)
	fmt.Println("lensize ->", lensize)

	if len(result) == 1 {
		res = string(result[0][0])
	} else {
		res = string(result[0][0]) + string(result[lensize-1][0])
	}

	return res
}

func MappingResponseDetailNasabah(data *GetDetailQueryStruct) *ResponseDetailNasabah {
	intTier := helper.Int64NullableToInt(data.PosisiTier)
	intUsia := helper.Int64NullableToInt(data.Usia)
	posisiTier := fmt.Sprintf("Tier %d", intTier)
	usia := fmt.Sprintf("%d Tahun", intUsia)

	nama := helper.StringNullableToString(data.NamaNasabahRekan)
	inisial := getInitialDetail(nama)

	var statusAkuisisiPinjaman *string
	if strings.ToLower(helper.StringNullableToString(data.StatusAkuisisiPinjaman)) == "sudah" {
		statusAkuisisiPinjaman = data.StatusAkuisisiPinjaman
	} else {
		statusAkuisisiPinjaman = nil
	}

	var statusAkuisisiSimpanan *string
	if strings.ToLower(helper.StringNullableToString(data.StatusAkuisisiSimpanan)) == "sudah" {
		statusAkuisisiSimpanan = data.StatusAkuisisiSimpanan
	} else {
		statusAkuisisiSimpanan = nil
	}

	// tierDiatasnya := data.TierDiatasnya
	// rmDiatasnya := data.RmDiatasnya
	// lamaNasabah := data.LamaNasabahBulan
	var responseLamaNasabah string
	lamaNasabahTahun, lamaNasabahBulan := helper.StringNullableToInt(data.LamaNasabahBulan)/12, helper.StringNullableToInt(data.LamaNasabahBulan)%12
	if lamaNasabahTahun == 0 {
		responseLamaNasabah = fmt.Sprintf("%d Bulan", lamaNasabahBulan)
	} else {
		responseLamaNasabah = fmt.Sprintf("%d Tahun %d Bulan", lamaNasabahTahun, lamaNasabahBulan)
	}

	var kolektabilitas *string
	if data.Kolektabilitas == nil || helper.StringNullableToString(data.Kolektabilitas) == "" {
		kolektabilitas = helper.StringToStringNullable("-")
	} else {
		kolektabilitas = data.Kolektabilitas
	}

	var pekerjaan *string
	if data.JenisPekerjaan == nil || helper.StringNullableToString(data.JenisPekerjaan) == "" {
		pekerjaan = helper.StringToStringNullable("-")
	} else {
		pekerjaan = data.JenisPekerjaan
	}

	var sektorEkonomi *string
	if data.SektorEkonomi == nil || helper.StringNullableToString(data.SektorEkonomi) == "" {
		sektorEkonomi = helper.StringToStringNullable("-")
	} else {
		sektorEkonomi = data.SektorEkonomi
	}

	var segmen *string
	if data.Segmen == nil || helper.StringNullableToString(data.Segmen) == "" {
		segmen = helper.StringToStringNullable("-")
	} else {
		segmen = data.Segmen
	}

	var alamatPinjaman *string
	if data.AlamatPinjaman == nil || helper.StringNullableToString(data.AlamatPinjaman) == "" {
		alamatPinjaman = helper.StringToStringNullable("-")
	} else {
		alamatPinjaman = data.AlamatPinjaman
	}

	var alamatDomisili *string
	if data.AlamatDomisili == nil || helper.StringNullableToString(data.AlamatDomisili) == "" {
		alamatDomisili = helper.StringToStringNullable("-")
	} else {
		alamatDomisili = data.AlamatDomisili
	}

	var alamatId *string
	if data.AlamatId == nil || helper.StringNullableToString(data.AlamatId) == "" {
		alamatId = helper.StringToStringNullable("-")
	} else {
		alamatId = data.AlamatId
	}

	var rmDiatasnya *string
	if data.RmDiatasnya == nil || helper.StringNullableToString(data.RmDiatasnya) == "" {
		rmDiatasnya = helper.StringToStringNullable("-")
	} else {
		rmDiatasnya = data.RmDiatasnya
	}

	var tierDiatasnya *string
	if data.TierDiatasnya == nil || helper.StringNullableToString(data.TierDiatasnya) == "" {
		tierDiatasnya = helper.StringToStringNullable("-")
	} else {
		tierDiatasnya = data.TierDiatasnya
	}

	var jenisKelamin *string
	if data.JenisKelamin == nil || helper.StringNullableToString(data.JenisKelamin) == "" {
		jenisKelamin = helper.StringToStringNullable("-")
	} else {
		jenisKelamin = data.JenisKelamin
	}

	var phoneNumber *string
	if data.Phonenumber == nil || helper.StringNullableToString(data.Phonenumber) == "" {
		phoneNumber = helper.StringToStringNullable("-")
	} else {
		phoneNumber = data.Phonenumber
	}

	var layananDigital []*string
	if helper.StringNullableToString(data.SmsBanking) == "1" && helper.StringNullableToString(data.InetBanking) == "1" {
		// layananDigital = helper.StringToStringNullable("SMS Banking & Internet Banking")
		layananDigital = []*string{helper.StringToStringNullable("SMS Banking"), helper.StringToStringNullable("Internet Banking")}
	} else if helper.StringNullableToString(data.SmsBanking) == "1" && helper.StringNullableToString(data.InetBanking) == "0" {
		layananDigital = []*string{helper.StringToStringNullable("SMS Banking")}
	} else if helper.StringNullableToString(data.SmsBanking) == "0" && helper.StringNullableToString(data.InetBanking) == "1" {
		layananDigital = []*string{helper.StringToStringNullable("Internet Banking")}
	} else {
		layananDigital = []*string{helper.StringToStringNullable("Nasabah belum memiliki layanan digital")}
	}

	// var produkBriYangDimiliki *string
	// // if (helper.StringNullableToString(data.ProdukPinjaman) != "" || data.ProdukPinjaman != nil) && (helper.StringNullableToString(data.ProdukSimpanan) != "" || data.ProdukSimpanan != nil) {
	// // if (data.ProdukPinjaman != nil || helper.StringNullableToString(data.ProdukPinjaman) != "") && (data.ProdukSimpanan != nil || helper.StringNullableToString(data.ProdukSimpanan) != "") {
	// // if (data.ProdukPinjaman != nil || len(helper.StringNullableToString(data.ProdukPinjaman)) != 0) && (data.ProdukSimpanan != nil || len(helper.StringNullableToString(data.ProdukSimpanan)) != 0) {
	// if (len(helper.StringNullableToString(data.ProdukPinjaman)) != 0) && (len(helper.StringNullableToString(data.ProdukSimpanan)) != 0) {
	// 	produkSimpanan := helper.StringNullableToString(data.ProdukSimpanan)
	// 	produkPinjaman := helper.StringNullableToString(data.ProdukPinjaman)
	// 	produkBriYangDimiliki = helper.StringToStringNullable(produkSimpanan + `,` + produkPinjaman)
	// 	fmt.Println("active->simpanan pinjaman")
	// 	// } else if (data.ProdukPinjaman != nil || helper.StringNullableToString(data.ProdukPinjaman) != "") && (data.ProdukSimpanan == nil || helper.StringNullableToString(data.ProdukSimpanan) == "") {
	// } else if (len(helper.StringNullableToString(data.ProdukPinjaman)) > 0) && (len(helper.StringNullableToString(data.ProdukSimpanan)) == 0) {
	// 	produkBriYangDimiliki = data.ProdukPinjaman
	// 	fmt.Println("active->pinjaman")
	// 	// } else if (data.ProdukPinjaman == nil || helper.StringNullableToString(data.ProdukPinjaman) == "") && (data.ProdukSimpanan != nil || helper.StringNullableToString(data.ProdukSimpanan) != "") {
	// } else if (len(helper.StringNullableToString(data.ProdukPinjaman)) == 0) && (len(helper.StringNullableToString(data.ProdukSimpanan)) > 0) {
	// 	produkBriYangDimiliki = data.ProdukSimpanan
	// 	fmt.Println("active->simpanan")
	// } else {
	// 	produkBriYangDimiliki = helper.StringToStringNullable("-")
	// }

	var produkBriYangDimiliki *[]string
	if (len(helper.StringNullableToString(data.ProdukPinjaman)) != 0) && (len(helper.StringNullableToString(data.ProdukSimpanan)) != 0) {
		// produkSimpanan := helper.StringNullableToString(data.ProdukSimpanan)
		// produkPinjaman := helper.StringNullableToString(data.ProdukPinjaman)
		// produkBriYangDimiliki = helper.StringToStringNullable(produkSimpanan + `,` + produkPinjaman)
		produkSimpanan := (strings.Split(helper.StringNullableToString(data.ProdukSimpanan), ","))
		produkPinjaman := (strings.Split(helper.StringNullableToString(data.ProdukPinjaman), ","))
		// var produkBri []string
		produkBri := append(produkSimpanan, produkPinjaman...)
		produkBriYangDimiliki = &produkBri

		fmt.Println("active->simpanan pinjaman")
		// } else if (data.ProdukPinjaman != nil || helper.StringNullableToString(data.ProdukPinjaman) != "") && (data.ProdukSimpanan == nil || helper.StringNullableToString(data.ProdukSimpanan) == "") {
	} else if (len(helper.StringNullableToString(data.ProdukPinjaman)) > 0) && (len(helper.StringNullableToString(data.ProdukSimpanan)) == 0) {
		// produkBriYangDimiliki = data.ProdukPinjaman
		produkPinjaman := (strings.Split(helper.StringNullableToString(data.ProdukPinjaman), ","))
		// produkBri := append(produkPinjaman)
		produkBriYangDimiliki = &produkPinjaman
		fmt.Println("active->pinjaman")
		// } else if (data.ProdukPinjaman == nil || helper.StringNullableToString(data.ProdukPinjaman) == "") && (data.ProdukSimpanan != nil || helper.StringNullableToString(data.ProdukSimpanan) != "") {
	} else if (len(helper.StringNullableToString(data.ProdukPinjaman)) == 0) && (len(helper.StringNullableToString(data.ProdukSimpanan)) > 0) {
		// produkBriYangDimiliki = data.ProdukSimpanan
		produkSimpanan := (strings.Split(helper.StringNullableToString(data.ProdukSimpanan), ","))
		produkBriYangDimiliki = &produkSimpanan
		fmt.Println("active->simpanan")
	} else {
		// produkBriYangDimiliki = helper.StringToStringNullable("-")
		produkBriYangDimiliki = &[]string{"-"}
	}

	intFrekuensiTransaksiDebit := helper.Int64NullableToInt(data.FreqDebitKeseluruhan)
	intFrekuensiTransaksiKredit := helper.Int64NullableToInt(data.FreqCreditKeseluruhan)
	frekuensiTransaksiDebit := fmt.Sprintf("%d kali", intFrekuensiTransaksiDebit)
	frekuensiTransaksiKredit := fmt.Sprintf("%d kali", intFrekuensiTransaksiKredit)

	var res *ResponseDetailNasabah

	res = &ResponseDetailNasabah{
		DetailCard: DetailCard{
			CIF:                    data.CifNasabahRekan,
			KeteranganInformasi:    helper.StringToStringNullable("CIF"),
			NamaNasabahRekanan:     data.NamaNasabahRekan,
			Inisial:                &inisial,
			StatusTindakLanjut:     helper.StringToStringNullable("Belum"),
			RatasSaldo:             helper.CurrencyFormat(data.RatasSaldoSimpananBri),
			TotalKreditKeseluruhan: helper.CurrencyFormat(data.TotalCreditKeseluruhan),
			TransaksiDebitNonBRI:   helper.CurrencyFormat(data.TotalDebitNonBri),
			// AkuisisiSimpanan:    data.AkuisisiSimpanan,
			// AkuisisiPinjaman:    data.AkuisisiPinjaman,
			SektorIndustri:   sektorEkonomi,
			AkuisisiSimpanan: statusAkuisisiSimpanan,
			AkuisisiPinjaman: statusAkuisisiPinjaman,
		},
		DetailTier: DetailTier{
			PosisiTier: helper.StringToStringNullable(posisiTier),
		},
		DetailInfoPersonal: DetailInfoPersonal{
			// TanggalLahir: data.TanggalLahir,
			Usia:         helper.StringToStringNullable(usia),
			JenisKelamin: jenisKelamin,
			PhoneNumber:  phoneNumber,
			Pekerjaan:    pekerjaan,
			// LayananDigital: layananDigital,
			SektorEkonomi:  sektorEkonomi,
			AlamatPinjaman: alamatPinjaman,
			AlamatDomisili: alamatDomisili,
			AlamatKTP:      alamatId,
		},
		DetailInfoRekening: DetailInfoRekening{
			CIF:                   data.CifNasabahRekan,
			StatusRekening:        data.StatusRekening,
			NamaUnitKerja:         data.Mbdesc,
			ProdukBRIYangDimiliki: produkBriYangDimiliki,
			LayananDigital:        layananDigital,
			// TotalTransaksiKreditBRI:    helper.CurrencyFormat(data.TotalCreditBri),
			// TotalTransaksiDebitBRI:     helper.CurrencyFormat(data.TotalDebitBri),
			// TotalTransaksiKreditNonBRI: helper.CurrencyFormat(data.TotalKreditNonBri),
			// TotalTransaksiDebitNonBRI:  helper.CurrencyFormat(data.TotalDebitNonBri),
			// TotalKreditKeseluruhan:     helper.CurrencyFormat(data.TotalCreditKeseluruhan),
			// TotalDebitKeseluruhan:      helper.CurrencyFormat(data.TotalDebitKeseluruhan),
			// UangKeluar:                 helper.CurrencyFormat(data.TotalDebitKeseluruhan),
			// FrekuensiTransaksiDebit:    &frekuensiTransaksiDebit,
			// UangMasuk:                  helper.CurrencyFormat(data.TotalCreditKeseluruhan),
			// FrekuensiTransaksiKredit:   &frekuensiTransaksiKredit,
		},
		DetailInfoTransaksi: DetailInfoTransaksi{
			TotalDebitKeseluruhan:      helper.CurrencyFormat(data.TotalDebitKeseluruhan),
			FrekuensiTransaksiDebit:    &frekuensiTransaksiDebit,
			TotalKreditKeseluruhan:     helper.CurrencyFormat(data.TotalCreditKeseluruhan),
			FrekuensiTransaksiKredit:   &frekuensiTransaksiKredit,
			TotalTransaksiDebitBRI:     helper.CurrencyFormat(data.TotalDebitBri),
			TotalTransaksiKreditBRI:    helper.CurrencyFormat(data.TotalCreditBri),
			TotalTransaksiDebitNonBRI:  helper.CurrencyFormat(data.TotalDebitNonBri),
			TotalTransaksiKreditNonBRI: helper.CurrencyFormat(data.TotalKreditNonBri),
		},
		DetailInfoSimpanan: &DetailInfoSimpanan{
			// StatusRekening:        data.StatusRekening,
			TierDiatasnya:      tierDiatasnya,
			AkuisisiSimpanan:   helper.CurrencyFormat(data.VolumePotensiSimpanan),
			RatasSaldoSimpanan: helper.CurrencyFormat(data.RatasSaldoSimpananBri),
			// UangMasukNonBRI:       helper.CurrencyFormat(data.TotalKreditNonBri),
			// SegmenPinjaman:        segmen,
			// TotalDebitKeseluruhan: helper.CurrencyFormat(data.TotalDebitKeseluruhan),
			// SektorEkonomi:         sektorEkonomi,
			RMDiatasnya: rmDiatasnya,
			// DebitNonBRI:           helper.CurrencyFormat(data.TotalDebitNonBri),
			LamaMenjadiNasabah: helper.StringToStringNullable(responseLamaNasabah),
		},
		DetailInfoPinjaman: &DetailInfoPinjaman{
			Kolektibilitas:   kolektabilitas,
			TotalOutstanding: helper.CurrencyFormat(data.TotalOutstanding),
			SegmenPinjaman:   segmen,
			// UangMasukDariNonBRI: helper.CurrencyFormat(data.TotalKreditNonBri),
			AkuisisiPinjaman: helper.CurrencyFormat(data.VolumePotensiPinjaman),
			TotalPlafon:      helper.CurrencyFormat(data.TotalPlafon),
			// AkuisisiPinjaman:    data.AkuisisiPinjaman,
			// TotalMutasiDebit:  helper.CurrencyFormat(data.TotalDebitKeseluruhan),
			// TotalMutasiCredit: helper.CurrencyFormat(data.TotalCreditKeseluruhan),
		},
	}

	return res
}

type ResponseDetailNasabahDitindaklanjuti struct {
	DetailCard                DetailCard                  `json:"card"`
	DetailCatatanTindaklanjut []DetailCatatanTindaklanjut `json:"catatan_tindaklanjut"`
	DetailTier                DetailTier                  `json:"posisi_tier_nasabah"`
	DetailInfoPersonal        DetailInfoPersonal          `json:"informasi_personal"`
	DetailInfoRekening        DetailInfoRekening          `json:"informasi_rekening"`
	DetailInfoTransaksi       DetailInfoTransaksi         `json:"informasi_transaksi"`
	DetailInfoSimpanan        *DetailInfoSimpanan         `json:"informasi_simpanan,omitempty"`
	DetailInfoPinjaman        *DetailInfoPinjaman         `json:"informasi_pinjaman,omitempty"`
}

type DetailCatatanTindaklanjut struct {
	JudulCatatan     *string `json:"judul_catatan"`
	DeskripsiCatatan *string `json:"deskripsi_catatan"`
}

func MappingResponseDetailNasabahDitindaklanjuti(data *GetDetailQueryStruct) *ResponseDetailNasabahDitindaklanjuti {
	intTier := helper.Int64NullableToInt(data.PosisiTier)
	intUsia := helper.Int64NullableToInt(data.Usia)
	posisiTier := fmt.Sprintf("Tier %d", intTier)
	usia := fmt.Sprintf("%d Tahun", intUsia)

	nama := helper.StringNullableToString(data.NamaNasabahRekan)
	inisial := getInitialDetail(nama)

	var detailCatatanTL []DetailCatatanTindaklanjut
	var tanggalHubungi, tanggalKunjungan *string
	var catatanKetertarikan, statusKetertarikan, nullValue *string

	defaulttime := time.Date(1900, 1, 1, 0, 0, 0, 0, time.Local)
	tglHubungiString := helper.DateTimeNullableToDateTime(data.TglHubungi)
	tglKunjunganString := helper.DateTimeNullableToDateTime(data.TglKunjungan)
	tanggalHubungi = helper.StringToStringNullable(tglHubungiString.Format("2 January 2006"))
	tanggalKunjungan = helper.StringToStringNullable(tglKunjunganString.Format("2 January 2006"))
	catatanKetertarikan = data.CatatanKetertarikan
	nullValue = helper.StringToStringNullableNullValue("")

	// tierDiatasnya := data.TierDiatasnya
	// rmDiatasnya := data.RmDiatasnya
	// lamaNasabah := data.LamaNasabahBulan

	var statusTindakLanjut *string
	if helper.Int64NullableToInt(data.KodeKunjungan) == 0 || helper.Int64NullableToInt(data.KodeKunjungan) == 2 {
		statusTindakLanjut = data.StatusHubungi
	} else if helper.Int64NullableToInt(data.KodeHubungi) == 2 && helper.Int64NullableToInt(data.KodeKunjungan) == 2 {
		statusTindakLanjut = data.StatusHubungi
	} else {
		statusTindakLanjut = data.StatusKunjungan
	}

	var statusAkuisisiPinjaman *string
	if strings.ToLower(helper.StringNullableToString(data.StatusAkuisisiPinjaman)) == "sudah" {
		statusAkuisisiPinjaman = data.StatusAkuisisiPinjaman
	} else {
		statusAkuisisiPinjaman = nil
	}

	var statusAkuisisiSimpanan *string
	if strings.ToLower(helper.StringNullableToString(data.StatusAkuisisiSimpanan)) == "sudah" {
		statusAkuisisiSimpanan = data.StatusAkuisisiSimpanan
	} else {
		statusAkuisisiSimpanan = nil
	}

	var responseLamaNasabah string
	lamaNasabahTahun, lamaNasabahBulan := helper.StringNullableToInt(data.LamaNasabahBulan)/12, helper.StringNullableToInt(data.LamaNasabahBulan)%12
	if lamaNasabahTahun == 0 {
		responseLamaNasabah = fmt.Sprintf("%d Bulan", lamaNasabahBulan)
	} else {
		responseLamaNasabah = fmt.Sprintf("%d Tahun %d Bulan", lamaNasabahTahun, lamaNasabahBulan)
	}

	var kolektabilitas *string
	if data.Kolektabilitas == nil || helper.StringNullableToString(data.Kolektabilitas) == "" {
		kolektabilitas = helper.StringToStringNullable("-")
	} else {
		kolektabilitas = data.Kolektabilitas
	}

	var pekerjaan *string
	if data.JenisPekerjaan == nil || helper.StringNullableToString(data.JenisPekerjaan) == "" {
		pekerjaan = helper.StringToStringNullable("-")
	} else {
		pekerjaan = data.JenisPekerjaan
	}

	var sektorEkonomi *string
	if data.SektorEkonomi == nil || helper.StringNullableToString(data.SektorEkonomi) == "" {
		sektorEkonomi = helper.StringToStringNullable("-")
	} else {
		sektorEkonomi = data.SektorEkonomi
	}

	var segmen *string
	if data.Segmen == nil || helper.StringNullableToString(data.Segmen) == "" {
		segmen = helper.StringToStringNullable("-")
	} else {
		segmen = data.Segmen
	}

	var alamatPinjaman *string
	if data.AlamatPinjaman == nil || helper.StringNullableToString(data.AlamatPinjaman) == "" {
		alamatPinjaman = helper.StringToStringNullable("-")
	} else {
		alamatPinjaman = data.AlamatPinjaman
	}

	var alamatDomisili *string
	if data.AlamatDomisili == nil || helper.StringNullableToString(data.AlamatDomisili) == "" {
		alamatDomisili = helper.StringToStringNullable("-")
	} else {
		alamatDomisili = data.AlamatDomisili
	}

	var alamatId *string
	if data.AlamatId == nil || helper.StringNullableToString(data.AlamatId) == "" {
		alamatId = helper.StringToStringNullable("-")
	} else {
		alamatId = data.AlamatId
	}

	var rmDiatasnya *string
	if data.RmDiatasnya == nil || helper.StringNullableToString(data.RmDiatasnya) == "" {
		rmDiatasnya = helper.StringToStringNullable("-")
	} else {
		rmDiatasnya = data.RmDiatasnya
	}

	var tierDiatasnya *string
	if data.TierDiatasnya == nil || helper.StringNullableToString(data.TierDiatasnya) == "" {
		tierDiatasnya = helper.StringToStringNullable("-")
	} else {
		tierDiatasnya = data.TierDiatasnya
	}

	var jenisKelamin *string
	if data.JenisKelamin == nil || helper.StringNullableToString(data.JenisKelamin) == "" {
		jenisKelamin = helper.StringToStringNullable("-")
	} else {
		jenisKelamin = data.JenisKelamin
	}

	var phoneNumber *string
	if data.Phonenumber == nil || helper.StringNullableToString(data.Phonenumber) == "" {
		phoneNumber = helper.StringToStringNullable("-")
	} else {
		phoneNumber = data.Phonenumber
	}

	intFrekuensiTransaksiDebit := helper.Int64NullableToInt(data.FreqDebitKeseluruhan)
	intFrekuensiTransaksiKredit := helper.Int64NullableToInt(data.FreqCreditKeseluruhan)
	frekuensiTransaksiDebit := fmt.Sprintf("%d kali", intFrekuensiTransaksiDebit)
	frekuensiTransaksiKredit := fmt.Sprintf("%d kali", intFrekuensiTransaksiKredit)

	var layananDigital []*string
	if helper.StringNullableToString(data.SmsBanking) == "1" && helper.StringNullableToString(data.InetBanking) == "1" {
		// layananDigital = helper.StringToStringNullable("SMS Banking & Internet Banking")
		layananDigital = []*string{helper.StringToStringNullable("SMS Banking"), helper.StringToStringNullable("Internet Banking")}
	} else if helper.StringNullableToString(data.SmsBanking) == "1" && helper.StringNullableToString(data.InetBanking) == "0" {
		layananDigital = []*string{helper.StringToStringNullable("SMS Banking")}
	} else if helper.StringNullableToString(data.SmsBanking) == "0" && helper.StringNullableToString(data.InetBanking) == "1" {
		layananDigital = []*string{helper.StringToStringNullable("Internet Banking")}
	} else {
		layananDigital = []*string{helper.StringToStringNullable("Nasabah belum memiliki layanan digital")}
	}

	// var produkBriYangDimiliki *string
	// if (data.ProdukPinjaman != nil || helper.StringNullableToString(data.ProdukPinjaman) != "") && (data.ProdukSimpanan != nil || helper.StringNullableToString(data.ProdukSimpanan) != "") {
	// 	produkSimpanan := helper.StringNullableToString(data.ProdukSimpanan)
	// 	produkPinjaman := helper.StringNullableToString(data.ProdukPinjaman)
	// 	produkBriYangDimiliki = helper.StringToStringNullable(produkSimpanan + `, ` + produkPinjaman)
	// } else if (data.ProdukPinjaman != nil || helper.StringNullableToString(data.ProdukPinjaman) != "") && (data.ProdukSimpanan == nil || helper.StringNullableToString(data.ProdukSimpanan) == "") {
	// 	produkBriYangDimiliki = data.ProdukPinjaman
	// } else if (data.ProdukPinjaman == nil || helper.StringNullableToString(data.ProdukPinjaman) == "") && (data.ProdukSimpanan != nil || helper.StringNullableToString(data.ProdukSimpanan) != "") {
	// 	produkBriYangDimiliki = data.ProdukSimpanan
	// } else {
	// 	produkBriYangDimiliki = helper.StringToStringNullable("-")
	// }
	var produkBriYangDimiliki *[]string
	if (len(helper.StringNullableToString(data.ProdukPinjaman)) != 0) && (len(helper.StringNullableToString(data.ProdukSimpanan)) != 0) {
		// produkSimpanan := helper.StringNullableToString(data.ProdukSimpanan)
		// produkPinjaman := helper.StringNullableToString(data.ProdukPinjaman)
		// produkBriYangDimiliki = helper.StringToStringNullable(produkSimpanan + `,` + produkPinjaman)
		produkSimpanan := (strings.Split(helper.StringNullableToString(data.ProdukSimpanan), ","))
		produkPinjaman := (strings.Split(helper.StringNullableToString(data.ProdukPinjaman), ","))
		// var produkBri []string
		produkBri := append(produkSimpanan, produkPinjaman...)
		produkBriYangDimiliki = &produkBri

		fmt.Println("active->simpanan pinjaman")
		// } else if (data.ProdukPinjaman != nil || helper.StringNullableToString(data.ProdukPinjaman) != "") && (data.ProdukSimpanan == nil || helper.StringNullableToString(data.ProdukSimpanan) == "") {
	} else if (len(helper.StringNullableToString(data.ProdukPinjaman)) > 0) && (len(helper.StringNullableToString(data.ProdukSimpanan)) == 0) {
		// produkBriYangDimiliki = data.ProdukPinjaman
		produkPinjaman := (strings.Split(helper.StringNullableToString(data.ProdukPinjaman), ","))
		// produkBri := append(produkPinjaman)
		produkBriYangDimiliki = &produkPinjaman
		fmt.Println("active->pinjaman")
		// } else if (data.ProdukPinjaman == nil || helper.StringNullableToString(data.ProdukPinjaman) == "") && (data.ProdukSimpanan != nil || helper.StringNullableToString(data.ProdukSimpanan) != "") {
	} else if (len(helper.StringNullableToString(data.ProdukPinjaman)) == 0) && (len(helper.StringNullableToString(data.ProdukSimpanan)) > 0) {
		// produkBriYangDimiliki = data.ProdukSimpanan
		produkSimpanan := (strings.Split(helper.StringNullableToString(data.ProdukSimpanan), ","))
		produkBriYangDimiliki = &produkSimpanan
		fmt.Println("active->simpanan")
	} else {
		// produkBriYangDimiliki = helper.StringToStringNullable("-")
		produkBriYangDimiliki = &[]string{"-"}
	}

	catatanVolumePotensiPinjaman := DetailCatatanTindaklanjut{
		JudulCatatan:     helper.StringToStringNullable("Volume Potensi Pinjaman"),
		DeskripsiCatatan: helper.CurrencyFormat(data.VolumePotensiRmPinjaman),
	}
	catatanVolumePotensiSimpanan := DetailCatatanTindaklanjut{
		JudulCatatan:     helper.StringToStringNullable("Volume Potensi Simpanan"),
		DeskripsiCatatan: helper.CurrencyFormat(data.VolumePotensiRmSimpanan),
	}

	if helper.Int64NullableToInt(data.KodeHubungi) == 1 && helper.Int64NullableToInt(data.KodeKunjungan) == 1 {
		fmt.Println("positive case 1")
		//positive case 1
		tanggalHubungi = tanggalHubungi

		catatanTanggalKunjungan := DetailCatatanTindaklanjut{
			JudulCatatan:     helper.StringToStringNullable("Tanggal Kunjungan"),
			DeskripsiCatatan: tanggalKunjungan,
		}
		// detailCatatanTL := make([]ResponseInfoTierNasabah, 0)
		if helper.FloatNUllableToFloat(data.VolumePotensiRmPinjaman) != 0 && helper.FloatNUllableToFloat(data.VolumePotensiRmSimpanan) == 0 {
			detailCatatanTL = append(detailCatatanTL, catatanTanggalKunjungan, catatanVolumePotensiPinjaman)
		} else if helper.FloatNUllableToFloat(data.VolumePotensiRmPinjaman) == 0 && helper.FloatNUllableToFloat(data.VolumePotensiRmSimpanan) != 0 {
			detailCatatanTL = append(detailCatatanTL, catatanTanggalKunjungan, catatanVolumePotensiSimpanan)
		} else if helper.FloatNUllableToFloat(data.VolumePotensiRmPinjaman) != 0 && helper.FloatNUllableToFloat(data.VolumePotensiRmSimpanan) != 0 {
			detailCatatanTL = append(detailCatatanTL, catatanTanggalKunjungan, catatanVolumePotensiSimpanan, catatanVolumePotensiPinjaman)
		} else {
			detailCatatanTL = append(detailCatatanTL, catatanTanggalKunjungan)
		}
		// detailcatatantindaklanjut := []DetailCatatanTindaklanjut{
		// 	{
		// 		JudulCatatan:     helper.StringToStringNullable("Tanggal Kunjungan"),
		// 		DeskripsiCatatan: tanggalKunjungan,
		// 	},
		// 	{
		// 		// JudulCatatan:     helper.StringToStringNullable("Alasan Tertarik"),
		// 		// DeskripsiCatatan: catatanKetertarikan,
		// 		JudulCatatan:     helper.StringToStringNullable("Volume Potensi"),
		// 		DeskripsiCatatan: helper.CurrencyFormat(data.VolumePotensiRm),
		// 	},
		// }
		// detailCatatanTL = detailcatatantindaklanjut
		statusKetertarikan = data.StatusKetertarikan
	} else if helper.Int64NullableToInt(data.KodeHubungi) == 1 && helper.Int64NullableToInt(data.KodeKunjungan) == 2 {
		fmt.Println("positive case 2")
		//positive case 2
		tanggalHubungi = tanggalHubungi
		catatanKunjungan := DetailCatatanTindaklanjut{
			JudulCatatan:     helper.StringToStringNullable("Tidak Akan Mengunjungi"),
			DeskripsiCatatan: data.CatatanKunjungan,
		}

		if helper.FloatNUllableToFloat(data.VolumePotensiRmPinjaman) != 0 && helper.FloatNUllableToFloat(data.VolumePotensiRmSimpanan) == 0 {
			detailCatatanTL = append(detailCatatanTL, catatanKunjungan, catatanVolumePotensiPinjaman)
		} else if helper.FloatNUllableToFloat(data.VolumePotensiRmPinjaman) == 0 && helper.FloatNUllableToFloat(data.VolumePotensiRmSimpanan) != 0 {
			detailCatatanTL = append(detailCatatanTL, catatanKunjungan, catatanVolumePotensiSimpanan)
		} else if helper.FloatNUllableToFloat(data.VolumePotensiRmPinjaman) != 0 && helper.FloatNUllableToFloat(data.VolumePotensiRmSimpanan) != 0 {
			detailCatatanTL = append(detailCatatanTL, catatanKunjungan, catatanVolumePotensiSimpanan, catatanVolumePotensiPinjaman)
		} else {
			detailCatatanTL = append(detailCatatanTL, catatanKunjungan)
		}
		// detailcatatantindaklanjut := []DetailCatatanTindaklanjut{
		// 	{
		// 		JudulCatatan:     helper.StringToStringNullable("Tidak Akan Mengunjungi"),
		// 		DeskripsiCatatan: data.CatatanKunjungan,
		// 	},
		// 	{
		// 		JudulCatatan:     helper.StringToStringNullable("Volume Potensi"),
		// 		DeskripsiCatatan: helper.CurrencyFormat(data.VolumePotensiRm),
		// 	},
		// }
		// detailCatatanTL = detailcatatantindaklanjut
		statusKetertarikan = data.StatusKetertarikan
	} else if helper.Int64NullableToInt(data.KodeHubungi) == 3 {
		fmt.Println("negative case 3 / tidak cocok")
		//negative case 3 - tidak cocok
		detailcatatantindaklanjut := []DetailCatatanTindaklanjut{
			{
				JudulCatatan:     helper.StringToStringNullable("Alasan Tidak Cocok"),
				DeskripsiCatatan: data.CatatanHubungi,
			},
		}
		statusKetertarikan = helper.StringToStringNullableNullValue("")
		detailCatatanTL = detailcatatantindaklanjut
	} else if helper.Int64NullableToInt(data.KodeHubungi) == 1 && helper.Int64NullableToInt(data.KodeKunjungan) == 0 && helper.Int64NullableToInt(data.KodeKetertarikan) == 2 {
		fmt.Println("negative case 1: sudah dihubungi, tidak tertarik")
		//negative case 1: sudah dihubungi, tidak tertarik
		detailcatatantindaklanjut := []DetailCatatanTindaklanjut{
			{
				JudulCatatan:     helper.StringToStringNullable("Alasan Tidak Tertarik"),
				DeskripsiCatatan: data.CatatanKetertarikan,
			},
		}
		detailCatatanTL = detailcatatantindaklanjut
		statusKetertarikan = data.StatusKetertarikan

		if helper.DateTimeNullableToDateTime(data.TglHubungi) == defaulttime || helper.DateTimeNullableToDateTime(data.TglHubungi).IsZero() {
			tanggalHubungi = nullValue
		} else {
			tanggalHubungi = tanggalHubungi
		}
	} else if helper.Int64NullableToInt(data.KodeHubungi) == 2 && helper.Int64NullableToInt(data.KodeKunjungan) == 2 {
		fmt.Println("negative case 2")
		//negative case 2

		if helper.DateTimeNullableToDateTime(data.TglHubungi) == defaulttime || helper.DateTimeNullableToDateTime(data.TglHubungi).IsZero() {
			tanggalHubungi = nullValue
		} else {
			tanggalHubungi = tanggalHubungi
		}

		detailcatatantindaklanjut := []DetailCatatanTindaklanjut{
			{
				JudulCatatan:     helper.StringToStringNullable("Tidak Dapat Dihubungi"),
				DeskripsiCatatan: data.CatatanHubungi,
			},
			{
				JudulCatatan:     helper.StringToStringNullable("Tidak Akan Mengunjungi"),
				DeskripsiCatatan: data.CatatanKunjungan,
			},
		}
		statusKetertarikan = helper.StringToStringNullableNullValue("")
		detailCatatanTL = detailcatatantindaklanjut
	} else if helper.Int64NullableToInt(data.KodeKunjungan) == 3 && helper.Int64NullableToInt(data.KodeKetertarikan) == 1 {
		fmt.Println("other case/positive case 1 tertarik")
		//other case tertarik/positive case 1 tertarik
		fmt.Println("tanggal hubungi ->", data.TglHubungi)
		fmt.Println("default time ->", defaulttime)

		if helper.DateTimeNullableToDateTime(data.TglHubungi) == defaulttime || helper.DateTimeNullableToDateTime(data.TglHubungi).IsZero() {
			tanggalHubungi = nil
		} else {
			tanggalHubungi = tanggalHubungi
		}

		catatanTanggalKunjungan := DetailCatatanTindaklanjut{
			JudulCatatan:     helper.StringToStringNullable("Tanggal Kunjungan"),
			DeskripsiCatatan: tanggalKunjungan,
		}
		// detailCatatanTL := make([]ResponseInfoTierNasabah, 0)
		if helper.FloatNUllableToFloat(data.VolumePotensiRmPinjaman) != 0 && helper.FloatNUllableToFloat(data.VolumePotensiRmSimpanan) == 0 {
			detailCatatanTL = append(detailCatatanTL, catatanTanggalKunjungan, catatanVolumePotensiPinjaman)
		} else if helper.FloatNUllableToFloat(data.VolumePotensiRmPinjaman) == 0 && helper.FloatNUllableToFloat(data.VolumePotensiRmSimpanan) != 0 {
			detailCatatanTL = append(detailCatatanTL, catatanTanggalKunjungan, catatanVolumePotensiSimpanan)
		} else if helper.FloatNUllableToFloat(data.VolumePotensiRmPinjaman) != 0 && helper.FloatNUllableToFloat(data.VolumePotensiRmSimpanan) != 0 {
			detailCatatanTL = append(detailCatatanTL, catatanTanggalKunjungan, catatanVolumePotensiSimpanan, catatanVolumePotensiPinjaman)
		} else {
			detailCatatanTL = append(detailCatatanTL, catatanTanggalKunjungan)
		}

		// detailcatatantindaklanjut := []DetailCatatanTindaklanjut{
		// 	{
		// 		JudulCatatan:     helper.StringToStringNullable("Tanggal Kunjungan"),
		// 		DeskripsiCatatan: tanggalKunjungan,
		// 		// DeskripsiCatatan: data.TglKunjungan.Format("2 January 2006"),
		// 	},
		// 	{
		// 		// JudulCatatan:     helper.StringToStringNullable("Alasan Tertarik"),
		// 		// DeskripsiCatatan: catatanKetertarikan,
		// 		JudulCatatan:     helper.StringToStringNullable("Volume Potensi"),
		// 		DeskripsiCatatan: helper.CurrencyFormat(data.VolumePotensiRm),
		// 	},
		// }
		// detailCatatanTL = detailcatatantindaklanjut
		statusKetertarikan = data.StatusKetertarikan
	} else if helper.Int64NullableToInt(data.KodeKunjungan) == 3 && helper.Int64NullableToInt(data.KodeKetertarikan) == 2 {
		fmt.Println("other case/positive case 1 tidak tertarik")
		//other case tertarik/positive case 1 tidak tertarik
		fmt.Println("tanggal hubungi ->", data.TglHubungi)
		fmt.Println("default time ->", defaulttime)

		if helper.DateTimeNullableToDateTime(data.TglHubungi) == defaulttime || helper.DateTimeNullableToDateTime(data.TglHubungi).IsZero() {
			tanggalHubungi = nullValue
		} else {
			tanggalHubungi = tanggalHubungi
		}

		detailcatatantindaklanjut := []DetailCatatanTindaklanjut{
			{
				JudulCatatan:     helper.StringToStringNullable("Tanggal Kunjungan"),
				DeskripsiCatatan: tanggalKunjungan,
				// DeskripsiCatatan: data.TglKunjungan.Format("2 January 2006"),
			},
			{
				JudulCatatan:     helper.StringToStringNullable("Alasan Tidak Tertarik"),
				DeskripsiCatatan: catatanKetertarikan,
			},
		}
		statusKetertarikan = data.StatusKetertarikan
		detailCatatanTL = detailcatatantindaklanjut
	} else if helper.Int64NullableToInt(data.KodeKunjungan) == 1 && helper.Int64NullableToInt(data.KodeKetertarikan) == 3 {
		fmt.Println("other case")
		//other case: tidak dapat dihubungi, saya akan mengunjungi, ketertarikan pending

		detailcatatantindaklanjut := []DetailCatatanTindaklanjut{
			{
				JudulCatatan:     helper.StringToStringNullable("Tanggal Kunjungan"),
				DeskripsiCatatan: tanggalKunjungan,
				// DeskripsiCatatan: data.TglKunjungan.Format("2 January 2006"),
			},
			{
				JudulCatatan:     helper.StringToStringNullable("Tidak Dapat Dihubungi"),
				DeskripsiCatatan: data.CatatanHubungi,
			},
		}

		detailCatatanTL = detailcatatantindaklanjut
		tanggalHubungi = nullValue
		statusKetertarikan = data.StatusKetertarikan
	}

	var res *ResponseDetailNasabahDitindaklanjuti
	res = &ResponseDetailNasabahDitindaklanjuti{
		DetailCard: DetailCard{
			CIF:                    data.CifNasabahRekan,
			KeteranganInformasi:    helper.StringToStringNullable("CIF"),
			NamaNasabahRekanan:     data.NamaNasabahRekan,
			Inisial:                &inisial,
			StatusTindakLanjut:     statusTindakLanjut,
			StatusKetertarikan:     statusKetertarikan,
			RatasSaldo:             helper.CurrencyFormat(data.RatasSaldoSimpananBri),
			TotalKreditKeseluruhan: helper.CurrencyFormat(data.TotalCreditKeseluruhan),
			TransaksiDebitNonBRI:   helper.CurrencyFormat(data.TotalDebitNonBri),
			// AkuisisiSimpanan:    data.AkuisisiSimpanan,
			// AkuisisiPinjaman:    data.AkuisisiPinjaman,
			AkuisisiSimpanan: statusAkuisisiSimpanan,
			AkuisisiPinjaman: statusAkuisisiPinjaman,
			SektorIndustri:   sektorEkonomi,
			TanggalHubungi:   tanggalHubungi,
		},
		DetailCatatanTindaklanjut: detailCatatanTL,
		DetailTier: DetailTier{
			PosisiTier: helper.StringToStringNullable(posisiTier),
		},
		DetailInfoPersonal: DetailInfoPersonal{
			// TanggalLahir:   data.TanggalLahir,
			Usia:           helper.StringToStringNullable(usia),
			JenisKelamin:   jenisKelamin,
			PhoneNumber:    phoneNumber,
			Pekerjaan:      pekerjaan,
			SektorEkonomi:  sektorEkonomi,
			AlamatPinjaman: alamatPinjaman,
			AlamatDomisili: alamatDomisili,
			AlamatKTP:      alamatId,
		},
		DetailInfoRekening: DetailInfoRekening{
			CIF:                   data.CifNasabahRekan,
			StatusRekening:        data.StatusRekening,
			NamaUnitKerja:         data.Mbdesc,
			ProdukBRIYangDimiliki: produkBriYangDimiliki,
			LayananDigital:        layananDigital,
		},
		DetailInfoTransaksi: DetailInfoTransaksi{
			TotalDebitKeseluruhan:      helper.CurrencyFormat(data.TotalDebitKeseluruhan),
			FrekuensiTransaksiDebit:    &frekuensiTransaksiDebit,
			TotalKreditKeseluruhan:     helper.CurrencyFormat(data.TotalCreditKeseluruhan),
			FrekuensiTransaksiKredit:   &frekuensiTransaksiKredit,
			TotalTransaksiDebitBRI:     helper.CurrencyFormat(data.TotalDebitBri),
			TotalTransaksiKreditBRI:    helper.CurrencyFormat(data.TotalCreditBri),
			TotalTransaksiDebitNonBRI:  helper.CurrencyFormat(data.TotalDebitNonBri),
			TotalTransaksiKreditNonBRI: helper.CurrencyFormat(data.TotalKreditNonBri),
		},
		DetailInfoSimpanan: &DetailInfoSimpanan{
			TierDiatasnya:      tierDiatasnya,
			AkuisisiSimpanan:   helper.CurrencyFormat(data.VolumePotensiSimpanan),
			RatasSaldoSimpanan: helper.CurrencyFormat(data.RatasSaldoSimpananBri),
			RMDiatasnya:        rmDiatasnya,
			LamaMenjadiNasabah: helper.StringToStringNullable(responseLamaNasabah),
		},
		DetailInfoPinjaman: &DetailInfoPinjaman{
			Kolektibilitas:   kolektabilitas,
			TotalOutstanding: helper.CurrencyFormat(data.TotalOutstanding),
			SegmenPinjaman:   segmen,
			AkuisisiPinjaman: helper.CurrencyFormat(data.VolumePotensiPinjaman),
			TotalPlafon:      helper.CurrencyFormat(data.TotalPlafon),
		},
	}

	return res
}

type ResponseInfoTierNasabah struct {
	PosisiTier  *string `json:"posisi_tier"`
	NamaNasabah *string `json:"nama_nasabah"`
	Inisial     *string `json:"inisial"`
	Keterangan  *string `json:"keterangan,omitempty"`
	NamaRM      *string `json:"nama_rm,omitempty"`
	Unit        *string `json:"unit,omitempty"`
	NoTelepon   *string `json:"no_telepon,omitempty"`
}

//MappingResponseInfoTierNasabah not used since v0.1.1
func MappingResponseInfoTierNasabah(data *GetDetailQueryStruct) *ResponseInfoTierNasabah {
	intTier := helper.Int64NullableToInt(data.PosisiTier)
	posisiTier := fmt.Sprintf("Tier %d", intTier)

	// nama := helper.StringNullableToString(data.NamaNasabahRekanan)
	// inisial := getInitialDetail(nama)

	var keterangan, namaRM, unit, noTelepon *string
	var inisial string

	if data.SnameRmSimpanan == nil {
		nama := helper.StringNullableToString(data.NamaNasabahRekan)
		inisial = getInitialDetail(nama)
		keterangan = helper.StringToStringNullableNullValue("")
		namaRM = helper.StringToStringNullableNullValue("")
		unit = helper.StringToStringNullableNullValue("")
		noTelepon = helper.StringToStringNullableNullValue("")
	} else {
		nama := helper.StringNullableToString(data.SnameRmSimpanan)
		inisial = getInitialDetail(nama)
		keterangan = helper.StringToStringNullable("Relationship Manager")
		namaRM = data.SnameRmSimpanan
		unit = data.Mbdesc
		noTelepon = data.PhonenumberRmPinjaman
	}

	res := &ResponseInfoTierNasabah{
		PosisiTier:  helper.StringToStringNullable(posisiTier),
		NamaNasabah: data.NamaNasabahRekan,
		Inisial:     helper.StringToStringNullable(inisial),
		Keterangan:  keterangan,
		NamaRM:      namaRM,
		Unit:        unit,
		NoTelepon:   noTelepon,
	}
	return res
}

type InfoTierCurrentNasabah struct {
	PosisiTier  *string `json:"posisi_tier"`
	NamaNasabah *string `json:"nama_nasabah"`
	Inisial     *string `json:"inisial_nasabah"`
}

func MappingInfoTierCurrentNasabah(data *GetDetailQueryStruct) *InfoTierCurrentNasabah {
	intTier := helper.Int64NullableToInt(data.PosisiTier)
	posisiTier := fmt.Sprintf("Tier %d", intTier)

	nama := helper.StringNullableToString(data.NamaNasabahRekan)
	inisial := getInitialDetail(nama)

	res := &InfoTierCurrentNasabah{

		PosisiTier:  helper.StringToStringNullable(posisiTier),
		NamaNasabah: data.NamaNasabahRekan,
		Inisial:     helper.StringToStringNullable(inisial),
	}
	return res
}

type ResponseInfoTierNasabahDto struct {
	HierarkiTierRM      []*ResponseInfoTierNasabah `json:"hierarki_tier_rm"`
	HierarkiTierNasabah []*InfoTierCurrentNasabah  `json:"hierarki_tier_nasabah"`
}

func MappingResponseInfoTierNasabahDto(dataTier []*ResponseInfoTierNasabah, dataNasabah []*InfoTierCurrentNasabah) *ResponseInfoTierNasabahDto {
	res := &ResponseInfoTierNasabahDto{
		HierarkiTierRM:      dataTier,
		HierarkiTierNasabah: dataNasabah,
	}
	return res
}

func MappingResponseInfoTierNasabahInRow(data *GetDetailQueryStruct) []*ResponseInfoTierNasabah {
	namarm1 := helper.StringNullableToString(data.SnameRmPinjamanTier1)
	namarm2 := helper.StringNullableToString(data.SnameRmPinjamanTier2)
	namarm3 := helper.StringNullableToString(data.SnameRmPinjamanTier3)
	namarm4 := helper.StringNullableToString(data.SnameRmPinjamanTier4)

	namanasabaht1 := helper.StringNullableToString(data.NamaNasabahTier1)
	namanasabaht2 := helper.StringNullableToString(data.NamaNasabahTier2)
	namanasabaht3 := helper.StringNullableToString(data.NamaNasabahTier3)
	namanasabaht4 := helper.StringNullableToString(data.NamaNasabahTier4)

	// var keterangan, namaUnit, namaRM, nomorRM *string

	var inisialrm1, inisialrm2, inisialrm3, inisialrm4 *string
	var unitrm1, unitrm2, unitrm3, unitrm4 *string
	var namarmt1, namarmt2, namarmt3, namarmt4 *string
	var phonenumberrmt1, phonenumberrmt2, phonenumberrmt3, phonenumberrmt4 *string
	var keteranganrm1, keteranganrm2, keteranganrm3, keteranganrm4 *string

	tierCurrentNasabah := helper.Int64NullableToInt(data.PosisiTier)
	switch tierCurrentNasabah {
	case 5:
		if data.SnameRmPinjamanTier4 == nil || namarm4 == "" {
			inisialrm4 = helper.StringToStringNullable(getInitialDetail(namanasabaht4))
			unitrm4 = nil
			keteranganrm4 = nil
			namarmt4 = nil
			phonenumberrmt4 = nil
		} else {
			inisialrm4 = helper.StringToStringNullable(getInitialDetail(namarm4))
			unitrm4 = data.UnitRmTier4
			keteranganrm4 = helper.StringToStringNullable("Relationship Manager")
			namarmt4 = data.SnameRmPinjamanTier4
			phonenumberrmt4 = data.PhonenumberTier4
		}

		if data.SnameRmPinjamanTier3 == nil || namarm3 == "" {
			inisialrm3 = helper.StringToStringNullable(getInitialDetail(namanasabaht3))
			unitrm3 = nil
			keteranganrm3 = nil
			namarmt3 = nil
			phonenumberrmt3 = nil
		} else {
			inisialrm3 = helper.StringToStringNullable(getInitialDetail(namarm3))
			unitrm3 = data.UnitRmTier3
			keteranganrm3 = helper.StringToStringNullable("Relationship Manager")
			namarmt3 = data.SnameRmPinjamanTier3
			phonenumberrmt3 = data.PhonenumberTier3
		}

		if data.SnameRmPinjamanTier2 == nil || namarm2 == "" {
			inisialrm2 = helper.StringToStringNullable(getInitialDetail(namanasabaht2))
			unitrm2 = nil
			keteranganrm2 = nil
			namarmt2 = nil
			phonenumberrmt2 = nil
		} else {
			inisialrm2 = helper.StringToStringNullable(getInitialDetail(namarm2))
			unitrm2 = data.UnitRmTier2
			keteranganrm2 = helper.StringToStringNullable("Relationship Manager")
			namarmt2 = data.SnameRmPinjamanTier2
			phonenumberrmt2 = data.PhonenumberTier2
		}

		if data.SnameRmPinjamanTier1 == nil || namarm1 == "" {
			inisialrm1 = helper.StringToStringNullable(getInitialDetail(namanasabaht1))
			unitrm1 = nil
			keteranganrm1 = nil
			namarmt1 = nil
			phonenumberrmt1 = nil
		} else {
			inisialrm1 = helper.StringToStringNullable(getInitialDetail(namarm1))
			unitrm1 = data.UnitRmTier1
			keteranganrm1 = helper.StringToStringNullable("Relationship Manager")
			namarmt1 = data.SnameRmPinjamanTier1
			phonenumberrmt1 = data.PhonenumberRmTier1
		}

	case 4:
		// inisialrm4 = helper.StringToStringNullableNullValue("")
		if data.SnameRmPinjamanTier3 == nil || namarm3 == "" {
			inisialrm3 = helper.StringToStringNullable(getInitialDetail(namanasabaht3))
			unitrm3 = nil
			keteranganrm3 = nil
			namarmt3 = nil
			phonenumberrmt3 = nil
		} else {
			inisialrm3 = helper.StringToStringNullable(getInitialDetail(namarm3))
			unitrm3 = data.UnitRmTier3
			keteranganrm3 = helper.StringToStringNullable("Relationship Manager")
			namarmt3 = data.SnameRmPinjamanTier3
			phonenumberrmt3 = data.PhonenumberTier3
		}

		if data.SnameRmPinjamanTier2 == nil || namarm2 == "" {
			inisialrm2 = helper.StringToStringNullable(getInitialDetail(namanasabaht2))
			unitrm2 = nil
			keteranganrm2 = nil
			namarmt2 = nil
			phonenumberrmt2 = nil
		} else {
			inisialrm2 = helper.StringToStringNullable(getInitialDetail(namarm2))
			unitrm2 = data.UnitRmTier2
			keteranganrm2 = helper.StringToStringNullable("Relationship Manager")
			namarmt2 = data.SnameRmPinjamanTier2
			phonenumberrmt2 = data.PhonenumberTier2
		}

		if data.SnameRmPinjamanTier1 == nil || namarm1 == "" {
			inisialrm1 = helper.StringToStringNullable(getInitialDetail(namanasabaht1))
			unitrm1 = nil
			keteranganrm1 = nil
			namarmt1 = nil
			phonenumberrmt1 = nil
		} else {
			inisialrm1 = helper.StringToStringNullable(getInitialDetail(namarm1))
			unitrm1 = data.UnitRmTier1
			keteranganrm1 = helper.StringToStringNullable("Relationship Manager")
			namarmt1 = data.SnameRmPinjamanTier1
			phonenumberrmt1 = data.PhonenumberRmTier1
		}
	case 3:
		// inisialrm4 = helper.StringToStringNullableNullValue("")
		// inisialrm3 = helper.StringToStringNullableNullValue("")
		if data.SnameRmPinjamanTier2 == nil || namarm2 == "" {
			inisialrm2 = helper.StringToStringNullable(getInitialDetail(namanasabaht2))
			unitrm2 = nil
			keteranganrm2 = nil
			namarmt2 = nil
			phonenumberrmt2 = nil
		} else {
			inisialrm2 = helper.StringToStringNullable(getInitialDetail(namarm2))
			unitrm2 = data.UnitRmTier2
			keteranganrm2 = helper.StringToStringNullable("Relationship Manager")
			namarmt2 = data.SnameRmPinjamanTier2
			phonenumberrmt2 = data.PhonenumberTier2
		}

		if data.SnameRmPinjamanTier1 == nil || namarm1 == "" {
			inisialrm1 = helper.StringToStringNullable(getInitialDetail(namanasabaht1))
			unitrm1 = nil
			keteranganrm1 = nil
			namarmt1 = nil
			phonenumberrmt1 = nil
		} else {
			inisialrm1 = helper.StringToStringNullable(getInitialDetail(namarm1))
			unitrm1 = data.UnitRmTier1
			keteranganrm1 = helper.StringToStringNullable("Relationship Manager")
			namarmt1 = data.SnameRmPinjamanTier1
			phonenumberrmt1 = data.PhonenumberRmTier1
		}
	case 2:
		// inisialrm4 = helper.StringToStringNullableNullValue("")
		// inisialrm3 = helper.StringToStringNullableNullValue("")
		// inisialrm2 = helper.StringToStringNullableNullValue("")
		if data.SnameRmPinjamanTier1 == nil || namarm1 == "" {
			inisialrm1 = helper.StringToStringNullable(getInitialDetail(namanasabaht1))
			unitrm1 = nil
			keteranganrm1 = nil
			namarmt1 = nil
			phonenumberrmt1 = nil
		} else {
			inisialrm1 = helper.StringToStringNullable(getInitialDetail(namarm1))
			unitrm1 = data.UnitRmTier1
			keteranganrm1 = helper.StringToStringNullable("Relationship Manager")
			namarmt1 = data.SnameRmPinjamanTier1
			phonenumberrmt1 = data.PhonenumberRmTier1
		}
	case 1:
		inisialrm4 = helper.StringToStringNullableNullValue("")
		inisialrm3 = helper.StringToStringNullableNullValue("")
		inisialrm2 = helper.StringToStringNullableNullValue("")
		inisialrm1 = helper.StringToStringNullableNullValue("")
	}
	// inisial := getInitialDetail(nama)
	t1 := &ResponseInfoTierNasabah{
		PosisiTier:  helper.StringToStringNullable("Tier 1"),
		NamaNasabah: data.NamaNasabahTier1,
		Inisial:     inisialrm1,
		Keterangan:  keteranganrm1,
		NamaRM:      namarmt1,
		Unit:        unitrm1,
		NoTelepon:   phonenumberrmt1,
	}

	t2 := &ResponseInfoTierNasabah{
		PosisiTier:  helper.StringToStringNullable("Tier 2"),
		NamaNasabah: data.NamaNasabahTier2,
		Inisial:     inisialrm2,
		Keterangan:  keteranganrm2,
		NamaRM:      namarmt2,
		Unit:        unitrm2,
		NoTelepon:   phonenumberrmt2,
	}

	t3 := &ResponseInfoTierNasabah{
		PosisiTier:  helper.StringToStringNullable("Tier 3"),
		NamaNasabah: data.NamaNasabahTier3,
		Inisial:     inisialrm3,
		Keterangan:  keteranganrm3,
		NamaRM:      namarmt3,
		Unit:        unitrm3,
		NoTelepon:   phonenumberrmt3,
	}

	t4 := &ResponseInfoTierNasabah{
		PosisiTier:  helper.StringToStringNullable("Tier 4"),
		NamaNasabah: data.NamaNasabahTier4,
		Inisial:     inisialrm4,
		Keterangan:  keteranganrm4,
		NamaRM:      namarmt4,
		Unit:        unitrm4,
		NoTelepon:   phonenumberrmt4,
	}

	dataTier := make([]*ResponseInfoTierNasabah, 0)
	switch tierCurrentNasabah {
	case 5:
		dataTier = append(dataTier, t1, t2, t3, t4)
	case 4:
		dataTier = append(dataTier, t1, t2, t3)
	case 3:
		dataTier = append(dataTier, t1, t2)
	case 2:
		dataTier = append(dataTier, t1)
	case 1:
		dataTier = append(dataTier)
	}
	// dataTier = append(dataTier, t1, t2, t3, t4)
	return dataTier
}

type TanggalUpdateStruct struct {
	TanggalUpdate *time.Time `json:"updated_date" gorm:"column:UPDATED_DATE"`
}
