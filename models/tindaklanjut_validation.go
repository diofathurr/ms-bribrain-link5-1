package models

import (
	"ms-bribrain-link5/helper"
	models2 "ms-bribrain-link5/helper/models"
)

func (request *RequestSubmitTindaklanjutPositive1) MappingToGlobalValidation() models2.GlobalValidation {
	res := models2.GlobalValidation{
		RequiredValidation: []models2.RequiredValidation{
			models2.RequiredValidation{
				Key:   "cif",
				Value: helper.StringNullableToString(request.Cif),
			},
			models2.RequiredValidation{
				Key:   "kode_hubungi",
				Value: helper.StringNullableToString(request.KodeHubungi),
			},
			models2.RequiredValidation{
				Key:   "kode_kunjungan",
				Value: helper.StringNullableToString(request.KodeKunjungan),
			},
			models2.RequiredValidation{
				Key:   "kode_ketertarikan",
				Value: helper.StringNullableToString(request.KodeKetertarikan),
			},
			models2.RequiredValidation{
				Key:   "catatan_ketertarikan",
				Value: helper.StringNullableToString(request.VolumePotensiRmPinjaman),
			},
			models2.RequiredValidation{
				Key:   "tanggal_hubungi",
				Value: helper.StringNullableToString(request.TglHubungi),
			},
			models2.RequiredValidation{
				Key:   "tanggal_kunjungan",
				Value: helper.StringNullableToString(request.TglKunjungan),
			},
		},
		DataTypeNumberIntValidation: []models2.DataTypeNumberIntValidation{
			models2.DataTypeNumberIntValidation{
				// Key:   "id_rekomendasi",
				// Value: helper.IntNullableToString(request.IdRekomendasi),
			},
		},
		PageLimitValidation:    []models2.PageLimitValidation{},
		MaxMinNumberValidation: []models2.MaxMinNumberValidation{},
	}
	return res
}
