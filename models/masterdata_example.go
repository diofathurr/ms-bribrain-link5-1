package models

func (a OnboardingText) MappingExampleDataNil() OnboardingText {
	res := OnboardingText{
		OnBoardingTitle:       "title",
		OnBoardingDescription: "description",
	}

	return res
}

func (a PilihanAlasan) MappingExampleData() PilihanAlasan {
	res := PilihanAlasan{
		Kode:      "1",
		Deskripsi: "deskripsi",
	}

	return res
}
