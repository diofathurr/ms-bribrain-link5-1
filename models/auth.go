package models

import (
	"os"

	"github.com/dgrijalva/jwt-go"
)

// BRIBrainClaims for JWT token
type BRIBrainClaims struct {
	User
	ExpiredToken
	jwt.StandardClaims
}

type ExpiredToken struct {
	ExpiredToken int64 `json:"expired_token,omitempty"`
}

type User struct {
	Pernr        string  `json:"pernr" gorm:"column:PERNR"`
	Sname        string  `json:"sname" gorm:"column:SNAME"`
	Role         string  `json:"role" gorm:"column:ROLE"`
	Description1 string  `json:"description_1" gorm:"column:DESCRIPTION_1"`
	Description2 string  `json:"description_2" gorm:"column:DESCRIPTION_2"`
	OrgehTx      string  `json:"orgeh_tx" gorm:"column:ORGEH_TX"`
	StellTx      string  `json:"stell_tx" gorm:"column:STELL_TX"`
	Rgdesc       string  `json:"rgdesc" gorm:"column:RGDESC"`
	Mbdesc       string  `json:"mbdesc" gorm:"column:MBDESC"`
	Brdesc       string  `json:"brdesc" gorm:"column:BRDESC"`
	Region       string  `json:"region" gorm:"column:REGION"`
	Mainbranch   float64 `json:"mainbr" gorm:"column:MAINBR"`
	Branch       float64 `json:"branch" gorm:"column:BRANCH"`
	JenisKelamin string  `json:"jenis_kelamin" gorm:"column:JENIS_KELAMIN"`
	AccessLevel  string  `json:"access_level" gorm:"column:ACCESS_LEVEL"`
	PnKecamatan  string  `json:"pn_kecamatan" gorm:"column:PN_KECAMATAN"`
}

type ValidateAccess struct {
	Roles []string `json:"roles"`
}

type BribrainAuditTrail struct {
	User    string `json:"user" gorm:"user"`
	Role    string `json:"role" gorm:"role"`
	Service string `json:"service" gorm:"service"`
	Action  string `json:"action" gorm:"action"`
	Remark  string `json:"remark" gorm:"remark"`
	Payload string `json:"payload" gorm:"payload"`
}

type PayloadBribrainAuditTrail struct {
	Url          string                           `json:"url"`
	Headers      HeadersPayloadBribrainAuditTrail `json:"headers"`
	BodyRequest  string                           `json:"body_request"`
	BodyResponse string                           `json:"body_response"`
	StatusCode   int                              `json:"status_code"`
}
type HeadersPayloadBribrainAuditTrail struct {
	ContentType   string `json:"content_type"`
	Authorization string `json:"authorization"`
}

func (BribrainAuditTrail) MappingBribrainAuditTrail(action string, remark string, user, role string, payload string) BribrainAuditTrail {
	res := BribrainAuditTrail{
		User:    user,
		Role:    role,
		Service: os.Getenv("APP_NAME"),
		Action:  action,
		Remark:  remark,
		Payload: payload,
	}
	return res
}

func (PayloadBribrainAuditTrail) MappingPayloadBribrainAuditTrail(url string, headers HeadersPayloadBribrainAuditTrail, bodyrequest, bodyresponse string, statuscode int) PayloadBribrainAuditTrail {
	res := PayloadBribrainAuditTrail{
		Url:          url,
		Headers:      headers,
		BodyRequest:  bodyrequest,
		BodyResponse: bodyresponse,
		StatusCode:   statuscode,
	}
	return res
}

func (HeadersPayloadBribrainAuditTrail) MappingHeadersPayloadBribrainAuditTrail(contentype string, authorization string) HeadersPayloadBribrainAuditTrail {
	res := HeadersPayloadBribrainAuditTrail{
		ContentType:   contentype,
		Authorization: authorization,
	}
	return res
}
