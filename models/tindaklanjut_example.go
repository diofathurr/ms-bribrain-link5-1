package models

import "ms-bribrain-link5/helper"

func (a *RequestSubmitTindaklanjutPositive1) MappingExampleData() *RequestSubmitTindaklanjutPositive1 {
	res := &RequestSubmitTindaklanjutPositive1{
		Cif:                     helper.StringToStringNullable("ABC2345"),
		KodeHubungi:             helper.StringToStringNullable("1"),
		KodeKunjungan:           helper.StringToStringNullable("1"),
		KodeKetertarikan:        helper.StringToStringNullable("1"),
		TglHubungi:              helper.StringToStringNullable("2006-01-02"),
		TglKunjungan:            helper.StringToStringNullable("2006-01-02"),
		VolumePotensiRmSimpanan: helper.StringToStringNullable("500000"),
		VolumePotensiRmPinjaman: helper.StringToStringNullable("500000"),
	}
	return res
}

func (a *RequestSubmitTindaklanjutPositive1) MappingExampleDataBadRequest() *RequestSubmitTindaklanjutPositive1 {
	res := &RequestSubmitTindaklanjutPositive1{
		Cif:                     helper.StringToStringNullable("ABC2345"),
		KodeHubungi:             helper.StringToStringNullable("A"),
		KodeKunjungan:           helper.StringToStringNullable("1"),
		KodeKetertarikan:        helper.StringToStringNullable("1"),
		TglHubungi:              helper.StringToStringNullable("2006-01-02"),
		TglKunjungan:            helper.StringToStringNullable("2006-01-02"),
		VolumePotensiRmSimpanan: helper.StringToStringNullable("500000"),
		VolumePotensiRmPinjaman: helper.StringToStringNullable("500000"),
	}
	return res
}

func (a *RequestSubmitTindaklanjutPositive2) MappingExampleData() *RequestSubmitTindaklanjutPositive2 {
	res := &RequestSubmitTindaklanjutPositive2{
		Cif:                     helper.StringToStringNullable("ABC2345"),
		KodeHubungi:             helper.StringToStringNullable("1"),
		KodeKunjungan:           helper.StringToStringNullable("2"),
		KodeKetertarikan:        helper.StringToStringNullable("1"),
		TglHubungi:              helper.StringToStringNullable("2006-01-02"),
		VolumePotensiRmSimpanan: helper.StringToStringNullable("500000"),
		VolumePotensiRmPinjaman: helper.StringToStringNullable("500000"),
		CatatanKunjungan:        helper.StringToStringNullable("catatan kunjungan"),
	}
	return res
}

func (a *RequestSubmitTindaklanjutPositive2) MappingExampleDataBadRequest() *RequestSubmitTindaklanjutPositive2 {
	res := &RequestSubmitTindaklanjutPositive2{
		Cif:                     helper.StringToStringNullable("ABC2345"),
		KodeHubungi:             helper.StringToStringNullable("A"),
		KodeKunjungan:           helper.StringToStringNullable("2"),
		KodeKetertarikan:        helper.StringToStringNullable("1"),
		TglHubungi:              helper.StringToStringNullable("2006-01-02"),
		VolumePotensiRmSimpanan: helper.StringToStringNullable("500000"),
		VolumePotensiRmPinjaman: helper.StringToStringNullable("500000"),
		CatatanKunjungan:        helper.StringToStringNullable("catatan kunjungan"),
	}
	return res
}

func (a *RequestSubmitTindaklanjutNegative1) MappingExampleData() *RequestSubmitTindaklanjutNegative1 {
	res := &RequestSubmitTindaklanjutNegative1{
		Cif:                 helper.StringToStringNullable("ABC2345"),
		KodeHubungi:         helper.StringToStringNullable("1"),
		KodeKetertarikan:    helper.StringToStringNullable("2"),
		TglHubungi:          helper.StringToStringNullable("2006-01-02"),
		CatatanKetertarikan: helper.StringToStringNullable("catatan ketertarikan"),
	}
	return res
}

func (a *RequestSubmitTindaklanjutNegative1) MappingExampleDataBadRequest() *RequestSubmitTindaklanjutNegative1 {
	res := &RequestSubmitTindaklanjutNegative1{
		Cif:                 helper.StringToStringNullable("ABC2345"),
		KodeHubungi:         helper.StringToStringNullable("A"),
		KodeKetertarikan:    helper.StringToStringNullable("2"),
		TglHubungi:          helper.StringToStringNullable("2006-01-02"),
		CatatanKetertarikan: helper.StringToStringNullable("catatan ketertarikan"),
	}
	return res
}

func (a *RequestSubmitTindaklanjutNegative2) MappingExampleData() *RequestSubmitTindaklanjutNegative2 {
	res := &RequestSubmitTindaklanjutNegative2{
		Cif:              helper.StringToStringNullable("ABC2345"),
		KodeHubungi:      helper.StringToStringNullable("2"),
		KodeKunjungan:    helper.StringToStringNullable("2"),
		CatatanHubungi:   helper.StringToStringNullable("catatan hubungi"),
		CatatanKunjungan: helper.StringToStringNullable("catatan kunjungan"),
	}
	return res
}

func (a *RequestSubmitTindaklanjutNegative2) MappingExampleDataBadRequest() *RequestSubmitTindaklanjutNegative2 {
	res := &RequestSubmitTindaklanjutNegative2{
		Cif:              helper.StringToStringNullable("ABC2345"),
		KodeHubungi:      helper.StringToStringNullable("A"),
		KodeKunjungan:    helper.StringToStringNullable("2"),
		CatatanHubungi:   helper.StringToStringNullable("catatan hubungi"),
		CatatanKunjungan: helper.StringToStringNullable("catatan kunjungan"),
	}
	return res
}

func (a *RequestSubmitTindaklanjutNegative3) MappingExampleData() *RequestSubmitTindaklanjutNegative3 {
	res := &RequestSubmitTindaklanjutNegative3{
		Cif:            helper.StringToStringNullable("ABC2345"),
		KodeHubungi:    helper.StringToStringNullable("3"),
		CatatanHubungi: helper.StringToStringNullable("catatan hubungi"),
	}
	return res
}

func (a *RequestSubmitTindaklanjutNegative3) MappingExampleDataBadRequest() *RequestSubmitTindaklanjutNegative3 {
	res := &RequestSubmitTindaklanjutNegative3{
		Cif:            helper.StringToStringNullable("ABC2345"),
		KodeHubungi:    helper.StringToStringNullable("A"),
		CatatanHubungi: helper.StringToStringNullable("catatan hubungi"),
	}
	return res
}

func (a *RequestSubmitTindaklanjutOther) MappingExampleData() *RequestSubmitTindaklanjutOther {
	res := &RequestSubmitTindaklanjutOther{
		Cif:              helper.StringToStringNullable("ABC2345"),
		KodeHubungi:      helper.StringToStringNullable("2"),
		KodeKunjungan:    helper.StringToStringNullable("1"),
		KodeKetertarikan: helper.StringToStringNullable("3"),
		CatatanHubungi:   helper.StringToStringNullable("catatan hubungi"),
		TglKunjungan:     helper.StringToStringNullable("2006-01-02"),
	}
	return res
}

func (a *RequestSubmitTindaklanjutOther) MappingExampleDataBadRequest() *RequestSubmitTindaklanjutOther {
	res := &RequestSubmitTindaklanjutOther{
		Cif:              helper.StringToStringNullable("ABC2345"),
		KodeHubungi:      helper.StringToStringNullable("A"),
		KodeKunjungan:    helper.StringToStringNullable("1"),
		KodeKetertarikan: helper.StringToStringNullable("3"),
		CatatanHubungi:   helper.StringToStringNullable("catatan hubungi"),
		TglKunjungan:     helper.StringToStringNullable("2006-01-02"),
	}
	return res
}

func (a *RequestSubmitTindaklanjutTandai) MappingExampleData() *RequestSubmitTindaklanjutTandai {
	res := &RequestSubmitTindaklanjutTandai{
		Cif:                     helper.StringToStringNullable("ABC2345"),
		KodeKunjungan:           helper.StringToStringNullable("3"),
		KodeKetertarikan:        helper.StringToStringNullable("1"),
		VolumePotensiRmSimpanan: helper.StringToStringNullable("500000"),
		VolumePotensiRmPinjaman: helper.StringToStringNullable("500000"),
		CatatanKetertarikan:     "catatan ketertarikan",
		// CatatanKetertarikan:     helper.StringToStringNullable("catatan ketertarikan"),
	}
	return res
}

func (a *RequestSubmitTindaklanjutTandai) MappingExampleDataBadRequest() *RequestSubmitTindaklanjutTandai {
	res := &RequestSubmitTindaklanjutTandai{
		Cif:                     helper.StringToStringNullable("ABC2345"),
		KodeKunjungan:           helper.StringToStringNullable("A"),
		KodeKetertarikan:        helper.StringToStringNullable("1"),
		VolumePotensiRmSimpanan: helper.StringToStringNullable("500000"),
		VolumePotensiRmPinjaman: helper.StringToStringNullable("500000"),
		CatatanKetertarikan:     "catatan ketertarikan",
		// CatatanKetertarikan:     helper.StringToStringNullable("catatan ketertarikan"),
	}
	return res
}

func (a *RequestSubmitTindaklanjutNonBri) MappingExampleData() *RequestSubmitTindaklanjutNonBri {
	res := &RequestSubmitTindaklanjutNonBri{
		KodeNonBri:        helper.StringToStringNullable("NONBRI6546"),
		KodeHubungi:       "1",
		TglHubungi:        "2006-01-02",
		CatatanHubungi:    "aa",
		NoTelepon:         "0856565656",
		Alamat:            "Jalan example No. 1",
		KodeCabangReferal: "345",
	}
	return res
}

func (a *RequestSubmitTindaklanjutNonBri) MappingExampleDataTLSendiri() *RequestSubmitTindaklanjutNonBri {
	res := &RequestSubmitTindaklanjutNonBri{
		KodeNonBri:     helper.StringToStringNullable("NONBRI6546"),
		KodeHubungi:    "1",
		TglHubungi:     "2006-01-02",
		CatatanHubungi: "aa",
		NoTelepon:      "0856565656",
		Alamat:         "Jalan example No. 1",
		// KodeCabangReferal: "345",
	}
	return res
}

func (a *RequestSubmitTindaklanjutNonBri) MappingExampleDataBadRequest() *RequestSubmitTindaklanjutNonBri {
	res := &RequestSubmitTindaklanjutNonBri{
		KodeNonBri:     helper.StringToStringNullable("NONBRI6546"),
		KodeHubungi:    "4",
		TglHubungi:     "2006-01-02",
		CatatanHubungi: "aa",
	}
	return res
}

func (a *RequestSubmitTindaklanjutNonBri) MappingExampleDataCase2() *RequestSubmitTindaklanjutNonBri {
	res := &RequestSubmitTindaklanjutNonBri{
		KodeNonBri:     helper.StringToStringNullable("NONBRI6546"),
		KodeHubungi:    "2",
		TglHubungi:     "2006-01-02",
		CatatanHubungi: "aa",
	}
	return res
}
