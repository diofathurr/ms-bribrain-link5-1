package models

import (
	"os"
)

var stringTableOnboarding = "TABLE_apps_onboarding"
var stringTableNasabahBRI = "TABLE_bribrain_link5_dashboard_detail_informasi_nasabah_bri"
var stringTableNasabahNonBRI = "TABLE_bribrain_link5_dashboard_detail_informasi_nasabah_non_bri"
var stringTableStatusTindaklanjut = "TABLE_bribrain_link5_status_tindaklanjut"
var stringTableMasterOpsiTindaklanjut = "TABLE_bribrain_link5_master_opsi_tindaklanjut"
var stringTableUserFilter = "TABLE_bribrain_link5_user_filter"
var stringTableReferal = "TABLE_bribrain_link5_referal"
var stringTableSystemVariable = "TABLE_bribrain_link5_system_variable"
var stringTableMasterWilayah = "TABLE_bribrain_link5_master_wilayah"

type MasterDataTable struct {
	AppsOnboarding                      *AppsOnboardingTable
	BribrainLink5MasterOpsiTindaklanjut *BribrainLink5MasterOpsiTindaklanjutTable
	BribrainLink5UserFilter             *BribrainLink5UserFilterTable
	BribrainLink5SystemVariable         *BribrainLink5SystemVariableTable
	BribrainLink5MasterWilayah          *BribrainLink5MasterWilayahTable
}

type NasabahTable struct {
	BribrainLink5DashboardDetailInformasiNasabahBRI    *BribrainLink5DashboardDetailInformasiNasabahBRITable
	BribrainLink5DashboardDetailInformasiNasabahNonBRI *BribrainLink5DashboardDetailInformasiNasabahNonBRITable
	BribrainLink5StatusTindaklanjut                    *BribrainLink5StatusTindaklanjutTable
	BribrainLink5Referal                               *BribrainLink5ReferalTable
}

//mapping
func (*MasterDataTable) MappingTable() *MasterDataTable {
	res := &MasterDataTable{
		AppsOnboarding:                      &AppsOnboardingTable{},
		BribrainLink5MasterOpsiTindaklanjut: &BribrainLink5MasterOpsiTindaklanjutTable{},
	}
	res.AppsOnboarding = res.AppsOnboarding.MappingTable()
	res.BribrainLink5MasterOpsiTindaklanjut = res.BribrainLink5MasterOpsiTindaklanjut.MappingTable()
	res.BribrainLink5UserFilter = res.BribrainLink5UserFilter.MappingTable()
	res.BribrainLink5SystemVariable = res.BribrainLink5SystemVariable.MappingTable()
	res.BribrainLink5MasterWilayah = res.BribrainLink5MasterWilayah.MappingTable()

	return res
}

func (*NasabahTable) MappingTable() *NasabahTable {
	res := &NasabahTable{
		BribrainLink5DashboardDetailInformasiNasabahBRI:    &BribrainLink5DashboardDetailInformasiNasabahBRITable{},
		BribrainLink5DashboardDetailInformasiNasabahNonBRI: &BribrainLink5DashboardDetailInformasiNasabahNonBRITable{},
		BribrainLink5StatusTindaklanjut:                    &BribrainLink5StatusTindaklanjutTable{},
		BribrainLink5Referal:                               &BribrainLink5ReferalTable{},
	}
	res.BribrainLink5DashboardDetailInformasiNasabahBRI = res.BribrainLink5DashboardDetailInformasiNasabahBRI.MappingTable()
	res.BribrainLink5DashboardDetailInformasiNasabahNonBRI = res.BribrainLink5DashboardDetailInformasiNasabahNonBRI.MappingTable()
	res.BribrainLink5StatusTindaklanjut = res.BribrainLink5StatusTindaklanjut.MappingTable()
	res.BribrainLink5Referal = res.BribrainLink5Referal.MappingTable()

	return res
}

type BribrainLink5MasterOpsiTindaklanjutTable struct {
	Table            string
	OpsiTindaklanjut string
	Kode             string
	Deskripsi        string
}

type BribrainLink5MasterWilayahTable struct {
	Table         string
	KodeProvinsi  string
	Provinsi      string
	KodeKota      string
	Kota          string
	KodeKecamatan string
	Kecamatan     string
	KodeKelurahan string
	Kelurahan     string
}

type BribrainLink5UserFilterTable struct {
	Table         string
	ParentCode    string
	ParentName    string
	KodeUnitKerja string
	UnitKerja     string
	TipeUker      string
}
type AppsOnboardingTable struct {
	Table                 string
	ProductID             string
	ProductName           string
	OnBoardingTitle       string
	OnBoardingDescription string
}

type BribrainLink5SystemVariableTable struct {
	Table    string
	Variable string
	Value    string
}

type BribrainLink5DashboardDetailInformasiNasabahBRITable struct {
	Table                  string
	CifNasabahRekan        string
	NamaNasabahRekan       string
	NamaNasabahUtama       string
	PosisiTier             string
	Usia                   string
	NomorKtp               string
	PnRm                   string
	JenisKelamin           string
	SektorEkonomi          string
	TierDiatasnya          string
	RmDiatasnya            string
	SegmenPinjaman         string
	Branch                 string
	Rgdesc                 string
	Mbdesc                 string
	Sbdesc                 string
	Brdesc                 string
	Phonenumber            string
	AlamatId               string
	AlamatDomisili         string
	AlamatPinjaman         string
	UsiaRekeningBulan      string
	ProdukSimpanan         string
	SnameRmSimpanan        string
	ProdukPinjaman         string
	SnameRmPinjaman        string
	UnitRmPinjaman         string
	FlagPinjaman           string
	Segmen                 string
	PhonenumberRmPinjaman  string
	GroupUsaha             string
	JenisPekerjaan         string
	BidangPekerjaan        string
	Kolektabilitas         string
	TotalPlafon            string
	TotalOutstanding       string
	InetBanking            string
	SmsBanking             string
	TotalCreditBri         string
	TotalDebitBri          string
	TotalKreditNonBri      string
	TotalDebitNonBri       string
	TotalKreditDebitNonBri string
	NamaNasabahNonBri      string
	NamaBankLawan          string
	RatasSaldoSimpananBri  string
	TotalCreditKeseluruhan string
	TotalDebitKeseluruhan  string
	CifNasabahTier1        string
	FlagPinjamanTier1      string
	NamaNasabahTier1       string
	SnameRmPinjamanTier1   string
	UnitRmTier1            string
	PhonenumberRmTier1     string
	CifNasabahTier2        string
	FlagPinjamanTier2      string
	NamaNasabahTier2       string
	SnameRmPinjamanTier2   string
	UnitRmTier2            string
	PhonenumberTier2       string
	CifNasabahTier3        string
	FlagPinjamanTier3      string
	NamaNasabahTier3       string
	SnameRmPinjamanTier3   string
	UnitRmTier3            string
	PhonenumberTier3       string
	CifNasabahTier4        string
	FlagPinjamanTier4      string
	NamaNasabahTier4       string
	SnameRmPinjamanTier4   string
	UnitRmTier4            string
	PhonenumberTier4       string
	StatusRekening         string
	AkuisisiSimpanan       string
	AkuisisiPinjaman       string
	LamaNasabahBulan       string
	TanggalLahir           string
	TotalMutasiDebit       string
	TotalMutasiKredit      string
	Ds                     string
	CreatedDate            string
	UpdatedDate            string
	IdStatus               string
	StatusTindaklanjut     string
	TopupSimpanan          string
	SuplesiPinjaman        string
	VolumePotensiPinjaman  string
	VolumePotensiSimpanan  string
	Peringkat              string
	FreqCreditKeseluruhan  string
	FreqDebitKeseluruhan   string
}

type BribrainLink5DashboardDetailInformasiNasabahNonBRITable struct {
	Table              string
	KodeNonBri         string
	NamaNasabahNonBri  string
	NamaBankLawan      string
	CifNasabahRekan    string
	NamaNasabahRekan   string
	Phonenumber        string
	TotalDebitNonBri   string
	TotalCreditNonBri  string
	TipeNasabah        string
	Ds                 string
	CreatedDate        string
	UpdatedDate        string
	PnRm               string
	IdStatus           string
	StatusTindaklanjut string
	AkuisisiSimpanan   string
	Peringkat          string
	FreqDebitNonBri    string
	FreqCreditNonBri   string
	NamaRm             string
	BranchRm           string
	PhonenumberRm      string
	KodeRegion         string
	Rgdesc             string
	KodeMainbranch     string
	Mbdesc             string
	InputAlamat        string
	InputPhonenumber   string
}

type BribrainLink5StatusTindaklanjutTable struct {
	Table                   string
	Id                      string
	CifnoRacer              string
	Cifno                   string
	KodeNonBri              string
	TipeCustomer            string
	StatusHubungi           string
	KodeHubungi             string
	TglHubungi              string
	CatatanHubungi          string
	StatusKetertarikan      string
	KodeKetertarikan        string
	CatatanKetertarikan     string
	StatusKunjungan         string
	KodeKunjungan           string
	TglKunjungan            string
	CatatanKunjungan        string
	CreatedDate             string
	UpdatedDate             string
	VolumePotensiRmPinjaman string
	VolumePotensiRmSimpanan string
	InputPotensiRmPinjaman  string
	InputPotensiRmSimpanan  string
	RatasSaldo              string
	TotalKredit             string
	TotalDebit              string
	PnRm                    string
	NamaNasabah             string
	InputNorek              string
	KodeRegion              string
	Rgdesc                  string
	KodeMainbranch          string
	Mbdesc                  string
}

type BribrainLink5ReferalTable struct {
	Table                 string
	KodeNonBri            string
	PnRmPengirim          string
	PnRmPenerima          string
	MainbranchCodeSebelum string
	MainbranchCodeSesudah string
	Alamat                string
	Phonenumber           string
	CreatedDate           string
}

func (*AppsOnboardingTable) MappingTable() *AppsOnboardingTable {
	res := &AppsOnboardingTable{
		Table:                 os.Getenv(stringTableOnboarding),
		ProductID:             os.Getenv(stringTableOnboarding) + "." + os.Getenv("apps_onboarding_onboarding_title"),
		ProductName:           os.Getenv(stringTableOnboarding) + "." + os.Getenv("apps_onboarding_product_name"),
		OnBoardingTitle:       os.Getenv(stringTableOnboarding) + "." + os.Getenv("apps_onboarding_onboarding_title"),
		OnBoardingDescription: os.Getenv(stringTableOnboarding) + "." + os.Getenv("apps_onboarding_onboarding_description"),
	}

	return res
}

func (*BribrainLink5MasterOpsiTindaklanjutTable) MappingTable() *BribrainLink5MasterOpsiTindaklanjutTable {
	res := &BribrainLink5MasterOpsiTindaklanjutTable{
		Table:            os.Getenv(stringTableMasterOpsiTindaklanjut),
		OpsiTindaklanjut: os.Getenv(stringTableMasterOpsiTindaklanjut) + "." + os.Getenv("bribrain_link5_master_opsi_tindaklanjut_opsi_tindaklanjut"),
		Kode:             os.Getenv(stringTableMasterOpsiTindaklanjut) + "." + os.Getenv("bribrain_link5_master_opsi_tindaklanjut_kode"),
		Deskripsi:        os.Getenv(stringTableMasterOpsiTindaklanjut) + "." + os.Getenv("bribrain_link5_master_opsi_tindaklanjut_deskripsi"),
	}

	return res
}

func (*BribrainLink5MasterWilayahTable) MappingTable() *BribrainLink5MasterWilayahTable {
	res := &BribrainLink5MasterWilayahTable{
		Table:         os.Getenv(stringTableMasterWilayah),
		KodeProvinsi:  os.Getenv(stringTableMasterWilayah) + "." + os.Getenv("bribrain_link5_master_wilayah_kode_provinsi"),
		Provinsi:      os.Getenv(stringTableMasterWilayah) + "." + os.Getenv("bribrain_link5_master_wilayah_provinsi"),
		KodeKota:      os.Getenv(stringTableMasterWilayah) + "." + os.Getenv("bribrain_link5_master_wilayah_kode_kota"),
		Kota:          os.Getenv(stringTableMasterWilayah) + "." + os.Getenv("bribrain_link5_master_wilayah_kota"),
		KodeKecamatan: os.Getenv(stringTableMasterWilayah) + "." + os.Getenv("bribrain_link5_master_wilayah_kode_kecamatan"),
		Kecamatan:     os.Getenv(stringTableMasterWilayah) + "." + os.Getenv("bribrain_link5_master_wilayah_kecamatan"),
		KodeKelurahan: os.Getenv(stringTableMasterWilayah) + "." + os.Getenv("bribrain_link5_master_wilayah_kode_kelurahan"),
		Kelurahan:     os.Getenv(stringTableMasterWilayah) + "." + os.Getenv("bribrain_link5_master_wilayah_kelurahan"),
	}

	return res
}

func (*BribrainLink5UserFilterTable) MappingTable() *BribrainLink5UserFilterTable {
	res := &BribrainLink5UserFilterTable{
		Table:         os.Getenv(stringTableUserFilter),
		ParentCode:    os.Getenv(stringTableUserFilter) + "." + os.Getenv("bribrain_link5_user_filter_parent_code"),
		ParentName:    os.Getenv(stringTableUserFilter) + "." + os.Getenv("bribrain_link5_user_filter_parent_name"),
		KodeUnitKerja: os.Getenv(stringTableUserFilter) + "." + os.Getenv("bribrain_link5_user_filter_kode_unit_kerja"),
		UnitKerja:     os.Getenv(stringTableUserFilter) + "." + os.Getenv("bribrain_link5_user_filter_unit_kerja"),
		TipeUker:      os.Getenv(stringTableUserFilter) + "." + os.Getenv("bribrain_link5_user_filter_tipe_uker"),
	}

	return res
}

func (*BribrainLink5SystemVariableTable) MappingTable() *BribrainLink5SystemVariableTable {
	res := &BribrainLink5SystemVariableTable{
		Table:    os.Getenv(stringTableSystemVariable),
		Variable: os.Getenv(stringTableSystemVariable) + "." + os.Getenv("bribrain_link5_system_variable_variable"),
		Value:    os.Getenv(stringTableSystemVariable) + "." + os.Getenv("bribrain_link5_system_variable_value"),
	}

	return res
}

func (*BribrainLink5DashboardDetailInformasiNasabahBRITable) MappingTable() *BribrainLink5DashboardDetailInformasiNasabahBRITable {
	res := &BribrainLink5DashboardDetailInformasiNasabahBRITable{
		Table:                  os.Getenv(stringTableNasabahBRI),
		CifNasabahRekan:        os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_cif_nasabah_rekan"),
		NamaNasabahRekan:       os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_nama_nasabah_rekan"),
		NamaNasabahUtama:       os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_nama_nasabah_utama"),
		PosisiTier:             os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_posisi_tier"),
		Usia:                   os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_usia"),
		NomorKtp:               os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_nomor_ktp"),
		PnRm:                   os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_pn_rm"),
		JenisKelamin:           os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_jenis_kelamin"),
		SektorEkonomi:          os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_sektor_ekonomi"),
		TierDiatasnya:          os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_tier_diatasnya"),
		RmDiatasnya:            os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_rm_diatasnya"),
		SegmenPinjaman:         os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_segmen_pinjaman"),
		Branch:                 os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_branch"),
		Rgdesc:                 os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_rgdesc"),
		Mbdesc:                 os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_mbdesc"),
		Sbdesc:                 os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_sbdesc"),
		Brdesc:                 os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_brdesc"),
		Phonenumber:            os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_phonenumber"),
		AlamatId:               os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_alamat_id"),
		AlamatDomisili:         os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_alamat_domisili"),
		AlamatPinjaman:         os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_alamat_pinjaman"),
		UsiaRekeningBulan:      os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_usia_rekening_bulan"),
		ProdukSimpanan:         os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_produk_simpanan"),
		SnameRmSimpanan:        os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_sname_rm_simpanan"),
		ProdukPinjaman:         os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_produk_pinjaman"),
		SnameRmPinjaman:        os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_sname_rm_pinjaman"),
		UnitRmPinjaman:         os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_unit_rm_pinjaman"),
		FlagPinjaman:           os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_flag_pinjaman"),
		Segmen:                 os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_segmen"),
		PhonenumberRmPinjaman:  os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_phonenumber_rm_pinjaman"),
		GroupUsaha:             os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_group_usaha"),
		JenisPekerjaan:         os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_jenis_pekerjaan"),
		BidangPekerjaan:        os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_bidang_pekerjaan"),
		Kolektabilitas:         os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_kolektabilitas"),
		TotalPlafon:            os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_total_plafon"),
		TotalOutstanding:       os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_total_outstanding"),
		InetBanking:            os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_inet_banking"),
		SmsBanking:             os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_sms_banking"),
		TotalCreditBri:         os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_total_credit_bri"),
		TotalDebitBri:          os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_total_debit_bri"),
		TotalKreditNonBri:      os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_total_kredit_non_bri"),
		TotalDebitNonBri:       os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_total_debit_non_bri"),
		TotalKreditDebitNonBri: os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_total_kredit_debit_non_bri"),
		NamaNasabahNonBri:      os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_nama_nasabah_non_bri"),
		NamaBankLawan:          os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_nama_bank_lawan"),
		RatasSaldoSimpananBri:  os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_ratas_saldo_simpanan_bri"),
		TotalCreditKeseluruhan: os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_total_credit_keseluruhan"),
		TotalDebitKeseluruhan:  os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_total_debit_keseluruhan"),
		CifNasabahTier1:        os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_cif_nasabah_tier_1"),
		FlagPinjamanTier1:      os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_flag_pinjaman_tier_1"),
		NamaNasabahTier1:       os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_nama_nasabah_tier_1"),
		SnameRmPinjamanTier1:   os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_sname_rm_pinjaman_tier_1"),
		UnitRmTier1:            os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_unit_rm_tier_1"),
		PhonenumberRmTier1:     os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_phonenumber_rm_tier_1"),
		CifNasabahTier2:        os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_cif_nasabah_tier_2"),
		FlagPinjamanTier2:      os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_flag_pinjaman_tier_2"),
		NamaNasabahTier2:       os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_nama_nasabah_tier_2"),
		SnameRmPinjamanTier2:   os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_sname_rm_pinjaman_tier_2"),
		UnitRmTier2:            os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_unit_rm_tier_2"),
		PhonenumberTier2:       os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_phonenumber_tier_2"),
		CifNasabahTier3:        os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_cif_nasabah_tier_3"),
		FlagPinjamanTier3:      os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_flag_pinjaman_tier_3"),
		NamaNasabahTier3:       os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_nama_nasabah_tier_3"),
		SnameRmPinjamanTier3:   os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_sname_rm_pinjaman_tier_3"),
		UnitRmTier3:            os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_unit_rm_tier_3"),
		PhonenumberTier3:       os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_phonenumber_tier_3"),
		CifNasabahTier4:        os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_cif_nasabah_tier_4"),
		FlagPinjamanTier4:      os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_flag_pinjaman_tier_4"),
		NamaNasabahTier4:       os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_nama_nasabah_tier_4"),
		SnameRmPinjamanTier4:   os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_sname_rm_pinjaman_tier_4"),
		UnitRmTier4:            os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_unit_rm_tier_4"),
		PhonenumberTier4:       os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_phonenumber_tier_4"),
		StatusRekening:         os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_status_rekening"),
		AkuisisiSimpanan:       os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_akuisisi_simpanan"),
		AkuisisiPinjaman:       os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_akuisisi_pinjaman"),
		LamaNasabahBulan:       os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_lama_nasabah_bulan"),
		TanggalLahir:           os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_tanggal_lahir"),
		TotalMutasiDebit:       os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_total_mutasi_debit"),
		TotalMutasiKredit:      os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_total_mutasi_kredit"),
		Ds:                     os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_ds"),
		CreatedDate:            os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_created_date"),
		UpdatedDate:            os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_updated_date"),
		IdStatus:               os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_id_status"),
		StatusTindaklanjut:     os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_status_tindaklanjut"),
		TopupSimpanan:          os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_topup_simpanan"),
		SuplesiPinjaman:        os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_suplesi_pinjaman"),
		VolumePotensiPinjaman:  os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_volume_potensi_pinjaman"),
		VolumePotensiSimpanan:  os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_volume_potensi_simpanan"),
		Peringkat:              os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_peringkat"),
		FreqCreditKeseluruhan:  os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_freq_credit_keseluruhan"),
		FreqDebitKeseluruhan:   os.Getenv(stringTableNasabahBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_bri_freq_debit_keseluruhan"),
	}

	return res
}

func (*BribrainLink5DashboardDetailInformasiNasabahNonBRITable) MappingTable() *BribrainLink5DashboardDetailInformasiNasabahNonBRITable {
	res := &BribrainLink5DashboardDetailInformasiNasabahNonBRITable{
		Table:              os.Getenv(stringTableNasabahNonBRI),
		KodeNonBri:         os.Getenv(stringTableNasabahNonBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_non_bri_kode_non_bri"),
		NamaNasabahNonBri:  os.Getenv(stringTableNasabahNonBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_non_bri_nama_nasabah_non_bri"),
		NamaBankLawan:      os.Getenv(stringTableNasabahNonBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_non_bri_nama_bank_lawan"),
		CifNasabahRekan:    os.Getenv(stringTableNasabahNonBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_non_bri_cif_nasabah_rekan"),
		NamaNasabahRekan:   os.Getenv(stringTableNasabahNonBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_non_bri_nama_nasabah_rekan"),
		Phonenumber:        os.Getenv(stringTableNasabahNonBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_non_bri_phonenumber"),
		TotalDebitNonBri:   os.Getenv(stringTableNasabahNonBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_non_bri_total_debit_non_bri"),
		TotalCreditNonBri:  os.Getenv(stringTableNasabahNonBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_non_bri_total_credit_non_bri"),
		TipeNasabah:        os.Getenv(stringTableNasabahNonBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_non_bri_tipe_nasabah"),
		Ds:                 os.Getenv(stringTableNasabahNonBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_non_bri_ds"),
		CreatedDate:        os.Getenv(stringTableNasabahNonBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_non_bri_created_date"),
		UpdatedDate:        os.Getenv(stringTableNasabahNonBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_non_bri_updated_date"),
		PnRm:               os.Getenv(stringTableNasabahNonBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_non_bri_pn_rm"),
		IdStatus:           os.Getenv(stringTableNasabahNonBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_non_bri_id_status"),
		StatusTindaklanjut: os.Getenv(stringTableNasabahNonBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_non_bri_status_tindaklanjut"),
		AkuisisiSimpanan:   os.Getenv(stringTableNasabahNonBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_non_bri_akuisisi_simpanan"),
		Peringkat:          os.Getenv(stringTableNasabahNonBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_non_bri_peringkat"),
		FreqDebitNonBri:    os.Getenv(stringTableNasabahNonBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_non_bri_freq_debit_non_bri"),
		FreqCreditNonBri:   os.Getenv(stringTableNasabahNonBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_non_bri_freq_credit_non_bri"),
		NamaRm:             os.Getenv(stringTableNasabahNonBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_non_bri_nama_rm"),
		BranchRm:           os.Getenv(stringTableNasabahNonBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_non_bri_branch_rm"),
		PhonenumberRm:      os.Getenv(stringTableNasabahNonBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_non_bri_phonenumber_rm"),
		KodeRegion:         os.Getenv(stringTableNasabahNonBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_non_bri_kode_region"),
		Rgdesc:             os.Getenv(stringTableNasabahNonBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_non_bri_rgdesc"),
		KodeMainbranch:     os.Getenv(stringTableNasabahNonBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_non_bri_kode_mainbranch"),
		Mbdesc:             os.Getenv(stringTableNasabahNonBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_non_bri_mbdesc"),
		InputAlamat:        os.Getenv(stringTableNasabahNonBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_non_bri_input_alamat"),
		InputPhonenumber:   os.Getenv(stringTableNasabahNonBRI) + "." + os.Getenv("bribrain_link5_dashboard_detail_informasi_nasabah_non_bri_input_phonenumber"),
	}

	return res
}

func (*BribrainLink5StatusTindaklanjutTable) MappingTable() *BribrainLink5StatusTindaklanjutTable {
	res := &BribrainLink5StatusTindaklanjutTable{
		Table:                   os.Getenv(stringTableStatusTindaklanjut),
		Id:                      os.Getenv(stringTableStatusTindaklanjut) + "." + os.Getenv("bribrain_link5_status_tindaklanjut_id"),
		CifnoRacer:              os.Getenv(stringTableStatusTindaklanjut) + "." + os.Getenv("bribrain_link5_status_tindaklanjut_cifno_racer"),
		Cifno:                   os.Getenv(stringTableStatusTindaklanjut) + "." + os.Getenv("bribrain_link5_status_tindaklanjut_cifno"),
		KodeNonBri:              os.Getenv(stringTableStatusTindaklanjut) + "." + os.Getenv("bribrain_link5_status_tindaklanjut_kode_non_bri"),
		TipeCustomer:            os.Getenv(stringTableStatusTindaklanjut) + "." + os.Getenv("bribrain_link5_status_tindaklanjut_tipe_customer"),
		StatusHubungi:           os.Getenv(stringTableStatusTindaklanjut) + "." + os.Getenv("bribrain_link5_status_tindaklanjut_status_hubungi"),
		KodeHubungi:             os.Getenv(stringTableStatusTindaklanjut) + "." + os.Getenv("bribrain_link5_status_tindaklanjut_kode_hubungi"),
		TglHubungi:              os.Getenv(stringTableStatusTindaklanjut) + "." + os.Getenv("bribrain_link5_status_tindaklanjut_tgl_hubungi"),
		CatatanHubungi:          os.Getenv(stringTableStatusTindaklanjut) + "." + os.Getenv("bribrain_link5_status_tindaklanjut_catatan_hubungi"),
		StatusKetertarikan:      os.Getenv(stringTableStatusTindaklanjut) + "." + os.Getenv("bribrain_link5_status_tindaklanjut_status_ketertarikan"),
		KodeKetertarikan:        os.Getenv(stringTableStatusTindaklanjut) + "." + os.Getenv("bribrain_link5_status_tindaklanjut_kode_ketertarikan"),
		CatatanKetertarikan:     os.Getenv(stringTableStatusTindaklanjut) + "." + os.Getenv("bribrain_link5_status_tindaklanjut_catatan_ketertarikan"),
		StatusKunjungan:         os.Getenv(stringTableStatusTindaklanjut) + "." + os.Getenv("bribrain_link5_status_tindaklanjut_status_kunjungan"),
		KodeKunjungan:           os.Getenv(stringTableStatusTindaklanjut) + "." + os.Getenv("bribrain_link5_status_tindaklanjut_kode_kunjungan"),
		TglKunjungan:            os.Getenv(stringTableStatusTindaklanjut) + "." + os.Getenv("bribrain_link5_status_tindaklanjut_tgl_kunjungan"),
		CatatanKunjungan:        os.Getenv(stringTableStatusTindaklanjut) + "." + os.Getenv("bribrain_link5_status_tindaklanjut_catatan_kunjungan"),
		CreatedDate:             os.Getenv(stringTableStatusTindaklanjut) + "." + os.Getenv("bribrain_link5_status_tindaklanjut_created_date"),
		UpdatedDate:             os.Getenv(stringTableStatusTindaklanjut) + "." + os.Getenv("bribrain_link5_status_tindaklanjut_updated_date"),
		VolumePotensiRmPinjaman: os.Getenv(stringTableStatusTindaklanjut) + "." + os.Getenv("bribrain_link5_status_tindaklanjut_volume_potensi_rm_pinjaman"),
		VolumePotensiRmSimpanan: os.Getenv(stringTableStatusTindaklanjut) + "." + os.Getenv("bribrain_link5_status_tindaklanjut_volume_potensi_rm_simpanan"),
		InputPotensiRmPinjaman:  os.Getenv(stringTableStatusTindaklanjut) + "." + os.Getenv("bribrain_link5_status_tindaklanjut_input_potensi_rm_pinjaman"),
		InputPotensiRmSimpanan:  os.Getenv(stringTableStatusTindaklanjut) + "." + os.Getenv("bribrain_link5_status_tindaklanjut_input_potensi_rm_simpanan"),
		RatasSaldo:              os.Getenv(stringTableStatusTindaklanjut) + "." + os.Getenv("bribrain_link5_status_tindaklanjut_ratas_saldo"),
		TotalKredit:             os.Getenv(stringTableStatusTindaklanjut) + "." + os.Getenv("bribrain_link5_status_tindaklanjut_total_kredit"),
		TotalDebit:              os.Getenv(stringTableStatusTindaklanjut) + "." + os.Getenv("bribrain_link5_status_tindaklanjut_total_debit"),
		PnRm:                    os.Getenv(stringTableStatusTindaklanjut) + "." + os.Getenv("bribrain_link5_status_tindaklanjut_pn_rm"),
		NamaNasabah:             os.Getenv(stringTableStatusTindaklanjut) + "." + os.Getenv("bribrain_link5_status_tindaklanjut_nama_nasabah"),
		InputNorek:              os.Getenv(stringTableStatusTindaklanjut) + "." + os.Getenv("bribrain_link5_status_tindaklanjut_input_norek"),
		KodeRegion:              os.Getenv(stringTableStatusTindaklanjut) + "." + os.Getenv("bribrain_link5_status_tindaklanjut_kode_region"),
		Rgdesc:                  os.Getenv(stringTableStatusTindaklanjut) + "." + os.Getenv("bribrain_link5_status_tindaklanjut_rgdesc"),
		KodeMainbranch:          os.Getenv(stringTableStatusTindaklanjut) + "." + os.Getenv("bribrain_link5_status_tindaklanjut_kode_mainbranch"),
		Mbdesc:                  os.Getenv(stringTableStatusTindaklanjut) + "." + os.Getenv("bribrain_link5_status_tindaklanjut_mbdesc"),
	}

	return res
}

func (*BribrainLink5ReferalTable) MappingTable() *BribrainLink5ReferalTable {
	res := &BribrainLink5ReferalTable{
		Table:                 os.Getenv(stringTableReferal),
		KodeNonBri:            os.Getenv(stringTableReferal) + "." + os.Getenv("bribrain_link5_referal_kode_non_bri"),
		PnRmPengirim:          os.Getenv(stringTableReferal) + "." + os.Getenv("bribrain_link5_referal_pn_rm_pengirim"),
		PnRmPenerima:          os.Getenv(stringTableReferal) + "." + os.Getenv("bribrain_link5_referal_pn_rm_penerima"),
		MainbranchCodeSebelum: os.Getenv(stringTableReferal) + "." + os.Getenv("bribrain_link5_referal_mainbranch_code_sebelum"),
		MainbranchCodeSesudah: os.Getenv(stringTableReferal) + "." + os.Getenv("bribrain_link5_referal_mainbranch_code_sesudah"),
		Alamat:                os.Getenv(stringTableReferal) + "." + os.Getenv("bribrain_link5_referal_alamat"),
		Phonenumber:           os.Getenv(stringTableReferal) + "." + os.Getenv("bribrain_link5_referal_phonenumber"),
		CreatedDate:           os.Getenv(stringTableReferal) + "." + os.Getenv("bribrain_link5_referal_created_date"),
	}

	return res
}

func (g *MasterDataTable) SelectAllColumnAppsOnboardingTable() string {
	query := g.AppsOnboarding.ProductID + ` AS product_id,` +
		g.AppsOnboarding.ProductName + ` AS jenis,` +
		g.AppsOnboarding.OnBoardingTitle + ` AS onboarding_title,` +
		g.AppsOnboarding.OnBoardingDescription + ` AS onboarding_description`

	return query
}

func (g *MasterDataTable) SelectAllColumnBribrainLink5UserFilterTable() string {
	query := g.BribrainLink5UserFilter.ParentCode + ` AS PARENT_CODE,` +
		g.BribrainLink5UserFilter.ParentName + ` AS PARENT_NAME,` +
		g.BribrainLink5UserFilter.KodeUnitKerja + ` AS KODE_UNIT_KERJA,` +
		g.BribrainLink5UserFilter.UnitKerja + ` AS UNIT_KERJA,` +
		g.BribrainLink5UserFilter.TipeUker + ` AS TIPE_UKER`

	return query
}

func (g *MasterDataTable) SelectProvinsiKotaKecamatanBribrainLink5MasterWilayahTable() string {
	query := g.BribrainLink5MasterWilayah.Provinsi + ` AS PROVINSI,` +
		g.BribrainLink5MasterWilayah.Kota + ` AS KOTA,` +
		g.BribrainLink5MasterWilayah.KodeKecamatan + ` AS KODE_KECAMATAN,` +
		g.BribrainLink5MasterWilayah.Kecamatan + ` AS KECAMATAN`

	return query
}

func (g *MasterDataTable) SelectKelurahanKotaKecamatanBribrainLink5MasterWilayahTable() string {
	query := g.BribrainLink5MasterWilayah.Kelurahan + ` AS KELURAHAN,` +
		g.BribrainLink5MasterWilayah.Kota + ` AS KOTA,` +
		g.BribrainLink5MasterWilayah.Kecamatan + ` AS KECAMATAN`

	return query
}

func (g *NasabahTable) SelectAllColumnBribrainLink5DashboardDetailInformasiNasabahBRITable() string {
	query := g.BribrainLink5DashboardDetailInformasiNasabahBRI.CifNasabahRekan + ` AS CIF_NASABAH_REKAN,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.NamaNasabahRekan + ` AS NAMA_NASABAH_REKAN,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.NamaNasabahUtama + ` AS NAMA_NASABAH_UTAMA,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.PosisiTier + ` AS POSISI_TIER,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.Usia + ` AS USIA,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.NomorKtp + ` AS NOMOR_KTP,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.PnRm + ` AS PN_RM,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.JenisKelamin + ` AS JENIS_KELAMIN,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.SektorEkonomi + ` AS SEKTOR_EKONOMI,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.TierDiatasnya + ` AS TIER_DIATASNYA,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.RmDiatasnya + ` AS RM_DIATASNYA,` +
		// g.BribrainLink5DashboardDetailInformasiNasabahBRI.SegmenPinjaman + ` AS SEGMEN_PINJAMAN,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.Branch + ` AS BRANCH,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.Rgdesc + ` AS RGDESC,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.Mbdesc + ` AS MBDESC,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.Sbdesc + ` AS SBDESC,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.Brdesc + ` AS BRDESC,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.Phonenumber + ` AS PHONENUMBER,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.AlamatId + ` AS ALAMAT_ID,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.AlamatDomisili + ` AS ALAMAT_DOMISILI,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.AlamatPinjaman + ` AS ALAMAT_PINJAMAN,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.UsiaRekeningBulan + ` AS USIA_REKENING_BULAN,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.ProdukSimpanan + ` AS PRODUK_SIMPANAN,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.SnameRmSimpanan + ` AS SNAME_RM_SIMPANAN,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.ProdukPinjaman + ` AS PRODUK_PINJAMAN,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.SnameRmPinjaman + ` AS SNAME_RM_PINJAMAN,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.UnitRmPinjaman + ` AS UNIT_RM_PINJAMAN,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.FlagPinjaman + ` AS FLAG_PINJAMAN,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.Segmen + ` AS SEGMEN,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.PhonenumberRmPinjaman + ` AS PHONENUMBER_RM_PINJAMAN,` +
		// g.BribrainLink5DashboardDetailInformasiNasabahBRI.GroupUsaha + ` AS GROUP_USAHA,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.JenisPekerjaan + ` AS JENIS_PEKERJAAN,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.BidangPekerjaan + ` AS BIDANG_PEKERJAAN,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.Kolektabilitas + ` AS KOLEKTABILITAS,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.TotalPlafon + ` AS TOTAL_PLAFON,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.TotalOutstanding + ` AS TOTAL_OUTSTANDING,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.InetBanking + ` AS INET_BANKING,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.SmsBanking + ` AS SMS_BANKING,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.TotalCreditBri + ` AS TOTAL_CREDIT_BRI,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.TotalDebitBri + ` AS TOTAL_DEBIT_BRI,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.TotalKreditNonBri + ` AS TOTAL_KREDIT_NON_BRI,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.TotalDebitNonBri + ` AS TOTAL_DEBIT_NON_BRI,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.TotalKreditDebitNonBri + ` AS TOTAL_KREDIT_DEBIT_NON_BRI,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.NamaNasabahNonBri + ` AS NAMA_NASABAH_NON_BRI,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.NamaBankLawan + ` AS NAMA_BANK_LAWAN,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.RatasSaldoSimpananBri + ` AS RATAS_SALDO_SIMPANAN_BRI,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.TotalCreditKeseluruhan + ` AS TOTAL_CREDIT_KESELURUHAN,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.TotalDebitKeseluruhan + ` AS TOTAL_DEBIT_KESELURUHAN,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.CifNasabahTier1 + ` AS CIF_NASABAH_TIER_1,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.FlagPinjamanTier1 + ` AS FLAG_PINJAMAN_TIER_1,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.NamaNasabahTier1 + ` AS NAMA_NASABAH_TIER_1,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.SnameRmPinjamanTier1 + ` AS SNAME_RM_PINJAMAN_TIER_1,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.UnitRmTier1 + ` AS UNIT_RM_TIER_1,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.PhonenumberRmTier1 + ` AS PHONENUMBER_RM_TIER_1,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.CifNasabahTier2 + ` AS CIF_NASABAH_TIER_2,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.FlagPinjamanTier2 + ` AS FLAG_PINJAMAN_TIER_2,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.NamaNasabahTier2 + ` AS NAMA_NASABAH_TIER_2,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.SnameRmPinjamanTier2 + ` AS SNAME_RM_PINJAMAN_TIER_2,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.UnitRmTier2 + ` AS UNIT_RM_TIER_2,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.PhonenumberTier2 + ` AS PHONENUMBER_TIER_2,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.CifNasabahTier3 + ` AS CIF_NASABAH_TIER_3,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.FlagPinjamanTier3 + ` AS FLAG_PINJAMAN_TIER_3,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.NamaNasabahTier3 + ` AS NAMA_NASABAH_TIER_3,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.SnameRmPinjamanTier3 + ` AS SNAME_RM_PINJAMAN_TIER_3,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.UnitRmTier3 + ` AS UNIT_RM_TIER_3,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.PhonenumberTier3 + ` AS PHONENUMBER_TIER_3,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.CifNasabahTier4 + ` AS CIF_NASABAH_TIER_4,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.FlagPinjamanTier4 + ` AS FLAG_PINJAMAN_TIER_4,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.NamaNasabahTier4 + ` AS NAMA_NASABAH_TIER_4,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.SnameRmPinjamanTier4 + ` AS SNAME_RM_PINJAMAN_TIER_4,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.UnitRmTier4 + ` AS UNIT_RM_TIER_4,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.PhonenumberTier4 + ` AS PHONENUMBER_TIER_4,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.StatusRekening + ` AS STATUS_REKENING,` +
		// g.BribrainLink5DashboardDetailInformasiNasabahBRI.AkuisisiSimpanan + ` AS AKUISISI_SIMPANAN,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.AkuisisiPinjaman + ` AS AKUISISI_PINJAMAN,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.LamaNasabahBulan + ` AS LAMA_NASABAH_BULAN,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.TanggalLahir + ` AS TANGGAL_LAHIR,` +
		// g.BribrainLink5DashboardDetailInformasiNasabahBRI.TotalMutasiDebit + ` AS TOTAL_MUTASI_DEBIT,` +
		// g.BribrainLink5DashboardDetailInformasiNasabahBRI.TotalMutasiKredit + ` AS TOTAL_MUTASI_KREDIT,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.Ds + ` AS DS,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.CreatedDate + ` AS CREATED_DATE,` +
		// g.BribrainLink5DashboardDetailInformasiNasabahBRI.IdStatus + ` AS ID_STATUS,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.Peringkat + ` AS PERINGKAT,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.StatusTindaklanjut + ` AS STATUS_TINDAKLANJUT,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.TopupSimpanan + ` AS TOPUP_SIMPANAN,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.SuplesiPinjaman + ` AS SUPLESI_PINJAMAN,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.FreqCreditKeseluruhan + ` AS FREQ_CREDIT_KESELURUHAN,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.FreqDebitKeseluruhan + ` AS FREQ_DEBIT_KESELURUHAN,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.VolumePotensiPinjaman + ` AS VOLUME_POTENSI_PINJAMAN,` +
		g.BribrainLink5DashboardDetailInformasiNasabahBRI.VolumePotensiSimpanan + ` AS VOLUME_POTENSI_SIMPANAN`

	return query
}

func (g *NasabahTable) SelectAllColumnBribrainLink5DashboardDetailInformasiNasabahNonBRITable() string {
	query := g.BribrainLink5DashboardDetailInformasiNasabahNonBRI.KodeNonBri + ` AS KODE_NON_BRI,` +
		g.BribrainLink5DashboardDetailInformasiNasabahNonBRI.NamaNasabahNonBri + ` AS NAMA_NASABAH_NON_BRI,` +
		g.BribrainLink5DashboardDetailInformasiNasabahNonBRI.NamaBankLawan + ` AS NAMA_BANK_LAWAN,` +
		g.BribrainLink5DashboardDetailInformasiNasabahNonBRI.CifNasabahRekan + ` AS CIF_NASABAH_REKAN_TABEL_NON,` +
		g.BribrainLink5DashboardDetailInformasiNasabahNonBRI.NamaNasabahRekan + ` AS NAMA_NASABAH_REKAN_TABEL_NON,` +
		g.BribrainLink5DashboardDetailInformasiNasabahNonBRI.Phonenumber + ` AS PHONENUMBER_TABEL_NON,` +
		g.BribrainLink5DashboardDetailInformasiNasabahNonBRI.TotalDebitNonBri + ` AS TOTAL_DEBIT_NON_BRI_TABEL_NON,` +
		g.BribrainLink5DashboardDetailInformasiNasabahNonBRI.TotalCreditNonBri + ` AS TOTAL_CREDIT_NON_BRI_TABEL_NON,` +
		g.BribrainLink5DashboardDetailInformasiNasabahNonBRI.TipeNasabah + ` AS TIPE_NASABAH,` +
		g.BribrainLink5DashboardDetailInformasiNasabahNonBRI.Ds + ` AS DS,` +
		g.BribrainLink5DashboardDetailInformasiNasabahNonBRI.CreatedDate + ` AS CREATED_DATE,` +
		g.BribrainLink5DashboardDetailInformasiNasabahNonBRI.PnRm + ` AS PN_RM,` +
		g.BribrainLink5DashboardDetailInformasiNasabahNonBRI.Peringkat + ` AS PERINGKAT,` +
		// g.BribrainLink5DashboardDetailInformasiNasabahNonBRI.IdStatus + ` AS ID_STATUS_TABEL_NON,` +
		g.BribrainLink5DashboardDetailInformasiNasabahNonBRI.FreqDebitNonBri + ` AS FREQ_DEBIT_NON_BRI,` +
		g.BribrainLink5DashboardDetailInformasiNasabahNonBRI.FreqCreditNonBri + ` AS FREQ_CREDIT_NON_BRI,` +
		g.BribrainLink5DashboardDetailInformasiNasabahNonBRI.NamaRm + ` AS NAMA_RM,` +
		g.BribrainLink5DashboardDetailInformasiNasabahNonBRI.BranchRm + ` AS BRANCH_RM,` +
		g.BribrainLink5DashboardDetailInformasiNasabahNonBRI.PhonenumberRm + ` AS PHONENUMBER_RM,` +
		g.BribrainLink5DashboardDetailInformasiNasabahNonBRI.StatusTindaklanjut + ` AS STATUS_TINDAKLANJUT_TABEL_NON,` +
		g.BribrainLink5DashboardDetailInformasiNasabahNonBRI.AkuisisiSimpanan + ` AS AKUISISI_SIMPANAN_TABEL_NON,` +
		g.BribrainLink5DashboardDetailInformasiNasabahNonBRI.KodeRegion + ` AS KODE_REGION_TABEL_NON,` +
		g.BribrainLink5DashboardDetailInformasiNasabahNonBRI.Rgdesc + ` AS RGDESC_TABEL_NON,` +
		g.BribrainLink5DashboardDetailInformasiNasabahNonBRI.KodeMainbranch + ` AS KODE_MAINBRANCH_TABEL_NON,` +
		g.BribrainLink5DashboardDetailInformasiNasabahNonBRI.Mbdesc + ` AS MBDESC_TABEL_NON,` +
		g.BribrainLink5DashboardDetailInformasiNasabahNonBRI.InputAlamat + ` AS INPUT_ALAMAT,` +
		g.BribrainLink5DashboardDetailInformasiNasabahNonBRI.InputPhonenumber + ` AS INPUT_PHONENUMBER`

	return query
}

func (g *NasabahTable) SelectAllColumnBribrainLink5StatusTindaklanjutTable() string {
	// query := g.BribrainLink5StatusTindaklanjut.Id + ` AS ID,` +
	query := g.BribrainLink5StatusTindaklanjut.CifnoRacer + ` AS CIFNO_RACER,` +
		g.BribrainLink5StatusTindaklanjut.Cifno + ` AS CIFNO,` +
		g.BribrainLink5StatusTindaklanjut.KodeNonBri + ` AS KODE_NON_BRI,` +
		g.BribrainLink5StatusTindaklanjut.TipeCustomer + ` AS TIPE_CUSTOMER,` +
		g.BribrainLink5StatusTindaklanjut.StatusHubungi + ` AS STATUS_HUBUNGI,` +
		g.BribrainLink5StatusTindaklanjut.KodeHubungi + ` AS KODE_HUBUNGI,` +
		g.BribrainLink5StatusTindaklanjut.TglHubungi + ` AS TGL_HUBUNGI,` +
		g.BribrainLink5StatusTindaklanjut.CatatanHubungi + ` AS CATATAN_HUBUNGI,` +
		g.BribrainLink5StatusTindaklanjut.StatusKetertarikan + ` AS STATUS_KETERTARIKAN,` +
		g.BribrainLink5StatusTindaklanjut.KodeKetertarikan + ` AS KODE_KETERTARIKAN,` +
		g.BribrainLink5StatusTindaklanjut.CatatanKetertarikan + ` AS CATATAN_KETERTARIKAN,` +
		g.BribrainLink5StatusTindaklanjut.StatusKunjungan + ` AS STATUS_KUNJUNGAN,` +
		g.BribrainLink5StatusTindaklanjut.KodeKunjungan + ` AS KODE_KUNJUNGAN,` +
		g.BribrainLink5StatusTindaklanjut.TglKunjungan + ` AS TGL_KUNJUNGAN,` +
		g.BribrainLink5StatusTindaklanjut.CatatanKunjungan + ` AS CATATAN_KUNJUNGAN,` +
		g.BribrainLink5StatusTindaklanjut.CreatedDate + ` AS CREATED_DATE,` +
		g.BribrainLink5StatusTindaklanjut.UpdatedDate + ` AS UPDATED_DATE,` +
		g.BribrainLink5StatusTindaklanjut.VolumePotensiRmPinjaman + ` AS VOLUME_POTENSI_RM_PINJAMAN,` +
		g.BribrainLink5StatusTindaklanjut.VolumePotensiRmSimpanan + ` AS VOLUME_POTENSI_RM_SIMPANAN,` +
		g.BribrainLink5StatusTindaklanjut.InputPotensiRmPinjaman + ` AS INPUT_POTENSI_RM_PINJAMAN,` +
		g.BribrainLink5StatusTindaklanjut.InputPotensiRmSimpanan + ` AS INPUT_POTENSI_RM_SIMPANAN,` +
		g.BribrainLink5StatusTindaklanjut.RatasSaldo + ` AS RATAS_SALDO,` +
		g.BribrainLink5StatusTindaklanjut.TotalKredit + ` AS TOTAL_KREDIT,` +
		g.BribrainLink5StatusTindaklanjut.TotalDebit + ` AS TOTAL_DEBIT,` +
		g.BribrainLink5StatusTindaklanjut.InputNorek + ` AS INPUT_NOREK,` +
		g.BribrainLink5StatusTindaklanjut.PnRm + ` AS PN_RM`
	// g.BribrainLink5StatusTindaklanjut.NamaNasabah + ` AS NAMA_NASABAH`

	return query
}
