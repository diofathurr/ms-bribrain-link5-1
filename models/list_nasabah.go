package models

import (
	"fmt"
	"ms-bribrain-link5/helper"
	"ms-bribrain-link5/helper/models"
	models2 "ms-bribrain-link5/helper/models"
	"strings"
	"time"
)

type RequestListNasabah struct {
	// Region     []string `json:"region" binding:"omitempty,dive,alpha"`
	// Mainbranch []string `json:"mainbranch" binding:"omitempty,dive,numeric"`
	// Branch     []string `json:"branch" binding:"omitempty,dive,numeric"`
	// Mantri     string   `json:"pn_mantri" binding:"omitempty,numeric"`
	Limit  string `json:"limit" validate:"number,max=2"`
	Page   string `json:"page" validate:"required,number"`
	Search string `json:"search"`
}

type RequestListNasabahTop20 struct {
	// Region     []string `json:"region" binding:"omitempty,dive,alpha"`
	// Mainbranch []string `json:"mainbranch" binding:"omitempty,dive,numeric"`
	// Branch     []string `json:"branch" binding:"omitempty,dive,numeric"`
	// Mantri     string   `json:"pn_mantri" binding:"omitempty,numeric"`
	Limit  string `json:"limit" validate:"number,eq=20,max=2"`
	Page   string `json:"page" validate:"number,eq=1"`
	Search string `json:"search"`
}

func (request *RequestListNasabah) MappingToGlobalValidation(limit, page string) models2.GlobalValidation {
	res := models2.GlobalValidation{
		RequiredValidation: []models2.RequiredValidation{
			models2.RequiredValidation{
				Key:   "limit",
				Value: limit,
			},
			models2.RequiredValidation{
				Key:   "page",
				Value: page,
			},
		},
		DataTypeNumberIntValidation: []models2.DataTypeNumberIntValidation{
			models2.DataTypeNumberIntValidation{
				// Key:   "id_rekomendasi",
				// Value: helper.IntNullableToString(request.IdRekomendasi),
			},
		},
		PageLimitValidation:    []models2.PageLimitValidation{},
		MaxMinNumberValidation: []models2.MaxMinNumberValidation{},
	}
	return res
}

func (request *RequestListNasabah) MappingToParamListRekomendasi(pnPengelola string, role string, limit int, page int, offset int) *ParamRequestListNasabah {
	res := &ParamRequestListNasabah{
		// Limit: helper.StringToInt(request.Limit),
		// Page:  helper.StringToInt(request.Page),
		Limit:       limit,
		Page:        page,
		Offset:      offset,
		Search:      request.Search,
		PnPengelola: pnPengelola,
		Role:        role,
	}
	return res
}

func (request *RequestListNasabahTop20) MappingToParamListRekomendasiTop20(pnPengelola string, role string, limit int, page int, offset int) *ParamRequestListNasabah {
	res := &ParamRequestListNasabah{
		// Limit: helper.StringToInt(request.Limit),
		// Page:  helper.StringToInt(request.Page),
		Limit:       limit,
		Page:        page,
		Offset:      offset,
		Search:      request.Search,
		PnPengelola: pnPengelola,
		Role:        role,
	}
	return res
}

type ParamRequestListNasabah struct {
	Offset      int    `json:"offset"`
	Limit       int    `json:"limit"`
	Page        int    `json:"page"`
	Search      string `json:"search"`
	PnPengelola string `json:"pn_pengelola"`
	Role        string `json:"role"`
}

type GetListQueryStruct struct {
	// CIF                 *string  `json:"cif" gorm:"column:cifno"`
	// KeteranganInformasi *string  `json:"keterangan_informasi"`
	// NamaNasabahRekanan  *string  `json:"nama_nasabah" gorm:"column:nama_nasabah_rekanan"`
	// PhoneNumber         *string  `json:"no_telepon" gorm:"column:phonenumber"`
	// GroupUsaha          *string  `json:"sektor_industri" gorm:"column:group_usaha"`
	// TotalRataRataSaldo  *float64 `json:"ratas_saldo" gorm:"column:total_rata_rata_saldo"`
	// TotalUangMasuk      *float64 `json:"total_uang_masuk" gorm:"column:columnx"`
	// UangKeluarKeNonBRI  *float64 `json:"uang_keluar_ke_non_bri" gorm:"column:columnx"`
	// PnRM                *string  `json:"pn_rm" gorm:"column:pn_rm"`

	CifNasabahRekan        *string  `json:"cif_nasabah_rekan" gorm:"column:CIF_NASABAH_REKAN"`
	KeteranganInformasi    *string  `json:"keterangan_informasi"`
	NamaNasabahRekan       *string  `json:"nama_nasabah_rekan" gorm:"column:NAMA_NASABAH_REKAN"`
	Phonenumber            *string  `json:"phonenumber" gorm:"column:PHONENUMBER"`
	GroupUsaha             *string  `json:"group_usaha" gorm:"column:GROUP_USAHA"`
	SektorEkonomi          *string  `json:"sektor_ekonomi" gorm:"column:SEKTOR_EKONOMI"`
	RatasSaldoSimpananBri  *float64 `json:"ratas_saldo_simpanan_bri" gorm:"column:RATAS_SALDO_SIMPANAN_BRI"`
	TotalKreditNonBri      *float64 `json:"total_kredit_non_bri" gorm:"column:TOTAL_KREDIT_NON_BRI"`
	TotalDebitNonBri       *float64 `json:"total_debit_non_bri" gorm:"column:TOTAL_DEBIT_NON_BRI"`
	PnRm                   *string  `json:"pn_rm" gorm:"column:PN_RM"`
	TotalCreditKeseluruhan *float64 `json:"total_credit_keseluruhan" gorm:"column:TOTAL_CREDIT_KESELURUHAN"`
	TotalDebitKeseluruhan  *float64 `json:"total_debit_keseluruhan" gorm:"column:TOTAL_DEBIT_KESELURUHAN"`
	AlamatDomisili         *string  `json:"alamat_domisili" gorm:"column:ALAMAT_DOMISILI"`
	FreqDebitKeseluruhan   *int64   `json:"freq_debit_keseluruhan" gorm:"column:FREQ_DEBIT_KESELURUHAN"`
	FreqCreditKeseluruhan  *int64   `json:"freq_credit_keseluruhan" gorm:"column:FREQ_CREDIT_KESELURUHAN"`

	// KodeTLHubungi      *float64 `json:"kode_hubungi" gorm:"column:kode_hubungi"`
	// KodeTLKunjungan    *float64 `json:"kode_kunjungan" gorm:"column:kode_kunjungan"`
	// KodeKetertarikan   *float64 `json:"kode_ketertarikan" gorm:"column:kode_ketertarikan"`
	// StatusTLHubungi    *string  `json:"status_hubungi" gorm:"column:status_hubungi"`
	// StatusTLKunjungan  *string  `json:"status_kunjungan" gorm:"column:status_kunjungan"`
	// StatusKetertarikan *string  `json:"status_ketertarikan" gorm:"column:status_ketertarikan"`

	KodeHubungi         *int64     `json:"kode_hubungi" gorm:"column:KODE_HUBUNGI"`
	KodeKunjungan       *int64     `json:"kode_kunjungan" gorm:"column:KODE_KUNJUNGAN"`
	KodeKetertarikan    *int64     `json:"kode_ketertarikan" gorm:"column:KODE_KETERTARIKAN"`
	StatusHubungi       *string    `json:"status_hubungi" gorm:"column:STATUS_HUBUNGI"`
	StatusKunjungan     *string    `json:"status_kunjungan" gorm:"column:STATUS_KUNJUNGAN"`
	StatusKetertarikan  *string    `json:"status_ketertarikan" gorm:"column:STATUS_KETERTARIKAN"`
	TglHubungi          *time.Time `json:"tgl_hubungi" gorm:"column:TGL_HUBUNGI"`
	CatatanHubungi      *string    `json:"catatan_hubungi" gorm:"column:CATATAN_HUBUNGI"`
	CatatanKetertarikan *string    `json:"catatan_ketertarikan" gorm:"column:CATATAN_KETERTARIKAN"`

	CifNasabahRekanTabelNon   *string  `json:"cif_nasabah_rekan_tabel_non" gorm:"column:CIF_NASABAH_REKAN_TABEL_NON"`
	NamaNasabahRekanTabelNon  *string  `json:"nama_nasabah_rekan_tabel_non" gorm:"column:NAMA_NASABAH_REKAN_TABEL_NON"`
	NamaBankLawan             *string  `json:"nama_bank_lawan" gorm:"column:NAMA_BANK_LAWAN"`
	NamaNasabahNonBRI         *string  `json:"nama_nasabah_non_bri" gorm:"column:NAMA_NASABAH_NON_BRI"`
	TotalDebitNonBRITabelNon  *float64 `json:"total_debit_non_bri" gorm:"column:TOTAL_DEBIT_NON_BRI_TABEL_NON"`
	TotalCreditNonBRITabelNon *float64 `json:"total_credit_non_bri" gorm:"column:TOTAL_CREDIT_NON_BRI_TABEL_NON"`
	TipeCustomer              *string  `json:"tipe_customer" gorm:"column:TIPE_CUSTOMER"`
	KodeNonBri                *string  `json:"kode_non_bri" gorm:"column:KODE_NON_BRI"`
	AkuisisiSimpananTabelNon  *string  `json:"akuisisi_simpanan_tabel_non" gorm:"column:AKUISISI_SIMPANAN_TABEL_NON"`
	FreqDebitNonBri           *int64   `json:"freq_debit_non_bri" gorm:"column:FREQ_DEBIT_NON_BRI"`
	FreqCreditNonBri          *int64   `json:"freq_credit_non_bri" gorm:"column:FREQ_CREDIT_NON_BRI"`
	NamaRm                    *string  `json:"nama_rm" gorm:"column:NAMA_RM"`
	BranchRm                  *string  `json:"branch_rm" gorm:"column:BRANCH_RM"`
	PhonenumberRm             *string  `json:"phonenumber_rm" gorm:"column:PHONENUMBER_RM"`
	PhonenumberTabelNon       *string  `json:"phonenumber_tabel_non" gorm:"column:PHONENUMBER_TABEL_NON"`
	InputNorek                *float64 `json:"input_norek" gorm:"column:INPUT_NOREK"`
}

type ResponseListRekomendasiNasabah struct {
	CIF                      *string `json:"cif" gorm:"column:cifno"`
	KeteranganInformasi      *string `json:"keterangan_informasi"`
	NamaNasabahRekanan       *string `json:"nama_nasabah" gorm:"column:nama_nasabah_rekanan"`
	Inisial                  *string `json:"inisial"`
	StatusTindakLanjut       *string `json:"status_tindaklanjut"`
	PhoneNumber              *string `json:"no_telepon" gorm:"column:phonenumber"`
	AlamatDomisili           *string `json:"alamat_domisili" gorm:"column:alamat_domisili"`
	SektorIndustri           *string `json:"sektor_industri" gorm:"column:group_usaha"`
	RatasSaldo               *string `json:"ratas_saldo" gorm:"column:total_rata_rata_saldo"`
	TotalUangMasuk           *string `json:"total_kredit_keseluruhan" gorm:"column:columnx"`
	UangKeluarKeNonBRI       *string `json:"transaksi_debit_non_bri" gorm:"column:columnx"`
	UangKeluar               *string `json:"total_transaksi_debit_bri"`
	FrekuensiTransaksiDebit  *string `json:"frekuensi_transaksi_debit"`
	UangMasuk                *string `json:"total_transaksi_kredit_bri"`
	FrekuensiTransaksiKredit *string `json:"frekuensi_transaksi_kredit"`
}

func (data *GetListQueryStruct) MappingResponseListRekomendasiNasabah() *ResponseListRekomendasiNasabah {
	// nama := helper.StringNullableToString(data.NamaNasabahRekan)
	// inisial := getInitialDetail(nama)

	var sektorEkonomi *string
	if data.SektorEkonomi == nil || helper.StringNullableToString(data.SektorEkonomi) == "" {
		sektorEkonomi = helper.StringToStringNullable("-")
	} else {
		sektorEkonomi = data.SektorEkonomi
	}

	var namaBri, inisialBri *string
	if data.NamaNasabahRekan == nil || helper.StringNullableToString(data.NamaNasabahRekan) == "" {
		namaBri = helper.StringToStringNullable("-")
		inisialBri = helper.StringToStringNullable("-")
	} else {
		namaBri = data.NamaNasabahRekan
		tempNamaBri := helper.StringNullableToString(data.NamaNasabahRekan)
		tempInisialBri := getInitialDetail(tempNamaBri)
		inisialBri = helper.StringToStringNullable(tempInisialBri)
	}

	var phonenumberNasabah *string
	if data.Phonenumber == nil || helper.StringNullableToString(data.Phonenumber) == "" {
		phonenumberNasabah = helper.StringToStringNullable("-")
	} else {
		phonenumberNasabah = data.Phonenumber
	}

	var alamatDomisili *string
	if data.AlamatDomisili == nil || helper.StringNullableToString(data.AlamatDomisili) == "" {
		alamatDomisili = helper.StringToStringNullable("-")
	} else {
		alamatDomisili = data.AlamatDomisili
	}

	intFrekuensiTransaksiDebit := helper.Int64NullableToInt(data.FreqDebitKeseluruhan)
	intFrekuensiTransaksiKredit := helper.Int64NullableToInt(data.FreqCreditKeseluruhan)
	frekuensiTransaksiDebit := fmt.Sprintf("%d kali", intFrekuensiTransaksiDebit)
	frekuensiTransaksiKredit := fmt.Sprintf("%d kali", intFrekuensiTransaksiKredit)

	res := &ResponseListRekomendasiNasabah{
		CIF:                      data.CifNasabahRekan,
		KeteranganInformasi:      helper.StringToStringNullable("CIF"),
		NamaNasabahRekanan:       namaBri,
		Inisial:                  inisialBri,
		StatusTindakLanjut:       helper.StringToStringNullable("Belum"),
		PhoneNumber:              phonenumberNasabah,
		AlamatDomisili:           alamatDomisili,
		SektorIndustri:           sektorEkonomi,
		RatasSaldo:               helper.CurrencyFormat(data.RatasSaldoSimpananBri),
		TotalUangMasuk:           helper.CurrencyFormat(data.TotalCreditKeseluruhan),
		UangKeluarKeNonBRI:       helper.CurrencyFormat(data.TotalDebitNonBri),
		UangKeluar:               helper.CurrencyFormat(data.TotalDebitKeseluruhan),
		FrekuensiTransaksiDebit:  &frekuensiTransaksiDebit,
		UangMasuk:                helper.CurrencyFormat(data.TotalCreditKeseluruhan),
		FrekuensiTransaksiKredit: &frekuensiTransaksiKredit,
	}

	// fmt.Println("sektor ekonomi ->", helper.StringNullableToString(data.SektorEkonomi))

	return res
}

type ResponseListRekomendasiNasabahPaginationDto struct {
	Data                []*ResponseListRekomendasiNasabah `json:"data"`
	DataTerakhirUpdated *string                           `json:"data_terakhir_updated"`
	Paginator           models.Paginator                  `json:"paginator"`
	TabCounter          TabCounter                        `json:"tab"`
}

func (a ResponseListRekomendasiNasabahPaginationDto) MappingToPaginatorDto(data []*ResponseListRekomendasiNasabah, lastUpdate *time.Time, page, limit, totalAllRecords, totalNonBRI, totalDitindaklanjuti int) ResponseListRekomendasiNasabahPaginationDto {
	// func (a ResponseListRekomendasiNasabahPaginationDto) MappingToPaginatorDto(data []*ResponseListRekomendasiNasabah, lastUpdate *time.Time, page, limit, totalAllRecords int) ResponseListRekomendasiNasabahPaginationDto {
	a.Data = data
	a.Paginator = a.Paginator.MappingPaginator(page, limit, totalAllRecords, len(data))
	a.DataTerakhirUpdated = helper.DateTimeNullableToStringNullableWithFormat(lastUpdate, "2 January 2006")
	a.TabCounter.NasabahBRI = totalAllRecords
	a.TabCounter.NonBRI = totalNonBRI
	a.TabCounter.Ditindaklanjuti = totalDitindaklanjuti
	return a
}

type TabCounter struct {
	NasabahBRI      int `json:"-"`
	NonBRI          int `json:"-"`
	Ditindaklanjuti int `json:"ditindaklanjuti"`
}

type ResponseListRekomendasiNasabahNonBRI struct {
	KodeNonBri               *string `json:"kode_non_bri"`
	Bank                     *string `json:"bank" gorm:"column:bank"`
	KeteranganInformasi      *string `json:"keterangan_informasi"`
	NamaNasabah              *string `json:"nama_nasabah"`
	Inisial                  *string `json:"inisial"`
	StatusTindakLanjut       *string `json:"status_tindaklanjut"`
	NasabahBRI               *string `json:"nasabah_bri"`
	CIF                      *string `json:"cif"`
	UangKeluar               *string `json:"total_transaksi_debit_bri"`
	FrekuensiTransaksiDebit  *string `json:"frekuensi_transaksi_debit"`
	UangMasuk                *string `json:"total_transaksi_kredit_bri"`
	FrekuensiTransaksiKredit *string `json:"frekuensi_transaksi_kredit"`
	PhoneNumber              *string `json:"no_telepon"`
	RmNasabahBri             *string `json:"-"`
	Cabang                   *string `json:"-"`
}

func (data *GetListQueryStruct) MappingResponseListRekomendasiNasabahNonBRI() *ResponseListRekomendasiNasabahNonBRI {
	// nama := helper.StringNullableToString(data.NamaNasabahNonBRI)
	// inisial := getInitialDetail(nama)
	var namaBankLawan *string
	if data.NamaBankLawan == nil {
		namaBankLawan = helper.StringToStringNullable("-")
	} else {
		namaBankLawan = data.NamaBankLawan
	}

	var namaNonBri, inisialNonBri *string
	if data.NamaNasabahNonBRI == nil || helper.StringNullableToString(data.NamaNasabahNonBRI) == "" {
		namaNonBri = helper.StringToStringNullable("-")
		inisialNonBri = helper.StringToStringNullable("-")
	} else {
		namaNonBri = data.NamaNasabahNonBRI
		tempNamaNonBri := helper.StringNullableToString(data.NamaNasabahNonBRI)
		tempInisialNonBri := getInitialDetail(tempNamaNonBri)
		inisialNonBri = helper.StringToStringNullable(tempInisialNonBri)
	}

	// var phonenumberRm *string
	// if data.PhonenumberRm == nil || helper.StringNullableToString(data.PhonenumberRm) == "" {
	// 	phonenumberRm = helper.StringToStringNullable("-")
	// } else {
	// 	phonenumberRm = data.PhonenumberRm
	// }

	var phonenumberNasabah *string
	if data.PhonenumberTabelNon == nil || helper.StringNullableToString(data.PhonenumberTabelNon) == "" {
		phonenumberNasabah = helper.StringToStringNullable("-")
	} else {
		phonenumberNasabah = data.PhonenumberTabelNon
	}

	var namaRm *string
	if data.NamaRm == nil || helper.StringNullableToString(data.NamaRm) == "" {
		namaRm = helper.StringToStringNullable("-")
	} else {
		namaRm = data.NamaRm
	}

	var branchRm *string
	if data.BranchRm == nil || helper.StringNullableToString(data.BranchRm) == "" {
		branchRm = helper.StringToStringNullable("-")
	} else {
		branchRm = data.BranchRm
	}

	intFrekuensiTransaksiDebit := helper.Int64NullableToInt(data.FreqDebitNonBri)
	intFrekuensiTransaksiKredit := helper.Int64NullableToInt(data.FreqCreditNonBri)
	frekuensiTransaksiDebit := fmt.Sprintf("%d kali", intFrekuensiTransaksiDebit)
	frekuensiTransaksiKredit := fmt.Sprintf("%d kali", intFrekuensiTransaksiKredit)

	res := &ResponseListRekomendasiNasabahNonBRI{
		KodeNonBri:               data.KodeNonBri,
		Bank:                     namaBankLawan,
		KeteranganInformasi:      helper.StringToStringNullable("BANK"),
		NamaNasabah:              namaNonBri,
		Inisial:                  inisialNonBri,
		StatusTindakLanjut:       helper.StringToStringNullable("Belum"),
		NasabahBRI:               data.NamaNasabahRekanTabelNon,
		CIF:                      data.CifNasabahRekanTabelNon,
		UangKeluar:               helper.CurrencyFormat(data.TotalDebitNonBRITabelNon),
		FrekuensiTransaksiDebit:  helper.StringToStringNullable(frekuensiTransaksiDebit),
		UangMasuk:                helper.CurrencyFormat(data.TotalCreditNonBRITabelNon),
		FrekuensiTransaksiKredit: helper.StringToStringNullable(frekuensiTransaksiKredit),
		PhoneNumber:              phonenumberNasabah,
		RmNasabahBri:             namaRm,
		Cabang:                   branchRm,
	}

	return res
}

type ResponseListRekomendasiNasabahNonBRIPaginationDto struct {
	Data                []*ResponseListRekomendasiNasabahNonBRI `json:"data"`
	DataTerakhirUpdated *string                                 `json:"data_terakhir_updated"`
	Paginator           models.Paginator                        `json:"paginator"`
	TabCounter          TabCounter                              `json:"tab"`
}

func (a ResponseListRekomendasiNasabahNonBRIPaginationDto) MappingToPaginatorDto(data []*ResponseListRekomendasiNasabahNonBRI, lastUpdate *time.Time, page, limit, totalRekomendasi, totalAllRecords, totalDitindaklanjuti int) ResponseListRekomendasiNasabahNonBRIPaginationDto {
	// func (a ResponseListRekomendasiNasabahNonBRIPaginationDto) MappingToPaginatorDto(data []*ResponseListRekomendasiNasabahNonBRI, lastUpdate *time.Time, page, limit, totalAllRecords int) ResponseListRekomendasiNasabahNonBRIPaginationDto {
	a.Data = data
	a.Paginator = a.Paginator.MappingPaginator(page, limit, totalAllRecords, len(data))
	a.DataTerakhirUpdated = helper.DateTimeNullableToStringNullableWithFormat(lastUpdate, "2 January 2006")
	a.TabCounter.NasabahBRI = totalRekomendasi
	a.TabCounter.NonBRI = totalAllRecords
	a.TabCounter.Ditindaklanjuti = totalDitindaklanjuti
	return a
}

type ResponseListDitindaklanjuti struct {
	Badge               *string `json:"badge"`
	CIF                 *string `json:"cif,omitempty"`
	KeteranganInformasi *string `json:"keterangan_informasi"`
	NamaNasabah         *string `json:"nama_nasabah"`
	Inisial             *string `json:"inisial"`
	StatusTindakLanjut  *string `json:"status_tindaklanjut,omitempty"`
	StatusKetertarikan  *string `json:"status_ketertarikan,omitempty"`
	PhoneNumber         *string `json:"no_telepon,omitempty"`
	SektorIndustri      *string `json:"sektor_industri,omitempty"`
	RatasSaldo          *string `json:"ratas_saldo,omitempty"`
	TotalUangMasuk      *string `json:"total_kredit_keseluruhan,omitempty"`
	UangKeluarKeNonBRI  *string `json:"transaksi_debit_non_bri,omitempty"`

	KodeNonBri               *string `json:"kode_non_bri,omitempty"`
	Bank                     *string `json:"bank,omitempty"`
	NasabahBRI               *string `json:"nasabah_bri,omitempty"`
	UangKeluar               *string `json:"total_transaksi_debit_bri,omitempty"`
	FrekuensiTransaksiDebit  *string `json:"frekuensi_transaksi_debit,omitempty"`
	UangMasuk                *string `json:"total_transaksi_kredit_bri,omitempty"`
	FrekuensiTransaksiKredit *string `json:"frekuensi_transaksi_kredit,omitempty"`
	RmNasabahBri             *string `json:"-,omitempty"`
	Cabang                   *string `json:"-,omitempty"`

	NomorRekening       *string `json:"nomor_rekening,omitempty"`
	HasilAkuisisi       *string `json:"hasil_akuisisi,omitempty"`
	TanggalDihubungi    *string `json:"tanggal_dihubungi,omitempty"`
	CatatanTindaklanjut *string `json:"catatan_tindaklanjut,omitempty"`
}

func (data *GetListQueryStruct) MappingResponseListDitindaklanjuti() *ResponseListDitindaklanjuti {

	//nama nasabah, cif, keterangan informasi
	// var nama string
	var inisial, namaNasabah, cif, keteranganInformasi, nasabahBRI, bank, statusKetertarikan, badge, rmNasabahBri, cabangRm, responsePhonenumber *string
	var kodeNonBri, ratasSaldo, totalUangMasuk, uangKeluarKeNonBRI *string
	var uangKeluar, uangMasuk, responseFrekuensiTransaksiDebit, responseFrekuensiTransaksiKredit *string
	var hasilAkuisisi, tanggalDihubungi, catatanTindaklanjut, nomorRekening *string

	var statusTindakLanjut *string
	if helper.Int64NullableToInt(data.KodeKunjungan) == 0 || helper.Int64NullableToInt(data.KodeKunjungan) == 2 {
		statusTindakLanjut = data.StatusHubungi
	} else if helper.Int64NullableToInt(data.KodeHubungi) == 2 && helper.Int64NullableToInt(data.KodeKunjungan) == 2 {
		statusTindakLanjut = data.StatusHubungi
	} else {
		statusTindakLanjut = data.StatusKunjungan
	}

	intFrekuensiTransaksiDebitNonBri := helper.Int64NullableToInt(data.FreqDebitNonBri)
	intFrekuensiTransaksiKreditNonBri := helper.Int64NullableToInt(data.FreqCreditNonBri)
	frekuensiTransaksiDebitNonBri := fmt.Sprintf("%d kali", intFrekuensiTransaksiDebitNonBri)
	frekuensiTransaksiKreditNonBri := fmt.Sprintf("%d kali", intFrekuensiTransaksiKreditNonBri)

	intFrekuensiTransaksiDebitBri := helper.Int64NullableToInt(data.FreqDebitKeseluruhan)
	intFrekuensiTransaksiKreditBri := helper.Int64NullableToInt(data.FreqCreditKeseluruhan)
	frekuensiTransaksiDebitBri := fmt.Sprintf("%d kali", intFrekuensiTransaksiDebitBri)
	frekuensiTransaksiKreditBri := fmt.Sprintf("%d kali", intFrekuensiTransaksiKreditBri)

	// var statusTindakLanjutNonBri *string

	var namaNonBri, inisialNonBri *string
	if data.NamaNasabahNonBRI == nil || helper.StringNullableToString(data.NamaNasabahNonBRI) == "" {
		namaNonBri = helper.StringToStringNullable("-")
		inisialNonBri = helper.StringToStringNullable("-")
	} else {
		namaNonBri = data.NamaNasabahNonBRI
		tempNamaNonBri := helper.StringNullableToString(data.NamaNasabahNonBRI)
		tempInisialNonBri := getInitialDetail(tempNamaNonBri)
		inisialNonBri = helper.StringToStringNullable(tempInisialNonBri)
	}

	var phonenumber *string
	if data.Phonenumber == nil || helper.StringNullableToString(data.Phonenumber) == "" {
		phonenumber = helper.StringToStringNullable("-")
	} else {
		phonenumber = data.Phonenumber
	}

	var phonenumberBriTabelNon *string
	if data.PhonenumberTabelNon == nil || helper.StringNullableToString(data.PhonenumberTabelNon) == "" {
		phonenumberBriTabelNon = helper.StringToStringNullable("-")
	} else {
		phonenumberBriTabelNon = data.PhonenumberTabelNon
	}

	var namaBri, inisialBri *string
	if data.NamaNasabahRekan == nil || helper.StringNullableToString(data.NamaNasabahRekan) == "" {
		namaBri = helper.StringToStringNullable("-")
		inisialBri = helper.StringToStringNullable("-")
	} else {
		namaBri = data.NamaNasabahRekan
		tempNamaBri := helper.StringNullableToString(data.NamaNasabahRekan)
		tempInisialBri := getInitialDetail(tempNamaBri)
		inisialBri = helper.StringToStringNullable(tempInisialBri)
	}

	if strings.ToLower(helper.StringNullableToString(data.TipeCustomer)) == "nonbri" {
		badge = helper.StringToStringNullable("Non BRI")
		kodeNonBri = data.KodeNonBri
		// nama = helper.StringNullableToString(data.NamaNasabahNonBRI)
		inisial = inisialNonBri
		namaNasabah = namaNonBri
		cif = data.CifNasabahRekanTabelNon
		keteranganInformasi = helper.StringToStringNullable("BANK")
		nasabahBRI = data.NamaNasabahRekanTabelNon
		statusTindakLanjut = data.StatusHubungi
		if data.NamaBankLawan == nil {
			bank = helper.StringToStringNullable("-")
		} else {
			bank = data.NamaBankLawan
		}

		ratasSaldo = helper.StringToStringNullableNullValue("")
		totalUangMasuk = helper.StringToStringNullableNullValue("")
		uangKeluarKeNonBRI = helper.StringToStringNullableNullValue("")

		responseFrekuensiTransaksiDebit = helper.StringToStringNullable(frekuensiTransaksiDebitNonBri)
		responseFrekuensiTransaksiKredit = helper.StringToStringNullable(frekuensiTransaksiKreditNonBri)
		uangKeluar = helper.CurrencyFormat(data.TotalDebitNonBRITabelNon)
		uangMasuk = helper.CurrencyFormat(data.TotalCreditNonBRITabelNon)
		// rmNasabahBri = data.NamaRm
		// cabangRm = data.BranchRm
		if data.NamaRm == nil {
			rmNasabahBri = helper.StringToStringNullable("-")
		} else {
			rmNasabahBri = data.NamaRm
		}
		if data.BranchRm == nil {
			cabangRm = helper.StringToStringNullable("-")
		} else {
			cabangRm = data.BranchRm
		}

		responsePhonenumber = phonenumberBriTabelNon

		tglHubungiString := helper.DateTimeNullableToDateTime(data.TglHubungi)
		tanggalHubungi := helper.StringToStringNullable(tglHubungiString.Format("2 January 2006"))

		if helper.Int64NullableToInt(data.KodeHubungi) == 1 && helper.Int64NullableToInt(data.KodeKetertarikan) == 1 {
			statusTindakLanjut = nil
			hasilAkuisisi = data.StatusKetertarikan
			tanggalDihubungi = tanggalHubungi
			catatanTindaklanjut = nil
			nomorRekening = helper.StringToStringNullable(helper.FloatNUllableToString(data.InputNorek))
		} else if helper.Int64NullableToInt(data.KodeHubungi) == 1 && helper.Int64NullableToInt(data.KodeKetertarikan) == 2 {
			statusTindakLanjut = nil
			hasilAkuisisi = data.StatusKetertarikan
			tanggalDihubungi = tanggalHubungi
			catatanTindaklanjut = data.CatatanKetertarikan
			nomorRekening = nil
		} else if helper.Int64NullableToInt(data.KodeHubungi) == 1 && helper.Int64NullableToInt(data.KodeKetertarikan) == 0 {
			hasilAkuisisi = nil
			tanggalDihubungi = tanggalHubungi
			catatanTindaklanjut = nil
			nomorRekening = nil
		} else {
			hasilAkuisisi = nil
			tanggalDihubungi = nil
			catatanTindaklanjut = data.CatatanHubungi
			nomorRekening = nil
		}

	} else {
		badge = helper.StringToStringNullable("Nasabah BRI")
		kodeNonBri = nil
		// nama = helper.StringNullableToString(data.NamaNasabahRekan)
		inisial = inisialBri
		namaNasabah = namaBri
		cif = data.CifNasabahRekan
		keteranganInformasi = helper.StringToStringNullable("CIF")
		nasabahBRI = nil
		statusKetertarikan = data.StatusKetertarikan

		ratasSaldo = helper.CurrencyFormat(data.RatasSaldoSimpananBri)
		totalUangMasuk = helper.CurrencyFormat(data.TotalCreditKeseluruhan)
		uangKeluarKeNonBRI = helper.CurrencyFormat(data.TotalDebitNonBri)

		responseFrekuensiTransaksiDebit = helper.StringToStringNullable(frekuensiTransaksiDebitBri)
		responseFrekuensiTransaksiKredit = helper.StringToStringNullable(frekuensiTransaksiKreditBri)
		uangKeluar = helper.CurrencyFormat(data.TotalDebitKeseluruhan)
		uangMasuk = helper.CurrencyFormat(data.TotalCreditKeseluruhan)
		rmNasabahBri = nil
		cabangRm = nil
		responsePhonenumber = phonenumber

		hasilAkuisisi = nil
		tanggalDihubungi = nil
		catatanTindaklanjut = nil
		nomorRekening = nil
	}
	res := &ResponseListDitindaklanjuti{
		Badge:                    badge,
		CIF:                      cif,
		Bank:                     bank,
		KeteranganInformasi:      keteranganInformasi,
		NamaNasabah:              namaNasabah,
		Inisial:                  inisial,
		StatusTindakLanjut:       statusTindakLanjut,
		StatusKetertarikan:       statusKetertarikan,
		NasabahBRI:               nasabahBRI,
		UangKeluar:               uangKeluar,
		UangMasuk:                uangMasuk,
		FrekuensiTransaksiDebit:  responseFrekuensiTransaksiDebit,
		FrekuensiTransaksiKredit: responseFrekuensiTransaksiKredit,

		KodeNonBri:         kodeNonBri,
		PhoneNumber:        responsePhonenumber,
		RmNasabahBri:       rmNasabahBri,
		Cabang:             cabangRm,
		SektorIndustri:     data.GroupUsaha,
		RatasSaldo:         ratasSaldo,
		TotalUangMasuk:     totalUangMasuk,
		UangKeluarKeNonBRI: uangKeluarKeNonBRI,

		HasilAkuisisi:       hasilAkuisisi,
		TanggalDihubungi:    tanggalDihubungi,
		CatatanTindaklanjut: catatanTindaklanjut,
		NomorRekening:       nomorRekening,
	}

	return res
}

type ResponseListDitindaklanjutiPaginationDto struct {
	Data                []*ResponseListDitindaklanjuti `json:"data"`
	DataTerakhirUpdated *string                        `json:"data_terakhir_updated"`
	Paginator           models.Paginator               `json:"paginator"`
	TabCounter          TabCounter                     `json:"tab"`
}

func (a ResponseListDitindaklanjutiPaginationDto) MappingToPaginatorDto(data []*ResponseListDitindaklanjuti, lastUpdate *time.Time, page, limit, totalRekomendasi, totalNonBRI, totalAllRecords int) ResponseListDitindaklanjutiPaginationDto {
	// func (a ResponseListDitindaklanjutiPaginationDto) MappingToPaginatorDto(data []*ResponseListDitindaklanjuti, lastUpdate *time.Time, page, limit, totalAllRecords int) ResponseListDitindaklanjutiPaginationDto {
	a.Data = data
	a.Paginator = a.Paginator.MappingPaginator(page, limit, totalAllRecords, len(data))
	a.DataTerakhirUpdated = helper.DateTimeNullableToStringNullableWithFormat(lastUpdate, "2 January 2006")
	a.TabCounter.NasabahBRI = totalRekomendasi
	a.TabCounter.NonBRI = totalNonBRI
	a.TabCounter.Ditindaklanjuti = totalAllRecords
	return a
}
