package main

import (
	"log"
	"ms-bribrain-link5/dbconn"
	"ms-bribrain-link5/helper/logger"
	"ms-bribrain-link5/middleware"
	"os"
	"strconv"
	"time"

	"github.com/joho/godotenv"

	_sampleModuleHttpHandler "ms-bribrain-link5/sample_module/delivery/http"
	_sampleModuleRepo "ms-bribrain-link5/sample_module/repository"
	_sampleModuleUsecase "ms-bribrain-link5/sample_module/usecase"

	"github.com/labstack/echo/v4"
	_echoMiddleware "github.com/labstack/echo/v4/middleware"

	_masterDataHttpHandler "ms-bribrain-link5/modules/master_data/delivery/http"
	_masterDataRepo "ms-bribrain-link5/modules/master_data/repository"
	_masterDataUsecase "ms-bribrain-link5/modules/master_data/usecase"

	_nasabahHttpHandler "ms-bribrain-link5/modules/nasabah/delivery/http"
	_nasabahRepo "ms-bribrain-link5/modules/nasabah/repository"
	_nasabahUsecase "ms-bribrain-link5/modules/nasabah/usecase"

	_tindaklanjutHttpHandler "ms-bribrain-link5/modules/tindaklanjut/delivery/http"
	_tindaklanjutRepo "ms-bribrain-link5/modules/tindaklanjut/repository"
	_tindaklanjutUsecase "ms-bribrain-link5/modules/tindaklanjut/usecase"

	_ "ms-bribrain-link5/swagger"

	echoSwagger "github.com/swaggo/echo-swagger"
)

// @title Echo Swagger ms-bribrain-link5 API
// @version 1.0
// @description This is a sample server server.
// @termsOfService http://swagger.io/terms/

// @contact.name API Support
// @contact.url http://www.swagger.io/support
// @contact.email support@swagger.io

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @BasePath /
// @schemes http
func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	dbConn := dbconn.DB()
	dbConn2 := dbconn.SharedDB()
	//var dbConn *gorm.DB = nil
	l := logger.L
	appPort := os.Getenv("PORT")
	timeout, _ := strconv.Atoi(os.Getenv("APP_TIMEOUT"))
	env := os.Getenv("APP_ENV")

	e := echo.New()
	middL := middleware.InitMiddleware()
	e.Use(middL.Log)
	//e.Use(middL.CORS)
	e.Use(_echoMiddleware.CORSWithConfig(_echoMiddleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept, echo.HeaderAuthorization},
		AllowMethods: []string{"POST", "PUT", "DELETE", "OPTIONS"},
	}))
	e.Use(middL.CORSValidationGlobalResponse)

	sampleModuleRepo := _sampleModuleRepo.NewSampleModuleRepository(dbConn, l)
	nasabahRepo := _nasabahRepo.NewNasabahRepository(dbConn, l)
	tindaklanjutRepo := _tindaklanjutRepo.NewTindaklanjutRepository(dbConn, l)
	masterDataRepo := _masterDataRepo.NewMasterDataRepository(dbConn, dbConn2, l)

	timeoutContext := time.Duration(timeout) * time.Second

	sampleModuleUsecase := _sampleModuleUsecase.NewSampleModuleUsecase(sampleModuleRepo, l, timeoutContext)
	nasabahUsecase := _nasabahUsecase.NewNasabahUsecase(nasabahRepo, l, timeoutContext)
	tindaklanjutUsecase := _tindaklanjutUsecase.NewTindaklanjutUsecase(tindaklanjutRepo, l, timeoutContext)
	masterDataUsecase := _masterDataUsecase.NewMasterDataUsecase(masterDataRepo, l, timeoutContext)

	if env != "production" {
		e.GET("/swagger/*", echoSwagger.WrapHandler)
	}

	_sampleModuleHttpHandler.NewsampleModuleHandler(e, sampleModuleUsecase, l)
	_nasabahHttpHandler.NewNasabahHandler(e, middL, nasabahUsecase, l)
	_tindaklanjutHttpHandler.NewTindaklanjutHandler(e, middL, tindaklanjutUsecase, l)
	_masterDataHttpHandler.NewmasterDataHandler(e, middL, masterDataUsecase, l)
	log.Fatal(e.Start(":" + appPort))
}
