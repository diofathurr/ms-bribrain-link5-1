package http

import (
	"context"
	"ms-bribrain-link5/helper"
	"ms-bribrain-link5/helper/logger"
	models2 "ms-bribrain-link5/helper/models"
	"ms-bribrain-link5/sample_module"
	_ "ms-bribrain-link5/swagger/models"
	"net/http"

	"github.com/labstack/echo/v4"
)

// ResponseError represent the response error struct
type ResponseError struct {
	Message string `json:"message"`
}

// sampleModuleHandler  represent the http handler for country
type SampleModuleHandler struct {
	SampleModuleUsecase sample_module.Usecase
	Log                 logger.Logger
}

// NewsampleModuleHandler will initialize the countrys/ resources endpoint
func NewsampleModuleHandler(e *echo.Echo, us sample_module.Usecase, log logger.Logger) {
	handler := &SampleModuleHandler{
		Log:                 log,
		SampleModuleUsecase: us,
	}
	e.GET("/", handler.Documentation)
	gapis := e.Group("/api")
	gapis.GET("/sample-module", handler.List)

}
func (a *SampleModuleHandler) Documentation(c echo.Context) error {
	http.Redirect(c.Response(), c.Request(), "/swagger/index.html", 301)
	return nil
}

// List godoc
// @Summary List.
// @Description List.
// @Tags sampleModule
// @Accept  json
// @Produce  json
// @Param page query string false "page"
// @Param limit query string false "limit"
// @Success 200 {object} models.SwaggerListSuccess
// @Failure 404 {object} models.SwaggerErrorNotFound
// @Failure 409 {object} models.SwaggerErrorConflict
// @Failure 401 {object} models.SwaggerErrorUnAuthorize
// @Failure 400 {object} models.SwaggerErrorBadParamInput
// @Failure 500 {object} models.SwaggerOtherInternalServerError
// @Failure 400 {object} models.SwaggerErrorInvalidMethod
// @Failure 405 {object} models.SwaggerErrorMethodNotAllowed
// @Router /api/sample-module [get]
func (a *SampleModuleHandler) List(c echo.Context) error {
	response := new(models2.Response)

	qpage := c.QueryParam("page")
	qperPage := c.QueryParam("limit")

	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}
	limit, page, offset := helper.Pagination(qpage, qperPage)
	result, message, err := a.SampleModuleUsecase.GetList(ctx, page, limit, offset)
	if err != nil {
		a.Log.Error("sample_module.delivery.http.SampleModuleHandler.List: %s", err.Error())
		response.MappingResponseError(helper.GetStatusCode(err), message, err.Error(), c)
		return c.JSON(response.StatusCode, response)
	}
	response.MappingResponseSuccess(message, result, c)
	return c.JSON(response.StatusCode, response)
}
