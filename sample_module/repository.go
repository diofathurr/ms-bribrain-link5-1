package sample_module

import (
	"ms-bribrain-link5/models"

	"golang.org/x/net/context"
)

type Repository interface {
	List(ctx context.Context, limit, offset int) (res []models.SampleModule, err error)
	Count(ctx context.Context) (res int, err error)
}
