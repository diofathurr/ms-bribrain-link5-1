package sample_module

import (
	"ms-bribrain-link5/models"

	"golang.org/x/net/context"
)

type Usecase interface {
	GetList(ctx context.Context, page, limit, offset int) (res *models.SampleModulePaginationDto, message string, err error)
}
