BINARY=engine
BRANCH=$(shell git rev-parse --abbrev-ref 'HEAD')
commit:
	go test ./... -coverprofile=coverage.out
	go tool cover -html=coverage.out
	git commit

test: 
	go test ./... -coverprofile=coverage.out
	go tool cover -html=coverage.out

engine:
	go build -o ${BINARY} main.go

unittest:
	go test -short  ./...

docker_build:
	docker build --pull --rm -f "Dockerfile" -t ms-bribrain-link5 "." --build-arg BRANCH=$(BRANCH)

docker_with_unit_test:
	go test ./... -coverprofile=coverage.out
	go tool cover -html=coverage.out
	docker build --pull --rm -f "Dockerfile" -t ms-bribrain-link5 "." --build-arg BRANCH=$(BRANCH)

swagger_documentation:
	swag init -g main.go --output swagger

docker_heroku:
	heroku container:login
	docker build . -t registry.heroku.com/ms-bribrain-link5-uat/web -f "hk.Dockerfile" --build-arg BRANCH=develop2
	docker push registry.heroku.com/ms-bribrain-link5-uat/web
	heroku container:release --app ms-bribrain-link5-uat web
	heroku open --app ms-bribrain-link5-uat

run:
	docker-compose up -d

stop:
	docker-compose down
