module ms-bribrain-link5

go 1.15

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/apache/calcite-avatica-go/v5 v5.0.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-playground/validator/v10 v10.9.0
	github.com/go-sql-driver/mysql v1.6.0
	github.com/google/uuid v1.3.0 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/labstack/echo/v4 v4.5.0
	github.com/leekchan/accounting v1.0.0
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/stretchr/testify v1.7.0
	github.com/swaggo/echo-swagger v1.1.2
	github.com/swaggo/swag v1.7.1
	golang.org/x/net v0.0.0-20210825183410-e898025ed96a
	gorm.io/driver/mysql v1.1.2
	gorm.io/gorm v1.21.13
)
