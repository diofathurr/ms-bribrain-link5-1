package models

type SwaggerRequestSubmitTindaklanjutPositive1 struct {
	Cif                 string `json:"cif" example:"ABC1264"`
	KodeHubungi         string `json:"kode_hubungi" example:"1"`
	KodeKunjungan       string `json:"kode_kunjungan" example:"1"`
	KodeKetertarikan    string `json:"kode_ketertarikan" example:"1"`
	TglHubungi          string `json:"tanggal_hubungi" example:"2021-10-08"`
	TglKunjungan        string `json:"tanggal_kunjungan" example:"2021-10-09"`
	CatatanKetertarikan string `json:"catatan_ketertarikan" example:"nasabah tertarik"`
}

type SwaggerRequestSubmitTindaklanjutPositive2 struct {
	Cif                 string `json:"cif" example:"ABC1264"`
	KodeHubungi         string `json:"kode_hubungi" example:"1"`
	KodeKunjungan       string `json:"kode_kunjungan" example:"2"`
	KodeKetertarikan    string `json:"kode_ketertarikan" example:"1"`
	TglHubungi          string `json:"tanggal_hubungi" example:"2021-10-08"`
	CatatanKetertarikan string `json:"catatan_ketertarikan" example:"nasabah tertarik"`
	CatatanKunjungan    string `json:"catatan_kunjungan" example:"Saya sudah mengenal nasabah Pak Akin sejak lama, jadi saya tidak perlu mengunjungi"`
}

type SwaggerRequestSubmitTindaklanjutNegative1 struct {
	Cif                 string `json:"cif" example:"ABC1264"`
	KodeHubungi         string `json:"kode_hubungi" example:"1"`
	KodeKetertarikan    string `json:"kode_ketertarikan" example:"2"`
	TglHubungi          string `json:"tanggal_hubungi" example:"2021-10-08"`
	CatatanKetertarikan string `json:"catatan_ketertarikan" example:"Nasabah sudah dihubungi namun untuk saat ini masih belum tidak tertarik karena cukup banyak alasan dan pertimbangannya."`
}

type SwaggerRequestSubmitTindaklanjutNegative2 struct {
	Cif              string `json:"cif" example:"ABC1264"`
	KodeHubungi      string `json:"kode_hubungi" example:"2"`
	KodeKunjungan    string `json:"kode_kunjungan" example:"2"`
	CatatanHubungi   string `json:"catatan_hubungi" example:"Nomor HP Tidak Aktif"`
	CatatanKunjungan string `json:"catatan_kunjungan" example:"Saya sudah mengenal nasabah Pak Akin sejak lama, jadi saya tidak perlu mengunjungi ke rumahnya"`
}

type SwaggerRequestSubmitTindaklanjutNegative3 struct {
	Cif            string `json:"cif" example:"ABC1264"`
	KodeHubungi    string `json:"kode_hubungi" example:"3"`
	CatatanHubungi string `json:"catatan_hubungi" example:"Tidak mengangkat telepon"`
}

type SwaggerRequestSubmitTindaklanjutOther struct {
	Cif              string `json:"cif" example:"ABC1264"`
	KodeHubungi      string `json:"kode_hubungi" example:"2"`
	KodeKunjungan    string `json:"kode_kunjungan" example:"1"`
	KodeKetertarikan string `json:"kode_ketertarikan" example:"3"`
	CatatanHubungi   string `json:"catatan_hubungi" example:"Nomor hp tidak aktif"`
	TglKunjungan     string `json:"tanggal_kunjungan" example:"2021-09-12"`
}

type SwaggerRequestSubmitTindaklanjutTandai struct {
	Cif                 string `json:"cif" example:"ABC1264"`
	KodeKunjungan       string `json:"kode_kunjungan" example:"3"`
	KodeKetertarikan    string `json:"kode_ketertarikan" example:"1"`
	CatatanKetertarikan string `json:"catatan_ketertarikan" example:"nasabah tertarik"`
}

type SwaggerRequestSubmitTindaklanjutNonBri struct {
	KodeNonBri     string `json:"kode_non_bri" example:"BRINON1650"`
	KodeHubungi    int    `json:"kode_hubungui" example:"2"`
	CatatanHubungi string `json:"catatan_hubungi" example:"Nasabah saya hubungi"`
}

type DefaultResponseSubmitTindaklanjut struct {
	Data       string `json:"data" example:"null"`
	Errors     string `json:"errors" example:"null"`
	Message    string `json:"message" example:"Sukses mengupdate tindaklanjut"`
	StatusCode int    `json:"status_code" example:"200"`
	StatusDesc string `json:"status_desc" example:"OK"`
}
