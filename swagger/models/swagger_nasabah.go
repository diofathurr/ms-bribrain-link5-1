package models

//request

type SwaggerGetDetailRequest struct {
	Cif string `json:"cif" example:"ABC1264"`
}

type SwaggerGetDetailRekomendasiSuccess struct {
	StatusCode int    `json:"status_code" example:"200"`
	Status     string `json:"status_desc" example:"OK"`
	Msg        string `json:"message" example:"Success"`
	Data       struct {
		DataTerakhirUpdated *string `json:"data_terakhir_updated" example:"17 August 2021"`
		Card                *struct {
			NamaDebitur        *string `json:"nama" example:"NURHAYATI"`
			Inisial            *string `json:"inisial" example:"N"`
			StatusTindakLanjut *string `json:"status_tindak_lanjut" example:"Belum"`
			RatasSaldo         *string `json:"ratas_saldo" example:"Rp10.000.000"`
			TotalUangMasuk     *string `json:"total_uang_masuk" example:"Rp10.000.000"`
			UangKeluarKeNonBRI *string `json:"uang_keluar_ke_non_bri" example:"Rp10.000.000"`
			AkuisisiSimpanan   *string `json:"akuisisi_simpanan" example:"Sudah"`
			AkuisisiPinjaman   *string `json:"akuisisi_pinjaman" example:"IBelum"`
			SektorIndustri     *string `json:"sektor_industri" example:"Industri Pengolahan"`
		} `json:"card"`
		DetailTier *struct {
			PosisiTier *string `json:"tier" example:"Tier 5"`
		} `json:"posisi_tier_nasabah"`
		InformasiPersonal *struct {
			TanggalLahir   *string `json:"tanggal_lahir" example:"26 Februari 1981"`
			Usia           *string `json:"usia" example:"40 Tahun"`
			JenisKelamin   *string `json:"jenis_kelamin" example:"Perempuan"`
			PhoneNumber    *string `json:"no_telepon" example:"0821999999999"`
			Pekerjaan      *string `json:"pekerjaan" example:"Pedagang"`
			AlamatPinjaman *string `json:"alamat_pinjaman" example:"Jl Bendungan Walahar No. 33"`
			AlamatDomisili *string `json:"alamat_domisili" example:"Jl Bendungan Hilir No. 1"`
			AlamatKTP      *string `json:"alamat_ktp" example:"Jl Bendungan Hilir No. 1"`
		} `json:"informasi_personal"`
		InformasiRekening *struct {
			CIF                        *string `json:"cif" example:"NRTI822"`
			NamaUnitKerja              *string `json:"nama_unit_kerja" example:"KC Bandung Dewi Sartika"`
			ProdukBRIYangDimiliki      *string `json:"produk_bri_yang_dimiliki" example:"BRITAMA"`
			TotalTransaksiKreditBRI    *string `json:"total_transaksi_kredit_bri" example:"Rp4.200.000"`
			TotalTransaksiDebitBRI     *string `json:"total_transaksi_debit_bri" example:"Rp4.200.000"`
			TotalTransaksiKreditNonBRI *string `json:"total_transaksi_kredit_non_bri" example:"Rp4.200.000"`
			TotalTransaksiDebitNonBRI  *string `json:"total_transaksi_debit_non_bri" example:"Rp4.200.000"`
			TotalKreditKeseluruhan     *string `json:"total_kredit_keseluruhan" example:"Rp4.200.000"`
			TotalDebitKeseluruhan      *string `json:"total_debit_keseluruhan" example:"Rp4.200.000"`
		} `json:"informasi_rekening"`
		Paginator struct {
			CurrentPage  int `json:"current_page" example:"1"`
			PerPage      int `json:"limit_per_page" example:"1"`
			PreviousPage int `json:"back_page" example:"1"`
			NextPage     int `json:"next_page" example:"2"`
			TotalRecords int `json:"total_records" example:"1"`
			TotalPages   int `json:"total_pages" example:"1"`
		} `json:"paginator"`
	} `json:"data"`
	Errors *string `json:"errors" example:"null"`
}

type SwaggerGetDetailDitindaklanjutiSuccess struct {
	StatusCode int    `json:"status_code" example:"200"`
	Status     string `json:"status_desc" example:"OK"`
	Msg        string `json:"message" example:"Success"`
	Data       struct {
		DataTerakhirUpdated *string `json:"data_terakhir_updated" example:"17 August 2021"`
		Card                *struct {
			NamaDebitur        *string `json:"nama" example:"NURHAYATI"`
			PhoneNumberMasking *string `json:"nomor_telepon" example:"08198888xxx"`
			Inisial            *string `json:"inisial" example:"N"`
			StatusTindakLanjut *string `json:"status_tindak_lanjut" example:"Belum"`
			StatusSimpanan     *string `json:"status_rekening_simpanan" example:"Dormant"`
			OSDH               *string `json:"sisa_os_dh" example:"Rp10.000.000"`
			PlafondAwal        *string `json:"plafond_awal" example:"Rp50.000.000"`
			TanggalHubungi     *string `json:"tanggal_dihubungi" example:"14 September 2021"`
			StatusKetertarikan *string `json:"status_ketertarikan" example:"Tertarik"`
			SektorEkonomi      *string `json:"sektor_ekonomi" example:"Industri Pengolahan"`
		} `json:"card"`
		DetailCatatanTindaklanjut []*struct {
			JudulCatatan     *string   `json:"judul_catatan" example:"Alasan Tertarik"`
			DeskripsiCatatan []*string `json:"deskripsi_catatan" example:"Ybs tertarik sekali"`
		} `json:"catatan_tindaklanjut"`
		Score *struct {
			Score               *float64 `json:"score" example:"721"`
			KeteranganDeskripsi *string  `json:"keterangan" example:"nasabah sangat direkomendasikan dll"`
		} `json:"score"`
		InformasiPersonal *struct {
			TanggalLahir        *string `json:"tanggal_lahir" example:"26 Februari 1981"`
			Usia                *string `json:"usia" example:"40 Tahun"`
			PhoneNumber         *string `json:"no_telepon" example:"0821999999999"`
			Pekerjaan           *string `json:"pekerjaan" example:"Pedagang"`
			AlamatTempatTinggal *string `json:"alamat_tempat_tinggal" example:"Jl Bendungan Walahar No. 33"`
			AlamatUsaha         *string `json:"alamat_usaha" example:"Jl Bendungan Hilir No. 1"`
		} `json:"informasi_personal"`
		InformasiRekening *struct {
			CIF                  *string `json:"cif" example:"NRTI822"`
			EstimasiPenghasilan  *string `json:"estimasi_penghasilan" example:"Rp7.000.000"`
			ProdukPinjaman       *string `json:"produk_pinjaman_saat_ini" example:"Kupedes"`
			RPC                  *string `json:"rpc_tersisa" example:"Rp4.200.000"`
			TanggalAktifTerakhir *string `json:"terakhir_aktif" example:"5 April 2020"`
		} `json:"informasi_rekening"`
		Paginator struct {
			CurrentPage  int `json:"current_page" example:"1"`
			PerPage      int `json:"limit_per_page" example:"1"`
			PreviousPage int `json:"back_page" example:"1"`
			NextPage     int `json:"next_page" example:"2"`
			TotalRecords int `json:"total_records" example:"1"`
			TotalPages   int `json:"total_pages" example:"1"`
		} `json:"paginator"`
	} `json:"data"`
	Errors *string `json:"errors" example:"null"`
}

type SwaggerGetListRequest struct {
	Limit  string `json:"limit" example:"1"`
	Page   string `json:"page" example:"5"`
	Search string `json:"search" example:""`
}

type SwaggerGetListRekomendasiSuccess struct {
	StatusCode int    `json:"status_code" example:"200"`
	Status     string `json:"status_desc" example:"OK"`
	Msg        string `json:"message" example:"Success"`
	Data       *struct {
		DataTerakhirUpdated *string `json:"data_terakhir_updated" example:"17 August 2021"`
		Card                []*struct {
			Acctno             float64 `json:"acctno" example:"123401003765106"`
			NamaDebitur        string  `json:"nama_debitur" example:"RASMANTO"`
			Inisial            string  `json:"inisial" example:"R"`
			PhoneNumber        string  `json:"no_telepon" example:"0867999000"`
			PhoneNumberMasking string  `json:"phonenumber_masking" example:"0867999xxx"`
			StatusSimpanan     string  `json:"status_rekening_simpanan" example:"Active"`
			SektorEkonomi      string  `json:"sektor_ekonomi" example:"Perdagangan Besar & Eceran"`
		} `json:"card"`
		Paginator struct {
			CurrentPage  int `json:"current_page" example:"1"`
			PerPage      int `json:"limit_per_page" example:"1"`
			PreviousPage int `json:"back_page" example:"1"`
			NextPage     int `json:"next_page" example:"2"`
			TotalRecords int `json:"total_records" example:"1"`
			TotalPages   int `json:"total_pages" example:"1"`
		} `json:"paginator"`
		DetailCountTab *struct {
			TabRekomendasi     int `json:"rekomendasi" example:"15"`
			TabDitindaklanjuti int `json:"ditindaklanjuti" example:"5"`
			TabPemberdayaan    int `json:"pemberdayaan" example:"2"`
		} `json:"tab"`
	} `json:"data"`
	Errors *string `json:"errors" example:"null"`
}

type SwaggerGetListDitindaklanjutiSuccess struct {
	StatusCode int    `json:"status_code" example:"200"`
	Status     string `json:"status_desc" example:"OK"`
	Msg        string `json:"message" example:"Success"`
	Data       *struct {
		DataTerakhirUpdated *string `json:"data_terakhir_updated" example:"17 August 2021"`
		Card                []*struct {
			Acctno             float64 `json:"acctno" example:"123401003765106"`
			NamaDebitur        string  `json:"nama_debitur" example:"RASMANTO"`
			Inisial            string  `json:"inisial" example:"R"`
			PhoneNumber        string  `json:"no_telepon" example:"0867999000"`
			PhoneNumberMasking string  `json:"phonenumber_masking" example:"0867999xxx"`
			StatusTindaklanjut string  `json:"status_tindak_lanjut" example:"Sudah Dikunjungi"`
			StatusKetertarikan string  `json:"status_ketertarikan" example:"Tidak Tertarik"`
		} `json:"card"`
		Paginator struct {
			CurrentPage  int `json:"current_page" example:"1"`
			PerPage      int `json:"limit_per_page" example:"1"`
			PreviousPage int `json:"back_page" example:"1"`
			NextPage     int `json:"next_page" example:"2"`
			TotalRecords int `json:"total_records" example:"1"`
			TotalPages   int `json:"total_pages" example:"1"`
		} `json:"paginator"`
		DetailCountTab *struct {
			TabRekomendasi     int `json:"rekomendasi" example:"15"`
			TabDitindaklanjuti int `json:"ditindaklanjuti" example:"5"`
			TabPemberdayaan    int `json:"pemberdayaan" example:"2"`
		} `json:"tab"`
	} `json:"data"`
	Errors *string `json:"errors" example:"null"`
}

type SwaggerGetListPemberdayaanSuccess struct {
	StatusCode int    `json:"status_code" example:"200"`
	Status     string `json:"status_desc" example:"OK"`
	Msg        string `json:"message" example:"Success"`
	Data       *struct {
		DataTerakhirUpdated *string `json:"data_terakhir_updated" example:"17 August 2021"`
		Card                []*struct {
			Acctno             float64 `json:"acctno" example:"123401003765106"`
			NamaDebitur        string  `json:"nama_debitur" example:"RASMANTO"`
			Inisial            string  `json:"inisial" example:"R"`
			PhoneNumber        string  `json:"no_telepon" example:"0867999000"`
			PhoneNumberMasking string  `json:"phonenumber_masking" example:"0867999xxx"`
			StatusPemberdayaan string  `json:"status_pemberdayaan" example:"Belum"`
			SektorEkonomi      string  `json:"sektor_ekonomi" example:"Perdagangan Besar & Eceran"`
		} `json:"card"`
		Paginator struct {
			CurrentPage  int `json:"current_page" example:"1"`
			PerPage      int `json:"limit_per_page" example:"1"`
			PreviousPage int `json:"back_page" example:"1"`
			NextPage     int `json:"next_page" example:"2"`
			TotalRecords int `json:"total_records" example:"1"`
			TotalPages   int `json:"total_pages" example:"1"`
		} `json:"paginator"`
		DetailCountTab *struct {
			TabRekomendasi     int `json:"rekomendasi" example:"15"`
			TabDitindaklanjuti int `json:"ditindaklanjuti" example:"5"`
			TabPemberdayaan    int `json:"pemberdayaan" example:"2"`
		} `json:"tab"`
	} `json:"data"`
	Errors *string `json:"errors" example:"null"`
}
