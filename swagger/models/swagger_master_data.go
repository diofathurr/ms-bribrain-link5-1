package models

type SwaggerGetAlasanTidakCocokSuccess struct {
	StatusCode int    `json:"status_code" example:"200"`
	Status     string `json:"status_desc" example:"OK"`
	Msg        string `json:"message" example:"Success"`
	Data       []*struct {
		Kode      int    `json:"kode" example:"1"`
		Deskripsi string `json:"deskripsi" example:"Karakter buruk"`
	} `json:"data"`
	Errors *string `json:"errors" example:"null"`
}

type SwaggerGetAlasanTidakDapatDihubungiSuccess struct {
	StatusCode int    `json:"status_code" example:"200"`
	Status     string `json:"status_desc" example:"OK"`
	Msg        string `json:"message" example:"Success"`
	Data       []*struct {
		Kode      int    `json:"kode" example:"1"`
		Deskripsi string `json:"deskripsi" example:"Nomor HP Tidak Aktif"`
	} `json:"data"`
	Errors *string `json:"errors" example:"null"`
}

type SwaggerResponseOnboardingTextSuccess struct {
	StatusCode int    `json:"status_code" example:"200"`
	Status     string `json:"status_desc" example:"OK"`
	Msg        string `json:"message" example:"Success"`
	Data       *struct {
		OnBoardingTitle       string `json:"onboarding_title" example:"Apa itu Nasabah Bridaya?"`
		OnBoardingDescription string `json:"onboarding_description" example:"Nasabah Bridaya adalah ..."`
	} `json:"data"`
	Errors *string `json:"errors" example:"null"`
}
