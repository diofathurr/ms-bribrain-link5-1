package middleware_test

import (
	"net/http"
	test "net/http/httptest"
	"testing"

	"ms-bribrain-link5/middleware"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestGoMiddleware_CORS(t *testing.T) {
	e := echo.New()
	req := test.NewRequest(echo.GET, "/", nil)
	res := test.NewRecorder()
	c := e.NewContext(req, res)
	m := middleware.InitMiddleware()

	t.Run("success", func(t *testing.T) {
		h := m.CORS(echo.HandlerFunc(func(c echo.Context) error {
			return c.NoContent(http.StatusOK)
		}))

		err := h(c)
		require.NoError(t, err)
		assert.Equal(t, "*", res.Header().Get("Access-Control-Allow-Origin"))
	})
}

func TestGoMiddleware_Log(t *testing.T) {
	e := echo.New()
	req := test.NewRequest(echo.GET, "/", nil)
	res := test.NewRecorder()
	c := e.NewContext(req, res)
	m := middleware.InitMiddleware()

	t.Run("success", func(t *testing.T) {
		h := m.Log(echo.HandlerFunc(func(c echo.Context) error {
			return c.NoContent(http.StatusOK)
		}))

		err := h(c)
		require.NoError(t, err)
	})
}

func TestGoMiddleware_CORSValidationGlobalResponse(t *testing.T) {
	e := echo.New()
	req := test.NewRequest(echo.GET, "/", nil)
	res := test.NewRecorder()
	c := e.NewContext(req, res)
	m := middleware.InitMiddleware()

	t.Run("success", func(t *testing.T) {
		h := m.CORSValidationGlobalResponse(echo.HandlerFunc(func(c echo.Context) error {
			return c.NoContent(http.StatusOK)
		}))

		err := h(c)
		require.NoError(t, err)
		assert.Equal(t, 200, c.Response().Status)
	})

	t.Run("error-validation-invalid-post-content-type", func(t *testing.T) {
		h := m.CORSValidationGlobalResponse(echo.HandlerFunc(func(c echo.Context) error {
			return c.NoContent(http.StatusOK)
		}))

		err := h(c)
		require.NoError(t, err)
		assert.Equal(t, 200, c.Response().Status)
	})

	t.Run("error-validation-invalid-method", func(t *testing.T) {
		h := m.CORSValidationGlobalResponse(echo.HandlerFunc(func(c echo.Context) error {
			return c.NoContent(http.StatusOK)
		}))

		err := h(c)
		require.NoError(t, err)
		assert.Equal(t, 200, c.Response().Status)
	})

	t.Run("error-validation-method-not-found", func(t *testing.T) {
		h := m.CORSValidationGlobalResponse(echo.HandlerFunc(func(c echo.Context) error {
			return c.NoContent(http.StatusOK)
		}))

		err := h(c)
		require.NoError(t, err)
		assert.Equal(t, 200, c.Response().Status)
	})

	t.Run("error-validation-method-not-allowed", func(t *testing.T) {
		h := m.CORSValidationGlobalResponse(echo.HandlerFunc(func(c echo.Context) error {
			return c.NoContent(http.StatusOK)
		}))

		err := h(c)
		require.NoError(t, err)
		assert.Equal(t, 200, c.Response().Status)
	})
}

func TestGoMiddleware_Authentication(t *testing.T) {
	e := echo.New()
	req := test.NewRequest(echo.GET, "/", nil)
	res := test.NewRecorder()
	c := e.NewContext(req, res)
	m := middleware.InitMiddleware()

	t.Run("success", func(t *testing.T) {
		h := m.CORS(echo.HandlerFunc(func(c echo.Context) error {
			return c.NoContent(http.StatusOK)
		}))

		err := h(c)
		require.NoError(t, err)
		assert.Equal(t, "*", res.Header().Get("Access-Control-Allow-Origin"))
	})
}
