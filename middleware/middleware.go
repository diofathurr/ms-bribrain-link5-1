package middleware

import (
	"bytes"
	"context"
	"crypto/tls"
	"encoding/json"
	"ms-bribrain-link5/helper"
	"ms-bribrain-link5/helper/logger"
	models2 "ms-bribrain-link5/helper/models"
	"ms-bribrain-link5/models"
	"net/http"
	"net/url"
	"os"
	"strconv"

	"github.com/google/uuid"

	"github.com/labstack/echo/v4"
)

// GoMiddleware represent the data-struct for middleware
type GoMiddleware struct {
	// another stuff , may be needed by middleware
}

// CORS will handle the CORS middleware
func (m *GoMiddleware) CORS(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		c.Request().Header.Set("Access-Control-Allow-Headers", "*")
		c.Request().Header.Set("Access-Control-Allow-Origin", "*")
		c.Request().Header.Set("Access-Control-Allow-Methods", "*")

		c.Response().Header().Set("Access-Control-Allow-Origin", "*")
		c.Response().Header().Set("Access-Control-Allow-Headers", "*")
		c.Response().Header().Set("Access-Control-Allow-Methods", "*")
		return next(c)
	}
}

// LOG will handle the LOG middleware
func (m *GoMiddleware) Log(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		l := logger.L
		// l.Info("Accepted")

		requestId := uuid.New().String()
		c.Set("REQUEST-ID", requestId)
		l.Info("Accepted Request ID : " + requestId)

		next(c)

		l.Info("[" + strconv.Itoa(c.Response().Status) + "] " + "[" + c.Request().Method + "] " + c.Request().Host + c.Request().URL.String())

		l.Info("Closing Request ID : " + requestId)
		return nil
	}
}

// CORSValidationGlobalResponse will handle the CORSValidationGlobalResponse middleware
func (m *GoMiddleware) CORSValidationGlobalResponse(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		if c.Request().Method == "POST" &&
			c.Request().Header.Get(echo.HeaderContentType) == echo.MIMEApplicationJSON {
			c.Request().Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
			response := new(models2.Response)
			response.MappingResponseError(http.StatusBadRequest, "Invalid "+
				echo.HeaderContentType+" "+echo.MIMEApplicationJSON, nil, c)
			c.JSON(response.StatusCode, response)
			return nil
		}

		if c.Request().Method == "POST" &&
			c.Request().Header.Get(echo.HeaderContentType) == "application-bribrain/json" {
			c.Request().Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		}

		err := next(c)

		if err != nil {
			// allowedMethod := []string{"GET", "POST"}
			allowedMethod := []string{"POST"}
			if !helper.InArray(c.Request().Method, allowedMethod) {
				response := new(models2.Response)
				response.MappingResponseError(http.StatusMethodNotAllowed, "Invalid Method", nil, c)
				c.JSON(response.StatusCode, response)
				return err
			} else if err.Error() == "code=404, message=Not Found" {
				response := new(models2.Response)
				response.MappingResponseError(http.StatusNotFound, "Routes Not Found", nil, c)

				c.JSON(response.StatusCode, response)
				return err
			} else if err.Error() == "code=405, message=Method Not Allowed" {
				response := new(models2.Response)
				response.MappingResponseError(http.StatusMethodNotAllowed, "Method Not Allowed", nil, c)

				c.JSON(response.StatusCode, response)
				return err
			}
		}
		return nil
	}
}

// Authentication will handle the Authentication Token middleware
func (m *GoMiddleware) Authentication(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		response := new(models2.Response)
		if c.Request().Header["Authorization"] != nil {
			roles := []string{"RM"}
			token := helper.StringToStringNullable(c.Request().Header.Get("Authorization"))
			resp, err := m.AuthValidate(c.Request().Context(), token, roles)
			if err != nil {
				response.MappingResponseError(http.StatusUnauthorized, err.Error(), nil, c)
				c.JSON(response.StatusCode, response)
				return err
			}
			if resp.StatusCode != http.StatusOK {
				response.MappingResponseError(resp.StatusCode, resp.Msg, resp.Errors, c)
				// userAuditTrail := models.BribrainAuditTrail{}
				// remarkAuditTrail := helper.IntToString(response.StatusCode) + " " + models2.ErrUnAuthorize.Error()
				// userAuditTrail = userAuditTrail.MappingBribrainAuditTrail("Validate Token", remarkAuditTrail, jwtClaims.Pernr, jwtClaims.Role)
				// _, errAuditTrail := m.StoreAuditTrail(c.Request().Context(), userAuditTrail)
				// if errAuditTrail != nil {
				// 	// m.Log.Error("nasabah.delivery.http.NasabahHandler.GetListRekomendasiNonBRI.userAuditTrail: %s", err.Error())
				// 	log.Println("middleware.Authentication.userAuditTrail: %s", errAuditTrail.Error())
				// }
				c.JSON(resp.StatusCode, resp)
				return nil
			}
			convertClaims := helper.ObjectToString(resp.Data)
			var jwtClaims *models.BRIBrainClaims
			if errUnmarshal := json.Unmarshal([]byte(string(convertClaims)), &jwtClaims); errUnmarshal != nil {
				response.MappingResponseError(http.StatusUnauthorized, models2.ErrUnAuthorize.Error(), nil, c)
				c.JSON(response.StatusCode, response)

				return models2.ErrUnAuthorize
			}
			c.Set("JWT_CLAIMS", jwtClaims)
			next(c)
		} else {
			response.MappingResponseError(http.StatusUnauthorized, models2.ErrUnAuthorize.Error(), nil, c)
			c.JSON(response.StatusCode, response)

			return models2.ErrUnAuthorize
		}
		return nil
	}
}

func (m *GoMiddleware) AuthenticationUsingSecret(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		response := new(models2.Response)
		if c.Request().Header["Authorization"] != nil {
			//roles := []string{}
			token := helper.StringToStringNullable(c.Request().Header.Get("Authorization"))

			if *token != os.Getenv("SECRET_KEY") {
				response.MappingResponseError(http.StatusUnauthorized, models2.ErrUnAuthorize.Error(), nil, c)
				c.JSON(response.StatusCode, response)
				return models2.ErrUnAuthorize
			}
			//resp, err := m.AuthValidate(c.Request().Context(), token, roles)
			//if err != nil {
			//	response.MappingResponseError(http.StatusUnauthorized, err.Error(), nil)
			//	c.JSON(response.StatusCode, response)
			//	return err
			//}
			//if resp.StatusCode != http.StatusOK {
			//	response.MappingResponseError(resp.StatusCode, resp.Msg, resp.Errors)
			//	// userAuditTrail := models.BribrainAuditTrail{}
			//	// remarkAuditTrail := helper.IntToString(response.StatusCode) + " " + models2.ErrUnAuthorize.Error()
			//	// userAuditTrail = userAuditTrail.MappingBribrainAuditTrail("Validate Token", remarkAuditTrail, jwtClaims.Pernr, jwtClaims.Role)
			//	// _, errAuditTrail := m.StoreAuditTrail(c.Request().Context(), userAuditTrail)
			//	// if errAuditTrail != nil {
			//	// 	// m.Log.Error("nasabah.delivery.http.NasabahHandler.GetListRekomendasiNonBRI.userAuditTrail: %s", err.Error())
			//	// 	log.Println("middleware.Authentication.userAuditTrail: %s", errAuditTrail.Error())
			//	// }
			//	c.JSON(resp.StatusCode, resp)
			//	return nil
			//}
			//convertClaims := helper.ObjectToString(resp.Data)
			//var jwtClaims *models.BRIBrainClaims
			//if errUnmarshal := json.Unmarshal([]byte(string(convertClaims)), &jwtClaims); errUnmarshal != nil {
			//	response.MappingResponseError(http.StatusUnauthorized, models2.ErrUnAuthorize.Error(), nil)
			//	c.JSON(response.StatusCode, response)
			//
			//	return models2.ErrUnAuthorize
			//}
			//c.Set("JWT_CLAIMS", jwtClaims)
			next(c)
		} else {
			response.MappingResponseError(http.StatusUnauthorized, models2.ErrUnAuthorize.Error(), nil, c)
			c.JSON(response.StatusCode, response)

			return models2.ErrUnAuthorize
		}
		return nil
	}
}

// Authentication will handle the Authentication Token middleware
func (m *GoMiddleware) AuthValidate(ctx context.Context, token *string, roles []string) (response *models2.Response, err error) {
	l := logger.L
	validateAccess := models.ValidateAccess{
		Roles: roles,
	}
	b, err := json.Marshal(validateAccess)
	if err != nil {
		l.Error("GoMiddleware.AuthValidate: %s", err.Error())
		return nil, models2.ErrUnAuthorize
	}

	url, err := url.Parse(os.Getenv("VALIDATE_TOKEN_URL"))
	if err != nil {
		l.Error("GoMiddleware.AuthValidate: %s", err.Error())
		return nil, models2.ErrUnAuthorize
	}

	req, err := http.NewRequestWithContext(ctx, "POST", url.String(), bytes.NewReader(b))
	if err != nil {
		l.Error("GoMiddleware.AuthValidate: %s", err.Error())
		return nil, models2.ErrUnAuthorize
	}

	req.Header.Add("Content-Type", "application-bribrain/json")
	req.Header.Add("Authorization", helper.StringNullableToString(token))

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{Transport: tr}

	resp, err := client.Do(req)
	if err != nil {
		l.Error("GoMiddleware.AuthValidate: %s", err.Error())
		return nil, models2.ErrUnAuthorize
	}
	defer resp.Body.Close()

	br := &models2.Response{}
	if err := json.NewDecoder(resp.Body).Decode(br); err != nil {
		l.Error("GoMiddleware.AuthValidate: %s", err.Error())
		return nil, models2.ErrUnAuthorize
	}

	return br, nil

}

// InitMiddleware intialize the middleware
func InitMiddleware() *GoMiddleware {
	return &GoMiddleware{}
}

func (m *GoMiddleware) StoreAuditTrail(ctx context.Context, param models.BribrainAuditTrail) (response *string, err error) {
	l := logger.L

	request, err := json.Marshal(param)
	if err != nil {
		l.Error("GoMiddleware.AuthValidate: %s", err.Error())
		return nil, models2.ErrUnAuthorize
	}

	url, err := url.Parse(os.Getenv("AUDIT_TRAIL_URL"))
	if err != nil {
		l.Error("GoMiddleware.AuthValidate: %s", err.Error())
		return nil, models2.ErrUnAuthorize
	}

	req, err := http.NewRequestWithContext(ctx, "POST", url.String(), bytes.NewReader(request))
	if err != nil {
		l.Error("GoMiddleware.AuthValidate: %s", err.Error())
		return nil, models2.ErrUnAuthorize
	}

	req.Header.Add("Content-Type", "application-bribrain/json")
	// req.Header.Add("Authorization", helper.StringNullableToString(token))

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{Transport: tr}

	resp, err := client.Do(req)
	if err != nil {
		l.Error("GoMiddleware.AuthValidate: %s", err.Error())
		return nil, models2.ErrUnAuthorize
	}
	defer resp.Body.Close()

	res := helper.StringToStringNullable("OK")

	return res, nil

}
