def project = 'concrete-plasma-244309'
def appName = 'ms-bribrain-link5'
def namespace = 'bribrain'
def proxyType = 'http'
def proxyAddress = '172.18.104.20'
def proxyPort = '1707'
def token = '1269437804:AAEf6ZiHBYTY5dWaIjfhQ0sdQTalN5Esk0s'
def chatID = '-430520574'

pipeline {
  agent {
    kubernetes {
      defaultContainer 'jnlp'
      yaml '''
        apiVersion: v1
        kind: Pod
        metadata:
        labels:
          component: ci
        spec:
          tolerations:
          - key: "jenkins"
            operator: "Equal"
            value: "agent"
            effect: "NoSchedule"
          affinity:
            nodeAffinity:
              requiredDuringSchedulingIgnoredDuringExecution:
               nodeSelectorTerms:
                -  matchExpressions:
                   - key: jenkins
                     operator: In
                     values:
                     - agent
          hostAliases:
          - ip: "172.18.46.212"
            hostnames:
            - "sast.bri.co.id"
          - ip: "172.18.224.218"
            hostnames:
            - "devops-k8s-cluster-1-master-1"
          - ip: "172.18.218.209"
            hostnames:
            - "k8s-loadbalancer-01"
          - ip: "172.18.224.56"
            hostnames:
            - "k8s-load-balancer-master-01"
          - ip: "172.18.219.115"
            hostnames:
            - "k8s-loadbalancer-01-sb"
          serviceAccountName: cd-jenkins
          containers:
          - name: gcloud
            image: google/cloud-sdk:263.0.0-alpine
            imagePullPolicy: IfNotPresent
            command:
            - cat
            tty: true
          - name: helm
            image: alpine/helm:2.14.0
            imagePullPolicy: IfNotPresent
            command:
            - cat
            tty: true
          - name: jnlp
            image: mfahry/bri-jnlp-slave:1.7
            imagePullPolicy: IfNotPresent
        '''
    }
  }

  stages {
    stage('Quality Node') {
      when {
        branch 'develop'
      }
      environment {
        scannerHome = tool 'sonarscanner'
      }
      steps {
        script {
          try {
            withSonarQubeEnv('sonarqube') {
              sh "${scannerHome}/bin/sonar-scanner -X"
            }
          }
        catch (err) {
            echo 'Application Scanned'
        }
        }
      }
    }

    stage('build image') {
      environment {
        IMAGE_REPO = "gcr.io/${project}/${appName}"
        IMAGE_TAG = "${env.GIT_COMMIT.substring(0, 7)}"
      }
      // when {
      //   branch 'develop'
      // }
      steps {
        container('gcloud') {
          // sh "PYTHONUNBUFFERED=1 gcloud builds submit -t ${IMAGE_REPO}:${IMAGE_TAG} ."
          withCredentials([file(credentialsId: 'k8s-builder-prod', variable: 'JSONKEY')]) {
            sh "gcloud config set proxy/type ${proxyType}"
            sh "gcloud config set proxy/address ${proxyAddress}"
            sh "gcloud config set proxy/port ${proxyPort}"

            sh "cat ${JSONKEY} >> key.json"
            sh 'gcloud auth activate-service-account --key-file=key.json'
            sh "gcloud builds submit --timeout=1h --project ${project} --config=gcloud/cloud-build-${env.GIT_BRANCH}.yaml --substitutions=_IMAGE_REPO=${IMAGE_REPO},_IMAGE_TAG=${IMAGE_TAG} ."
          }
        }
      }
    }
    stage ('Deployment') {
      parallel {
        stage('Deploy to development') {
          when {
            branch 'develop'
          }
          environment {
            IMAGE_REPO = "gcr.io/${project}/${appName}"
            IMAGE_TAG = "${env.GIT_COMMIT.substring(0, 7)}"
          }
          steps {
            script {
              container('helm') {
                // sh "helm upgrade --debug --install -f helm/values.prd.yml --set-string image.repository=${IMAGE_REPO},image.tag=${IMAGE_TAG} ms-dgb-dashboard ./helm/ms-dgb-dashboard"
                withCredentials([file(credentialsId: 'kubeconfig', variable: 'KUBECONFIG')]) {
                  // setup kube config
                  sh 'mkdir -p ~/.kube/'
                  sh "cat ${KUBECONFIG} >> ~/.kube/config"

                  sh """
              helm upgrade --force ${appName} ./helm/${appName} \
                --set-string image.repository=${IMAGE_REPO},image.tag=${IMAGE_TAG} \
                -f ./helm/values.dev.yml --debug --install --namespace ${namespace}
            """
                }
              }
            }
          }
        }
        stage('Deploy to uat') {
          when {
            branch 'uat'
          }
          environment {
            IMAGE_REPO = "gcr.io/${project}/${appName}"
            IMAGE_TAG = "${env.GIT_COMMIT.substring(0, 7)}"
          }
          steps {
            container('helm') {
              // sh "helm upgrade --debug --install -f helm/values.prd.yml --set-string image.repository=${IMAGE_REPO},image.tag=${IMAGE_TAG} ms-dgb-dashboard ./helm/ms-dgb-dashboard"
              withCredentials([file(credentialsId: 'kubeconfig-gti-uat', variable: 'KUBECONFIG')]) {
                // setup kube config
                sh 'mkdir -p ~/.kube/'
                sh "cat ${KUBECONFIG} >> ~/.kube/config"

                sh """
              helm upgrade --force ${appName} ./helm/${appName} \
                --set-string image.repository=${IMAGE_REPO},image.tag=${IMAGE_TAG} \
                -f ./helm/values.uat.yml --debug --install --namespace ${namespace}
            """
              }
            }
          }
        }
        stage('Deploy to staging') {
          when {
            branch 'staging'
          }
          environment {
            IMAGE_REPO = "gcr.io/${project}/${appName}"
            IMAGE_TAG = "${env.GIT_COMMIT.substring(0, 7)}"
          }
          steps {
            container('helm') {
              // sh "helm upgrade --debug --install -f helm/values.prd.yml --set-string image.repository=${IMAGE_REPO},image.tag=${IMAGE_TAG} ms-dgb-dashboard ./helm/ms-dgb-dashboard"
              withCredentials([file(credentialsId: 'kubeconfig-staging', variable: 'KUBECONFIG')]) {
                // setup kube config
                sh 'mkdir -p ~/.kube/'
                sh "cat ${KUBECONFIG} >> ~/.kube/config"

                sh """
              helm upgrade --force ${appName} ./helm/${appName} \
                --set-string image.repository=${IMAGE_REPO},image.tag=${IMAGE_TAG} \
                -f ./helm/values.stg.yml --debug --install --namespace ${namespace}
            """
              }
            }
          }
        }
        stage('Deploy to production') {
          when {
            branch 'master'
          }
          environment {
            IMAGE_REPO = "gcr.io/${project}/${appName}"
            IMAGE_TAG = "${env.GIT_COMMIT.substring(0, 7)}"
          }
          steps {
            container('helm') {
              // sh "helm upgrade --debug --install -f helm/values.prd.yml --set-string image.repository=${IMAGE_REPO},image.tag=${IMAGE_TAG} ms-dgb-dashboard ./helm/ms-dgb-dashboard"
              withCredentials([file(credentialsId: 'kubeconfig', variable: 'KUBECONFIG')]) {
                // setup kube config
                sh 'mkdir -p ~/.kube/'
                sh "cat ${KUBECONFIG} >> ~/.kube/config"

                sh """
              helm upgrade --force ${appName} ./helm/${appName} \
                --set-string image.repository=${IMAGE_REPO},image.tag=${IMAGE_TAG} \
                -f ./helm/${appName}/values.yaml --debug --install --namespace ${namespace}
            """
              }
            }
          }
        }
      }
    }
    stage('Notif Telegram') {
      steps {
        script {
          sh """
                    https_proxy=http://${proxyAddress}:${proxyPort} curl -s -X POST https://api.telegram.org/bot${token}/sendMessage -d chat_id=${chatID} -d parse_mode="HTML" -d text="<b>Project</b> : "${appName}"  \
                    <b>Branch </b> : ${env.GIT_BRANCH} \
                    <b>Status</b> = SUCCESS"
                  """
        }
      }
    }
    stage('SAST') {
      when {
         branch 'uat'
       }
      steps {
        script {
          step([$class: 'CxScanBuilder', comment: '', credentialsId: '', excludeFolders: 'helm, vendor', exclusionsSetting: 'job', failBuildOnNewResults: false, failBuildOnNewSeverity: 'HIGH', filterPattern: '!.* ', fullScanCycle: 10, generatePdfReport: true, groupId: '2d95991a-f4d4-43f6-8cb8-4029b9ab4410', incremental: true, password: '{AQAAABAAAAAQ6aiwQviXnYu3oM9JZHpd8ZG+N1gQtlTUD76ogPfFv00=}', preset: '3', projectName: "$appName", sastEnabled: true, serverUrl: 'https://sast.bri.co.id', sourceEncoding: '1', username: '', vulnerabilityThresholdResult: 'FAILURE', waitForResultsEnabled: false])
        }
      }
    }
  }
}
